// Load the FaceBook and VK SDK Asynchronously
$(document).ready(function () {
    var d = document;
    var js, id = 'facebook-jssdk';
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    d.getElementsByTagName('head')[0].appendChild(js);

    setTimeout(function () {
        var el = document.createElement("script");
        el.type = "text/javascript";
        el.src = "http://vkontakte.ru/js/api/openapi.js";
        el.async = true;
        document.getElementById("vk_api_transport").appendChild(el);
    }, 0);

    //odnoklassniki.odn_loadNewEvents();
});

window.fbAsyncInit = function () {
    FB.init({
        appId:'450611818333840', // App ID
        status:true, // check login status
        cookie:true, // enable cookies to allow the server to access the session
        xfbml:true, // parse XFBML
        channelUrl:'http://valitovsimplefit.lan/channel.html' // Channel File
    });
};

var facebook = {
    posted : false,

    login:function () {
        FB.login(function (response) {
            if (response.authResponse) {
                facebook.connected = true;
                var userId = response.authResponse.userID;
                var signedRequest = response.authResponse.signedRequest;

                $.post('/ws/signin/fb/', {'signed_request':signedRequest}, function (response) {
                    if (window.location.hash.indexOf('blogs') > -1) {
                        response.redirect = 'http://blogs.simplefit.ru';
                    }

                    switch (response.status) {
                        case 'ok':
                            facebook.posted = response.fb_posted;

                            if (facebook.posted == '1') { // == '1' is needed
                                window.location.assign(response.redirect);
                            }
                            else {
                                facebook.wallPost(response.redirect);
                            }
                            break;

                        case 'no_user':
                            FB.api('/me', function(r) {
                                signup($.extend(r, {'sig':signedRequest}), 'fb');
                                hideLayer();
                            });
                            break;
                    }
                }, 'json');
            }
        }, {scope:'publish_stream, email'});
    },

    wallPost:function (redirect) {
        var data = {
            method:'feed',
            link:'http://simplefit.ru',
            picture: 'http://images.simplefit.ru/images/logo.png',
            description: 'Начал использовать SimpleFit - сервис для ведения дневников питания и контроля за диетами.'
        };

        FB.ui(data, function(response) {
            if (response && response.post_id) {
                $.post('/ws/social/fb/', {post_id : response.post_id}, function(res) {
                    window.location.assign(redirect);
                });
            }
            else {
                window.location.assign(redirect);
            }
        });
    },

    postResults:function(results) {
        var data = {
            method:'feed',
            link:'http://simplefit.ru',
            picture:results.image,
            caption:results.title,
            description:results.text
        };

        FB.ui(data, function (response) {
            if (response && response.post_id) {
                window.location.reload(true);
            }
        });
    }
};

window.vkAsyncInit = function () {
    VK.init({
        apiId:3292241
    });

    var data = {
        type:"mini",
        height:20,
        pageTitle:"Simplefit.ru - дневник питания",
        pageDescription:"Симплфит.ру - это удобный сервис для ведения дневников питания и контроля за диетами, калькулятор калорий и идеального веса.",
        pageUrl:"http://simplefit.ru",
        pageImage:"http://images.simplefit.ru/images/logo.png"
    };

    VK.Widgets.Like('vk_like', data, 1);
};

var vkontakte = {
    user : {},
    posted: false,

    login: function () {
        VK.Auth.login(function (response) {
            if (response.session) {
                vkontakte.session = response.session;
                vkontakte.user = response.session.user;
                //vkontakte.notifications();

                hideLayer();

                $.post('/ws/signin/vk/', function (response) {
                    if (window.location.hash.indexOf('blogs') > -1) {
                        response.redirect = 'http://blogs.simplefit.ru';
                    }

                    switch (response.status) {
                        case 'ok':
                            window.location.assign(response.redirect);
//                            if (vkontakte.posted) {
//                                window.location.assign(response.redirect);
//                            }
//                            else {
//                                vkontakte.wallPost(response.redirect);
//                            }
                            break;

                        case 'no_user':
                            var signupForm = $('#signup-form');

                            VK.Api.call('users.get', {'uids':vkontakte.user.id, 'fields' : 'sex'}, function (r) {
                                if (r.response) {
                                    var data = r.response[0];
                                    signupForm.find('input[name=first_name]').val(vkontakte.user.first_name).parent().hide();
                                    signupForm.find('input[name=last_name]').val(vkontakte.user.last_name);
                                    signupForm.find('input[name=gender]').val(data.sex);

                                    signupForm.find('input[name=password]').parent().hide();
                                    signupForm.find('input[name=password2]').parent().hide();

                                    $('#signup-type').val('vk');
                                    showLayer('signup');
                                }
                            });
                            break;
                    }
                }, 'json');
            }
        }, 532480);
    },

    wallPost: function(redirect) {
        VK.api('wall.post', {message:'Начал использовать SimpleFit - сервис для ведения дневников питания и контроля за диетами.', attachments:'http://simplefit.ru'}, function (data) {
            if (data.response && data.response.post_id) {
                $.post('/ws/social/vk/', {posted_id:data.response.post_id}, function (res) {
                    window.location.assign(redirect);
                });
            }
            else {
                window.location.assign(redirect);
            }
        });
    },

    postResults1: function(data) {
        //<type><owner_id>_<media_id>,
        var message = data.header + '\n' + data.message;
        VK.api('wall.post', {message:message, attachments:'http://simplefit.ru'}, function (data) {
            if (data && data.post_id) {
                window.location.reload(true);
            }
        });
    },

    postResults: function(data) {
        var popupName = '_blank';
        var width = 554;
        var height = 349;
        var w = $(window);
        var left = (w.width() - width) / 2;
        var top = (w.height() - height) / 2;
        var url = 'http://vk.com/share.php?';

        var params = {
            'url': 'http://simplefit.ru',
            'title': data.title,
            'description': data.text,
            'image': data.image
        };

        for (x in params) {
            url += x + '=' + encodeURIComponent(params[x]) + '&';
        }
        url += 'noparse=1';

        var popupParams = 'scrollbars=0, resizable=1, menubar=0, left=' + left + ', top=' + top + ', width=' + width + ', height=' + height + ', toolbar=0, status=0';
        var popup = window.open(url, popupName, popupParams);
    }
};

var twitter = {
    login:function () {
        $.get('https://api.twitter.com/oauth/request_token',
            function (e) {
                console.log(e)
            }
        );
    },
    tw_getmentions:function (T) {
        if (this.connected == true) {
            var counter = 0;
            var mentions = T.currentUser.mentions();
            var t = mentions.first(10);
            t.each(function (status, e) {
                var dt = Date.parse(status.createdAt);
                var current_date = Math.round(new Date().getTime());
                if ((current_date - dt) < 43200000) {
                    counter = counter + 1;
                    $('#tw-count').empty().html(counter);
                    $('.tw .soc-icons-ballun').show();
                }
                else {

                }
            });
        }
        else {
            //console.log('twitter_check_false');
        }
    }
};



var mirmailru = {
    connected: false,
    session: {},
    posted: false,
    postType: 'login',

    init: function() {
        mailru.connect.init(696481, 'cd4d7539f4c00c382a3d7e56c773064c');
        mailru.events.listen(mailru.connect.events.login, mirmailru.signin);
        mailru.events.listen(mailru.common.events.streamPublish, mirmailru.published);

        mailru.connect.getLoginStatus(function (result) {
            mirmailru.session = result;
            if (result.is_app_user != 1) {
                mirmailru.connected = false;
            } else {
                mirmailru.connected = true;
            }
        });
    },

    login: function () {
        if (mirmailru.connected) {
            mirmailru.signin(mirmailru.session);
        }
        else {
            mailru.connect.login(['widget', 'stream']);
        }
    },

    signin:function (session) {
        hideLayer();

        $.post('/ws/signin/mm/', function (response) {
            if (window.location.hash.indexOf('blogs') > -1) {
                response.redirect = 'http://blogs.simplefit.ru';
            }

            switch (response.status) {
                case 'ok':
                    mirmailru.posted = response.mm_posted;

                    if (mirmailru.posted == '1') { // == '1' is needed.
                        window.location.assign(response.redirect);
                    }
                    else {
                        mirmailru.wallPost(response.redirect);
                    }
                    break;

                case 'no_user':
                    signup({}, 'mm');
                    hideLayer();
                    break;
            }
        }, 'json');
    },

    wallPost:function (redirect) {
        var data = {
            'title':'Симплфит - Дневник питания',
            'text':'Начал использовать SimpleFit - сервис для ведения дневников питания и контроля за диетами.',
            'img_url':'http://images.simplefit.ru/images/logo.png',
            'action_links':[
                {'text':'simplefit.ru', 'href':'http://simplefit.ru'}
            ]
        };
        mirmailru.redirect = redirect;

        mailru.common.stream.post(data);
    },

    postResults:function(results) {
        var data = {
            'title':results.title,
            'text':results.text,
            'img_url':results.image,
            'action_links':[
                {'text':'simplefit.ru', 'href':'http://simplefit.ru'}
            ]
        };

        mailru.common.stream.post(data);
        mirmailru.postType = 'results';
    },

    published:function(event) {
        if (mirmailru.postType == 'login') {
            if (event && event.status == 'publishSuccess') {
                $.post('/ws/social/mm/', {posted:1}, function (res) {
                    window.location.assign(mirmailru.redirect);
                });
            }
            else {
                window.location.assign(mirmailru.redirect);
            }
        }
        else {
            if (event && event.status == 'publishSuccess') {
                window.location.reload(true);
            }
        }
    }
};


mailru.loader.require('api', mirmailru.init);

var odnoklassniki = {
    login: function (target) {
        ODKL.Oauth2(target, 127573248, 'VALUABLE ACCESS; SET STATUS', 'http://' + document.location.host + '/ws/signin/od/');
    }
};