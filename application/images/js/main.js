$(document).ready(function () {

    $(".select").select2();

    $('#signin-form a.signin').click(function (e) {
        e.preventDefault();
        var data = $('#signin-form').serializeObject();

        if (!checkEmail(data['email'])) {
            alert('Неверный email или пароль');
            return false;
        }

        $.post('/ws/signin/', data, function (response) {
            if (window.location.hash.indexOf('blogs') > -1) {
                response.redirect = 'http://blogs.simplefit.ru';
            }

            if (response.status == 'ok') {
                window.location.assign(response.redirect);
            }
            else {
                alert('Неверный email или пароль');
            }
        }, 'json');
    });

    $('#signin-form').keydown(function(e) {
        if (e.keyCode == 13) {
            $('#signin-form a.signin').click();
        }
    });

    $('#signup-form button.signup').click(function (e) {
        e.preventDefault();
        var data = $('#signup-form').serializeObject();

        if (!data['first_name']) {
            alert('Неверное значение поля "Имя пользователя"');
            return;
        }

        if (!checkEmail(data['email'])) {
            alert('Неверный email');
            return false;
        }

        var loginType = $('#signup-type').val();

        if (!loginType && (!data['password'] || data['password'] != data['password2'])) {
            alert('Пароли не равны');
            return false;
        }

        signup(data, loginType);
    });

    $('#password-remind-link').click(function (e) {
        e.preventDefault();
        showLayer('password-remind');
    });

    $('#password-remind-layer button').click(function (e) {
        e.preventDefault();
        var email = $('#password-remind-email').val();
        $('#password-remind-layer div.error').hide();

        if (!checkEmail(email)) {
            $('#password-remind-layer div.error-email').show();
            return false;
        }
        $('#password-remind-sent-layer .sent-email').html(email);

        $.post('/ws/password/remind/', {'email':email}, function(result) {
            if (result.status == 'ok') {
                showLayer('password-remind-sent');
            }
            else {
                $('#password-remind-layer div.no-user').show();
            }
        }, 'json');
    });

    $('#password-remind-sent-layer button').click(function (e) {
        e.preventDefault();
        hideLayer();
        $('#password-remind-layer input').val('');
    });

    $('.social-icons li.social-ico a').click(function (e) {
        e.preventDefault();
        var icon = $(this).attr('class').split('-').shift();

        switch (icon) {
            case 'vk':
                vkontakte.login();
                break;

            case 'fb':
                facebook.login();
                break;

            case 'mm':
                mirmailru.login();
                break;

            case 'od':
                odnoklassniki.login(this);
                break;
        }
    });

    $('#feedback-form button').click(function(e) {
        e.preventDefault();
        $('#feedback-form .error').hide();

        var data = $('#feedback-form').serializeObject();

        if (!checkData(data)) {
            return false;
        }

        $.post('/ws/feedback/', data, function() {
            showLayer('feedback-sent');
        });
    });

    $('#feedback-sent-layer span.button').click(function(e) {
        e.preventDefault();
        hideLayer();
    });
});

function checkEmail(email) {
    var re_email = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    return re_email.test(email);
}

function checkData(data, exclude) {
    var result = true;
    if (!exclude) {
        exclude = [];
    }

    for (x in data) {
        if (exclude.indexOf(x) != -1) {
            continue;
        }

        if (!data[x] || data[x] == 0) {
            result = false;
            $('input[name=' + x + '],select[name=' + x + ']').parents('dd').find('div.error').show();
            continue;
        }

        if (x == 'email' && !checkEmail(data[x])) {
            result = false;
            $('input[name=email]').siblings('div.error').show();
        }

        var re = /^\d+((\.|,)\d+)?$/;

        if ((x == 'weight' || x == 'weight_goal') && !re.test(data[x])) {
            result = false;
            $('input[name=' + x + ']').parents('dd').find('div.error').show();
        }
    }

    return result;
}

function signup(data, type) {
    var url = '/ws/signup/';

    if (type) {
        url += type + '/';
    }

    $.post(url, data, function (response) {
        if (response.status == 'ok') {
            if (type == 'fb' && response.fb_posted != '1') {
                facebook.wallPost(response.redirect);
            }
            else if (type == 'mm' && response.mm_posted != '1') {
                mirmailru.wallPost(response.redirect);
            }
            else {
                window.location.assign(response.redirect);
            }
        }
        else {
            //alert('user exists');
            $('#confirm-email-layer .confirm-user-email').attr('href', 'mailto:' + data['email']).html(data.email);
            showLayer('confirm-email');
        }
    }, 'json');
}

function sendConfirmEmail(email) {
    $.post('/ws/confirm/vk/', {'email':email}, function () {
        hideLayer();
    });
}

function resend(){
    $.post('/ws/signup/resend', {'email':'email'}, function () {
        hideLayer();
    });

}