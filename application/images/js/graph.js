var $pointers_container;
var points_data = {};
var canvas = null;
var lines = [];

$(document).ready(function () {

    //canvas = new fabric.Canvas('graph', { selection:false });
    $pointers_container = $(".pointers-container");

    $.getJSON('/ws/graph/weight/', function (data) {
        points_data = data;
        drawGraph(data.pointers.length - 12);
    });

    var $add_data_action = $(".add_data"),
        $norma_height = $(".diagram").data("norm")/2 - 19.5;
    $add_data_action.css("bottom", $norma_height)

});

function makeLine(coords)
{
    var line = new fabric.Line(coords, {
        fill:'#ff7600',
        strokeWidth:1,
        selectable:false
    });
    lines.push(line);
    return line;
}

function drawGraph(offset)
{
    if (offset < 0) {
        offset = 0;
    }

    if (offset > points_data.pointers.length - 12) {
        offset = points_data.pointers.length - 12;
    }

    var canvasContainer = $('div.canvas-container');
    var weightRange = $('#weight-range').empty();

    data = points_data;
    var misc = data.misc[0],
        lines = data.lines,
        pointers = data.pointers,
        range_max = misc.range_max,
        range_min = misc.range_min,
        range = (range_max - range_min) * 20,
        arr_line_length = lines.length + 1,
//        line_width = 950 / arr_line_length;
        line_width = 79.1666;

    if (canvas) {
        canvas.clear();
        $pointers_container.empty();
    }
    else {
        $('#graph').attr('height', range);
        canvas = new fabric.Canvas('graph', { selection:false });
    }

    for (var x = range_min; x <= range_max; x++) {
        weightRange.prepend('<li>' + x + '</li>');
    }

    $.each(lines.slice(offset, offset + 12), function (i, val) {
        if (val.x2 && val.y2) {
            var point_x1 = (val.x1 - offset) * line_width - line_width / 2,
                point_y1 = range - (val.y1 - range_min) * 20 ,
                point_x2 = (val.x2 - offset) * line_width - line_width / 2,
                point_y2 = range - (val.y2 - range_min) * 20;
            canvas.add(makeLine([point_x1, point_y1, point_x2, point_y2]))
        }
    });

    $.each(pointers.slice(offset, offset + 12), function (i, val) {
        var weight = val.y,
            point_data = val.data,
            x = (val.x - offset) * line_width - line_width / 2 + "px",
            y = range - (weight - range_min) * 20 + "px";
        if (point_data == true) {
            $("<div class='point' style='top:" + y + ";left:" + x + "'>" +
                "<span class='point_ins'></span>" +
                "<span class='point_ins_hover'>" + weight + "</span>" +
                "</div>").appendTo($pointers_container);
        } else {
            $('<div date="' + val.date + '" index="'+(i+offset)+'" class="point" style="top:' + y + ';left:' + x + '">' +
                "<span class='point_add_data'>+</span>" +
                "<div class='tooltip bc'>Добавить запись</div>" +
                "</div>").appendTo($pointers_container).click(function () {
                    $('#graph-add-weight-form input[name=date]').val($(this).attr('date'));
                    $('#graph-add-weight-form').attr('data', $(this).attr('index'));
                    showLayer('add-weight');
                });
        }
    });
}
