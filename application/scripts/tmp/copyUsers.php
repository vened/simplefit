<?php
/**
 * @author: vadim
 * @date: 30.05.13
 */

$_SERVER['SERVER_ROLE'] = 'test';
include __DIR__ . "/../../boot.php";

$userMapper = new UserMapper();
$users = $userMapper->query('SELECT * FROM Simplefit.User');
$blogsUserMapper = new \Blogs\UserMapper();
$blogsSessionMapper = new \Blogs\SessionMapper();
$blogsBlogMapper = new \Blogs\BlogMapper();

foreach ($users as $user) {
    $blogsUser = $blogsUserMapper->findByEmail($user->getEmail());
    if (!$blogsUser->getId()) {
        $blogsUser->setLogin($user->getId());
        $blogsUser->setPassword($user->getPassword());
        $blogsUser->setEmail($user->getEmail());
        $blogsUser->setDateRegister();
        $blogsUser->setUserIpRegister('127.0.0.1');
        $userName = $user->getFirstName();
        if ($user->getLastName()) {
            $userName .= ' ' . $user->getLastName();
        }
        $blogsUser->setUserName($userName);
    }

    if ($sex = $user->getGender()) {
        $blogsUser->setUserSex($sex);
    }
    $blogsUser->setUserProfileDate();
    $blogsUser->setUserActivate();
    //$blogsUserMapper->save($blogsUser);

    $blogsSession = $blogsSessionMapper->findByUserId($user->getId());
    $blogsSession->setSessionKey();
    $blogsSession->setUserId($blogsUser->getId());
    $blogsSession->setIpCreate('127.0.0.1');
    $blogsSession->setIpLast('127.0.0.1');
    $blogsSession->setDateCreate();
    $blogsSession->setDateLast();
    //$blogsSessionMapper->save($blogsSession);

    $blog = $blogsBlogMapper->findByUserId($blogsUser->getId());

    if ($blog->isEmpty()) {
        $blog->setUserId($blogsUser->getId());
        $blog->setUserName($blogsUser->getUserName());
        $blogsBlogMapper->save($blog);
    }
    break;
}

echo 'Done' . PHP_EOL;
