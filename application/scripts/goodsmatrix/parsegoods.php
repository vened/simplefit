<?php
/**
 * @author: mix
 * @date: 03.02.13
 */

$dsn = "mysql:host=localhost;dbname=test;charset=utf8";

$h = new PDO($dsn, "monamour2", "monamourchik");
q("set names utf8");

$sql = "select * from goods where parsed='0' ";

//$link = "http://www.goodsmatrix.ru/goods/h/4601021045542.html";
//$sql = "select * from goods where link='$link'";

do{
    $rows = s($sql);
    foreach ($rows as $row){
        parse_content($row);
    }

}while(count($rows == 5000));

function parse_content($row){
    $link = $row["link"];


    $text = load($link);


    $dom = new DOMDocument();

    @$dom->loadHTML($text);

    $e = $dom->getElementById("_ctl0_ContentPH_ESL");
    /**
     * @var DomElement $e
     */

    $params = array();
    if($e){
        for($i=0;$i < $e->childNodes->length;$i++){
            $child = $e->childNodes->item($i);
            if($child instanceof DOMText){
                $text = decode(trim($child->nodeValue));
                if($text){
                    list($name,$value) = explode(":", $text);
                    $value = str_replace(",",".",$value);
                    $value = (float)$value;
                    switch($name){
                        case "Белки" : $params["protein"] = $value; break;
                        case "Жиры" : $params["fat"] = $value; break;
                        case "Углеводы" : $params["carbohydrate"] = $value; break;
                        case "Энергетическая ценность" : $params["energy"] = $value; break;
                    }
                }
            }
        }


    }


    if(!$row["name"])
    {
        $name = decode($dom->getElementById("_ctl0_ContentPH_GoodsName")->nodeValue);
        $params["name"] = str_replace("\n"," ", $name);
    }
    $pp = array();
    foreach($params as $name => $value){
        $pp[] = "$name='$value'";
    }
    $pp[] = "parsed=1";
    if($pp){
        $sql = "update test.goods set ".implode(",",$pp)." where id='$row[id]'";
        q($sql);
    }
    //

}




function decode($in){

    $ch = curl_init("http://www.artlebedev.ru/tools/decoder/advanced/?random=0.9948204380925745");
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'text=' . urlencode($in) . "&Decode=go&csin=22&csout=11");
    ob_start();
    curl_exec($ch);
    $data = ob_get_contents();
    ob_end_clean();
    curl_close($ch);

    preg_match('/CDATA\[(\".*)\]\]/',$data,$matches);

    list($a,$data) = explode("<![CDATA[",$data);
    list($data,$b) = explode("]]>",$data);
    $data = str_replace("\n", " ", $data);
    return $data;


}

function q($sql){
    global $h;
    $r =    $h->query($sql) or die($sql . "\n". print_r($h->errorInfo(),1));
    return  $r;
}

function s($sql){
    /**
     * @var PDOStatement
     */
    $r = q($sql);

    //var_dump($r);
    return $r->fetchAll();
}


function load($link){

    static $current = "direct";
    static $proxyId = 0;

    $proxyList = array(
        '119.134.251.187:1098',
        '203.76.140.11:80',
        '218.108.85.59:82',
        '119.187.148.34:8000',
        '184.95.43.12:3128',
        '94.180.118.34:8080',
        '62.77.130.35:3128 ',
        '62.201.207.14:8080',
    );

    do{

        switch($current){
            case  "direct" :
                echo "fetch direct $link\n";
                if($text = file_get_contents($link))return $text;
            case "proxy" :
                $current = "proxy";
                for($proxyId;$proxyId < count($proxyList); $proxyId++){
                    $proxy = $proxyList[$proxyId];
                    $aContext = array(
                        'http' => array(
                            'proxy' => "tcp://$proxy",
                            'request_fulluri' => true,
                            'timeout' => 3,
                        ),
                    );
                    $cxContext = stream_context_create($aContext);
                    echo "fetch via $proxy $link\n";
                    $start = microtime(1);
                    for($i=0;$i<3;$i++){
                        if($text = file_get_contents($link,false,$cxContext)){
                            echo "done in ". round(microtime(1) - $start,2) . "s\n";
                            return $text;
                        }
                        echo "fail in ". round(microtime(1) - $start,2) . "s\n";
                    }
                }
                $proxyId = 0;
                $current = "direct";
        }
        sleep(90);

    }while(!$text);
    return $text;
}

