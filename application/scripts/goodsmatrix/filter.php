<?php

$dsn = "mysql:host=localhost;dbname=test;charset=utf8";

$h = new PDO($dsn, "monamour2", "monamourchik");
q("set names utf8");


/**
 * step 1: фильтр по скобкам, кавычкам
 *

$filter = $filtero = array(
    "(", ")", "]", ";", "...", '"',"»","«", '"'
);


$ff = array();

foreach($filter  as &$f){
    $f = "%$f%";
    $ff[] = "name like ?";
}

$sql = "select * from Simplefit.Products where ". implode(" or ",$ff) . " ";

foreach(s($sql, $filter) as $row){
    //echo $row["name"] . "\n";
    $name =  str_replace($filtero, " ", $row["name"]) . "\n";
    q("update Simplefit.Products set `name`=? where id=?", array($name, $row["id"]));
}

//*/

/**
 * step2: фильтр пробелов
 *

$sql = "select * from Simplefit.Products where name like '%  %'";
foreach(s($sql)  as $row){
    $words = array();
    foreach(explode(" ", trim($row["name"])) as $word){
        trim($word);
        if($word){
            $words[] = $word;
        }
    }
    $name = implode(" ", $words);
    echo $name . "\n";
    q("update Simplefit.Products set `name`=? where id=?", array($name, $row["id"]));
}
//*/

/**
 * step 3. хитрые фильтры
 */


/**
 * 3.1 заменяем штуки, граммы
 */

foreach(s("select * from Simplefit.Products where name like '%штуки%'") as $row){
    $out = preg_replace("/штуки\.*/ui", "штук",$row["name"]);
    echo "$row[name]\n$out\n";
    updateName($out, $row["id"]);
}

foreach(s("select * from Simplefit.Products where name like '%грамм%'") as $row){
    $out = preg_replace("/грамм(ов)*\.*/ui", "гр",$row["name"]);
    echo "$row[name]\n$out\n";
    updateName($out, $row["id"]);
}

foreach(s("select * from Simplefit.Products where name like '% гров%'") as $row){
    $out = preg_replace("/гров*\.*/ui", "гр",$row["name"]);
    echo "$row[name]\n$out\n";
    updateName($out, $row["id"]);
}



/**
 * 3.2 размерность
 */

//replace('/(\.|,|;)*\s+[\d,\.]+\s*(гр|кг|мл|штук)+(\.|,|;)*/iu');
//replace('/(\.|,|;)*\s+[\d,\.]+\s*(г|к|л|шт)+(\.|,|;)*/iu');


/**
 * 3.3 пакетики
 */

//replace('/(\.|,|;)*\s+\d+\s+пакетиков по/iu');


/**
 * 3.4 в/у, ж/б
 */

//replace('/(в )*в\/у/iu');

//replace('/(в )*ж\/б/iu');


/**
 * 4. дубли
 */


foreach(s("select count(*), name from Simplefit.Products group by name having count(*) >1 order by count(*) desc") as $row){

    //var_dump($row);

    $stack = s("select * from Simplefit.Products where name = ? order by calories desc", array($row["name"]));
    array_shift($stack);

    foreach($stack as $row2){
        q("update Simplefit.Products set moderated=1, is_available=0 where id=$row2[id]");
    }

}


function replace($p){
    $last_id=0;
    do{
        $res = s("select * from Simplefit.Products where id > $last_id limit 1000 ");
        foreach($res as $r){
            preg_match_all($p,$r["name"], $out);
            if($out[0]){
                $name = trim(str_replace($out[0], "", $r["name"]));
                echo "$r[name]\n$name\n";
                updateName($name, $r["id"]);
            }
            $last_id = $r["id"];
        }
    }while(count($res) == 1000);
}

function updateName($name,$id){
    q("update Simplefit.Products set `name`=? where id=?", array($name, $id));
}


function q($sql,$params=array()){
    global $h;

    $s = $h->prepare($sql) or die($sql . "\n" . var_export($h->errorInfo(),1));
    $s->execute($params) or die($sql . "\n". var_export($h->errorInfo(),1));
    return  $s;
}

function s($sql, $params = array()){
    /**
     * @var PDOStatement
     */
    $r = q($sql,$params);

    //var_dump($r);
    return $r->fetchAll(PDO::FETCH_ASSOC);
}