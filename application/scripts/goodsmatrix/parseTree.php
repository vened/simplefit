<?php
/**
 * @author: mix
 * @date: 02.02.13
 */

$dsn = "mysql:host=localhost;dbname=test;charset=utf8";

$h = new PDO($dsn, "monamour2", "monamourchik");
$h->query("set names utf8");


//$link = "http://www.goodsmatrix.ru/goods-catalogue/Spirits/Whisky.html";

foreach(s("select t1.id, count(t2.id) as cnt, t1.link from test.themes as t1 left join test.themes as t2 on t1.id=t2.parent_id where t1.parsed=0 group by t1.id having cnt=0") as $row){
    echo "fetching $row[link]\n";
    fetchUrl($row["link"]);
    q("update test.themes set parsed=1 where id=$row[id]");
}




function fetchUrl($link){



    do{

        $text = file_get_contents($link);
        if(!$text){
            echo "sleep on $link\n";
            sleep(600);
        }
    }while(!$text);


    $dom = new DOMDocument();
    @$dom->loadHTML($text);

    $e = $dom->getElementById("aspnetForm");
    $table = $dom->getElementById("_ctl0_ContentPH_GoodsDG");
    if($table){
    parsegoods($table);
    }

    $params = findParams($e);



    $params["__LASTFOCUS"] = "";
    $params["__EVENTARGUMENT"] = "";
    $params["_ctl0:MainHeader:BarCode:BarCodeNum"] = "";
    $params["_ctl0:MainHeader:BarCode:SearchLB:"] = "1";
    $params["_ctl0:GSearch:SVal"] = "";


    foreach(getPages($e) as $page){

        $params["__EVENTTARGET"] = ($page);

        $post = array();
        foreach($params as $name => $value){
            $post[] = urlencode($name) ."=".urlencode($value);
        }

        do{
            $ch = curl_init($link);
            curl_setopt($ch, CURLOPT_POSTFIELDS, implode("&", $post));
            ob_start();
            curl_exec($ch);
            $data = ob_get_contents();
            ob_end_clean();
            curl_close($ch);
            if(!$data){
                echo "sleep on $link\n";
                sleep(600);
            }
        }while(!$data);

        $dom = new DOMDocument();
        @$dom->loadHTML($data);
        $table = $dom->getElementById("_ctl0_ContentPH_GoodsDG");
        if($table){
            parsegoods($table);
        }
    }

}


function getPages($e){
    $out = array();
    if($e instanceof DOMElement){
        if($e->nodeName == "a"){
            $href = $e->getAttribute("href");
            if(strpos($href,'javascript:__doPostBack(\'_ctl0$ContentPH$GoodsDG$_ctl14$_ctl') === 0){
                preg_match('/__doPostBack\(\'(.*)\',\'/',$href,$matches);
                //var_dump($matches);
                return array($matches[1]);
            }
        }else{
            $list = $e->childNodes;

            for($i = 0; $i < $list->length; $i++){
                $node = $list->item($i);
                $out = array_merge($out,getPages($node));
            }
        }
    }
    return $out;
}


function findParams($e){
    global $form;

    $paramNames = array(
        "__VIEWSTATE",
        "__EVENTVALIDATION",
    );

    $list = $e->childNodes;
    $form = array();

    for($i = 0; $i < $list->length; $i++){
        $node = $list->item($i);

        if($node instanceof DOMElement){
            if($node->nodeName == "input"){
                $name = trim($node->getAttribute("name"));
                if(in_array($name, $paramNames)){
                    $form[$name] = trim($node->getAttribute("value"));
                }

            }else{
                $form = array_merge($form,findParams($node));
            }
        }

    }
    return $form;

}



function parsegoods(DOMElement $e){
    $list = $e->childNodes;
    for($i=0;$i< $list->length; $i++){
        $node = $list->item($i);
        if($node instanceof DOMElement){

            if($node->nodeName == "a"){

                if(preg_match("/_ctl0_ContentPH_GoodsDG__ct/",$node->getAttribute("id"))){
                    $p = explode("_", $node->getAttribute("id"));
                    if($p[6] == "A3"){;
                        $link = $node->getAttribute("href");
                        $name = trim($node->nodeValue);
                        $name = decode($name);
                        $params = array("link" => $link, "name" => $name);
                        q("insert ignore into test.goods set link=:link, name=:name", $params);

                    }
                }
            }else{
                parsegoods($node);
            }
        }
    }
}

function q($sql,$matches=array()){
    global $h;

    $vals = array();
    foreach($matches as $key => $m){
        $vals[":$key"] = $h->quote($m);
    }

    $sql = str_replace(array_keys($vals), array_values($vals),$sql);

    $r =    $h->query($sql) or die($sql . "\n". print_r($h->errorInfo(),1));
    return  $r;
}

function s($sql){
    /**
     * @var PDOStatement
     */
    $r = q($sql);

    //var_dump($r);
    return $r->fetchAll();
}


function decode($in){

    $ch = curl_init("http://www.artlebedev.ru/tools/decoder/advanced/?random=0.9948204380925745");
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'text=' . urlencode($in) . "&Decode=go&csin=22&csout=11");
    ob_start();
    curl_exec($ch);
    $data = ob_get_contents();
    ob_end_clean();
    curl_close($ch);

    preg_match('/CDATA\[(\".*)\]\]/',$data,$matches);

    list($a,$data) = explode("<![CDATA[",$data);
    list($data,$b) = explode("]]>",$data);
    $data = str_replace("\n", " ", $data);
    return $data;


}