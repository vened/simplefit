<?php
/**
 * @author: mix
 * @date: 26.03.13
 */

include __DIR__. "/../boot.php";
echo "start: " . date("Y-m-d H:i:s") . "\n";
$h = new Helper_Email();
$h->sendWeightNotification();
echo "end  : " . date("Y-m-d H:i:s") . "\n";
