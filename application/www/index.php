<?php
require '../boot.php';

$application = new Application();
$application->addModule('ws');
$application->addModule('ifr');
$application->addModule('exchange');
$application->run();
