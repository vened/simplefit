<?php

class Config
{
    private static $config = array(
        'producation_host' => 'www.simplefit.ru',
        'images_host' => array(
            'dev' => 'http://images.%s',
            'test' => 'http://testimages.%s',
            'production' => 'http://images.%s',
        ),

        'images_root' => array(
            'dev' => "images",
            'test' => "../../images.test",
            'production' => '/simplefit/images',
        ),


        'logs' => array(
            'pdo_log' => '/monamour/pdologs/',
        ),

        'template' => array(
            'twig_path' => '/monamour/Twig/',
            'cache_path' => array(
                'dev' => false,
                'test' => false,
                'production' => '/simplefit/templates_cache/',
            )
        ),

        'database' => array(
            'dev' => array(
                'host' => '127.0.0.1',
                'port' => '3306',
                'user' => 'root',
                'password' => 'root',
                'dbname' => 'Simplefit',
            ),
            'test' => array(
                'host' => 'localhost',
                'user' => 'cenometr',
                'password' => 'z1KkdTHPdn',
                'dbname' => 'Simplefit',
            ),
            'production' => array(
                'host' => 'localhost',
                'user' => 'cenometr',
                'password' => 'z1KkdTHPdn',
                'dbname' => 'Simplefit',
            )
        ),

        'social_app' => array(
            'vk' => array(
                'id' => '3292241',
                'secret' => 'ss8oHFfemnjJ3sH2aaC7',
            ),

            'fb' => array(
                'id' => '450611818333840',
                'secret' => '41d53fe697318e2b85e90f07f31d8b4f',
            ),

            'mm' => array(
                'id' => '696481',
                'secret' => 'c57e7d8396dc1fa2afb28a1360510e81',
            ),

            'od' => array(
                'id' => '127573248',
                'secret' => '8BAFD5E2B45F0E1E8A1AC2FB',
            ),
        ),
    );

    public static function get()
    {
        $vars = func_get_args();
        $result = self::$config;
        $path = array();

        foreach ($vars as $var)
        {
            $path[] = $var;

            if (is_array($result) && isset($result[$var])) {
                $result = $result[$var];
            }
            else {
                throw new Exception(sprintf('Error getting the config section: %s', implode('->', $path)));
            }
        }

        return $result;
    }


}