<?php

class PlanController extends Controller_Base
{
    protected $fromDate;
    protected $toDate;
    protected $fromTime;
    protected $toTime;
    protected $selectedDate;
    protected $selectedTime;
    protected $planMapper;
    protected $optimalCalories;

    public function __construct()
    {
        parent::__construct();

        $this->planMapper = new PlanMapper($this->user->getPrefix());
    }

    protected function getDates()
    {
        $this->selectedDate = Request::get('date');
        $this->fromDate = Request::get('f');
        $this->toDate = Request::get('t');

        if (!$this->fromDate) {
            $this->fromDate = '2013-03-01';
           //$this->fromDate = date('Y-m-d', time() - 86400*11);
            $fromDate = date('Y-m-d', time() - 86400 * 59);
            if ($fromDate > $this->fromDate) {
                $this->fromDate = $fromDate;
            }
        }

        if (!$this->toDate) {
            $this->toDate = date('Y-m-d');
        }

        $this->fromTime = strtotime($this->fromDate);
        $this->toTime = strtotime($this->toDate);
        $registerTime = strtotime($this->user->getCreatedDate());

        if ($this->fromTime < $registerTime) {
            $this->fromTime = $registerTime;
            $this->fromDate = date('Y-m-d', $registerTime);
        }

        if ($this->toTime < $this->fromTime) {
            $this->toTime = time();
            $this->toDate = date('Y-m-d');
        }

        if (!$this->selectedDate) {
            $this->selectedDate = $this->toDate;
            $this->selectedTime = $this->toTime;
        }
        else {
            $this->selectedTime = strtotime($this->selectedDate);
            if ($this->selectedTime > $this->toTime) {
                $this->toTime = $this->selectedTime;
                $this->toDate = $this->selectedDate;
            }
        }
    }

    public function indexAction()
    {
        $this->getDates();
        $days = array();
        $toTime = $this->toTime + 11 * 86400;

        $plans = $this->planMapper->getPlanFromDate($this->user->getId(), $this->fromDate);
        $planDates = array();
        $selectedDayMenu = array();
        $selectedCalories = 0;

        //print_r($plans);

        foreach ($plans as $plan) {
            /**
             * @var Plan $plan
             */
            $date = $plan->getCreatedDate();
            $planDates[] = $date;
            if ($date == $this->selectedDate) {

                $products = Helper_Diary::unserializeProductsData($plan->getData());
                $cc = 0;
                foreach($products as $product){
                    $cc += $product["calories"];
                }

                $selectedDayMenu[] = array(
                    'id' => $plan->getId(),
                    'food_type' => $plan->getType(),
                    'products' => $products,
                    'created' => $plan->getCreated(),
                    'calories' => $cc,
                );

                $selectedCalories += $plan->getCalories();
            }
        }

        for ($date = $this->fromTime; $date <= $toTime; $date += 86400) {
            $key = date('Y-m-d', $date);
            $days[$key] = array(
                'day' => substr($key, 8, 2) . '.' . substr($key, 5, 2),
                'has_data' => in_array($key, $planDates),
                'visible' => $date >= $this->selectedTime
            );
        }

        $nextCopyDate = array();
        $currentDate = date('Y-m-d');
        $tomorrowTime = strtotime($currentDate) + 86400;
        $endTime = strtotime(end($planDates)) + 86400;

        for ($date = $tomorrowTime; $date <= $endTime; $date += 86400) {
            $key = date('Y-m-d', $date);
            $day = substr($key, 8, 2);
            $month = substr($key, 5, 2);

            if (!isset($days[$key])) {
                $days[$key] = array(
                    'day' => $day . '.' . $month,
                    'has_data' => true,
                    'visible' => false,
                );
            }

            if (!$nextCopyDate && !in_array($key, $planDates)) {
                $nextCopyDate['day'] = $day;
                $nextCopyDate['month'] = $month;
                break;
            }
        }

        if (!$nextCopyDate) {
            $nextDate = date('d.m', $endTime);
            $nextCopyDate['day'] = substr($nextDate, 0, 2);
            $nextCopyDate['month'] = substr($nextDate, 3, 2);
        }

        $showInfoLayer = !in_array($currentDate, $planDates) ||
                         !in_array(date('Y-m-d', $tomorrowTime), $planDates);
        $result = array(
            'selected_date' => $this->selectedDate,
            'current_date' => $currentDate,
            'from_date' => $this->fromDate,
            'to_date' => $this->toDate,
            'days' => $days,
            'selected_menu' => $selectedDayMenu,
            'selected_calories' => $selectedCalories,
            'show_prev_link' => count($days) > 12,
            'show_next_link' => true, //$cnt < $count,
            'show_edit_block' => $this->selectedDate >= $currentDate,
            'next_copy_date' => $nextCopyDate,
            'show_info_layer' => $showInfoLayer && !Request::getCookie('hide_plan_info'),
            'date'=>@$_REQUEST["date"]?:date("Y-m-d"),
        );

        return $result;
    }

}