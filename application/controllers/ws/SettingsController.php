<?php

class ws_SettingsController extends Controller_Base
{
    protected $needProfileData = false;

    public function indexAction()
    {
        return false;
    }

    public function mainAction()
    {
        $firstName = trim(Request::get('first_name'));
        $lastName = trim(Request::get('last_name'));
        $email = trim(Request::get('email'));
        $password = trim(Request::get('password'));
        $changePassword = Request::get('change_password');
        $userMapper = new UserMapper();

        if ($firstName) {
            $this->user->setFirstName($firstName);
        }

        $this->user->setLastName($lastName);

        if ($email) {
            $this->user->setEmail($email);
        }

        if ($changePassword && $password && $password != '******') {
            $this->user->setPassword($password);
        }

        $notificationSettings = array();
        foreach(array("reminder","news") as $key){
            if(Request::get("email_$key")){
                $notificationSettings[] = $key;
            }
        }
        $this->user->setProperty("email_notify", implode(",", $notificationSettings));

        $userMapper->save($this->user);

        return array('status' => 'ok');
    }

    public function profileAction()
    {
        $gender = Request::get('gender');
        $bYear = (int) Request::get('birthday_year');
        $bMonth = (int) Request::get('birthday_month');
        $bDay = (int) Request::get('birthday_day');
        $height = (int) Request::get('height');
        $goal = Request::get('goal');
        $weight = trim(Request::get('weight'));
        $weightGoal = trim(Request::get('weight_goal'));
        $vegetarianism = Request::get('vegetarianism');
        $activity = (int) Request::get('activity');

        if (!in_array($gender, $this->user->enumGender())) {
            return array('status' => 'wrong_data');
        }

        if (!$bYear || !$bMonth || !$bDay || $bYear < 1940 || !$height || !$weight || !$weightGoal) {
            return array('status' => 'wrong_data');
        }

        if (!in_array($goal, $this->user->enumGoal())) {
            return array('status' => 'wrong_data');
        }

        $weight = (float) str_replace(',', '.', $weight);
        $weightGoal = (float) str_replace(',', '.', $weightGoal);

        if (!$weight || !$weightGoal) {
            return array('status' => 'wrong_data');
        }

        if (!in_array($vegetarianism, $this->user->enumVegetarianism())) {
            return array('status' => 'wrong_data');
        }

        $params = array(
            'gender' => $gender,
            'birthdate' => $bYear . '-' . $bMonth . '-' . $bDay,
            'height' => $height,
            'goal' => $goal,
            'status' => 'Active',
            'weight' => $weight,
            'weight_goal' => $weightGoal,
            'activity' => $activity,
            'vegetarianism' => $vegetarianism,
        );

        if (Request::get('photo_uploaded') && Session::get('up-photo')) {
            $photoMapper = new PhotoMapper($this->user->getPrefix());
            $photo = new Photo();
            $photo->setUserId($this->user->getId());
            //$photo->setDescription('');
            $photoMapper->save($photo);
            Helper_Photo::uploadPhoto($photo, Session::get('up-photo'), true);
            Session::remove('up-photo');

            if ($this->user->getMainPhotoId()) {
                $photoMapper = new PhotoMapper($this->user->getPrefix());
                $prevPhoto = new Photo();
                $prevPhoto->setProperty('photo_id', $this->user->getMainPhotoId());
                $prevPhoto->setUserId($this->user->getId());
                $photoMapper->drop($prevPhoto);
            }

            $params['main_photo_id'] = $photo->getId();
        }

        $this->user->setProperties($params);


        $userMapper = new UserMapper();
        $userMapper->save($this->user);

        return array('status' => 'ok');
    }

    public function planAction()
    {
        Request::setCookie('hide_plan_info', 1, 7*86400);

        return array('status' => 'ok');
    }
}