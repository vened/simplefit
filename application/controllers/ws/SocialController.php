<?php

class ws_SocialController extends Controller_Base
{
    protected $userDataMapper;
    protected $userData;

    public function __construct()
    {
        parent::__construct();

        $this->userDataMapper = new UserDataMapper();
        $this->userData = $this->userDataMapper->find($this->user->getId());

        if ($this->userData->getUserId()) { // not empty (exists in db)
            $this->userData->setId($this->user->getId());
        } else {
            $this->userData->setUserId($this->user->getId());
        }
    }
    public function vkAction()
    {
        $post_id = Request::get('post_id');

        if ($post_id) {
            $this->userData->setVkPosted();
            $this->userDataMapper->save($this->userData);
        }

        return array();
    }

    public function fbAction()
    {
        $post_id = Request::get('post_id');

        if ($post_id) {
            $this->userData->setFbPosted();
            $this->userDataMapper->save($this->userData);
        }

        return array();
    }

    public function mmAction()
    {
        $posted = Request::get('posted');

        if ($posted) {
            $this->userData->setMmPosted();
            $this->userDataMapper->save($this->userData);
        }

        return array();
    }
}