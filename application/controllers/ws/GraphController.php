<?php

class ws_GraphController extends Controller_Base
{
    public function weightAction()
    {
        $from = Request::get('f');
        $to = Request::get('t');

        if (!$from) {
            //$from = date('Y-m-d', time() - 86400 * 11);
            $from = date('Y-m-d', time() - 86400 * 59);
        }

        if (!$to) {
            $to = date('Y-m-d');
        }

        $startTime = strtotime($from);
        $endTime = strtotime($to);
        $registerTime = strtotime($this->user->getCreatedDate());

        if ($startTime < $registerTime) {
            $startTime = $registerTime;
            $from = date('Y-m-d', $registerTime);
        }

        if ($endTime > time() || $endTime < $startTime) {
            $endTime = time();
            $to = date('Y-m-d');
        }

        $diaryMapper = new DiaryMapper($this->user->getPrefix());
        $weightData = $diaryMapper->getWeightData($this->user->getId(), $from, $to);
        $result = array();

        foreach ($weightData as $weight) {
            $result[$weight->getCreatedDate()] = $weight->toArray();
        }

        $lines = array();
        $points = array();
        $count = 0;
        $minWeight = 500;
        $maxWeight = 0;
        $weight = $this->user->getWeight();

        for ($date = $startTime; $date <= $endTime; $date += 86400) {
            $count++;
            $key = date('Y-m-d', $date);
            $pointData = false;

            if (isset($result[$key])) {
                $weight = $result[$key]['data'];
                $pointData = true;
            } else {
            }

            if (isset($lines[$count - 1])) {
                $lines[$count - 1]['x2'] = $count;
                $lines[$count - 1]['y2'] = $weight;
            }

            $lines[$count] = array(
                'x1' => $count,
                'y1' => $weight,
            );

            $points[$count] = array(
                'x' => $count,
                'y' => $weight,
                'data' => $pointData,
                'date' => $key,
            );

            if ($weight < $minWeight) {
                $minWeight = $weight;
            }

            if ($weight > $maxWeight) {
                $maxWeight = $weight;
            }

            if ($count >= 60) {
                break;
            }
        }

        return array(
            'lines' => array_values($lines),
            'pointers' => array_values($points),
            'misc' => array(array(
                'range_min' => max(floor($minWeight) - 1, 0),
                'range_max' => ceil($maxWeight) + 1
            )),
        );
    }
}