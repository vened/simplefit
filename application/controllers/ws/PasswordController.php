<?php

class ws_PasswordController extends Controller_Base
{
    protected $needAuth = false;

    public function remindAction()
    {
        $email = trim(Request::get('email'));

        $user = $this->getUserData($email);

        if (!$user) {
            return array('status' => 'error');
        }
        $this->sendMail($user);

        return array('status' => 'ok');
    }

    public function newAction()
    {
        $password = trim(Request::get('password'));
        $pwdCode = Request::get('pwdcode');

        if ($pwdCode) {
            $mcache = MCache::getInstance();
            $key = 'password_' . $pwdCode;

            if ($user = $mcache->get($key)) {
                $userMapper = new UserMapper();
                if ($user instanceof User) {
                    $user->setPassword($password);
                    $userMapper->save($user);
                    $mcache->delete($key);
                    Auth::authByUser($user);
                }
            }
        }

        return array('status' => 'ok');
    }

    protected function getUserData($email)
    {
        if (!$email) {
            return null;
        }

        $userMapper = new UserMapper();
        $user = $userMapper->findByEmail($email);

        return $user->isEmpty() ? null : $user;
    }

    protected function sendMail($user)
    {
        $mail = new Email_RemindPassword($user);
        $mail->send();
    }
}