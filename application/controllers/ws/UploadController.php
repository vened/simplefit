<?php

class qqUploadedFileXhr
{
    private $tmpName = null;
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path)
    {
        $tmpName = $this->getTmpName();
        if (!$tmpName) {
            return false;
        }

        $target = fopen($path, "w");
        $temp = fopen($tmpName, 'r');
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);
        fclose($temp);
        unlink($tmpName);
        chmod($path, 0666);

        return true;
    }

    function getName()
    {
        return $_GET['qqfile'];
    }

    function getSize()
    {
        if (isset($_SERVER["CONTENT_LENGTH"])) {
            return (int)$_SERVER["CONTENT_LENGTH"];
        } else {
            throw new Exception('Getting content length is not supported.');
        }
    }

    function getTmpName()
    {
        if ($this->tmpName) {
            return $this->tmpName;
        }

        $input = fopen("php://input", "r");
        $tempName = tempnam('/tmp', 'up');

        if (!$tempName) {
            throw new Exception('Temorary file is not created');
        }

        $temp = fopen($tempName, 'w+');

        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);
        fclose($temp);

        if ($realSize != $this->getSize()) {
            @unlink($tempName);
            return false;
        }

        $this->tmpName = $tempName;

        return $tempName;
    }
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm
{
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path)
    {
        if (!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)) {
            return false;
        }
        chmod($path, 0666);
        return true;
    }

    function getName()
    {
        return $_FILES['qqfile']['name'];
    }

    function getSize()
    {
        return $_FILES['qqfile']['size'];
    }

    function getTmpName()
    {
        return $_FILES['qqfile']['tmp_name'];
    }


}

class qqFileUploader
{
    private $allowedExtensions = array();
    private $sizeLimit = 10485760;
    private $file;
    private $withIframe = false;

    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760)
    {
        $allowedExtensions = array_map("strtolower", $allowedExtensions);

        $this->allowedExtensions = $allowedExtensions;
        $this->sizeLimit = $sizeLimit;

        $this->checkServerSettings();

        if (isset($_GET['qqfile'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
            $this->withIframe = true;
        } else {
            $this->file = false;
        }
    }

    public function withIframe()
    {
        return $this->withIframe;
    }

    private function checkServerSettings()
    {
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));

        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit) {
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");
        }
    }

    private function toBytes($str)
    {
        $val = trim($str);
        $last = strtolower($str[strlen($str) - 1]);
        switch ($last) {
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }
        return $val;
    }

    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
    function handleUpload($uploadDirectory, $isMainPhoto = false)
    {
        $uploadDirectory .= '/';

        if (!is_writable($uploadDirectory)) {
            return array('error' => "Server error. Upload directory isn't writable.");
        }

        if (!$this->file) {
            return array('error' => 'No files were uploaded.');
        }

        $size = $this->file->getSize();

        if ($size == 0) {
            return array('error' => 'File is empty');
        }

        if ($size > $this->sizeLimit) {
            return array('error' => _('Превышен размер файла'));
        }

        $tmpFileName = $this->file->getTmpName();
        $imageSize = getimagesize($tmpFileName);


        if ($isMainPhoto) {
            if ($imageSize[0] < 400 || $imageSize[1] < 400) {
                if (file_exists($tmpFileName)) {
                    @unlink($tmpFileName);
                }
                return array('error' => _('Минимальное разрешение — 400x400 пикселей'));
            }
        }
        else {
            if ($imageSize[0] < 400 || $imageSize[1] < 300) {
                if (file_exists($tmpFileName)) {
                    @unlink($tmpFileName);
                }
                return array('error' => _('Минимальное разрешение — 400x300 пикселей'));
            }
        }

        $pathinfo = pathinfo($this->file->getName());
        $filename = $pathinfo['filename'];
        //$filename = md5(uniqid());
        $ext = $pathinfo['extension'];

        if ($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)) {
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of ' . $these . '.');
        }

        $filename = Auth::getUser()->getId();
        Session::set('up-photo', $filename . '.' . $ext);

        if ($this->file->save($uploadDirectory . $filename . '.' . $ext)){
            return array('success'=>true);
        } else {
            return array('error'=> 'Could not save uploaded file.' .
                'The upload was cancelled, or server error encountered');
        }


//        try {
//            //$result = $Mapper->updatePhoto($UserID, $tmpFileName, isset($_POST['photo_id']) ? $_POST['photo_id'] : 0);
//        } catch (Exception $ex) {
//            $result = false;
//        }

        if (file_exists($tmpFileName)) {
            @unlink($tmpFileName);
        }

        if (!$result) {
            return array('error' => _('Невозможно загрузить файл.'));
        }

        return array('success' => true);
    }
}

class ws_UploadController extends Controller_Base
{
    protected $needProfileData = false;

    public function indexAction()
    {
        return array();
    }

    public function photoAction()
    {
        $allowedExtensions = array();
        // max file size in bytes
        $sizeLimit = 1 * 1024 * 1024;
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload('/tmp', Request::get('main'));

        if ($uploader->withIframe()) {
            echo '<html><head><script type="text/javascript">var domain="' . $_SERVER['HTTP_HOST'] . '"; document.domain = domain.replace("www.", "");</script></head>' .
                '<body>' . htmlspecialchars(json_encode($result), ENT_NOQUOTES) . '</body></html>';
            exit;
        }

        return $result;
    }
}