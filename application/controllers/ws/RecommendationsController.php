<?php
/**
 * @author: mix
 * @date: 23.05.13
 */
class ws_RecommendationsController extends Controller_Base{

    public function changeAction(){
        $f = new Recommendation_Factory($this->user);
        $res =  $f->changeSystem("triple", $this->request()->get("type"))->toArray();

        return $res;
    }

}