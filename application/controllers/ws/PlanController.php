<?php

class ws_PlanController extends Controller_Base
{
    protected $planMapper;

    public function __construct()
    {
        parent::__construct();

        $this->planMapper = new PlanMapper($this->user->getPrefix());
    }

    public function indexAction()
    {
        return array();
    }

    public function addAction()
    {
        $foodType = Request::get('food_type');
        $products = Request::get('products');
        $date = Request::get('date');
        $allCalories = 0;

        foreach ((array) $products as $product) {
            $allCalories += $product['calories'];
        }

        $date = Helper_Diary::getFoodTime($foodType, $date);
        $plan = $this->planMapper->getMenuByUserId($this->user->getId(), $foodType, $date);

        if ($plan->isEmpty()) {
            $plan->setUserId($this->user->getId());
            $plan->setProperties(array(
                'type' => $foodType,
                'data' => Helper_Diary::serializeProductData($products),
                'calories' => $allCalories,
                'created' => $date,
            ));
        }
        else {
            $productsData = Helper_Diary::unserializeProductsData($plan->getData());
            $productsData = array_merge($productsData, $products);
            $plan->setData(Helper_Diary::serializeProductData($productsData));
            $plan->setCalories($plan->getCalories()+$allCalories);
        }

        $this->planMapper->save($plan);
        $diaryMapper = new DiaryMapper($this->user->getPrefix());
        $currentWeight = $diaryMapper->getCurrentWeight($this->user->getId())->getData();

        $this->addStaffUserDiary($plan, $currentWeight);

        return array('status' => 'ok');
    }

    public function deleteAction()
    {
        $id = Request::get('id');

        $plan = $this->planMapper->find($id);

        if (!$plan->isEmpty() && $plan->getUserId() == $this->user->getId()) {
            $this->planMapper->drop($plan);

            $staffUserDiaryMapper = new \Staff\UserDiaryMapper();
            $staffUserDiary = $staffUserDiaryMapper->findByDiaryId($plan->getId());

            if (!$staffUserDiary->isEmpty() && !$staffUserDiary->isApproved()) {
                $staffUserDiaryMapper->drop($staffUserDiary);
            }
        }

        return array('status' => 'ok');
    }

    public function copyAction()
    {
        $dateFrom = Request::get('date_from');
        $dayTo = Request::get('day_to');
        $monthTo = Request::get('month_to');
        $dateTo = date('Y') . '-' . $monthTo . '-' . $dayTo;
        $confirmed = Request::get('confirmed');

        if ($dateTo <= $dateFrom) {
            return array('error' => 'wrong date');
        }

        $plan = $this->planMapper->getByUserId($this->user->getId(), $dateTo, $dateTo);

        if (!$confirmed && $plan) {
            return array('error' => 'exists');
        }

        if ($confirmed) {
            foreach ($plan as $p) {
                $this->planMapper->drop($p);
            }
        }

        $plan = $this->planMapper->getByUserId($this->user->getId(), $dateFrom, $dateFrom, false);
        $diaryMapper = new DiaryMapper($this->user->getPrefix());
        $currentWeight = $diaryMapper->getCurrentWeight($this->user->getId())->getData();

        foreach ($plan as $p) {
            $created = $dateTo . ' ' . substr($p->getCreated(), 11);
            $newPlan = new Plan($p->toArray());
            $newPlan->setCreated($created);
            $newPlan->setId(null);
            $this->planMapper->save($newPlan);
            $this->addStaffUserDiary($newPlan, $currentWeight);
        }

        return array('status' => 'ok', 'date' => $dateTo);
    }

    protected function addStaffUserDiary(Plan $plan, $currentWeight)
    {
        $staffUserDiaryMapper = new \Staff\UserDiaryMapper();
        $staffUserDiary = $staffUserDiaryMapper->findByDiaryId($plan->getId());
        $staffUserDiary->setUserId($plan->getUserId());
        $staffUserDiary->setDiaryId($plan->getId());
        $staffUserDiary->setGender($this->user->getGender());
        $staffUserDiary->setAge($this->user->getAge());
        $staffUserDiary->setHeight($this->user->getHeight());
        $staffUserDiary->setStartWeight($this->user->getWeight());
        $staffUserDiary->setGoalWeight($this->user->getWeightGoal());
        $staffUserDiary->setCurrentWeight($currentWeight);
        $staffUserDiary->setActivity($this->user->getActivity());
        $staffUserDiary->setVegetarianism($this->user->getVegetarianism());
        $staffUserDiary->setType('plan');
        $staffUserDiary->setFoodType($plan->getType());
        $staffUserDiary->setCalories($plan->getCalories());
        $staffUserDiary->setProductsData($plan->getData());
        $staffUserDiary->setDiaryDate($plan->getCreated());
        $staffUserDiaryMapper->save($staffUserDiary);
    }

    public function editAction()
    {
        $foodType = Request::get('food_type');
        $products = Request::get('products');
        $date = Request::get('date');
        $allCalories = 0;

        foreach ((array)$products as $product) {
            $allCalories += $product['calories'];
        }

        $date = Helper_Diary::getFoodTime($foodType, $date);
        $plan = $this->planMapper->getMenuByUserId($this->user->getId(), $foodType, $date);

        if ($plan->isEmpty()) {
            $plan->setUserId($this->user->getId());
            $plan->setProperties(array(
                'type' => $foodType,
                'data' => Helper_Diary::serializeProductData($products),
                'calories' => $allCalories,
                'created' => $date,
            ));
        } else {
            $plan->setData(Helper_Diary::serializeProductData($products));
            $plan->setCalories($allCalories);
        }

        $this->planMapper->save($plan);
//        $diaryMapper = new DiaryMapper($this->user->getPrefix());
//        $currentWeight = $diaryMapper->getCurrentWeight($this->user->getId())->getData();
//
//        $this->addStaffUserDiary($plan, $currentWeight);

        return array('status' => 'ok');
    }
}