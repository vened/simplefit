<?php

class ws_PaymentController extends Controller_Base
{
    public function indexAction()
    {
        $cost = $this->request()->get('cost');

        switch ($cost) {
            case \Billing\Payment::COST_1:
            case \Billing\Payment::COST_2:
            case \Billing\Payment::COST_3:
                break;

            default:
                return array('error' => 'Wrong cost');
        }

        $payment = new \Billing\Payment();
        $payment->init($this->user, $cost);

        return array('url' => $payment->getRedirectUrl());
    }
}