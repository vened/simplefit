<?php

class ws_FeedbackController extends Controller_Base
{
    protected $needAuth = false;

    public function indexAction()
    {
        $userEmail = trim(Request::get('email'));
        $message = trim(Request::get('message'));
        $result = array();

        if (!$userEmail || !$message || !filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
            return $result;
        }

        $email = Email_Transport_Factory::getMailer("feedback");//new Email_Simple('feedback@simplefit.ru',"обратная связь");
        $email->setSubject('Обратная связь');

        $message = nl2br($message);
        $message = 'Вопрос от ' . $userEmail . "<br />\n" . $message;
        $email->setHtmlBody($message);
        $email->send("");

        return array();
    }
}