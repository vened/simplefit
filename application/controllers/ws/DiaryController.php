<?php

class ws_DiaryController extends Controller_Base
{
    protected $diaryMapper;

    public function __construct()
    {
        parent::__construct();

        $this->diaryMapper = new DiaryMapper($this->user->getPrefix());
    }

    public function indexAction()
    {
        return array();
    }

    public function caloriesAction()
    {
        $foodType = Request::get('food_type');
        $products = Request::get('products');
        $date = Request::get('date');
        $allCalories = 0;

        foreach ((array) $products as $product) {
            $allCalories += $product['calories'];
        }

        $date = Helper_Diary::getFoodTime($foodType, $date);
        $diary = $this->diaryMapper->getMenuByUserId($this->user->getId(), $foodType, $date);

        if ($diary->isEmpty()) {
            $diary->setUserId($this->user->getId());
            $diary->setProperties(array(
                'type' => 'calories',
                'food_type' => $foodType,
                'data' => $allCalories,
                'products_data' => Helper_Diary::serializeProductData($products),
                'created' => $date,
            ));
        }
        else {
            $productsData = Helper_Diary::unserializeProductsData($diary->getProductsData());
            $productsData = array_merge($productsData, $products);
            $diary->setProductsData(Helper_Diary::serializeProductData($productsData));
            $diary->setData($diary->getData()+$allCalories);
        }

        $this->diaryMapper->save($diary);
        $currentWeight = $this->diaryMapper->getCurrentWeight($this->user->getId())->getData();

        $staffUserDiaryMapper = new \Staff\UserDiaryMapper();
        $staffUserDiary = $staffUserDiaryMapper->findByDiaryId($diary->getId());
        $staffUserDiary->setUserId($diary->getUserId());
        $staffUserDiary->setDiaryId($diary->getId());
        $staffUserDiary->setGender($this->user->getGender());
        $staffUserDiary->setAge($this->user->getAge());
        $staffUserDiary->setHeight($this->user->getHeight());
        $staffUserDiary->setStartWeight($this->user->getWeight());
        $staffUserDiary->setGoalWeight($this->user->getWeightGoal());
        $staffUserDiary->setCurrentWeight($currentWeight);
        $staffUserDiary->setActivity($this->user->getActivity());
        $staffUserDiary->setVegetarianism($this->user->getVegetarianism());
        $staffUserDiary->setFoodType($diary->getFoodType());
        $staffUserDiary->setCalories($diary->getData());
        $staffUserDiary->setProductsData($diary->getProductsData());
        $staffUserDiary->setDiaryDate($diary->getCreated());
        $staffUserDiaryMapper->save($staffUserDiary);

        return array('status' => 'ok');
    }

    public function weightAction()
    {
        $weight = Request::get('weight');
        $weight = floatval(str_replace(',', '.', $weight));
        $date = Request::get('date');

        if (!$weight || ($date && !strtotime($date))) {
            return array('status' => 'error');
        }

        if (!$date) {
            $date = date('Y-m-d');
        }

        $lastDiaryWeight = $diary = $this->diaryMapper->getCurrentWeight($this->user->getId(), $date);

        if ($diary->isEmpty()) {
            $diary->setUserId($this->user->getId());
            $diary->setType('weight');
            $lastDiaryWeight = $this->diaryMapper->getCurrentWeight($this->user->getId());
        }

        $lastWeight = $lastDiaryWeight->getProperty("data");
        if($lastWeight){
            if($lastWeight - $weight > 0){
                $h = new Helper_WeightLoss();
                $h->updateTotal($lastWeight - $weight);
            }
        }

        if (strlen($date) == 10) {
            $date .= ' ' . date('H:i:s');
        }

        $diary->setData($weight);
        $diary->setCreated($date);

        $this->diaryMapper->save($diary);

        return array('status' => 'ok');
    }

    public function photoAction()
    {
        if (!Session::get('up-photo')) {
            return array('status' => 'error');
        }

        $photoMapper = new PhotoMapper($this->user->getPrefix());
        $photo = new Photo();
        $photo->setUserId($this->user->getId());
        //$photo->setDescription('');
        $photoMapper->save($photo);

        Helper_Photo::uploadPhoto($photo, Session::get('up-photo'));
        Session::remove('up-photo');


        $diary = new Diary();
        $diary->setProperties(array(
            'user_id' => $this->user->getId(),
            'type' => 'photo',
            'data' => $photo->getId(),
        ));

        $this->diaryMapper->save($diary);

        return array('photo_url' => Helper_Photo::getDiaryPhotoUrl($photo));
    }

    public function deleteAction()
    {
        $id = Request::get('id');

        $diary = $this->diaryMapper->find($id);

        if (!$diary->isEmpty() && $diary->getUserId() == $this->user->getId()) {
            $this->diaryMapper->drop($diary);

            if ($diary->getType() == 'photo') {
                $photoMapper = new PhotoMapper($this->user->getPrefix());
                $photo = new Photo();
                $photo->setProperty('photo_id', $diary->getData());
                $photo->setUserId($diary->getUserId());
                $photoMapper->drop($photo);
            }

            $staffUserDiaryMapper = new \Staff\UserDiaryMapper();
            $staffUserDiary = $staffUserDiaryMapper->findByDiaryId($diary->getId());

            if (!$staffUserDiary->isEmpty() && !$staffUserDiary->isApproved()) {
                $staffUserDiaryMapper->drop($staffUserDiary);
            }
        }

        return array('status' => 'ok');
    }

    public function editAction()
    {
        $id = Request::get('id');
        $foodType = Request::get('food_type');
        $products = Request::get('products');
        $date = Request::get('date');
        $allCalories = 0;

        $diary = $this->diaryMapper->find($id);

        if (!$diary->isEmpty() && $diary->getUserId() == $this->user->getId()) {

            foreach ((array)$products as $product) {
                $allCalories += $product['calories'];
            }

            $diary->setProperties(array(
                'food_type' => $foodType,
                'data' => $allCalories,
                'products_data' => Helper_Diary::serializeProductData($products),
            ));

            $this->diaryMapper->save($diary);

            $staffUserDiaryMapper = new \Staff\UserDiaryMapper();
            $staffUserDiary = $staffUserDiaryMapper->findByDiaryId($diary->getId());

            if (!$staffUserDiary->isEmpty() && !$staffUserDiary->isApproved()) {
                $currentWeight = $this->diaryMapper->getCurrentWeight($this->user->getId())->getData();
                $staffUserDiary->setUserId($diary->getUserId());
                $staffUserDiary->setDiaryId($diary->getId());
                $staffUserDiary->setGender($this->user->getGender());
                $staffUserDiary->setAge($this->user->getAge());
                $staffUserDiary->setHeight($this->user->getHeight());
                $staffUserDiary->setStartWeight($this->user->getWeight());
                $staffUserDiary->setGoalWeight($this->user->getWeightGoal());
                $staffUserDiary->setCurrentWeight($currentWeight);
                $staffUserDiary->setActivity($this->user->getActivity());
                $staffUserDiary->setVegetarianism($this->user->getVegetarianism());
                $staffUserDiary->setFoodType($diary->getFoodType());
                $staffUserDiary->setCalories($diary->getData());
                $staffUserDiary->setProductsData($diary->getProductsData());
                $staffUserDiary->setDiaryDate($diary->getCreated());
                $staffUserDiary->setStatus('New');
                $staffUserDiaryMapper->save($staffUserDiary);
            }
        }

        return array('status' => 'ok');
    }
}