<?php

class ws_ConfirmController extends Controller_Base
{
    protected $needAuth = false;

    public function indexAction()
    {
        return array();
    }

    public function vkAction()
    {
        $userEmail = Request::get('email');

        $userMapper = new UserMapper();
        $user = $userMapper->findByEmail($userEmail);

        if ($user->isEmpty()) {
            return array();
        }

        $vkConfig = Config::get('social_app', 'vk');
        $vkCookie = Helper_Vk::parseCookieData(Request::getCookie('vk_app_' . $vkConfig['id']));

        if (Helper_Vk::checkSignature($vkCookie, $vkConfig['secret'])) {
            $data = array(
                'type' => 'vk',
                'vk_id' => $vkCookie['mid'],
            );

            $this->sendMail($user);
        }

        return array();
    }

    protected function generateConfirmCode($userId, $data = array())
    {
        $data['user_id'] = $userId;
        if (empty($data['type'])) {
            $data['type'] = 'email';
        }
        $code = md5(uniqid($userId));
        $key = 'confirm_' . $code;

        $mcache = MCache::getInstance();
        $mcache->set($key, $data, MCache::EXPIRY_NORMAL);

        return $code;
    }

    protected function sendMail(User $user){
        $email = new Email_Confirm($user);
        $email->send();
    }
}