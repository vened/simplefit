<?php

if(!class_exists("SphinxClient")){
    include_once 'packages/sphinxapi.php';
}

class ws_ProductsController extends Controller_Base
{
    protected $checkReferer = false;
    protected $needProfileData = false;

    public function __construct()
    {
        parent::__construct();

        if ($this->checkReferer) {
            if (empty($_SERVER['HTTP_REFERER'])) {
                exit;
            }

            $url = parse_url($_SERVER['HTTP_REFERER']);

            if ($url['host'] != BASE_HOST) {
                exit;
            }
        }
    }

    public function indexAction()
    {
        return array();
    }

    public function searchAction()
    {
        $q = trim(Request::get('q'));

        $sphinx = new SphinxClient();
        $sphinx->SetServer('127.0.0.1', 9312);
        $sphinx->SetMatchMode(SPH_MATCH_EXTENDED2);
        $sphinx->SetRankingMode(SPH_RANK_SPH04);
        $sphinx->SetSortMode(SPH_SORT_RELEVANCE);
        $sphinx->SetLimits(0, 7);

        $sphinx->SetFilter('is_available', array('1'));
        $sphinx->AddQuery($q, '*');
        $sphinx->ResetFilters();

        if ($this->user) {
            $sphinx->SetFilter('is_available', array('0'));
            $sphinx->SetFilter('user_id', array($this->user->getId()));
            $sphinx->AddQuery($q, '*');
        }

        $results = $sphinx->RunQueries();
        $return = array();

        if ($results) {
            foreach ($results as $result) {
                if ($result['total_found'] && !empty($result['matches'])) {
                    $productsMapper = new ProductsMapper();
                    $products = $productsMapper->findByIds(array_keys($result['matches']), true);

                    foreach ($products as $product) {
                        $return[] = $product->toArray();
                    }
                }
            }
        }

        /*
        $return[] = array(
            'id' => 1,
            'name' => 'test',
            'calories' => 120,
        );
        */

        return $return;
    }

    public function addAction()
    {
        $productName = Request::get('name');
        $calories = (float) Request::get('calories');
        $proteins = (float) Request::get('proteins');
        $fats = (float) Request::get('fats');
        $carbohydrates = (float) Request::get('carbohydrates');

        $productsMapper = new ProductsMapper();
        $product = new Product();
        $product->setProperties(array(
            'name' => $productName,
            'calories' => $calories,
            'proteins' => $proteins,
            'fats' => $fats,
            'carbohydrates' => $carbohydrates,
            'user_id' => $this->user->getId(),
            'is_available' => '0',
        ));
        $productsMapper->save($product);
        header("Content-type: text/json");
        return array('status' => 'ok', 'product' => array($product->toArray()));
    }
}