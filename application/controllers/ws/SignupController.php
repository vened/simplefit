<?php

class ws_SignupController extends ws_SigninController
{
    protected $needAuth = false;
    protected $prefixMapper;

    public function __construct()
    {
        parent::__construct();

        $this->prefixMapper = new PrefixMapper();
    }

    public function indexAction()
    {
        $name = trim(Request::get('first_name'));
        $email = trim(Request::get('email'));
        $password = trim(Request::get('password'));
        $result = array(
            'status' => 'ok',
        );

        $this->user = $this->userMapper->findByEmail($email);

        if ($this->user->isEmpty()) {
            $properties = array(
                'first_name' => $name,
                'email' => $email,
                'password' => $password,
                'recommendation_count' => 0,
            );
            try{
                Db_Database::getInstance()->begin();
                $this->register($properties);
                $this->sendMail($this->user);
                Db_Database::getInstance()->commit();
            }catch (Db_Exception $e){
                Db_Database::getInstance()->rollback();
                throw $e;
            }

            $result['redirect'] = Navigation::get('settings');
        }
        else {
            $result['status'] = 'mail_exists';
        }

        return $result;
    }

    protected function register($properties)
    {
        return Helper_Register::Register($this->user,$properties);
    }

    public function vkAction()
    {
        $vkConfig = Config::get('social_app', 'vk');
        $vkCookie = Helper_Vk::parseCookieData(Request::getCookie('vk_app_' . $vkConfig['id']));
        $result = array(
            'status' => 'ok',
            'vk_posted' => 0,
        );

        if (Helper_Vk::checkSignature($vkCookie, $vkConfig['secret'])) {
            $vkId = $vkCookie['mid'];
            $this->user = $this->userMapper->findByVkId($vkId);

            if (!$this->user->isEmpty()) {
                $this->postLogin();
                $result['redirect'] = Navigation::get('profile');
                $result['vk_posted'] = $this->user->vkPosted();
            }
            else {
                $name = Request::get('first_name');
                $lastName = Request::get('last_name');
                $email = Request::get('email');
                $gender = Request::get('gender');

                if ($name && $lastName && $this->checkEmail($email)) {
                    $existUser = $this->userMapper->findByEmail($email);
                    $data = array(
                        'type' => 'vk',
                        'vk_id' => $vkId,
                    );

                    if (!$existUser->isEmpty()) {
                        $result['status'] = 'user_exists';
                        $code = $this->generateConfirmCode($existUser->getId(), $data);
                        $this->sendMail($existUser, $code);
                    }
                    else {
                        $properties = array(
                            'first_name' => $name,
                            'last_name' => $lastName,
                            'email' => $email,
                            'gender' => Helper_Vk::getSex($gender),
                        );
                        $this->register($properties);

                        $result['redirect'] = Navigation::get('settings/profile');
                        $code = $this->generateConfirmCode($this->user->getId(), $data);
                        $this->sendMail($this->user, $code);
                    }
                }
                else {
                    $result['status'] = 'incorrect_field';
                }
            }
        }
        else {
            $result['status'] = 'error';
        }

        return $result;
    }

    public function fbAction()
    {
        $firstName = Request::get('first_name');
        $lastName = Request::get('last_name');
        $email = Request::get('email');
        $gender = Request::get('gender');
        $sigRequest = Request::get('sig');
        $fbConfig = Config::get('social_app', 'fb');
        $data = Helper_Facebook::parseSignedRequest($sigRequest, $fbConfig['secret']);

        $result = array(
            'status' => 'ok',
            'fb_posted' => 0,
        );

        if ($data && $firstName && $lastName && $this->checkEmail($email)) {
            $fbId = $data['user_id'];
            $this->user = $this->userMapper->findByFbId($fbId);

            if (!$this->user->isEmpty()) {
                $this->postLogin();
                $result['redirect'] = Navigation::get('profile');
                $result['fb_posted'] = $this->user->fbPosted();
            }
            else {
                $existUser = $this->userMapper->findByEmail($email);

                if (!$existUser->isEmpty()) {
                    $existUser->setFbId($fbId);
                    $existUser->setLastLogin();
                    $existUser->setSecret();
                    $existUser->setEmailConfirmed();
                    $this->userMapper->save($existUser);

                    Auth::authByUser($existUser);
                    $result['redirect'] = Navigation::get('profile');
                    $result['fb_posted'] = $existUser->fbPosted();
                }
                else {
                    //$birthdate = explode('/', $data['registration']['birthday']);
                    $properties = array(
                        'first_name' => $firstName,
                        'last_name' => $lastName,
                        'email' => $email,
                        //'birthdate' => $birthdate ? $birthdate[2].'-'.$birthdate[0].'-'.$birthdate[1] : '',
                        'gender' => Helper_Facebook::getGender($gender),
                        'email_confirmed' => 1,
                        'fb_id' => $fbId,
                    );
                    $this->register($properties);
                    $result['redirect'] = Navigation::get('settings/profile');
                }
            }
        } else {
            $result['status'] = 'error';
        }

        return $result;
    }

    public function mmAction()
    {
        $mmConfig = Config::get('social_app', 'mm');
        $cookie = Helper_Mailru::getCookieData();

        $result = array(
            'status' => 'ok',
            'mm_posted' => 0,
        );

        if (Helper_Mailru::checkSignature($cookie, $mmConfig['secret'])) {
            Helper_Mailru::setParams($mmConfig['id'], $mmConfig['secret']);
            $userInfo = Helper_Mailru::getUserInfo($cookie);
            $mmId = $userInfo['uid'];

            $this->user = $this->userMapper->findByMailruId($mmId);

            if (!$this->user->isEmpty()) {
                $this->postLogin();
                $result['redirect'] = Navigation::get('profile');
                $result['mm_posted'] = $this->user->mmPosted();
            } else {
                $existUser = $this->userMapper->findByEmail($userInfo['email']);

                if (!$existUser->isEmpty()) {
                    $existUser->setMailruId($mmId);
                    $existUser->setLastLogin();
                    $existUser->setSecret();
                    $existUser->setEmailConfirmed();
                    $this->userMapper->save($existUser);

                    Auth::authByUser($existUser);
                    $result['redirect'] = Navigation::get('profile');
                    $result['mm_posted'] = $existUser->mmPosted();
                } else {
                    $properties = array(
                        'first_name' => $userInfo['first_name'],
                        'last_name' => $userInfo['last_name'],
                        'email' => $userInfo['email'],
                        'gender' => Helper_Mailru::getGender($userInfo['sex']),
                        'email_confirmed' => 1,
                        'mm_id' => $mmId,
                    );
                    $this->register($properties);
                    $result['redirect'] = Navigation::get('settings/profile');
                }
            }
        }
        else {
            $result['status'] = 'error';
        }

        return $result;
    }

    protected function checkEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    protected function generateConfirmCode($userId, $data = array())
    {
        $data['user_id'] = $userId;
        if (empty($data['type'])) {
            $data['type'] = 'email';
        }
        $code = md5(uniqid($userId));
        $key = 'confirm_' . $code;

        $mcache = MCache::getInstance();
        $mcache->set($key, $data, MCache::EXPIRY_NORMAL);

        return $code;
    }

    protected function sendMail()
    {
        $mail = new Email_Confirm($this->user);
        $mail->send();
    }


    public function resendAction(){

        if($this->user){
            $this->sendMail($this->user);
        }
        return "Ok";
    }
}