<?php

class ws_SigninController extends Controller_Base
{
    protected $needAuth = false;
    protected $userMapper;

    public function __construct()
    {
        parent::__construct();

        $this->userMapper = new UserMapper();
    }
    public function indexAction()
    {
        $email = Request::get('email');
        $password = Request::get('password');

        $referer = parse_url($_SERVER["HTTP_REFERER"],PHP_URL_PATH);

        if(strpos($referer,"/recommendations") ===0){
            $redirect = $referer;
        }else{
            $redirect = Navigation::get('profile');
        }

        $result = array(
            'status' => 'ok',
            'redirect' => $redirect,
        );

        $rememberMe = Request::get('remember');

        if (!Auth::authorize($email, $password, $rememberMe)) {
            $result = array('status' => 'error');
        }

        return $result;
    }

    public function vkAction()
    {
        $vkConfig = Config::get('social_app', 'vk');
        $vkCookie = Helper_Vk::parseCookieData(Request::getCookie('vk_app_' . $vkConfig['id']));
        $result = array(
            'status' => 'ok',
            'vk_posted' => 0,
        );

        if (Helper_Vk::checkSignature($vkCookie, $vkConfig['secret'])) {
            $vkId = $vkCookie['mid'];
            $this->user = $this->userMapper->findByVkId($vkId);

            if (!$this->user->isEmpty()) {
                $this->postLogin();
                $result['redirect'] = Navigation::get('profile');
                $result['vk_posted'] = $this->user->vkPosted();
            }
            else {
                $result['status'] = 'no_user';
            }
        }
        else {
            $result['status'] = 'error';
        }

        return $result;
    }

    public function fbAction()
    {
        $sigRequest = Request::get('signed_request');
        $fbConfig = Config::get('social_app', 'fb');
        $data = Helper_Facebook::parseSignedRequest($sigRequest, $fbConfig['secret']);

        $result = array(
            'status' => 'ok',
            'fb_posted' => 0,
        );

        if ($data) {
            $fbId = $data['user_id'];
            $this->user = $this->userMapper->findByFbId($fbId);

            if (!$this->user->isEmpty()) {
                $this->postLogin();

                $result['fb_posted'] = $this->user->fbPosted();
                $result['redirect'] = Navigation::get('profile');
            }
            else {
                $result['status'] = 'no_user';
            }
        }
        else {
            $result['status'] = 'error';
        }

        return $result;
    }

    public function mmAction()
    {
        $mmConfig = Config::get('social_app', 'mm');
        $cookie = Helper_Mailru::getCookieData();

        $result = array(
            'status' => 'ok',
            'mm_posted' => 0,
        );

        if (Helper_Mailru::checkSignature($cookie, $mmConfig['secret'])) {
            $mmId = $cookie['oid'];
            $this->user = $this->userMapper->findByMailruId($mmId);

            if (!$this->user->isEmpty()) {
                $this->postLogin();

                $result['mm_posted'] = $this->user->mmPosted();
                $result['redirect'] = Navigation::get('profile');
            } else {
                $result['status'] = 'no_user';
            }
        } else {
            $result['status'] = 'error';
        }

        return $result;
    }

    public function odAction()
    {
        print_r($_REQUEST);
        print_r($_COOKIE);
    }

    protected function postLogin()
    {
        if ($this->user->isEmpty()) {
            return false;
        }

        $this->user->setLastLogin();
        $this->user->setSecret();
        $this->userMapper->save($this->user);
        Auth::authByUser($this->user);

        return true;
    }
}