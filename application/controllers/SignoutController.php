<?php

class SignoutController extends Controller_Base
{
    protected $needAuth = false;

    public function __construct()
    {
        parent::__construct();

        Auth::logout();
        $this->user = null;

        if (isset($_SERVER['HTTP_REFERER']) && preg_match('/blogs\.simplefit\.ru/', $_SERVER['HTTP_REFERER'])) {
            throw new RedirectException("http://blogs.simplefit.ru/");
        }
        else {
            throw new RedirectException("/");
        }
    }

    public function indexAction()
    {
        return array();
    }
}