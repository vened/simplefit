<?php

class ifr_DiaryController extends Controller_Base
{
    const ITEMS_PER_PAGE = 16;

    public function indexAction()
    {
        $offset = (int) Request::get('offset');
        $diaryMapper = new DiaryMapper($this->user->getPrefix());
        $data = $diaryMapper->findByUserId($this->user->getId(), self::ITEMS_PER_PAGE, $offset);
        $allCount = $diaryMapper->calcFoundRows();
        $lastData = array();

        if ($data) {
            $date = $data[count($data) - 1]->getCreated();
            $lastData = $diaryMapper->findByDate($this->user->getId(), $date);
            $data = array_merge($data, $lastData);
        }

        $photoMapper = new PhotoMapper($this->user->getPrefix());
        $count = 0;

        foreach ($data as $key => $val) {
            $side = $key % 2 ? 'left' : 'right';
            $data = $val->toArray();
            $date = substr($val->getCreated(), 0, 10);

            //if (isset($result['diary_data'][$date]['left'][$val->getId()])) {
            if (isset($result['diary_data'][0]['left'][$val->getId()])) {
                continue;
            } //elseif (isset($result['diary_data'][$date]['right'][$val->getId()])) {
            elseif (isset($result['diary_data'][0]['right'][$val->getId()])) {
                continue;
            }

            if ($data['type'] == 'photo') {
                $photo = $photoMapper->find($val->getData());
                $data['photo_url'] = Helper_Photo::getDiaryPhotoUrl($photo);
                $data['photo_descr'] = $photo->getDescription();
            } elseif ($data['type'] == 'calories') {
                $data['products_data'] = Helper_Diary::unserializeProductsData($data['products_data']);
            }

            //$result['diary_data'][$date][$side][$val->getId()] = $data;
            $result['diary_data'][0][$side][$val->getId()] = $data;
            $count++;
        }

        $result['show_next_page'] = $allCount > $count;

        return $result;
    }
}