<?php

class AnalyticsController extends Controller_Base
{
    protected $fromDate;
    protected $toDate;
    protected $fromTime;
    protected $toTime;
    protected $selectedDate;
    protected $diaryMapper;
    protected $optimalCalories;

    public function __construct()
    {
        parent::__construct();
        $this->preAction();

        $this->diaryMapper = new DiaryMapper($this->user->getPrefix());
    }

    protected function getDates()
    {
        $this->selectedDate = Request::get('date');
        $this->fromDate = Request::get('f');
        $this->toDate = Request::get('t');

        if (!$this->fromDate) {
            //$this->fromDate = date('Y-m-d', time() - 86400*11);
            $this->fromDate = date('Y-m-d', time() - 86400 * 59);
        }

        if (!$this->toDate) {
            $this->toDate = date('Y-m-d');
        }

        $this->fromTime = strtotime($this->fromDate);
        $this->toTime = strtotime($this->toDate);
        $registerTime = strtotime($this->user->getCreatedDate());

        if ($this->fromTime < $registerTime) {
            $this->fromTime = $registerTime;
            $this->fromDate = date('Y-m-d', $registerTime);
        }

        if ($this->toTime > time() || $this->toTime < $this->fromTime) {
            $this->toTime = time();
            $this->toDate = date('Y-m-d');
        }

        if (!$this->selectedDate) {
            $this->selectedDate = $this->toDate;
        }
    }

    public function indexAction()
    {
        $this->getDates();
        $this->optimalCalories = $this->user->getCalories();
        $foodDiaries = $this->diaryMapper->getCaloriesData($this->user->getId(), $this->fromDate, $this->toDate);

        $norm = round($this->optimalCalories / 14, 4);
        $caloriesData = array();
        $selectedDayMenu = array();
        $selectedDayCalories = 0;
        $allMenu = array();
        $userGoal = $this->user->getGoal();
        $maxCaloriesVal = $this->optimalCalories;
        $visibleStartTime = $this->toTime - 11 * 86400;

        foreach ($foodDiaries as $diary) {
            $caloriesVal = $diary->getData();
            $date = $diary->getCreatedDate();

            if (isset($caloriesData[$date])) {
                $caloriesVal += $caloriesData[$date]['calories'];
            }

            $maxVal = round($caloriesVal / 14, 4);

            if ($caloriesVal > $maxCaloriesVal) {
                $maxCaloriesVal = $caloriesVal;
            }

            if ($userGoal == 'weight_gain') {
                $caloriesData[$date] = array(
                    'calories' => $caloriesVal,
                    'max_val' => $maxVal,
                    'is_ok' => $this->optimalCalories - $caloriesVal < 50,
                    'norm' => round(($this->optimalCalories - 200) / 14, 4),
                    'norm_light_limit' => $norm,
                );
            }
            else {
                $caloriesData[$date] = array(
                    'calories' => $caloriesVal,
                    'max_val' => $maxVal,
                    'is_ok' => $caloriesVal - $this->optimalCalories < 50,
                    'norm' => $norm,
                    'norm_light_limit' => round(($this->optimalCalories + 200) / 14, 4),
                );
            }

            if ($date == $this->selectedDate) {
                $selectedDayCalories += $diary->getData();
                $selectedDayMenu[] = array(
                    'id' => $diary->getId(),
                    'food_type' => $diary->getFoodType(),
                    'calories' => $diary->getData(),
                    'products' => Helper_Diary::unserializeProductsData($diary->getProductsData()),
                    'created' => $diary->getCreated(),
                );
            }

            if (strtotime($date) >= $visibleStartTime) {
                $allMenu[$date][] = array(
                    'id' => $diary->getId(),
                    'food_type' => $diary->getFoodType(),
                    'calories' => $diary->getData(),
                    'products' => Helper_Diary::unserializeProductsData($diary->getProductsData()),
                    'created' => $diary->getCreated(),
                );
            }
        }

        $res = array();
        $count = 0;
        $days = array();
        $selectedIndex = 0;

        for ($date = $this->fromTime; $date <= $this->toTime; $date+=86400) {
            $key = date('Y-m-d', $date);
            $days[$key] = array('day' => date('d.m', $date));

            if (isset($caloriesData[$key])) {
                $res[$key] = $caloriesData[$key];
                $res[$key]['has_data'] = true;
                $days[$key]['has_data'] = true;
            }
            else {
                $res[$key] = array('has_data' => false);
                $days[$key]['has_data'] = false;
            }

            if ($date >= $visibleStartTime) {
                $res[$key]['visible'] = true;
                $days[$key]['visible'] = true;
            }
            else {
                $res[$key]['visible'] = false;
                $days[$key]['visible'] = false;
            }

            if ($key == $this->selectedDate) {
                $selectedIndex = $count;
            }

            $count++;
            if ($count >= 60) {
                break;
            }
        }

        /**
         * $selectedIndex has values 0, 1, 2, ... etc
         * $count has values 1, 2, 3, ... etc
         */
        $cnt = $count;
        if ($count - $selectedIndex > 12) {
            while ($cnt - $selectedIndex > 12) {
                $cnt -= 6;
            }

            $index = 0;
            foreach ($res as $key => $val) {
                // $cnt starts from 1. So index is $cnt-1
                if ($index >= $cnt-12 && $index <= $cnt-1) {
                    $res[$key]['visible'] = true;
                    $days[$key]['visible'] = true;
                }
                else {
                    $res[$key]['visible'] = false;
                    $days[$key]['visible'] = false;
                }

                $index++;
            }
        }

        $weightData = $this->getWeightData();
        $result = array(
            'current_weight' => $weightData['weight'],
            'last_date' => $weightData['date'],
            'weight_diff' => $this->user->getWeightGoal() - $weightData['weight'],
            'selected_date' => date('d.m', strtotime($this->selectedDate)),
            'from_date' => $this->fromDate,
            'to_date' => $this->toDate,
            'calories_data' => $res,
            'no_data_calories_val' => round($this->optimalCalories / 14, 4),
            'diagram_container_height' => round($maxCaloriesVal / 14, 4) + 40,
            'days' => $days,
            'optimal_calories' => $this->optimalCalories,
            'calories_range' => $this->getCaloriesRange(ceil($maxCaloriesVal/500)),
            'selected_menu' => $selectedDayMenu ? array($selectedDayMenu) : $allMenu,
            'show_date_delimiter' => $selectedDayMenu ? false : true,
            'selected_calories' => $selectedDayCalories,
            'show_prev_link' => $count >= 12,
            'show_next_link' => $cnt < $count,
            'current_date' => $this->selectedDate,
        );

        return $result;
    }

    protected function getWeightData()
    {
        $currentWeight = $this->diaryMapper->getCurrentWeight($this->user->getId());
        $weight = $currentWeight->getData();
        $lastDate = $currentWeight->getCreated();

        if (!$weight) {
            $weight = $this->user->getWeight();
            $lastDate = $this->user->getCreated();
        }

        return array('weight' => $weight, 'date' => $lastDate);
    }

    protected function getCaloriesRange($maxPoints = 7)
    {
        //$diff = floor($this->optimalCalories / 500) + 1;
        //$caloriesStart = max($diff - 5, 0) + 0;

        $caloriesRange = array();
        $lastFound = false;

        // Fill calories range
        for ($x = $maxPoints; $x > 0; $x--) {
            $last = ($this->optimalCalories >= $x * 500 && $this->optimalCalories < ($x + 1) * 500);
            if (!$lastFound) {
                $lastFound = $last;

                if ($lastFound) {
                    // Norm line
                    $caloriesRange[] = array(
                        'val' => $this->optimalCalories,
                        'css_val' => round($this->optimalCalories / 14 - 36, 4),
                        'norm' => true,
                    );
                }
            }

            $val = ($lastFound && $this->optimalCalories - $x * 500 >= 100) ? $x * 500 : '';
            $caloriesRange[] = array(
                'val' => $val,
                'norm' => false,
                'last' => $last,
            );
        }

        return $caloriesRange;
    }
}