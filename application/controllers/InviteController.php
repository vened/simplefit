<?php
/**
 * @author: mix
 * @date: 14.08.13
 */

class InviteController extends Controller_Base
{
    protected $needAuth = false;


    private function getObject(){
        $i = new InviteMapper();
        $obj = $i->find($this->request()->get("id"));
        if($obj && $obj->getProperty("email") == $this->request()->get("email")){
            return $obj;
        }
        return null;
    }

    public function hitAction(){

        $this->updateTime("hit");

        if($obj = $this->getObject()){
            return array(
                "email" => $obj->getProperty("email"),
                "id" => $obj->getId(),
                "name" => $obj->getProperty("name"),
            );
        }
        header("Location: /");
        exit();

    }


    public function registerAction(){
        if($obj = $this->getObject()){
            $mapper = new UserMapper();
            $user = $mapper->findByEmail($obj->getProperty("email"));

            if($user->isEmpty()){
                Helper_Register::Register($user, array(
                    'first_name' => $obj->getProperty("name"),
                    'email' => $obj->getProperty("email"),
                    'password' => $this->request()->get("password"),
                    'recommendation_count' => 0,
                    'email_confirmed' => 1,
                    "gender" => $obj->getProperty("gender"),
                ));
            }else{
                Auth::authByUser($user);
            }
        }
        header("Location: /");
        exit();
    }

    public function unsubscribeAction(){
        $this->updateTime("unsubscribe");
    }


    public function a_jpgAction(){
        $this->updateTime("view");
        header("Content-type: image/jpeg");
        echo file_get_contents(IMAGES_ROOT . "a.jpg");
        exit();
    }

    private function updateTime($name){
        if($obj = $this->getObject()){
            $i = new InviteMapper();
            $obj->setProperty($name, date("Y-m-d H:I:s"));
            $i->save($obj);
        }
    }




}