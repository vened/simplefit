<?php

class IndexController extends Controller_Base
{
    const SIG_SESSION_KEY = "password_recovery_sig";

    protected $needAuth = false;

    private function navigateToProfile(){
        throw new RedirectException(Navigation::get('profile'));
    }

    public function indexAction()
    {

        if ($this->getUser()) {
            $this->navigateToProfile();
        }

        $wh = new Helper_WeightLoss();
        $total = round($wh->getTotal());
        $numbers = array();
        while($total > 10){
            array_unshift($numbers, $total % 10);
            $total = floor($total/10);
        }
        array_unshift($numbers, $total);
        $shift = count($numbers) % 3;
        $numbersOut = array();
        $i = 3 - $shift;
        $tri = array();
        foreach($numbers as $num){

            $tri[] = $num;
            $i++;
            if($i == 3){
                $numbersOut[] = $tri;
                $i = 0;
                $tri = array();
            }

        }
        if($tri){
            $numbersOut[] = $tri;
        }
        $result = array("total_loss" => $numbersOut);

        return $result;
    }


}