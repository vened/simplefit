<?php
/**
 * @author: mix
 * @date: 25.03.13
 */

class EmailController extends Controller_Base {

    public function selfRender($action){
        switch($action){
            case "confirmEmail" : return true;
            case "weightNotify" : return true;
            case "remind_password" : return true;
            case "invite" : return true;
            default : return false;
        }
    }


    public function confirmEmailAction(){

        $mail = new Email_Confirm($this->user,true);
        $out = $mail->renderBody();

        return $out;
    }

    public function remind_passwordAction(){

        $mail = new Email_RemindPassword($this->user,true);
        $out = $mail->renderBody();

        return $out;
    }

    public function weightNotifyAction(){

        $mail = new Email_WeightNotify($this->user,true);
        $out = $mail->renderBody();

        return $out;
    }

    public function inviteAction(){
        $i = new InviteMapper();
        $mail = new Email_Invite($i->find(202));
        $out = $mail->renderBody();
        return $out;
    }


}