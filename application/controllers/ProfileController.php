<?php

class ProfileController extends Controller_Base
{
    const ITEMS_PER_PAGE = 1600;

    public function __construct()
    {
        if (strpos($_SERVER['REQUEST_URI'], Navigation::get('settings')) !== false) {
            $this->needProfileData = false;
        }

        parent::__construct();
    }

    public function indexAction()
    {
        $weightMapper = new WeightMapper();
        $optimalWeight = $weightMapper->findByHeight($this->user->getHeight(), $this->user->getGender());
        $userWeight = $this->user->getWeight();

        $weightText = 'normal';
        if ($userWeight < $optimalWeight->getMinWeight()) {
            $weightText = 'low';
        }
        elseif ($userWeight > $optimalWeight->getMaxWeight()) {
            $weightText = 'excess';
        }

        $calories = $this->user->getCalories();

        $diaryMapper = new DiaryMapper($this->user->getPrefix());
        $data = $diaryMapper->findByUserId($this->user->getId(), self::ITEMS_PER_PAGE);
        $allCount = $diaryMapper->calcFoundRows();
        $lastData = array();

        if ($data) {
            $date = $data[count($data) - 1]->getCreated();
            $lastData = $diaryMapper->findByDate($this->user->getId(), $date);
        }

        $currentWeight = $diaryMapper->getCurrentWeight($this->user->getId());
        $weight = $currentWeight->getData();
        if (!$weight) {
            $weight = $this->user->getWeight();
        }

        $result = array(
            'user_at_site' => Helper_User::diffDate($this->user->getProperty('created')),
            'optimal_weight_min' => $optimalWeight->getMinWeight(),
            'optimal_weight_max' => $optimalWeight->getMaxWeight(),
            'weight_text' => $weightText,
            'current_weight' => $weight,
            'needed_weight_diff' => $this->user->getWeightGoal() - $weight,
            'calories' => $calories,
            'today' => date('d.m.Y'),
            'data' => array(
                'left' => array(),
                'right' => array(),
            ),
        );

        if ($this->user->getMainPhotoId()) {
            $photo = new Photo(array(
                'photo_id' => $this->user->getMainPhotoId(),
                'user_id' => $this->user->getId(),
            ));

            $result['main_photo_url'] = Helper_Photo::getMainPhotoUrl($photo, Helper_Photo::SIZE_329);
        }

        $photoMapper = new PhotoMapper($this->user->getPrefix());
        $data = array_merge($data, $lastData);
        $count = 0;
        $result['last_photo'] = array();

        foreach ($data as $key => $val) {
            $side = $key % 2 ? 'left' : 'right';
            $data = $val->toArray();
            $date = substr($val->getCreated(), 0, 10);

            //if (isset($result['diary_data'][$date]['left'][$val->getId()])) {
            if (isset($result['diary_data'][0]['left'][$val->getId()])) {
                continue;
            }
            //elseif (isset($result['diary_data'][$date]['right'][$val->getId()])) {
            elseif (isset($result['diary_data'][0]['right'][$val->getId()])) {
                continue;
            }

            if ($data['type'] == 'photo') {
                $photo = $photoMapper->find($val->getData());
                $photoUrl = Helper_Photo::getDiaryPhotoUrl($photo);
                $data['photo_url'] = $photoUrl;
                $data['photo_descr'] = $photo->getDescription();

                // Last photo is the first in array, because sorted by created DESC.
                if (!$result['last_photo']) {
                    $result['last_photo'] = array(
                        'photo_url' => $photoUrl,
                        'photo_descr' => $photo->getDescription(),
                    );
                }
            }
            elseif ($data['type'] == 'calories') {
                $data['products_data'] = Helper_Diary::unserializeProductsData($data['products_data']);
            }

            //$result['diary_data'][$date][$side][$val->getId()] = $data;
            $result['diary_data'][0][$side][$val->getId()] = $data;
            $count++;
        }

        $result['show_next_page'] = $allCount > $count;

        $startWeight = $this->user->getWeight();
        $result['weight_diff_all'] = round($weight - $startWeight, 1);
        $result['weight_diff_week'] = $result['weight_diff_all'];
        $result['weight_diff_month'] = $result['weight_diff_all'];
        $result['weight_diff_3months'] = $result['weight_diff_all'];
        $result['selected_product'] = $this->getSelectedProduct();
        $weightData = $diaryMapper->getAllWeightData($this->user->getId());

        // Week ago
        $time = time() - 86400*7;
        $diffName = 'week';
        foreach ($weightData as $weight) {
            if (strtotime($weight->getCreatedDate()) <= $time) {
                $result['weight_diff_' . $diffName] = round($weight->getData() - $startWeight, 1);

                if ($diffName == 'week') {
                    // Month ago
                    $time = time() - 86400 * 30;
                    $diffName = 'month';
                }
                elseif ($diffName == 'month') {
                    // 3 months ago
                    $time = time() - 86400 * 90;
                    $diffName = '3months';
                }
                else {
                    break;
                }
            }
        }

        return $result;
    }

    protected function getSelectedProduct()
    {
        $productData = Request::get('product');

        if (!$productData) {
            return array();
        }

        list($productId, $weight) = explode('_', $productData);
        $result = array();

        $productsMapper = new ProductsMapper();
        $product = $productsMapper->find((int) $productId);

        if (!$product->isEmpty()) {
            $result = $product->toArray();
            $result['weight'] = $weight;
            $result['weight_calories'] = round($result['calories'] / 100 * $weight);
        }

        return $result;
    }

    public function settingsAction()
    {
        $section = Request::get('sec');
        $result = array();

        if ($section == 'profile') {
            $result = $this->getProfileSettings();
        }
        else {
            $section = 'main';
        }

        $result['section'] = $section;

        return $result;
    }

    protected function getProfileSettings()
    {
        $userBirthday = explode('-', $this->user->getProperty('birthdate'));
        $mainPhotoUrl = '';
        $mainPhotoId = $this->user->getMainPhotoId();

        if ($mainPhotoId) {
            $photo = new Photo(array(
                'photo_id' => $mainPhotoId,
                'user_id' => $this->user->getId()
            ));

            $mainPhotoUrl = Helper_Photo::getMainPhotoUrl($photo, Helper_Photo::SIZE_140);
        }

        return array(
            'main_photo_url' => $mainPhotoUrl,
            'birthday' => array(
                'day' => $userBirthday[2],
                'month' => $userBirthday[1],
                'year' => $userBirthday[0],
                'from_year' => date('Y') - 70,
                'to_year' => date('Y') - 18,
            ),
        );
    }
}