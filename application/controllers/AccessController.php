<?php
/**
 * @author: mix
 * @date: 16.06.13
 */

class AccessController extends Controller_Base
{
    protected $needAuth = false;

    public function indexAction()
    {
        $action = Request::get("action");
        if ($action) {
            $sig = Request::getPost("sig");
            $password = Request::getPost("password");
            if ($sig && Session::get(IndexController::SIG_SESSION_KEY) == $sig) {
                $this->user->setPassword($password);
                $m = new UserMapper();
                $m->save($this->user);
                $this->navigateToProfile();
            }
            return array();
        } else {
            $uid = (int)Request::get("uid");
            $token = Request::get("code");
            $o = Auth::loginByToken($uid, $token);
            $return = array();
            if ($o) {
                $this->user = Auth::getUser();
                $return["template"] = $o->getProperty("template");

                $unsub = Request::get("unsubscribe");
                if($unsub == "on"){
                    throw new RedirectException(Navigation::get('settings'));
                }
                switch ($o->getProperty("template")) {
                    case "confirmEmail" :
                        $this->user->setEmailConfirmed();
                        $m = new UserMapper();
                        $m->save($this->user);
                        $this->navigateToProfile();
                        break;
                    case "remindPassword" :
                        $sig = mt_rand(0, 1000000000);
                        $return["sig"] = $sig;

                        Session::set(IndexController::SIG_SESSION_KEY, $sig);
                        break;
                    case "weightNotify" :
                        $this->navigateToProfile();
                        break;
                    default :
                        error_log($o->getProperty("template"));
                }
            }
            return $return;
        }
    }

    private function navigateToProfile(){
        throw new RedirectException(Navigation::get('profile'));
    }

    public function viewEmailAction(){
        $m = new Admin_MailStatMapper();

        $o = $m->findByToken($this->request()->get("uid"), $this->request()->get("code"));

        if($o && !$o->getProperty("viewed")){
            $o->setProperty("viewed", date("Y-m-d H:i:s"));
            $m->save($o);
        }
        header("Content-type: image/jpeg");
        echo file_get_contents(IMAGES_ROOT . "a.jpg");
        exit();
    }

    public function a_jpgAction(){
        $this->viewEmailAction();
    }

}