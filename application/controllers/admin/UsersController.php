<?php
/**
 * @author: mix
 * @date: 05.03.13
 */

class admin_UsersController extends Controller_AdminBase{

    const LIMIT = 100;

    public function indexAction(){
        $m = new UserMapper();

        if(isset($_REQUEST["orderby"]) && in_array($_REQUEST["orderby"],array("u.created","dw_cnt","dc_cnt","p_cnt","d_cnt"))){
            $orderBy = $_REQUEST["orderby"];
        }else{
            $orderBy="dc_cnt";
        }

        if(isset($_REQUEST["page"])){
            $page =  (int)$_REQUEST["page"];
        }else{
            $page = 1;
        }

        $res = $m->getStatList($page-1,self::LIMIT,$orderBy);
        $res["page"] = $page;

        if($page >= self::LIMIT){
            $res["pageprev"] = $page - self::LIMIT;
        }

        if($page + self::LIMIT < $res["total"]){
            $res["pagenext"] = $page + self::LIMIT;
        }

        $res["orderby"] = $orderBy;

        return $res;
    }

    public function statsAction(){


        $m = new UserStatsMapper();
        if(isset($_GET["page"]))$page = (int)Request::get("page") - 1;
        else $page=0;
        $ret = $m->fetchStat($page);



        if($page > 0)$ret["pageprev"] = $page;

        if($page < floor($ret["total"]/self::LIMIT + 1))$ret["pagenext"] = $page + 2 ;

        return $ret;
    }
}