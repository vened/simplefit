<?php

class admin_IndexController extends Controller_AdminBase
{
    protected $needAuth = false;

    public function indexAction()
    {
        $result = array();
        return $result;
    }



    public function loginAction(){
        $m = new Admin_AdminMapper();
        if($this->request()->isPost()){
            $admin = $m->login($_POST["login"], $_POST["password"]);
            if($admin){
                $_SESSION["admin_id"] = $admin->getId();
                throw new RedirectException("/");
            }else{
                return array("error" => "wrong password");
            }
        }
        return array();
    }

    public function logoutAction(){
        $_SESSION["admin_id"] = 0;
        throw new RedirectException("/");
    }

    public function adminAction(){
        $m = new Admin_AdminMapper();
        if($this->request()->isPost()){
            $admin = new Admin_Admin();
            $admin->setProperty("name", $_POST["name"]);
            $admin->setProperty("login", $_POST["login"]);
            $m->saveAdmin($admin,$_POST["password"]);
        }

        return array("admins" => $m->getList());
    }

}
