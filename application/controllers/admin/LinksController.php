<?php
/**
 * @author: mix
 * @date: 27.01.13
 */
class admin_LinksController extends Controller_AdminBase {

    public function manageAction(){
        $m = new Admin_LinkMapper();
        if($this->request()->isPost()){
            $l = new Admin_Link();
            $l->setProperty("url", $_POST["url"]);
            $l->setProperty("description", $_POST["description"]);
            $l->setProperty("created", date("Y-m-d H:i:s"));
            $m->save($l);
        }
        $linksRes = $m->findAll();

        $links = $linksRes->fetchAll();
        foreach($links as &$link){
            $link["url"] .= ((strpos($link["url"],"?")?"&":"?") . "link_id=$link[id]");
        }
        return array("links" => $links,array("total" => $linksRes->calcFoundRows()));
    }

    public function statsAction(){
        $m = new Admin_LinkMapper();

        $date_from = (isset($_GET["date_from"])?$_GET["date_from"]:date("Y-m-d"));
        $date_to = (isset($_GET["date_to"])?$_GET["date_to"]:date("Y-m-d"));
        $orderby = (isset($_GET["orderby"])?$_GET["orderby"]:"hits");


        return array(
            "links" => $m->getStat($date_from,$date_to,$orderby)->fetchAll(),
            "total" => $m->getTotal($date_from,$date_to),
            "date_from" => $date_from,
            "date_to" => $date_to,
            "orderby" =>$orderby
        );
    }

    public function editAction(){
        $m = new Admin_LinkMapper();
        $link = $m->find($_GET["id"]);
        if(!$link){
            throw new RedirectException($_POST["back"]);
        }

        if($this->request()->isPost()){
            $link->setProperty("url", $_POST["url"]);
            $link->setProperty("description", $_POST["description"]);
            $m->save($link);
            throw new RedirectException($_POST["back"]);
        }


        return array("back" => $_SERVER["HTTP_REFERER"], "description" => $link->getProperty("description"), "url" => $link->getProperty("url"));
    }
}