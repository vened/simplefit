<?php
/**
 * @author: vadim
 * @date: 28.03.13
 */
if(!class_exists("SphinxClient")){
    include_once 'packages/sphinxapi.php';
}
class admin_DiariesController extends Controller_AdminBase
{
    const COUNT_PER_PAGE = 10;
    const PAGER_LENGTH = 17;

    public function userAction()
    {
        $page = $this->request()->get("page", 1);
        $type = $this->request()->get("type", "New");
        $offset = ($page - 1) * self::COUNT_PER_PAGE;

        $staffUserDiaryMapper = new \Staff\UserDiaryMapper();
        $userIds = $staffUserDiaryMapper->getUserIds($type, self::COUNT_PER_PAGE, $offset)->fetchColumn();
        $total = $staffUserDiaryMapper->calcFoundRows();
        $result = array();

        foreach ($userIds as $userId) {
            $diaries = $staffUserDiaryMapper->findByUserId($userId, $type);

            foreach ($diaries as $diary) {
                $data = $diary->toArray();
                $data['products_data'] = Helper_Diary::unserializeProductsData($data['products_data']);
                $data['activity'] = Helper_User::getActivityText($data['activity']);
                $data['vegetarianism'] = Helper_User::getVegetarianism($data['vegetarianism']);
                $data['food_type'] = Helper_Diary::getFoodTypeText($data['food_type']);
                $result[$userId][$diary->getCreatedDate()][] = $data;
            }
        }

        $total_pages = ceil($total/self::COUNT_PER_PAGE);

        $pp = array();
        $start = $page - 9;
        if ($start < 0) $start = 0;
        $end = $start + self::PAGER_LENGTH;
        if ($end > $total_pages) $end = $total_pages;
        $start = $end - self::PAGER_LENGTH;
        if ($start < 0) $start = 0;
        for ($p = $start; $p < $end; $p++) {
            $pp[] = $p;
        }

        return array(
            "data" => $result,
            "total" => $total,
            "current_page" => $page,
            "pager" => $pp,
            "total_pages" => $total_pages,
            "type" => $type
        );
    }

    public function moderateAction()
    {
        $id = $this->request()->get('id', 0);
        $status = null;

        switch ($this->request()->get('action')) {
            case 'approve':
                $status = 'Approved';
                break;

            case 'reject':
                $status = 'Rejected';
                break;
        }

        if ($status) {
            $staffUserDiaryMapper = new \Staff\UserDiaryMapper();
            $diary = $staffUserDiaryMapper->find($id);

            if (!$diary->isEmpty()) {
                $diary->setStatus($status);
                $staffUserDiaryMapper->save($diary);
            }
        }

        echo 'OK';
        exit;
    }


}