<?php
/**
 * @author: mix
 * @date: 16.06.13
 */

class admin_PlanController extends Controller_AdminBase{

    CONST ITEM_PER_PAGE = 20;

    private $params = array(
        "food_type",
        "gender",
        "vegetarianism",
        "activity",
    );

    private $statuses = array(
        "New" => "Новые",
        "nope" => "Все",
        "Approved" => "Одобренные",
        "Rejected" => "Отклоненные",
    );


    private function getParam($name){
        if(in_array($name,$this->params)){
            return $this->request()->get($name,"nope");
        }elseif($name == "status"){
            return $this->request()->get($name,"New");
        }elseif($name == "stype"){
            return $this->currentAction;
        }else{
            return $this->request()->get($name,0);
        }

    }

    private function getParams(){
        $out = array();
        foreach($this->params as $param){
            $out[$param] = $this->getParam($param);
        }

        $out["page"] = $this->getParam("page");
        $out["status"] = $this->getParam("status");
        $out["stype"] = $this->getParam("stype");
        return $out;
    }

    private function buildLink($params){
        $l = array();
        foreach($params as $name => $value){
            $l[] = "$name=$value";
        }
        return implode("&", $l);
    }


    private function createBlock($type,$name,$array){

        $m = new \Staff\UserDiaryMapper();

        $list = array();
        $current = $this->getParam($type);
        $cparams = $this->getParams();
        $cparams[$type] = "nope";
        $p  = array (
            "name" => "Все",
            "v" => "nope",
            "link" => $this->buildLink($cparams),
            "total" => $m->getTotalByParams($cparams),
        );
        $p["v"] = "nope";

        if($current == "nope"){
            $p["active"] = 1;
        }
        $list[]=$p;

        foreach($array as $item){

            $cparams[$type] = $item["v"];

            $p = array(
                "name" => $item["text"],
                "v" => $item["v"],
                "link" => $this->buildLink($cparams),
                "total" => $m->getTotalByParams($cparams),
            );
            if($current == $item["v"] && $current !== "nope"){
                $p["active"] = 1;
            }
            $list[] = $p;
        }

        return array(
            "name" => $name,
            "list" => $list,
        );
    }

    private function getList(){
        $m = new \Staff\UserDiaryMapper();

        $ret = $m->findByParams($this->getParams(),self::ITEM_PER_PAGE);
        $total = $ret["total"];
        $pages  = ceil($total/self::ITEM_PER_PAGE);

        $g = array(
            array("text" => "Ж", "v" => "F"),
            array("text" => "М", "v" => "M"),
        );

        $ret["selects"][] = $this->createBlock("food_type","Время", Helper_User::getFoodList());
        $ret["selects"][] = $this->createBlock("gender","Пол", $g);
        $ret["selects"][] = $this->createBlock("vegetarianism","Вегетарианство", Helper_User::vegetarianismList());
        $ret["selects"][] = $this->createBlock("activity","Активность", Helper_User::activityList());

        foreach($this->statuses as $status => $name){
            $r = array("name" => $name, "link" => $status);
            if($this->getParam("status") == $status){
                $r["active"] = 1;
            }
            $r["total"] = $m->getTotalByParams(array("status" => $status, "stype" => $this->getParam("stype")));
            $ret["statuses"][] = $r;
        }


        $params = $this->getParams();
        $params["page"]--;
        $ret["pre_link"] = $this->buildLink($params);

        $params = $this->getParams();
        $params["page"]++;
        $ret["next_link"] = $this->buildLink($params);

        $ret["page"] = $this->getParam("page");
        $ret["max_page"] = $pages -1;

        $ret["params"] = $this->buildLink($this->getParams());
        for($i = 0; $i < $pages; $i++){
            $ret["paginator"][] = array("page" => $i);
        }
        return $ret;

    }

    private function getSelects(){
        return array(
            'activity' => Helper_User::activityList(),
            'veg' => Helper_User::vegetarianismList(),
            'food_type' => Helper_User::getFoodList(),
        );
    }

    public function systemAction(){
        if($this->request()->isPost()){
            $m = new \Staff\UserDiaryMapper();
            $o = new \Staff\UserDiary();
            $o->setProperties($this->request()->getPostData());
            $m->save($o);
        }

        return array_merge(
            $this->getSelects(),
            $this->getlist(),
            array("action" => "system")
        );
    }


    public function addAction(){
        if(Request::isPost()){
            $m = new \Staff\UserDiaryMapper();
            $o = new \Staff\UserDiary();
            $o->setProperties($_POST);
            $id = $m->save($o);
            throw new RedirectException("/diaries/edit?id=$id");
        }

        throw new RedirectException("/plan/system");
    }

    public function editAction(){
        $m = new \Staff\UserDiaryMapper();
        $o = $m->find($_REQUEST["id"]);
        if($this->request()->isPost()){
            $o->setProperties($this->request()->getPostData());
            $m->save($o);
        }

        $ret = array(
            "item" => $o->toArray(),
            "food" => json_decode($o->getProperty("products_data")),
        );

        return array_merge($ret,$this->getSelects());
    }

    public function searchAction()
    {
        $q = trim(Request::get('q'));

        $sphinx = new SphinxClient();
        $sphinx->SetServer('127.0.0.1', 9312);
        $sphinx->SetMatchMode(SPH_MATCH_ANY);
        $sphinx->SetSortMode(SPH_SORT_RELEVANCE);
        $sphinx->SetLimits(0, 40);

        $sphinx->SetFilter('is_available', array('1'));
        $sphinx->AddQuery($q, '*');
        $sphinx->ResetFilters();

        if ($this->user) {
            $sphinx->SetFilter('is_available', array('0'));
            $sphinx->SetFilter('user_id', array($this->user->getId()));
            $sphinx->AddQuery($q, '*');
        }

        $results = $sphinx->RunQueries();
        $return = array();

        if ($results) {
            foreach ($results as $result) {
                if ($result['total_found'] && !empty($result['matches'])) {
                    $productsMapper = new ProductsMapper();
                    $products = $productsMapper->findByIds(array_keys($result['matches']));

                    foreach ($products as $product) {
                        $return[] = $product->toArray();
                    }
                }
            }
        }

        header("Content-type: text/json");
        echo json_encode($return);
        exit();
    }

    public function addFoodAction(){
        $m = new \Staff\UserDiaryMapper();

        $id = $this->request()->get("rec_id");
        $o = $m->find($id);
        $pm = new ProductsMapper();
        $product = $pm->find($this->request()->get("id"));
        $o->addProduct($product,$this->request()->get("weight"));
        $m->save($o);
        throw new RedirectException("/plan/edit?id=$id");
    }

    public function delAction(){
        $m = new \Staff\UserDiaryMapper();

        $id = $this->request()->get("id");
        $o = $m->find($id);

        $o->delProduct($this->request()->get("product_id"));
        $m->save($o);
        throw new RedirectException("/plan/edit?id=$id");
    }

    public function newAction(){
        return array("action"=>"new");
    }

    public function usersAction(){

        $m = new \Staff\UserDiaryMapper();
        return array_merge(
            $this->getlist(false),
            array("action"=>"users"));
    }

    public function getProductDataAction(){
        $m = new \Staff\UserDiaryMapper();
        $o = $m->find($this->request()->get("id"));
        $p = $o->getProducts();

        $data = $this->
            app()->
            getTemplateEngine()->
            loadTemplate("admin/plan/common/productlist.tpl")->
            render(
                array_merge(
                    $this->app()->
                        getTemplateGlobals(),array("food"=>$p)
                )
            );

        $reurn = array("html" => $data);
        header("Content-type: text/json");
        echo json_encode($reurn);
        exit();

    }


    public function approveAction(){
        $m = new \Staff\UserDiaryMapper();
        $o = $m->find($this->request()->get("id"));
        if($o){
            $o->setStatus("Approved");
            $m->save($o);
        }
        throw new RedirectException($this->request()->getReferer());

    }

    public function rejectAction(){
        $m = new \Staff\UserDiaryMapper();
        $o = $m->find($this->request()->get("id"));
        if($o){
            $o->setStatus("Rejected");
            $m->save($o);
        }
        throw new RedirectException($this->request()->getReferer());


    }

    public function payAction(){
        $m = new \Staff\UserDiaryMapper();
        $o = $m->find($this->request()->get("id"));
        if($o){
            $o->setUserId(0);
            $m->save($o);
        }
        throw new RedirectException($this->request()->getReferer());
    }





}