<?php
/**
 * @author: mix
 * @date: 12.02.13
 */
class admin_GoodsController extends Controller_AdminBase{

    const COUNT_PER_PAGE = 30;
    const PAGER_LENGTH = 17;

    public function indexAction(){


        $page = $this->request()->get("page",0);
        $q = $this->request()->get("q", "");
        $type = $this->request()->get("type", "all");

        $m = new ProductsMapper();

        $where =array();
        if($q){
            $where[] = "name like '%$q%'";
        }

        switch($type){
            case "new" : $where[] = "moderated=0"; break;
            case "approved" : $where[] = "moderated=1 AND is_available=1"; break;
            case "rejected" : $where[] = "moderated=1 AND is_available=0"; break;
        }

        $data = $m->findUserProducts($where, $page * self::COUNT_PER_PAGE, self::COUNT_PER_PAGE );
        $total = $m->findUserProductsTotal();

        $total_pages = ceil($total/self::COUNT_PER_PAGE);

        $pp = array();
        $start = $page -9;
        if($start < 0)$start = 0;
        $end = $start + self::PAGER_LENGTH;
        if($end > $total_pages)$end = $total_pages;
        $start = $end - self::PAGER_LENGTH;
        if($start < 0)$start = 0;
        for($p = $start; $p < $end; $p++){
            $pp[] = $p;
        }

        return array("data" => $data, "total" => $total, "current_page" => $page, "pager" => $pp, "q" => $q, "total_pages" => $total_pages, "type" => $type);
    }

    public function editAction(){

        $return = $_POST["return"];
        $m = new ProductsMapper();
        foreach($_POST["data"] as $id => $data){

            $item = $m->find($id);

            $moder =  $data["moderation"];
            unset($data["moderation"]);
            switch($moder){
                case "approved" :
                    $data["is_available"] = 1;
                    $data["moderated"] = 1;
                    break;
                case "rejected" :
                    $data["is_available"] = 0;
                    $data["moderated"] = 1;
                    break;
            }

            $item->setProperties($data);
            error_log("saving " . $item->getId());
            $m->save($item);
        }

        $loc = "http://".$_SERVER["HTTP_HOST"]."/goods?$return";
        throw new RedirectException($loc);

    }
}