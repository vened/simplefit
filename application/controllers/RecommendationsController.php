<?php
class RecommendationsController extends Controller_Base {

    protected $needAuth = false;

    public function indexAction(){
        $out = array();
/*
        $rq = $this->request();
        if($rq->has("day_to")){
            $day = $rq->get("day_to");
            $month = $rq->get("month_to");
            $m = new PlanMapper($this->user->getPrefix());
            $planExists =$m->getPlanFromDate($this->user->getId(),date("Y-$month-$day"));
            if(mktime(0,0,0, $month, $day, date("Y")) < ((int)date("U") - 3600*24)){
                $out["error_prev"] = 1;
            }elseif($planExists && !$rq->has("force")){
                $out["plan_exists"] = 1;
                $out["day_to"] = $day;
                $out["month_to"] = $month;

            }else{
                $f = new Recommendation_Factory($this->user);
                $m = new PlanMapper($this->user->getPrefix());
                $date = date("Y")."-$month-$day";
                $m->saveRecommendations(
                    $this->user->getId(),$f->getUserRecommendation("triple"),
                    $date
                );
                throw new RedirectException("/plan?date=".$date);
            }
        }

        $f = new Recommendation_Factory($this->user);


        $total = 0;
        foreach( $f->getUserRecommendation("triple") as $food){

            $out["recommendations"][] = array(
                "type" => $food->getType(),
                "products" => $food->getProducts(),
                "total" => $food->getCalories()
            );
            $total += $food->getCalories();

        }
        $out["total"] = $total;
*/
        return $out;
    }

    public function previewAction(){
        if($this->user->getRecommendationCount() < 1){
            throw new RedirectException("/recommendations");
        }

        return array(
            "json_data" => json_encode($this->getPlan($this->getUser(),true)),
            "date" => $this->request()->get("date")
        );
    }

    public function saveSystemAction(){

        $f = new Recommendation_Factory($this->user);
        $m = new PlanMapper($this->user->getPrefix());
        $m->saveRecommendations(
            $this->user->getId(),$f->getSystemRecommendation("triple"),
            $this->request()->get("date")
        );
        $this->user->setProperty(
            "recommendation_count",
            $this->user->getProperty("recommendation_count") -1
        );

        $m = new UserMapper();
        $m->save($this->user);
        throw new RedirectException("/plan?date=".$this->request()->get("date"));
    }

    private function getPlan(\User $user,$system){

        $f = new Recommendation_Factory($user);

        if($system){
            $items = $f->getSystemRecommendation("triple");
        }else{
            $items = $f->getUserRecommendation("triple");
        }

        $out = array();
        foreach($items as $item){
            $out[] = $item->toArray();
        }
        return $out;
    }
}
