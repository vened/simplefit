<?php

class ConfirmController extends Controller_Base
{
    protected $needAuth = false;

    public function indexAction()
    {
        $confirmCode = trim(Request::get('code'));

        $mcache = MCache::getInstance();

        if ($data = $mcache->get('confirm_' . $confirmCode)) {
            $userMapper = new UserMapper();
            $user = $userMapper->find($data['user_id']);

            switch ($data['type']) {
                case 'email':
                    $user->setEmailConfirmed();
                    $userMapper->save($user);
                    break;

                case 'vk':
                    $user->setVkId($data['vk_id']);
                    $user->setLastLogin();
                    $user->setSecret();
                    $user->setEmailConfirmed();
                    $userMapper->save($user);
                    break;
            }

            Auth::authByUser($user);
            throw new RedirectException(Navigation::get('profile') . '?email_confirmed=1');
        }

        /**
         * Wrong confirm code.
         */
    }
}