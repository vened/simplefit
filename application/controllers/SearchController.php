<?php

include_once 'packages/sphinxapi.php';

class SearchController extends Controller_Base
{
    const PRODUCTS_PER_PAGE = 10;
    const MAX_PAGES = 5;

    protected $needAuth = false;
    protected $productsMapper;

    public function __construct()
    {
        parent::__construct();

        $this->productsMapper = new ProductsMapper();
    }

    public function indexAction()
    {
        $query = trim(Request::get('q'));
        $page = (int) Request::get('p');
        if ($page >= self::MAX_PAGES) {
            $page = self::MAX_PAGES - 1;
        }

        $offset = $page * self::PRODUCTS_PER_PAGE;

        $result = array(
            'query' => htmlspecialchars($query),
            'products' => array(),
        );

        if ($query) {
            $products = $this->findProducts($query, $offset);
            $result['count'] = $products['count'];
            if ($products['count']) {
                $result['products'] = $products['products'];
                $result['search_enabled'] = 1;
            }
        }
        else {
            $products = $this->getLastProducts($offset);
            $result['products'] = $products['products'];
            $result['count'] = $products['count'];
        }

        $result['show_next_link'] = $page < self::MAX_PAGES - 1 && $result['count'] > ($page + 1) * self::PRODUCTS_PER_PAGE;
        $productsMapper = new ProductsMapper();
        $result['product_count'] = floor($productsMapper->getTotal()/100)*100;
        return $result;
    }

    protected function getLastProducts($offset = 0)
    {
        $userId = $this->user ? $this->user->getId() : null;
        $products = $this->productsMapper->getLastProducts($userId, self::PRODUCTS_PER_PAGE, $offset);

        return array('products' => $products, 'count' => self::PRODUCTS_PER_PAGE * self::MAX_PAGES);
    }

    protected function findProducts($query, $offset = 0)
    {
        $sphinx = new SphinxClient();
        $sphinx->SetServer('127.0.0.1', 9312);
        $sphinx->SetMatchMode(SPH_MATCH_EXTENDED2);
        $sphinx->SetRankingMode(SPH_RANK_SPH04);
        $sphinx->SetSortMode(SPH_SORT_RELEVANCE);
        $sphinx->SetLimits($offset, self::PRODUCTS_PER_PAGE);

        $sphinx->SetFilter('is_available', array('1'));
        $sphinx->AddQuery($query, '*');
        $sphinx->ResetFilters();

        if ($this->user) {
            $sphinx->SetFilter('is_available', array('0'));
            $sphinx->SetFilter('user_id', array($this->user->getId()));
            $sphinx->AddQuery($query, '*');
        }

        $results = $sphinx->RunQueries();
        $totalFound = 0;
        $return = array();
        $count = 0;

        if ($results) {
            foreach ($results as $result) {
                if ($result['total_found'] && !empty($result['matches'])) {
                    $totalFound += $result['total_found'];
                    $productsMapper = new ProductsMapper();
                    $products = $productsMapper->findByIds(array_keys($result['matches']), true);

                    if ($count < self::PRODUCTS_PER_PAGE) {
                        foreach ($products as $product) {
                            $return['products'][] = $product->toArray();

                            $count++;
                            if ($count >= self::PRODUCTS_PER_PAGE) {
                                break;
                            }
                        }
                    }
                }
            }
        }
        $return['count'] = $totalFound;

        return $return;
    }

    public function tableAction(){
        $m = new ProductsMapper();
        $page = @intval($_REQUEST["page"]);
        $total = $m->getTotal();


        $ret =  array(
            'product_count' => floor($total/100)*100,
            'current_page' => $page,
        );



        if($page > 8){
            $ret["fast_back"] = 1;
        }

        if($total - $page * self::PRODUCTS_PER_PAGE > 8){
            $ret["fast_forward"] = 1;
        }

        $start = $page - 4;
        if($start < 0 )$start = 0;
        $end = $start + 9;
        if($end > $total / self::PRODUCTS_PER_PAGE - 1){
            $end = ceil($total / self::PRODUCTS_PER_PAGE - 1);
            $start = $end - 9;
        }

        for($i = $start; $i<=$end; $i++){
            if($i < $page){
                $ret["pager_back"][] = $i;
            }elseif($i > $page){
                $ret["pager_front"][] = $i;
            }
        }
        $ret["products"] = $m->productList($page,self::PRODUCTS_PER_PAGE);
        return $ret;
    }
}