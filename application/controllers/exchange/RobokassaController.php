<?php

class exchange_RobokassaController extends Controller_Base
{
    protected $needAuth = false;

    public function indexAction()
    {
        $amount = $this->request()->get('OutSum');
        $transactionId = $this->request()->get('InvId');
        $sig = $this->request()->get('SignatureValue');

        $params = array(
            'OutSum' => $amount,
            'InvId' => $transactionId,
            'sig' => $sig
        );

        $payment = new \Billing\Payment();
        if (!$payment->getPaymentMethod()->checkCallbackParams($params)) {
            $this->printResult('Error. Wrong signature.');
        }

        $transaction = $payment->getTransaction($transactionId);
        if ($transaction->isEmpty() || $transaction->getAmount() != $amount) {
            $this->printResult('Error. Wrong transaction.');
        }

        if (!$payment->closeTransaction()) {
            $this->printResult('Error');
        }

        $this->printResult('OK' . $transaction->getId());
    }

    protected function printResult($result)
    {
        if ($result) {
            echo $result;
        }

        exit;
    }
}