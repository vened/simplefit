{% for product in products %}
    <li>
        <!--div class="item-image">
            <a href=""><img src="{{img_host}}/images/tmp/100x70.gif" alt=""/></a>
        </div-->

        <div class="item-description">
            <span class="vam-c" style="max-width: 294px;">
                <strong>{{product.name}}</strong>
                <div class="gray" style="display: none;">###Вкусы: клубника, яблоко-груша</div>
            </span>
            <b class="vam"></b>
        </div>

        <div class="item-actions">
            <b class="vam"></b>
            <span class="vam-c">
                <span class="yellow calories-val">{{product.calories}} ккал</span>
                <span class="fs21">&nbsp;в&nbsp;</span>
                <input id="weight-{{product.id}}" type="text" class="text small width1" name="weight" value="100" /> <b>&nbsp;гр&nbsp;</b>
                <a href="#" class="button button-gray add-menu">В дневник</a>
            </span>
        </div>
    </li>
{% endfor %}

<li class="next-page" style="display: none;">{% if show_next_link %}1{% else %}0{% endif %}</li>