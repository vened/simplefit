{% extends 'common/base.tpl' %}

{% block title %}
<title>Симплфит - Рекомендации</title>
{% endblock %}

{% block javascript %}
{% endblock %}

{% block content %}


<div class="content wrapper">

    <h1 class="mb10 txt-c">Рекомендации ({{user.recommendation_count}})</h1>

    <div class="yellow_block mb40">

        <div class="fs18" style='text-align: center'>
            <i>Здесь вы можете изменить меню на день с учётом рекомендаций Simplefit</i>
        </div>
        <p style='margin-left:100px; margin-right:100px;'>
            Вы можете заменить неподходящие приёмы пищи в рекомендуемом меню до его добавления в план. Для этого, нажмите на кнопку «Заменить». После добавления рекомендуемого меню в план вы сможете удалить или добавить собственные продукты или блюда.
        </p>

    </div>

    <div class='box_border my_diary'>
        <div class='fl-l '><h2>Предварительный просмотр рекомендации</h2></div>
        <div class='fl-r fs16'><a id="get-payment-plan" class="button button-green small" href="/recommendations/saveSystem?date={{date}}">Сохранить</a></div>
<br><br><br><br>

        <div id='breakfast' style='display:none'>
        <div class='fl-l fs18' style='margin-left:15px;'>Завтрак  <b class="yellow" id='total-breakfast'>0</b><b class="yellow"> ккал</b></div>
        <div class='fl-r fs16'><a id="change-breakfast" class="link fs16 repl" href="#" type='breakfast'>Заменить</a></div>
        <br><br>
        <ul class='product-list' id='breakfast-list'>
        </ul>
        </div>

        <div id='lunch' style='display:none' >
            <div class='fl-l fs18' style='margin-left:15px;'>Обед <b class="yellow" id='total-lunch'>0</b><b class="yellow"> ккал</b></div>
            <div class='fl-r fs16'><a id="change-lunch" class="link fs16 repl" href="#" type='lunch' >Заменить</a></div>
            <br><br>
            <ul class='product-list' id='lunch-list'>
            </ul>
        </div>

        <div id='dinner' style='display:none'>
            <div class='fl-l fs18' style='margin-left:15px;'>Ужин <b class="yellow" id='total-dinner'>0</b><b class="yellow"> ккал</b></div>
            <div class='fl-r fs16'><a id="change-dinner" class="link fs16 repl" href="#" type='dinner' >Заменить</a></div>
            <br><br>
            <ul class='product-list' id='dinner-list'>
            </ul>
        </div>


        <div class='fs20'>Итого: <b class="yellow" id='total-c'>0</b><b class="yellow"> ккал</b></div>
    </div>


</div>
<script>
    var init_data = {{json_data}}

    for(i in init_data){
        init_e(init_data[i])
    }

    function init_e(e){
        var type=e["food_type"];
        $("#"+type).css("display","block");
        $("#total-c").text(
            parseInt($("#total-c").text()) + parseInt(e["calories"])
        )
        $("#total-"+type).text(e["calories"]);
        var text ="";

        for (j in e["products_data"]){
            //alert(e["products_data"][i][3]);
            text += '<li>'+e["products_data"][j][1]+' '+e["products_data"][j][2]+' г =  <b class="yellow">'+e["products_data"][j][3]+' ккал</b></li>'
        }
        $("#"+type + "-list").html(text);

    }

    $(function(){
        $("a.repl").click(function(e){
            var type = $(e.target).attr("type");
            $.ajax({
                url: "/ws/recommendations/change?type=" + type
            }).done(function(data) {
                var type=data["food_type"];
                $("#total-c").text(
                        parseInt($("#total-c").text()) - parseInt($("#total-"+type).text())
                )
                init_e(data);
            });
            return false;
        });
    })
</script>
{% endblock %}
