{% extends 'common/base.tpl' %}

{% block title %}
<title>Симплфит - Дневник питания</title>
{% endblock %}

{% block javascript %}
{% endblock %}

{% block content %}

<style>
    .wrapper {
        width: 990px;
    }
</style>

<div class="content">
    <div class="index-first">
        <div class="wrapper">
            <h1 class="mb15 txt-c">Будьте здоровы. <strong>Управляйте своим весом.</strong></h1>

            <ul class="steps">
                <li class="step1">
                    <div class="mb5"><a href="#" class="fs24 login-link">Поставьте цель</a></div>
                    <em>Хотите похудеть, набрать вес или начать правильно питаться?</em>
                </li>
                <li class="step2">
                    <div class="mb5"><a href="#" class="fs24 login-link">Получите рекомендации</a></div>
                    <em>Наш сервис посчитает оптимальный вес и составит рекомендации.</em>
                </li>
                <li class="step3">
                    <div class="mb5"><a href="#" class="fs24 login-link">Следите за результатом</a></div>
                    <em>Выполняйте свой дневник, держитесь плана и вы добьётесь своего.</em>
                </li>
            </ul>

            <div class="txt-c">
                <a href="#" class="button button-green login-link">Я хочу быть здоровым</a>
            </div>
        </div>
    </div>
    <div class="index-second">
        <b class="shadow-after"></b>

        <div class="wrapper">
            <ul class="steps">
                <li class="step1">
                    <strong>Отслеживайте вес</strong>

                    <div class="image"></div>
                    <p>Ведите ежедневный учёт веса и следите за своими успехами.</p>
                </li>
                <li class="step2">
                    <strong>Контролируйте рацион</strong>

                    <div class="image"></div>
                    <p>Учитывайте калории и наблюдайте за результатом в дневнике питания. Оптимизируйте свой рацион на
                        основе истории ваших успехов. Просто введите название блюда, а сервис сам посчитает калории.</p>
                </li>
                <li class="step3">
                    <strong>Калькулятор калорий</strong>

                    <div class="image"></div>
                    <p>Рассчитывать калорийность ваших рецептов легко, а если в нашей базе не оказалось необходимого вам
                        продукта, вы сможете добавить его сами.</p>
                </li>
                <li class="step4">
                    <strong>«До» и «После»</strong>

                    <div class="image"></div>
                    <p>Сохраняйте фото ваших успехов на любом этапе достижения цели.<br/>Сравните результаты «До» и
                        «После».</p>
                </li>
                <li class="step5">
                    <strong>Подходит всем</strong>

                    <div class="image"></div>
                    <p>Сервис оптимален как для наблюдения динамики веса тела, так и для контроля соблюдения совершенно
                        любой диеты.</p>
                </li>
            </ul>

            <hr/>
            <div class="fs24 mt25">
                Наши пользователи сбросили
                {% for nums in total_loss %}
                <b class="fs50 va-b" style="color: #000;">&nbsp;</b>
                    {% for num in nums %}
                    <span class="num">{{num}}</span>
                    {% endfor %}
                {% endfor %}
                килограмм, <a href="#" class="login-link">присоединитесь к ним</a>
            </div>
        </div>
    </div>

    <div class="index-third">
        <b class="shadow-after"></b>

        <div class="wrapper">
            <ul class="newusers">
                <li>
                    <div class="user-box">
                        <var>-2,5 кг</var>
                        <a href="#" class="user-photo login-link"><img src="{{img_host}}/images/users/1.jpg" alt=""/></a>

                        <div class="user-info">
                            <a href="#" class="fs18 login-link">Екатерина</a> <em class="gray">24 года</em>

                            <div class="mt5">На сайте: <b>17 дней</b></div>
                            <div class="mb5">Цель: <b>Похудеть</b></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="user-box">
                        <var>-4 кг</var>
                        <a href="#" class="user-photo login-link"><img src="{{img_host}}/images/users/2.jpg" alt=""/></a>

                        <div class="user-info">
                            <a href="#" class="fs18 login-link">Сергей</a> <em class="gray">30 лет</em>

                            <div class="mt5">На сайте: <b>1 месяц</b></div>
                            <div class="mb5">Цель: <b>Похудеть</b></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="user-box">
                        <var>-1,2 кг</var>
                        <a href="#" class="user-photo login-link"><img src="{{img_host}}/images/users/3.jpg" alt=""/></a>

                        <div class="user-info">
                            <a href="#" class="fs18 login-link">Анастасия</a> <em class="gray">28 лет</em>

                            <div class="mt5">На сайте: <b>12 дней</b></div>
                            <div class="mb5">Цель: <b>Похудеть</b></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="user-box">
                        <var>+0,9 кг</var>
                        <a href="#" class="user-photo login-link"><img src="{{img_host}}/images/users/4.jpg" alt=""/></a>

                        <div class="user-info">
                            <a href="#" class="fs18 login-link">Тимон</a> <em class="gray">19 лет</em>

                            <div class="mt5">На сайте: <b>15 дней</b></div>
                            <div class="mb5">Цель: <b>Набрать вес</b></div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="txt-c">
        <a href="#" class="fs26 login-link">Начните следить за своим рационом уже сегодня</a>
    </div>

</div>

{% endblock %}

{% block layers %}
{% if change_password %}
<div id="change-password-layer" class="modal" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Смена пароля</h2>
        </div>

        <p>
            <input id="new-password" name="password" placeholder="Новый пароль" class="text normal" />
            <sup class="required">*</sup>

            <div class="error" style="display: none;">
                Неверное значение
            </div>
        </p>
        <button class="button button-orange small">Сохранить</button>
    </div>
</div>

<div id="password-changed-layer" class="modal" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Пароль изменён</h2>
        </div>

        <p>
            Теперь вы можете войти на сайт используя свой email и новый пароль
        </p>
        <button class="button button-orange small">Закрыть</button>
    </div>
</div>
{% endif %}
{% endblock%}

{% block javascript_footer %}
<script type="text/javascript">
$(function () {
    var hash = window.location.hash.split('/');

    if (hash[0] == '#signin') {
        showLayer('signin');
    }
    else if (hash[0] == '#signup') {
        showLayer('signup');
    }
});

{% if change_password %}
$(function () {
    showLayer('change-password');

    $('#change-password-layer button').click(function(e) {
        e.preventDefault();
        var data = {
            'pwdcode' : '{{pwd_code}}',
            'password' : $('#new-password').val()
        };

        $.post('/ws/password/new/', data, function(result) {
            if (result.status == 'ok') {
                showLayer('password-changed');
            }
        }, 'json');
    });

    $('#password-changed-layer button').click(function(e) {
        e.preventDefault();
        hideLayer();
        window.location.assign('/profile/');
    });
});
{% endif %}
</script>
{% endblock %}
