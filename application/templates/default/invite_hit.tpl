{% extends 'common/base.tpl' %}
{% block title %}
    <title>Симплфит - Приглашение</title>
{% endblock %}


{% block content %}
    <div class="content wrapper">
        <br>
        <h2 class="mb15 txt-c">Придумайте пароль</h2>

        <i>{{ name }}! Вы получили приглашение на сайт здорового образа жизни. Для того что бы попасть внутрь придумайте пароль для входа.</i><br>
        <i>Логином будет ваш почтовый адрес: {{ email }}</i>
        <br>
        <br>
        <form method='post' action='/invite/register' class="txt-c" id='pass_form'>
            <input name='password' class="txt-c" style='font-size:40px;'><br>
            <input type='hidden' name='email' class='mb50' value='{{email}}' >
            <input type='hidden' name='id' class='mb50' value='{{id}}' >
            <button class='button button-orange large mt20' type='submit' id='pass_form_submit'>Сохранить</button>
        </form>
        </div>
{% endblock%}