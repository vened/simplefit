{% extends 'common/base.tpl' %}

{% block title %}
<title>Симплфит – Калорийность продуктов и блюд</title>
{% endblock %}

{% block meta %}
<meta name="keywords" content="калорийность, калорийность продуктов, таблица калорийности, таблица калорийности продуктов, калорийность блюд, калорийность готовых блюд">
{% endblock %}

{% block javascript %}
{% endblock %}

{% block content %}
<div class="content">
    <div class="wrapper">
        <h1 class="mb30 txt-c">Калорийность продуктов</h1>

        <div class="search-page">
            <div class="big-search">
                <form action="/search/" id="search-form">
                    <input type="text" placeholder="Например:  Йогурт растишка" class="text width4" name="q" value="{{query}}" />
                    <a href="#" class="button button-green">Найти продукт</a>
                </form>
            </div>

            <div class="search-about">В нашей базе уже <strong>{{product_count}} продуктов</strong>. Достаточно задать название продукта, который вы ищите и Симплфит отобразит все соответствующие вашему запросу варианты.</div>

            {% if count %}

            <table class="crossed mb30 fs30">
                <tr>
                    <td class="left"></td>
                    <td class="title">
                        {% if query %}
                        {{_n_rus('Найден', 'Найдено', 'Найдено', count)}} <var>{{count}}</var> {{_n_rus('продукт', 'продукта', 'продуктов', count)}}
                        {% else %}
                        Новые продукты, доступные для поиска
                        {% endif %}
                    </td>
                    <td class="right"></td>
                </tr>
            </table>

            <div>
                <ul class="items-product">
                    {% include 'ifr/search.tpl' %}
                </ul>
            </div>

            {% if show_next_link %}
                {% if search_enabled %}
                <div class="fs26 txt-c mt30 mb20"><a id="more-products-link" href="#">Больше продуктов</a></div>
                {% else %}
                <div class="fs26 txt-c mt30 mb20"><a href="/search/table">Вся таблица калорийности</a></div>
                {% endif %}
            {% endif %}

            {% endif %}

            <br>
            <div class="add-product-link" id="add-prodict-button">
                <a href="#"  class="icons icon_add va-m"></a><a href="#" class="button button-gray ml10">Добавить
                продукт</a>
            </div>


        </div>

    </div>
</div>
{% endblock %}

{% block javascript_footer %}
<script type="text/javascript">
$(function() {
    $('#search-form .button').click(function(e) {
        e.preventDefault();
        $('#search-form').submit();
    });

    $('#add-prodict-button a').click(function(e) {
        e.preventDefault();
        {% if user %}
            showLayer('add-product');
        {% else %}
            showLayer('signin');
        {% endif %}
    });

    $('#add-product-form button').click(function(e) {
        e.preventDefault();
        $('#add-product-form div.error').hide();
        var data = $('#add-product-form').serializeObject();

        if (!checkProductData(data, ['proteins', 'fats', 'carbohydrates'])) {
            return false;
        }

        $.post('/ws/products/add/', data, function() {
            $('#add-product-form input[type=text]').val('');
            hideLayer();
            showLayer('add-product-thanks');
        });
    });

    $('#add-product-thanks-layer div button').click(function(e) {
        hideLayer();
    });

    var page = 1;
    $('#more-products-link').click(function(e) {
        e.preventDefault();
        var target = $(this);
        var query = $.trim($('#search-form input[name=q]').val());

        $.post('/ifr/search/', {'p' : page, 'q' : query}, function(result) {
            if (result) {
                page++;
                var html = $(result);
                $('ul.items-product').append(html);

                var showNext = $('li.next-page').filter(':last').html();
                if (page >= 5 || showNext == 0) {
                    target.parent().hide();
                }
            }
        });
    });

    var foundProducts = {};
    $('ul.items-product').keyup(function (e) {
        var target = $(e.target);
        if (target.attr('name') == 'weight') {
            var val = target.val();
            var val2 = val.replace(/[^0-9]/g, '');
            if (val2 != val) {
                val = val2;
                target.val(val);
            }
            var calories = target.siblings('.calories-val').html().split(' ');
            var productId = target.attr('id').split('-')[1];

            if (!(productId in foundProducts)) {
                foundProducts[productId] = parseFloat(calories[0]);
            }

            target.siblings('.calories-val').html(foundProducts[productId] * val / 100 + ' ' + calories[1]);
        }
    });

    $('ul.items-product a.add-menu').click(function(e) {
        e.preventDefault();

        {% if user %}
            var target = $(this);
            var weight = target.siblings('input[name=weight]');
            var productId = weight.attr('id').split('-')[1];
            var productData = productId + '_' + weight.val();
            window.location.assign('/profile/?product='+productData);
        {% else %}
            showLayer('signin');
        {% endif %}
    });
});

function checkProductData(data, exclude) {
    var result = true;
    if (!exclude) {
        exclude = [];
    }

    for (x in data) {
        if (exclude.indexOf(x) != -1) {
            continue;
        }

        if (!data[x] || data[x] == 0) {
            result = false;
            $('input[name=' + x + ']').siblings('div.error').show();
            continue;
        }

        var re = /^\d+((\.|,)\d+)?$/;

        if (x != 'name' && !re.test(data[x])) {
            result = false;
            $('input[name=' + x + ']').siblings('div.error').show();
        }
    }

    return result;
}
</script>
{% endblock %}
