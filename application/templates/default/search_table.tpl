{% extends 'common/base.tpl' %}

{% block title %}
<title>Симплфит – Калорийность продуктов и блюд</title>
{% endblock %}

{% block meta %}
<meta name="keywords" content="{% for product in products %}калорийность {{product.name}}, {% endfor %}">
{% endblock %}

{% block javascript %}
{% endblock %}

{% block content %}
<div class="content">
    <div class="wrapper">
        <h1 class="mb30 txt-c">Калорийность продуктов</h1>
        <div class="search-about">В нашей базе уже более <strong>{{product_count}} продуктов</strong>.</div> <br>
        <div>
            <ul class="items-product">
            {% include 'ifr/search.tpl' %}
            </ul>
        </div>
        <div style='text-align:center;font-size:20px;'>
            {% if fast_back %}<a href="?page={{current_page-8}}">...</a>{% endif %}
            {% for page in pager_back %}
            <a href="?page={{page}}">{{page + 1}}</a>
            {% endfor %}
            <b>{{current_page + 1}}</b>
            {% for page in pager_front %}
                <a href="?page={{page}}">{{page + 1}}</a>
            {% endfor %}
            {% if fast_forward %}<a href="?page={{current_page + 8}}">...</a>{% endif %}
        </div>

    </div>
</div>
{% endblock %}

{% block javascript_footer %}
<script type="text/javascript">
    $(function() {
        $('#search-form .button').click(function(e) {
            e.preventDefault();
            $('#search-form').submit();
        });

        $('#add-prodict-button a').click(function(e) {
            e.preventDefault();
        {% if user %}
            showLayer('add-product');
        {% else %}
            showLayer('signin');
        {% endif %}
        });

        $('#add-product-form button').click(function(e) {
            e.preventDefault();
            $('#add-product-form div.error').hide();
            var data = $('#add-product-form').serializeObject();

            if (!checkProductData(data, ['proteins', 'fats', 'carbohydrates'])) {
                return false;
            }

            $.post('/ws/products/add/', data, function() {
                $('#add-product-form input[type=text]').val('');
                hideLayer();
                showLayer('add-product-thanks');
            });
        });

        $('#add-product-thanks-layer div button').click(function(e) {
            hideLayer();
        });

        var page = 1;
        $('#more-products-link').click(function(e) {
            e.preventDefault();
            var target = $(this);
            var query = $.trim($('#search-form input[name=q]').val());

            $.post('/ifr/search/', {'p' : page, 'q' : query}, function(result) {
                if (result) {
                    page++;
                    var html = $(result);
                    $('ul.items-product').append(html);

                    var showNext = $('li.next-page').filter(':last').html();
                    if (page >= 5 || showNext == 0) {
                        target.parent().hide();
                    }
                }
            });
        });

        var foundProducts = {};
        $('ul.items-product').keyup(function (e) {
            var target = $(e.target);
            if (target.attr('name') == 'weight') {
                var val = target.val();
                var val2 = val.replace(/[^0-9]/g, '');
                if (val2 != val) {
                    val = val2;
                    target.val(val);
                }
                var calories = target.siblings('.calories-val').html().split(' ');
                var productId = target.attr('id').split('-')[1];

                if (!(productId in foundProducts)) {
                    foundProducts[productId] = parseFloat(calories[0]);
                }

                target.siblings('.calories-val').html(foundProducts[productId] * val / 100 + ' ' + calories[1]);
            }
        });

        $('ul.items-product a.add-menu').click(function(e) {
            e.preventDefault();

        {% if user %}
            var target = $(this);
            var weight = target.siblings('input[name=weight]');
            var productId = weight.attr('id').split('-')[1];
            var productData = productId + '_' + weight.val();
            window.location.assign('/profile/?product='+productData);
        {% else %}
            showLayer('signin');
        {% endif %}
        });
    });

    function checkProductData(data, exclude) {
        var result = true;
        if (!exclude) {
            exclude = [];
        }

        for (x in data) {
            if (exclude.indexOf(x) != -1) {
                continue;
            }

            if (!data[x] || data[x] == 0) {
                result = false;
                $('input[name=' + x + ']').siblings('div.error').show();
                continue;
            }

            var re = /^\d+((\.|,)\d+)?$/;

            if (x != 'name' && !re.test(data[x])) {
                result = false;
                $('input[name=' + x + ']').siblings('div.error').show();
            }
        }

        return result;
    }
</script>
{% endblock %}
