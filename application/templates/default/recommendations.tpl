{% extends 'common/base.tpl' %}

{% block title %}
<title>Симплфит - Рекомендации</title>
{% endblock %}

{% block javascript %}
{% endblock %}

{% block content %}


<div class="content wrapper">

    <h1 class="mb10 txt-c">Рекомендации {% if user %}({{user.recommendation_count}}){% endif %}</h1>

    <div class="mb30 txt-c fs18">
        <i>
           Сервис рекомендаций поможет вам спланировать меню на два дня, одну или две недели.<br>
           Simplefit автоматически составит для вас сбалансированное меню с учетом ваших целей
        </i>
    </div>
    <div class="mb30">
        Тысячи пользователей Симплфита показывают положительные результаты от использования сервиса рекомендаций,
        которые система автоматически учитывает, и под контролем наших специалистов совершенствует каждое отдельно взятое меню.
        Сервис рекомендаций составляет для вас индивидуальный план питания на срок от двух дней до двух недель.
        Уже полученные рекомендации можно использовать неограниченное количество раз, экспериментируя с рекомендуемыми и собственными продуктами.
    </div>
    <div class="row35 mb35">
        <div class="span3">
            <h3>План на 2 дня</h3>
            <p class="gray">
                Самый простой из доступных тарифов, позволяет оценить качество сервиса.
            </p>
            <a class="button button-orange small {% if user %}pay_button{% else %}login-link{% endif %}" href="#" pay='100'>100 рублей</a>
        </div>
        <div class="span3">
            <h3>План на 7 дней</h3>

            <p class="gray">
                Тариф оптимален для среднесрочного планирования.
            </p>
            <a class="button button-orange small {% if user %}pay_button{% else %}login-link{% endif %}" href="#" pay='150'>150 рублей</a>
        </div>
        <div class="span3">
            <h3>План на 14 дней</h3>

            <p class="gray">
                Двухнедельное планирование идеально для  эффективного достижения цели.
            </p>
            <a class="button button-orange small {% if user %}pay_button{% else %}login-link{% endif %}" href="#" pay='240'>240 рублей</a>
        </div>
    </div>

    <script type="text/javascript">
        $(function(){
            if (window.location.hash == '#payment-ok') {
                showLayer('pay-ok');
                window.location.hash = '';
            }

            $('#copy-plan-link').click(function(e) {
                e.preventDefault();
                showLayer('copy-plan');
            });

            $(".pay_button").click(function(e){
                var val = $(e.target).attr("pay");
                $("#pay_button").html("Оплатить " + val + " рублей").attr('pay', val);
                showLayer("pay-request");
            })

            {% if error_prev %}$(function(){
                showLayer('copy-plan-error');
            })
            {% endif %}

            {% if plan_exists %}$(function(){
                showLayer('plan-exists');
            })
            {% endif %}

            $("#pay_button").click(function(e) {
                e.preventDefault();
                $.post('/ws/payment/', {'cost' :$(this).attr('pay')}, function(response) {
                    if ('url' in response) {
                        window.location.assign(response.url);
                    }
                });
            });
        });

    </script>

{% endblock %}

{% block layers %}
{% include 'common/copy_plan_layer.tpl' %}
<div id="pay-request-layer" class="modal" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Получить Рекомендации</h2>

            <div>После оплаты сервиса, вы сможете получить рекомендации на любой выбранный вами день. Для этого перейдите в на страницу план питания и нажмите кнопку «Получить рекомендацию», далее следуйте инструкциям.
                Оплата производится через сервис интернет-платежей Робокасса.</div>
        </div>

        <div>
            <button class="button button-orange va-m close" id='pay_button' pay="0"></button>
        </div>
    </div>
</div>

    <div id="pay-ok-layer" class="modal" style="top:30px; display: none;">
        <div class="inset">
            <i class="icons icon_close"></i>

            <div class="title">
                <h2>Оплата прошла успешно</h2>

                <div>Для того чтобы воспользоваться сервисом перейдите  на страницу плана питания и нажмите кнопку «Получить рекомендацию», далее следуйте инструкциям.</div>
            </div>

            <div>
                <a class="button button-orange va-m close" href='/plan'>Перейти в план</a>
            </div>
        </div>
    </div>

    <div id="plan-exists-layer" class="modal" style="top:30px; display: none;">
        <div class="inset">
            <i class="icons icon_close"></i>

            <div class="title">
                <h2>Заменить план?</h2>

                <div>На выбранную дату план уже составлен, для замены уже существующего плана нажмите кнопку «Заменить»</div>
            </div>

            <div>
                <a class="button button-orange va-m close" href='?day_to={{day_to}}&month_to={{month_to}}&force=1'>Заменить</a>
            </div>
        </div>
    </div>

{% endblock %}

