{% extends 'common/base.tpl' %}

{% block title %}
<title>Симплфит - Настройки</title>
{% endblock %}

{% block css %}
<link rel="stylesheet" type="text/css" href="{{img_host}}/js/ajaxupload/fileuploader.css?{{img_version}}">
{% endblock %}

{% block javascript %}
<script type="text/javascript" src="{{img_host}}/js/ajaxupload/fileuploader.js"></script>
{% endblock %}

{% block content %}
<div class="content wrapper">
    <h1 class="mb15 txt-c">Мои настройки</h1>

    <div class="mb50 txt-c">
        <div class="btn-group">
            <a href="{{nav('settings')}}"><span class="btn{% if section == 'main' %} selected{% endif %}">Основные настройки</span></a>
            <a href="{{nav('settings')}}?sec=profile"><span class="btn{% if section == 'profile' %} selected{% endif %}">Редактировать профиль</span></a>
        </div>
    </div>

    {% if section == 'main' %}
    <div class="width4 m-c">

        <form id="main-settings-form">
        <dl class="form_field">
            <dt><label for="username">Имя:</label></dt>
            <dd>
                <input class="text width3" id="username" type="text" name="first_name" value="{{user.first_name}}">
                <sup class="required">*</sup>

                <div class="error">Некорректное значение</div>
            </dd>
        </dl>

        <dl class="form_field">
            <dt><label for="surname">Фамилия:</label></dt>
            <dd>
                <input class="text width3" id="surname" type="text" name="last_name" value="{{user.last_name}}">
                <!--sup class="required">*</sup-->

                <div class="error">Некорректное значение</div>
            </dd>
        </dl>

        <dl class="form_field mb50">
            <dt><label for="email">Эл. почта:</label></dt>
            <dd>
                <input class="text width3" id="email" type="text" name="email" value="{{user.email}}">
                <sup class="required">*</sup>

                <div class="error">Некорректное значение</div>
            </dd>
        </dl>

        <dl class="form_field">
            <dt><label for="password">Пароль:</label></dt>
            <dd>
                <input class="text width3" id="password" type="text" name="password" value="******" disabled="disabled" />
                <sup class="required">*</sup>

                <div class="error">Некорректное значение</div>

                <div class="mt5 form_text">
                    <label><input type="checkbox" class="checkbox" name="change_password" value="1" />&nbsp;<span class="fs16">Изменить пароль</span></label>
                </div>
            </dd>
        </dl>
        <h2 class="mb15 txt-c">Оповещения</h2>

        <dl class="form_field">
            <dt><label for="email_reminder">Напоминаия</label></dt>
            <dd>
                <input class="text width3" id="email_reminder" type="checkbox" name="email_reminder" {% if user.email_reminder %}checked{% endif %} value='on'>
            </dd>
        </dl>
        <dl class="form_field">
            <dt><label for="email_news">Новости</label></dt>
            <dd>
                <input class="text width3" id="email_news" type="checkbox" name="email_news" {% if user.email_news %}checked{% endif %} value='on'>
            </dd>
        </dl>

            <dl class="form_field mt30">
            <dt></dt>
            <dd>
                <a href="#" id="main-settings-save" class="button button-green small">Сохранить изменения</a>
            </dd>
        </dl>
        </form>

    </div>

    {% elseif section == 'profile' %}

    <table class="crossed mb50 fs30">
        <td class="left"></td>
        <td class="title">Профиль</td>
        <td class="right"></td>
    </table>

    <form id="profile-settings-form">
    <div class="width4 m-c">
        <dl class="form_field">
            <dt><label for="gender">Пол:</label></dt>
            <dd>
                <div id="gender" class="form_text row35 fs30">
                    <input type="radio" class="radio" name="gender" value="" {% if user.gender not in ['M', 'F'] %}checked="checked"{% endif%} style="display: none;" />
                    <label class="span"><input type="radio" class="radio" name="gender" value="M" {% if user.gender == 'M' %}checked="checked"{% endif %} />&nbsp;<span class="fs20">мужской</span></label>
                    <label class="span"><input type="radio" class="radio" name="gender" value="F" {% if user.gender == 'F' %}checked="checked"{% endif %} />&nbsp;<span class="fs20">женский</span></label>
                </div>

                <div class="error">Некорректное значение</div>
            </dd>
        </dl>

        <dl class="form_field">
            <dt><label for="username">Дата рождения:</label></dt>
            <dd class="birthday">
                <select class="select" name="birthday_day">
                    <option value="">&nbsp;</option>
                    {% for day in 1..31 %}
                    <option value="{{day}}" {% if day == birthday.day %}selected="selected"{% endif %}>{% if day < 10 %}0{% endif %}{{day}}</option>
                    {% endfor %}
                </select>
                <select class="select width2" name="birthday_month">
                    <option value="">&nbsp;</option>
                    <option value="1" {% if birthday.month == 1 %}selected="selected"{% endif %}>январь</option>
                    <option value="2" {% if birthday.month == 2 %}selected="selected"{% endif %}>февраль</option>
                    <option value="3" {% if birthday.month == 3 %}selected="selected"{% endif %}>март</option>
                    <option value="4" {% if birthday.month == 4 %}selected="selected"{% endif %}>апрель</option>
                    <option value="5" {% if birthday.month == 5 %}selected="selected"{% endif %}>май</option>
                    <option value="6" {% if birthday.month == 6 %}selected="selected"{% endif %}>июнь</option>
                    <option value="7" {% if birthday.month == 7 %}selected="selected"{% endif %}>июль</option>
                    <option value="8" {% if birthday.month == 8 %}selected="selected"{% endif %}>август</option>
                    <option value="9" {% if birthday.month == 9 %}selected="selected"{% endif %}>сентябрь</option>
                    <option value="10" {% if birthday.month == 10 %}selected="selected"{% endif %}>октябрь</option>
                    <option value="11" {% if birthday.month == 11 %}selected="selected"{% endif %}>ноябрь</option>
                    <option value="12" {% if birthday.month == 12 %}selected="selected"{% endif %}>декабрь</option>
                </select>
                <select class="select" name="birthday_year">
                    <option value="">&nbsp;</option>
                    {% for year in birthday.to_year..birthday.from_year %}
                    <option value="{{year}}" {% if year == birthday.year %}selected="selected"{% endif %}>{{year}}</option>
                    {% endfor %}
                </select>

                <sup class="required">*</sup>

                <div class="error">Некорректное значение</div>
            </dd>
        </dl>

        <dl class="form_field mb35">
            <dt><label for="height">Рост:</label></dt>
            <dd>
                <!--input class="text width2" id="height" type="text" value="185"-->
                <select id="height" class="select" name="height">
                    <option value="">&nbsp;</option>
                    {% for height in 140..210 %}
                    <option value="{{height}}" {% if height == user.height %}selected="selected"{% endif %}>{{height}}</option>
                    {% endfor %}
                </select>
                <span class="help-inline ml5">см</span>
                <sup class="required">*</sup>

                <div class="error">Некорректное значение</div>
            </dd>
        </dl>


        <dl class="form_field" style="display: none;">
            <dt><label for="country">Страна:</label></dt>
            <dd>
                <select id="country" class="select select-large">
                    <option>Россия</option>
                </select>
            </dd>
        </dl>

        <dl class="form_field mb50" style="display: none;">
            <dt><label for="city">Город:</label></dt>
            <dd>
                <select id="city" class="select select-large">
                    <option>Россия</option>
                </select>
            </dd>
        </dl>
    </div>

    <table class="crossed mb50 fs30">
        <td class="left"></td>
        <td class="title">Цель</td>
        <td class="right"></td>
    </table>

    <div class="width4 m-c">
        <dl class="form_field">
            <dt><label for="target">Ваша цель:</label></dt>
            <dd>
                <select id="target" class="select select-large" name="goal">
                    <option value="">&nbsp;</option>
                    <option value="weight_loss"{% if user.goal == 'weight_loss' %} selected="selected"{% endif %}>Снижение веса</option>
                    <option value="weight_gain"{% if user.goal == 'weight_gain' %} selected="selected"{% endif %}>Набор веса</option>
                    <option value="weight_maintenance"{% if user.goal == 'weight_maintenance' %} selected="selected"{% endif %}>Поддержание веса</option>
                </select>

                <sup class="required">*</sup>

                <div class="error">Некорректное значение</div>
            </dd>
        </dl>

        <dl class="form_field">
            <dt><label for="weight_current">Начальный вес:</label></dt>
            <dd>
                <input class="text width2" id="weight_current" name="weight" type="text" value="{% if user.weight %}{{user.weight}}{% endif %}">
                <span class="help-inline ml5">кг</span>
                <sup class="required">*</sup>

                <div class="error">Некорректное значение</div>
            </dd>
        </dl>

        <dl class="form_field mb50">
            <dt><label for="weight_want">Желаемый вес:</label></dt>
            <dd>
                <input class="text width2" id="weight_want" name="weight_goal" type="text" value="{% if user.weight_goal %}{{user.weight_goal}}{% endif %}">
                <span class="help-inline ml5">кг</span>
                <sup class="required">*</sup>

                <div class="error">Некорректное значение</div>
            </dd>
        </dl>
    </div>

    <table class="crossed mb50 fs30">
        <td class="left"></td>
        <td class="title">Образ жизни</td>
        <td class="right"></td>
    </table>

    <div class="width4 m-c">
        <dl class="form_field">
            <dt><label for="activity">Активность:</label></dt>
            <dd>
                <select id="activity" class="select select-large" name="activity">
                    <option value="">&nbsp;</option>
                    <option value="1"{% if user.activity == '1' %} selected="selected"{% endif %}>
                        Сидячий образ жизни
                    </option>
                    <option value="2"{% if user.activity == '2' %} selected="selected"{% endif %}>
                        Средняя активность (легкие упражнения 1-3 в неделю)
                    </option>
                    <option value="3"{% if user.activity == '3' %} selected="selected"{% endif %}>
                        Высокая активность (интенсивные занятия 3-5 раз в неделю)
                    </option>
                    <option value="4"{% if user.activity == '4' %} selected="selected"{% endif %}>
                        Очень высокая активность (тяжелые физические нагрузки 6-7 раз в неделю)
                    </option>
                    <option value="5"{% if user.activity == '5' %} selected="selected"{% endif %}>
                        Экстремальная активность (очень тяжелые физические нагрузки)
                    </option>
                </select>

                <sup class="required">*</sup>

                <div class="error">Некорректное значение</div>
            </dd>
        </dl>
    </div>

    <table class="crossed mb50 fs30">
        <td class="left"></td>
        <td class="title">Отношение к вегетарианству</td>
        <td class="right"></td>
    </table>

    <div class="txt-c mb50">
        <select class="select txt-l select-xlarge" name="vegetarianism">
            <option value="">&nbsp;</option>
            <option value="no"{% if user.vegetarianism == 'no' %} selected="selected"{% endif %}>Не готов отказаться от продуктов животного происхождения</option>
            <option value="vegetarianism"{% if user.vegetarianism == 'vegetarianism' %} selected="selected"{% endif %}>Вегетарианство</option>
            <option value="veganism"{% if user.vegetarianism == 'veganism' %} selected="selected"{% endif %}>Веганство</option>
            <option value="raw_food_diet"{% if user.vegetarianism == 'raw_food_diet' %} selected="selected"{% endif %}>Сыроедение</option>
            <option value="fruit_diet"{% if user.vegetarianism == 'fruit_diet' %} selected="selected"{% endif %}>Фрукторианство</option>
        </select>
        <sup class="required">*</sup>

        <div class="error" style="color: #fe2200; display: none;">Некорректное значение</div>
    </div>


    <table class="crossed mb50 fs30">
        <td class="left"></td>
        <td class="title">Юзерпик</td>
        <td class="right"></td>
    </table>

    <div class="width4 m-c">
        <dl class="form_field mt40 mb40">
            <dt>
                {% if main_photo_url %}
                <img src="{{main_photo_url}}" alt="" class="img-circle settings-avatar" width="140" />
                {% else %}
                <img src="{{img_host}}/images/noimage150.png" alt="" class="img-circle settings-avatar" width="140" />
                {%  endif %}
            </dt>
            <dd>
                <input type="hidden" name="photo_uploaded" value="0" />
                <span id="file-uploader-photo" class="qq-uploader">
                    <noscript><p>Ваш браузер не поддерживает javascript</p></noscript>
                </span>

                <span style="display: none;">
                    <span class="dl">
                        <i class="icons icon_photoupload va-m ml10"></i><span class="ml10">serega-pohudel.jpg</span>
                    </span>
                    <a class="ml10" href="">Удалить</a>
                </span>

                <div class="gray mt15"><em>jpg, jpeg, png. Максимальный размер файла: 800 Кб</em></div>
            </dd>
        </dl>

        <dl class="form_field mt30">
            <dt></dt>
            <dd>
                <a href="#" id="profile-settings-save" class="button button-green small">Сохранить изменения</a>
            </dd>
        </dl>
    </div>
    </form>

    {% endif %}
</div>

<script type="text/javascript">

$(function() {
    {% if section == 'main' %}

    var form = $('#main-settings-form');
    var oldVal = form.find('input[name=password]').val();

    $('#main-settings-save').click(function(e) {
        e.preventDefault();
        $('div.error').hide();

        var data = $('#main-settings-form').serializeObject();

        if (!checkData(data, ['last_name'])) {
            return false;
        }

        $.post('/ws/settings/main/', data, function(result) {
            if (result.status == 'ok') {
                //alert('Сохранено!');
                form.find('input[name=change_password]').removeAttr('checked');
                form.find('input[name=password]').attr('disabled', 'disabled').val(oldVal);
                window.location.assign('/profile/');
            }
        }, 'json');
    });

    form.find('input[name=change_password]').click(function (e) {
        if ($(this).filter(':checked').size()) {
            form.find('input[name=password]').removeAttr('disabled').val('');
        }
        else {
            form.find('input[name=password]').attr('disabled', 'disabled').val(oldVal);
        }
    });

    {% elseif section == 'profile' %}

    $('#profile-settings-save').click(function (e) {
        e.preventDefault();
        $('div.error').hide();

        var data = $('#profile-settings-form').serializeObject();

        if (!checkData(data, ['photo_uploaded'])) {
            return false;
        }

        $.post('/ws/settings/profile/', data, function (result) {
            if (result.status == 'ok') {
                //alert('Сохранено!');
                window.location.assign('/profile/');
            }
        }, 'json');
    });

    var uploader = new qq.FileUploader({
        element: document.getElementById('file-uploader-photo'),
        action: 'http://' + document.location.host + '/ws/upload/photo/',
        params: {'main' : 1},
        template: '<span class="qq-upload-drop-area"><span>Drop files here to upload</span></span>' +
                '<span class="qq-upload-button"><a href="#" class="button button-gray">Выберите файл</a></span>' +
                '<ul class="qq-upload-list"></ul>',
        debug: true,
        onComplete: photoUploadComplete
    });
    {% endif %}
});

function photoUploadComplete(id, fileName, responseJSON) {
    if (!('error' in responseJSON)) {
        $('#profile-settings-form input[name=photo_uploaded]').val(1);
    }
}
</script>
{% endblock %}