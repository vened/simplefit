{% extends 'common/base.tpl' %}

{% block title %}
<title>Симплфит - План питания</title>
{% endblock %}

{% block javascript %}
{% endblock %}

{% block content %}
<div class="content wrapper">

    <h1 class="mb10 txt-c">План питания</h1>

    <div class="yellow_block mb40 txt-c" {% if not show_info_layer %}style="display: none;"{% endif %}>
        <i class="icons yellow_block_arr2" style="left: 175px"></i>
        <span class="block_close">Скрыть</span>

        <div class="fs18">
            <i>Спланируйте ваше меню на несколько дней вперёд.</i>
        </div>
        <div class="mb15">
            Планирование – эффективная методика контроля за вашим питанием. Воспользуйтесь возможностью оптимизировать
            ваш рацион и спланировать покупку необходимого набора продуктов на несколько дней вперёд.
        </div>
        {#<a href="#" class="button button-green min">Внести данные</a>#}
    </div>

<br>
    <div class="period_list">
        <i class="icons prev" {% if not show_prev_link %}style="display: none;"{% endif %}></i>
        <ul id="calories-period-list" class="">
            {% for key,day in days %}
            <li id="day-{{key}}" {% if key == selected_date %}class="current"{% endif %} {% if not day.visible %}style="display: none;"{% endif %}>
                {% if key >= current_date or day.has_data %}
                <a href="/plan/?date={{key}}" class="blue_dotted">{{day.day}}</a>
                {% else %}
                <span>{{day.day}}</span>
                {% endif %}
            </li>
            {% endfor %}
        </ul>
        <i class="icons next" {% if not show_next_link %}style="display: none;"{% endif %}></i>
    </div>
    <div id="calories-section" class="box_border my_diary">

        {% set params =  {
            'today' : selected_date|date('d.m.Y'),
            'disable_form' : not show_edit_block,
            'form_action' : '/ws/plan/add/',
            'current_date' : selected_date,
        } %}

        {% include 'common/add_menu.tpl' with params %}

        {% for menu in selected_menu %}
        <hr />
        <dl class="item_content">
            <dt>
                <i class="icons icon_note"></i>
            </dt>
            <dd>
                <div>
                    <b class="fs16">
                        {% if menu.food_type == 'breakfast' %}
                        Завтрак
                        {% elseif menu.food_type == 'breakfast2' %}
                            Поздний завтрак
                        {% elseif menu.food_type == 'lunch' %}
                            Обед
                        {% elseif menu.food_type == 'lunch2' %}
                            Полдник
                        {% elseif menu.food_type == 'dinner' %}
                            Ужин
                        {% elseif menu.food_type == 'dinner2' %}
                            Поздний ужин
                        {% elseif menu.food_type == 'water' %}
                            Вода
                        {% endif %}
                        {# в {{menu.created}}--> #}


                    </b>
                    &nbsp;
                    {% if show_edit_block %}
                    <div id="edit-{{menu.id}}" class="edit_block">
                        <i class="icons icon_edit2"{% if not show_edit_block %}style="display: none;"{% endif %}>
                            <div class="tooltip">Редактировать</div>
                        </i>
                        <i class="icons icon_trash">
                            <div class="tooltip">Удалить</div>
                        </i>
                    </div>
                    {% endif %}
                </div>
                <div class="products-data">
                    {% for product in menu.products %}
                        <span style="display: none;">{{ product.id }}</span> {{product.weight}}г - {{product.name}} <b>+{{product.calories}} ккал</b>;
                    {% endfor %}
                </div>
                <b>Итого:</b> <b class="yellow">+{{menu.calories}} ккал</b>
            </dd>
        </dl>
        {% endfor %}
        <hr>
        {% if selected_calories %}
        <div class="clearfix mt25 mb5">
            <div class="fl-l fs20"><b>Итого:</b> <b class="yellow">+{{selected_calories}} ккал</b></div>
            <div class="fl-r fs16">
                <a id="copy-plan-link" class="link fs16" href="#">Повторить план</a>
            </div>
        </div>
        {% endif %}
    </div>

        <a id='get-payment-plan' class="button button-green small" href="/recommendations/preview?date={{date}}" >Получить рекомендацию</a>


</div>

{% endblock %}

{% block layers %}
        {% include 'common/copy_plan_layer.tpl' %}
{% endblock %}

{% block javascript_footer %}
<script type="text/javascript">
var requestInProcess = false;
var data = {};
$(function() {

    $('.icons.prev').click(function() {
        var hiddenDay = $('#calories-period-list').children().filter(':visible').filter(':first').prev();

        if (hiddenDay.size()) {
            var visibleDay = $('#calories-period-list').children().filter(':visible').filter(':last');
            $('.icons.next').show();

            for (var x = 0; x < 6; x++) {
                visibleDay.hide();
                hiddenDay.show();
                visibleDay = visibleDay.prev();
                hiddenDay = hiddenDay.prev();

                if (hiddenDay.size() == 0) {
                    $(this).hide();
                    break;
                }
            }
        }
    });

    $('.icons.next').click(function () {
        var periodList = $('#calories-period-list');
        var hiddenDay = periodList.children().filter(':visible').filter(':last').next();
        var visibleDay = periodList.children().filter(':visible').filter(':first');

        if (hiddenDay.size()) {
            $('.icons.prev').show();

            for (var x = 0; x < 6; x++) {
                visibleDay.hide();
                hiddenDay.show();
                visibleDay = visibleDay.next();
                hiddenDay = hiddenDay.next();

                if (hiddenDay.size() == 0) {
                    //$(this).hide();
                    break;
                }
            }
        }
        else {
            for (var x = 0; x < 6; x++) {
                var lastDay = periodList.children().filter(':visible').filter(':last');
                var date = new Date(lastDay.attr('id').substr(4));
                date.setTime(date.getTime() + 86400000);
                var nextDate = date.toISOString().substr(0, 10);
                var nextDay = lastDay.clone().removeClass('current');
                nextDay.attr('id', 'day-' + nextDate);
                var href = nextDay.find('a').attr('href').replace(/\d{4}-\d{2}-\d{2}/, '');
                var parts = nextDate.split('-');
                nextDay.find('a').attr('href', href + nextDate).html(parts[2] + '.' + parts[1]);
                visibleDay.hide();
                periodList.append(nextDay);
                visibleDay = periodList.children().filter(':visible').filter(':first');
            }
        }
    });

    $('#copy-plan-link').click(function(e) {
        e.preventDefault();
        showLayer('copy-plan');
    });

    $('#copy-plan-form button').click(function(e) {
        e.preventDefault();
        data = $('#copy-plan-form').serializeObject();

        $.post('/ws/plan/copy/', data, function(response) {
            if ('error' in response) {
                if (response.error == 'wrong date') {
                    showLayer('copy-plan-error');
                }
                else if (response.error == 'exists') {
                    showLayer('copy-plan-confirm');
                }
            }
            else {
                hideLayer();
                window.location.assign('?date=' + response.date);
            }
        }, 'json');
    });

    $('#copy-plan-error-layer button').click(function(e) {
        e.preventDefault();
        hideLayer();
    });

    $('#copy-plan-confirm-layer button.replace').click(function (e) {
        e.preventDefault();
        data['confirmed'] = 1;

        $.post('/ws/plan/copy/', data, function (response) {
            hideLayer();
            window.location.assign('?date=' + response.date);
        });
    });

    $('#copy-plan-confirm-layer button.close').click(function (e) {
        e.preventDefault();
        data = {};
        hideLayer();
    });

    $('dl.item_content .icon_trash').click(function () {
        var target = $(this);
        var id = target.parent().attr('id').split('-');
        $.post('/ws/plan/delete/', {'id':id[1]}, function (res) {
            target.parents('dl.item_content').filter(':first').prev().remove();
            target.parents('dl.item_content').filter(':first').remove();
            window.location.reload(true);
        });
    });

    $('span.block_close').click(function(e) {
        var target = $(this);
        $.post('/ws/settings/plan/', {'hide_plan_info' : 1}, function(response) {
            target.parent().hide();
        });
    });

    $('i.icon_edit2').click(function(e) {
        var target = $(this);
        var id = target.parents('div.edit_block').attr('id').split('-');
        var planId = id[1];
        var date = $('#diary-calories-form').children('input[name=date]').val();
        var type = $.trim(target.parents('div.edit_block').siblings('b').html());
        var dataItem = target.parents('div.edit_block').parent().siblings('.products-data');
        var products = getProducts(dataItem);
        window.action = '/ws/plan/edit/';

        var header = $('h2.added-products-list');
        var oldVal = header.html();
        $('select[name=food_type]').children().each(function (index, element) {
            var el = $(element);
            el.attr('selected', false);

            if (el.html() == type) {
                el.attr('selected', true);
                $('a.select2-choice').children('span').filter(':first').html(type);
            }
        });

        var productsList = $('#products-list');
        var item = productsList.children().not(':first').remove();

        for (var x in products) {
            drawAddedProduct(products[x]);
        }

        $('html, body').animate({scrollTop:$("#calories-section").offset().top}, 200);
    });
});

function getProducts(dataItem) {
    var productsData = dataItem.text().split(';');
    var products = [];

    for (var x in productsData) {
        var data = $.trim(productsData[x]);

        if (!data) {
            continue;
        }

        //var parts = data.split(' - ');
        var re = /(\d+)\s+(\d+)г\s+\-\s+(.+?)\s+\+(\d+)\s+ккал/;
        var result = re.exec(data);

        var product = {
            'id':parseInt(result[1]),
            'weight':parseFloat(result[2]),
            'name':result[3],
            'calories':parseFloat(result[4])
        };

        products.push(product);
    }

    return products;
}
</script>
{% endblock %}