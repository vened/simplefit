{% extends 'common/base.tpl' %}

{% block title %}
<title>Симплфит - Страница не найдена</title>
{% endblock %}

{% block content %}

<style>
    .wrapper {
        width: 990px;
    }
</style>

<div class="content">
    <div class="wrapper">
        Страница не найдена.
        {% if DEBUG and error_message %}
            <br /> {{error_message}}
        {% endif %}
    </div>
</div>

{% endblock %}