{% extends 'common/base.tpl' %}

{% block title %}
<title>Симплфит - Аналитика</title>
{% endblock %}

{% block javascript %}
<script src="{{img_host}}/js/fabric_js.0.9.15.min.js" type="text/javascript"></script>
<script src="{{img_host}}/js/graph.js" type="text/javascript"></script>
{% endblock %}

{% block content %}
<div class="content wrapper">

    <h1 class="mb10 txt-c">Мои результаты</h1>

    <div class="mb10 fs30 black txt-c">
        Вес
        <div class="my_with">
            <input id="current-weight" type="text" class="text" value="{{current_weight}}">
            <i class="icons refresh"></i>
        </div>
        {% if weight_diff > 0 %}
            осталось набрать {{weight_diff}} кг
        {% elseif weight_diff < 0 %}
            осталось сбросить {{weight_diff|abs}} кг
        {% endif %}
    </div>
    <div class="mb30 txt-c">Последняя запись {{last_date|date('d.m.Y')}}</div>

    <div class="yellow_block mb40 txt-c" style="display: none;">
        <i class="icons yellow_block_arr2" style="left: 175px"></i>
        <span class="block_close">Скрыть</span>

        <div class="fs18">
            <i>Поздравляем, ваш вес не превышает запланированные показатели на данный период.</i>
        </div>
        <div class="mb15">
            Рекомендуем внести данные за пропущенные дни, это позволит делать более точные прогнозы в будущем.
        </div>
        <a href="#" class="button button-green min">Внести данные</a>
    </div>

    <hr>

    <div class="data_filter">
        <div class="period" style="display: none;">
            <a class="selected" href="#">день</a>
            <a href="#">неделя</a>
            <a href="#">месяц</a>
            <a href="#">год</a>
        </div>
        <div class="fl-r" style="display: none;">
            от
            <div class="data_selector" style="margin-right: 15px">
                <input type="text" placeholder="31.10.2012" class="text normal">
                <i class="icons calendar"></i>
            </div>
            до
            <div class="data_selector">
                <input type="text" placeholder="31.10.2012" class="text normal">
                <i class="icons calendar"></i>
            </div>
        </div>
    </div>

    <div class="clearfix">
        <div class="fl-l fs20 black">Динамика веса и калорий</div>
        <a href="#" class="fl-r blue_dotted" style="display: none;">Фильтр результатов</a>
    </div>

    <br />
    <div class="box_border result_filter" style="display: none;">
        <i class="icons top_arr"></i>

        <div class="filter_day">
            Посмотреть:
            <a href="#">эффективные дни</a>
            <a href="#">неэффективные дни</a>
        </div>
        <label class="form">
            Поиск по продуктам:
            <div class="search_field">
                <input type="text" class="text normal">
                <button>
                    <i class="icons icon_search"></i>
                </button>
            </div>
        </label>
    </div>

    <div class="graph">

        <div class="graphics">
            <ul id="weight-range" class="range">
                <li>89</li>
                <li>88</li>
                <li>87</li>
                <li>86</li>
                <li>85</li>
                <li>84</li>
                <li>83</li>
                <li>82</li>
            </ul>
            <canvas id="graph" height="140" width="950"></canvas>
            <div class="pointers-container" style="height: 140px"></div>
            <div class="text">
            {% if weight_diff > 0 %}
                осталось набрать <b class="yellow">{{ weight_diff }} кг</b>
            {% elseif weight_diff < 0 %}
                осталось сбросить <b class="yellow">{{ weight_diff|abs }} кг</b>
            {% endif %}
            </div>
        </div>

        <div class="diagram-container" style="height: {{diagram_container_height}}px">
            <ul class="range">
                {% for val in calories_range %}
                    {% if val.norm %}
                    <li class="norma-line" style="bottom: {{val.css_val}}px;">Норма <b>{{val.val}}</b></li><!-- 187.1428 = 2620/14 - 36 -->
                    {%  else %}
                    <li>{{val.val}}</li>
                    {% endif %}
                {% endfor %}
            </ul>
            <ul id="calories-diagram" class="diagram{% if user.goal == 'weight_gain' %} invert{% endif %}" data-norm="{{no_data_calories_val}}">
                {% for date,data in calories_data %}
                    {% if data.has_data %}
                    <li {% if not data.visible %}style="display: none;"{% endif %}>
                        <div class="count">
                            <div class="yellow_block"><i class="icons {% if data.is_ok %}ok{% else %}no_ok{% endif %}"></i> {{data.calories}}</div>
                        </div>
                        <div class="value" style="height:{{data.max_val}}px"><!-- 228.5714 = 3200/14 -->
                            <div class="norma" style="height: {{data.norm}}px"></div>
                            <div class="norma-light-limit" style="height: {{data.norm_light_limit}}px"></div>
                        </div>
                    </li>
                    {% else %}
                    <li class="no_data" {% if not data.visible %}style="display: none;"{% endif %}>
                        <div class="count">
                            <div class="yellow_block">нет данных</div>
                        </div>
                        <div class="value" style="height:{{no_data_calories_val}}px">
                            <a class="add_data" href="#" data="{{date}}"><i class="icons add"></i></a>
                        </div>
                    </li>
                    {% endif %}
                {% endfor %}
            </ul>
        </div>

    </div>

    <div class="period_list">
        <i class="icons prev" {% if not show_prev_link %}style="display: none;"{% endif %}></i>
        <ul id="calories-period-list" class="">
            {% for key,day in days %}
            <li id="day-{{key}}" {% if day.day == selected_date %}class="current"{% endif %} {% if not day.visible %}style="display: none;"{% endif %}>
                {% if day.has_data or day.day == selected_date %}
                <a href="/analytics/?date={{key}}" class="blue_dotted">{{day.day}}</a>
                {% else %}
                    <span data="{{key}}">{{day.day}}</span>
                {% endif %}
            </li>
            {% endfor %}
        </ul>
        <i class="icons next" {% if not show_next_link %}style="display: none;"{% endif %}></i>
    </div>

    <div class="box_border my_diary">

        {% set params =  {
            'today' : current_date|date('d.m.Y'),
            'form_action' : '/ws/diary/calories/'
        } %}

        {% include 'common/add_menu.tpl' with params %}

        {% for date,data in selected_menu %}
            {% if show_date_delimiter %}
            <hr>
            <div style="padding-bottom: 10px;"><b class="fs16">{{date|date('d.m.Y')}}</b></div>
            {% endif %}
            {% for menu in data %}
                {% if not show_date_delimiter %}
                <hr />
                {% endif %}
                <dl class="item_content">
                    <dt>
                        <i class="icons icon_note"></i>
                    </dt>
                    <dd>
                        <div>
                            <b class="fs16">
                                {% if menu.food_type == 'breakfast' %}
                                Завтрак
                                {% elseif menu.food_type == 'breakfast2' %}
                                    Поздний завтрак
                                {% elseif menu.food_type == 'lunch' %}
                                    Обед
                                {% elseif data.food_type == 'lunch2' %}
                                    Полдник
                                {% elseif menu.food_type == 'dinner' %}
                                    Ужин
                                {% elseif menu.food_type == 'dinner2' %}
                                    Поздний ужин
                                {% elseif menu.food_type == 'water' %}
                                    Вода
                                {% endif %}<!-- в {{menu.created}}-->
                            </b>
                            &nbsp;
                            <div id="edit-{{menu.id}}" class="edit_block">
                                <i class="icons icon_edit2" style="display: none;">
                                    <div class="tooltip">Редактировать</div>
                                </i>
                                <i class="icons icon_trash">
                                    <div class="tooltip">Удалить</div>
                                </i>
                            </div>
                        </div>
                        <div>
                            {% for product in menu.products %}
                                {{product.weight}}г - {{product.name}} <b>+{{product.calories}} ккал</b>
                            {% endfor %}
                        </div>
                        <b>Итого:</b> <b class="yellow">+{{menu.calories}} ккал</b>
                    </dd>
                </dl>
            {% endfor %}
        {% endfor %}
        <hr>
        {% if selected_calories %}
        <div class="clearfix mt25 mb5">
            <div class="fl-l fs20"><b>Итого:</b> <b class="yellow">+{{selected_calories}} ккал</b></div>
            <div class="fl-r fs16">
                {% if selected_calories <= optimal_calories %}
                <i>Отлично! Вы держитесь своей дневной нормы</i>
                {% else %}
                <i>Вы превысили дневную норму. Отработаете завтра :)</i>
                {% endif %}
            </div>
        </div>
        {% endif %}
    </div>


    <div class="row35" style="display: none;">
        <div class="span2 box_border item">
            <h2 class="mb10 fs20">Lorem ipsum</h2>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt, magna ac laoreet volutpat, orci
                mi
                dignissim arcu, ac pharetra arcu orci commodo urna.
                In hac habitasse platea dictumst. Pellentesque a sagittis nisl. In at magna nisi. Sed tempus iaculis
                scelerisque.
            </p>

            <div class="txt-r fs12"><a href="#">more lorem</a></div>
        </div>
        <div class="span2 box_border item">
            <h2 class="mb10 fs20">Lorem ipsum</h2>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt, magna ac laoreet volutpat, orci
                mi
                dignissim arcu, ac pharetra arcu orci commodo urna.
                In hac habitasse platea dictumst. Pellentesque a sagittis nisl. In at magna nisi. Sed tempus iaculis
                scelerisque.
            </p>

            <div class="txt-r fs12"><a href="#">more lorem</a></div>
        </div>
    </div>

</div>

{% endblock %}

{% block layers %}
<div id="add-weight-layer" class="modal" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Вам нужно задать свой вес:</h2>
        </div>
        <form id="graph-add-weight-form" data="">
            <input type="hidden" name="date" value="" />
            <input type="text" name="weight" placeholder="Вес" class="text width2 normal_big va-m"
                   style="width: 210px"> <b>кг</b>
            <button class="button button-orange va-m">Сохранить</button>
        </form>
    </div>
</div>
{% endblock %}

{% block javascript_footer %}
<script type="text/javascript">
var requestInProcess = false;
$(function() {
    var form = $('#graph-add-weight-form');
    form.find('button').click(function(e) {
        e.preventDefault();

        if (requestInProcess) {
            return false;
        }

        requestInProcess = true;
        var data = form.serializeObject();

        var index = form.attr('data');
        if (currentOffset == -1) {
            currentOffset = points_data.pointers.length - 12;
        }

        points_data.lines[index].y1 = data.weight;
        points_data.pointers[index].y = data.weight;
        points_data.pointers[index].data = true;
        if (index >= 1) {
            points_data.lines[index-1].y2 = data.weight;
        }
        //drawGraph(currentOffset);

        $.post('/ws/diary/weight/', data, function() {
            requestInProcess = false;
            window.location.reload(true);
        });
    });

    var currentOffset = -1;

    $('.icons.prev').click(function() {
        if (currentOffset == -1) {
            currentOffset = points_data.pointers.length - 12;
        }

        var hiddenChild = $('#calories-diagram').children().filter(':visible').filter(':first').prev();
        var hiddenDay = $('#calories-period-list').children().filter(':visible').filter(':first').prev();

        if (hiddenChild.size()) {
            var visibleChild = $('#calories-diagram').children().filter(':visible').filter(':last');
            var visibleDay = $('#calories-period-list').children().filter(':visible').filter(':last');
            $('.icons.next').show();

            for (var x = 0; x < 6; x++) {
                visibleChild.hide();
                visibleDay.hide();
                hiddenChild.show();
                hiddenDay.show();
                visibleChild = visibleChild.prev();
                visibleDay = visibleDay.prev();
                hiddenChild = hiddenChild.prev();
                hiddenDay = hiddenDay.prev();
                currentOffset--;

                if (hiddenChild.size() == 0 || hiddenDay.size() == 0) {
                    $(this).hide();
                    break;
                }
            }

            drawGraph(currentOffset);
        }
    });

    $('.icons.next').click(function () {
        var hiddenChild = $('#calories-diagram').children().filter(':visible').filter(':last').next();
        var hiddenDay = $('#calories-period-list').children().filter(':visible').filter(':last').next();

        if (hiddenChild.size()) {
            var visibleChild = $('#calories-diagram').children().filter(':visible').filter(':first');
            var visibleDay = $('#calories-period-list').children().filter(':visible').filter(':first');
            $('.icons.prev').show();

            for (var x = 0; x < 6; x++) {
                visibleChild.hide();
                visibleDay.hide();
                hiddenChild.show();
                hiddenDay.show();
                visibleChild = visibleChild.next();
                visibleDay = visibleDay.next();
                hiddenChild = hiddenChild.next();
                hiddenDay = hiddenDay.next();
                currentOffset++;

                if (hiddenChild.size() == 0 || hiddenDay.size() == 0) {
                    $(this).hide();
                    break;
                }
            }

            drawGraph(currentOffset);
        }
    });

    $('#calories-diagram .add_data').click(function(e) {
        e.preventDefault();
        var date = $(this).attr('data');
        var dateParts = date.split('-');
        var visibleDate = dateParts[2]  + '.' + dateParts[1] + '.' + dateParts[0];
        $('#diary-calories-form input[name=date]').val(date);

        var header = $('h2.added-products-list');
        var title = header.html().split(' ');
        title[title.length-1] = visibleDate;
        header.html(title.join(' '));

        $('#calories-period-list').children('.current').removeClass('current');
        $('#day-' + date).addClass('current');

        $('div.my_diary').children().not('form').not('script').hide();
        window.scrollBy(0, 80);
    });


    function updateCurrentWeight(e)
    {
        e.preventDefault();

        if (requestInProcess) {
            return false;
        }

        var weight = $('#current-weight').val();
        var re = /^\d+((\.|,)\d+)?$/;

        if (!weight || !re.test(weight)) {
            alert('Неверное значение');
            return false;
        }

        requestInProcess = true;
        $.post('/ws/diary/weight/', {'weight' : weight}, function () {
            requestInProcess = false;
            window.location.reload(true);
        });
    }

    $('#current-weight').keydown(function (e) {
        if (e.keyCode == 13) {
            updateCurrentWeight(e);
        }
    });

    $('#current-weight').siblings('.refresh').click(updateCurrentWeight);

    $('dl.item_content .icon_trash').click(function() {
        var target = $(this);
        var id = target.parent().attr('id').split('-');
        $.post('/ws/diary/delete/', {'id':id[1]}, function (res) {
            target.parents('dl.item_content').filter(':first').prev().remove();
            target.parents('dl.item_content').filter(':first').remove();
            window.location.reload(true);
        });
    });
});
</script>
{% endblock %}