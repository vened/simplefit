{% extends 'common/base.tpl' %}

{% block title %}
<title>Симплфит - Дневник питания</title>
{% endblock %}

{% block css %}
<link rel="stylesheet" type="text/css" href="{{img_host}}/js/ajaxupload/fileuploader.css?{{img_version}}">
{% endblock %}

{% block javascript %}
<script type="text/javascript" src="{{img_host}}/js/ajaxupload/fileuploader.js"></script>
{% endblock %}

{% block content %}
<div class="content wrapper">

<div class="diary">

<div class="username">
    <h1 class="">{{user.first_name}} {{user.last_name}}
        <span class="age">{{_n_rus('%s год', '%s года', '%s лет', user.age)}}</span>
    </h1>

    <div style="display: none;">Россия - Санкт Петербург</div>
</div>

<div class="general_info row25 mb50">
    <div class="span4">
        <dl class="mb20">
            <dt class="black"><b>На сайте:</b></dt>
            <dd>
                {% if user_at_site.years > 0 %}
                {{_n_rus('%s год', '%s года', '%s лет', user_at_site.years)}}
                {% endif %}

                {% if user_at_site.months > 0 %}
                {{_n_rus('%s месяц', '%s месяца', '%s месяцев', user_at_site.months)}}
                {% endif %}

                {% if user_at_site.days > 0 %}
                {{_n_rus('%s день', '%s дня', '%s дней', user_at_site.days)}}
                {% else %}
                первый день
                {% endif %}
            </dd>
        </dl>
        <dl class="mb20">
            <dt class="black"><b>Цель:</b></dt>
            <dd>
                {{user.goal_text}} до {{user.weight_goal}} кг
            </dd>
        </dl>
        <dl>
            <dt class="black"><b>Отношение к вегетарианству:</b></dt>
            <dd>{{user.vegetarianism_text}}</dd>
        </dl>
    </div>
    <div class="span2">
        <div class="avatar">
            {% if main_photo_url %}
            <img src="{{main_photo_url}}" alt="{{user.first_name}} {{user.last_name}}" width="329" />
            {% else %}
            <img src="{{ img_host }}/images/noimage400.png" alt="{{ user.first_name }} {{ user.last_name }}" width="329" />
            {% endif %}

            <div class="photo_counter" style="display: none;">
                <i class="icons icon_photo"></i> 13
            </div>
            <div class="weight">
                <div>Вес</div>
                <div class="fs37">{{current_weight}}кг</div>
            </div>
            <div class="growth">
                <div>Рост</div>
                <div class="fs24">{{user.height}}см</div>
            </div>
        </div>
    </div>
    <div class="span4 txt-r">
        <div class="mb15">Ваш оптимальный вес находится в диапазоне:</div>
        <div class="mb10 fs24 black">от {{optimal_weight_min}} до {{optimal_weight_max}} кг</div>
        <span class="didecated">
            {% if weight_text == 'low' %}
                У вас недостаточный вес
            {% elseif weight_text == 'normal' %}
                У вас нормальный вес
            {% else %}
                У вас избыточный вес
            {% endif %}
        </span>

        <div>Рекомендуется потреблять<br><b class="black">не более {{calories}} ккал</b> в сутки</div>
    </div>
</div>

<div class="yellow_block weight_index" style="display: none;">
    <i class="icons yellow_block_arr1" style="left: 175px"></i>

    <p class="fs18">
        <i>Ваш индекс массы тела: <b>37</b>?????</i>
    </p>

    <p>
        Он значительно выше нормы. Возможно, снижение потребления калорий не приведёт к ожидаемому
        результату. Для получения персональных рекомендаций, обратитесь к специалисту.
    </p>
    <a href="#" class="button button-orange small">Отправить запрос</a>
</div>

<div class="menu">
    <div class="btn-group">
        <span data="calories" class="btn selected">Счетчик калорий</span>
        <span data="weight" class="btn">Записать вес</span>
        <span data="photo" class="btn">Добавить фото</span>
        <span data="social" class="btn">Опубликовать успехи</span>
    </div>
</div>


<div id="calories-section" class="box_border gradient">
    {% include 'common/add_menu.tpl' with {'form_action' : '/ws/diary/calories/'} %}
</div>


<div id="weight-section" class="box_border gradient" style="display: none;">
    <div class="weight_edit">
        <form id="diary-weight-form" class="txt-c">
            <label class="fs18 black" for="weight">Мой вес:</label>
            <input type="text" name="weight" value="" id="weight" class="text width2 normal_big">

            <span class="fs16">Желаемый вес {{user.weight_goal}} кг</span>
            <a href="#" class="button button-green small">Сохранить вес</a>
        </form>
    </div>
</div>


<div id="photo-section" class="box_border gradient" style="display: none;">
    <div class="fl-r mt5">
        <a href="#" id="add-photo-link" class="button button-green small">Сохранить фото</a>
    </div>

    <span id="file-uploader-photo" class="qq-uploader">
        <noscript><p>Ваш браузер не поддерживает javascript</p></noscript>
    </span>
    <span style="display: none;">
        <span class="dl">
            <i class="icons icon_photoupload va-m ml10"></i><span class="ml10">#name#</span>
        </span>
        <a class="ml10" href="#">Удалить</a>
    </span>

    <div class="gray mt5"><em>jpg, jpeg, png. Максимальный размер файла: 800 Кб</em></div>
</div>


<div id="social-section" class="box_border gradient" style="display: none;">
    <div class="add_timeline_read">

        <div class="period">
            <div class="btn-group">
                <a href="#" class="btn" data-diff="{{weight_diff_week}}" data-text="неделю" data-period="week">за неделю</a>
                <a href="#" class="btn" data-diff="{{weight_diff_month}}" data-text="месяц" data-period="month">за месяц</a>
                <a href="#" class="btn" data-diff="{{weight_diff_3months}}" data-text="3 месяца" data-period="3months">за 3 месяца</a>
                <a href="#" class="btn selected" data-diff="{{weight_diff_all}}" data-text="всё время" data-period="all">за всё время</a>
            </div>
            <div class="fl-r" style="display: none;">
                Период от
                <div style="margin-right: 15px" class="data_selector">
                    <input type="text" class="text normal" placeholder="31.10.2012">
                    <i class="icons calendar"></i>
                </div>
                до
                <div class="data_selector">
                    <input type="text" class="text normal" placeholder="31.10.2012">
                    <i class="icons calendar"></i>
                </div>
            </div>
        </div>

        <div class="box_border mb20">
            <table>
                <tr>
                    <td class="photo_box">
                        <div class="photo">
                            <img id="photo-social" src="{% if last_photo %}{{last_photo.photo_url}}{% else %}{{img_host}}/images/logo.png{% endif %}" width="136" />

                            <div class="photo_counter" style="display: none;">
                                <i class="icons icon_photo"></i> 13
                            </div>
                        </div>
                        <div class="mt5 form_text">
                            <label><input type="checkbox" value="1" name="no_photo" class="checkbox">&nbsp;<span class="fs12">без фото</span></label>
                        </div>
                    </td>
                    <td>
                        <div class="mb5 fs24 black">
                            <div class="diff-null" {% if weight_diff_all != 0 %}style="display: none;"{% endif %}>
                                Осталось
                                {% if needed_weight_diff > 0 %}
                                    набрать
                                {% else %}
                                    сбросить
                                {% endif %}
                                {{needed_weight_diff|abs}} кг
                            </div>
                            {# weight_diff_all is default value. Then modified by javascript #}
                            <div class="diff-not-null" {% if weight_diff_all == 0 %}style="display: none;"{% endif %}>
                                <span id="period-text" style="display: none;">За неделю я</span>
                                <span id="all-period-text">Я</span>
                                <span class="weight-loss" {% if weight_diff_all > 0 %}style="display: none;"{% endif %}>
                                    {% if user.gender == 'M' %}похудел{% else %}похудела{% endif %} на
                                </span>
                                <span class="weight-gain" {% if weight_diff_all < 0 %}style="display: none;"{% endif %}>
                                    {% if user.gender == 'M' %}набрал{% else %}набрала{% endif %}
                                </span>
                                <span class="yellow"><span class="weight">{{weight_diff_all}}</span> кг</span>
                            </div>
                        </div>
                        <div class="mb5 fs18">
                            <i class="current-weight-text not-null" {% if weight_diff_all == 0 %}style="display: none;"{% endif %}>
                                {#Мой текущий вес <b class="black">{{current_weight}} кг</b>,#}
                                Осталось
                                {% if needed_weight_diff > 0 %}
                                    набрать
                                {% else %}
                                    сбросить
                                {% endif %}
                                <b class="black">{{needed_weight_diff|abs}} кг</b>
                            </i>
                            <i class="current-weight-text null" {% if weight_diff_all != 0 %}style="display: none;"{% endif %}>
                                SimpleFit - сервис для ведения дневников питания и контроля за диетами.
                            </i>
                        </div>
                        <textarea id="user-comment" class="textarea" placeholder="Ваш коментарий к публикации"></textarea>

                        <div>
                            <span id="uploader-photo-social" class="qq-uploader">
                                <noscript><p>Ваш браузер не поддерживает javascript</p></noscript>
                            </span>
                            <i>Если хотите опубликовть другую фотографию</i>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div class="clearfix">
            <span class="fl-l fs18 black" style="padding: 8px 0 0 0">Выберите соц. сеть для публикации:</span>
            <ul class="social_publick fl-l">
                <li class="selected" data="vk"><a class="social-ico vk" href="#"></a></li>
                <li data="fb"><a class="social-ico fb" href="#"></a></li>
                {#<li><a class="social-ico tw" href="#"></a></li>#}
                <li data="mm"><a class="social-ico mm" href=""></a></li>
                {#<li><a class="social-ico ya" href=""></a></li>#}
                {#<li><a class="social-ico gp" href=""></a></li>#}
                {#<li><a class="social-ico od" href=""></a></li>#}
            </ul>
            <button id="social-publish" class="fl-r button button-green small">Опубликовать</button>
        </div>

    </div>
</div>


<div class="timeline">

    {% for date,records in diary_data %}

    {% if not loop.first %}
    <div class="timeline_delimitter">
        <i class="va-m">{{date|date('d.m.Y')}}</i> <b class="fs14 yellow va-m">+2589 ккал</b>
    </div>
    {% endif %}

    <div class="left_col" style="padding-top: 90px;">

        {% for data in records.left %}
        <div class="box_border item">
            <i class="arrow"></i>

            <div id="diary-{{ data.id }}" class="edit_block">
                {% if data.type != 'photo' and data.type != 'weight' %}
                <i class="icons icon_edit2">
                    <div class="tooltip edit-link">Редактировать</div>
                </i>
                {% endif %}
                <i class="icons icon_trash">
                    <div class="tooltip delete-link">Удалить</div>
                </i>
            </div>
            <div class="data">{{data.created|date('d.m.Y')}}</div>
            {% if data.type == 'calories' %}
            <dl class="item_content">
                <dt>
                    <i class="icons icon_note"></i>
                </dt>
                <dd>
                    <h3>
                        {% if data.food_type == 'breakfast' %}
                            Завтрак:
                        {% elseif data.food_type == 'breakfast2' %}
                            Поздний завтрак:
                        {% elseif data.food_type == 'lunch' %}
                            Обед:
                        {% elseif data.food_type == 'lunch2' %}
                            Полдник:
                        {% elseif data.food_type == 'dinner' %}
                            Ужин:
                        {% elseif data.food_type == 'dinner2' %}
                            Поздний ужин:
                        {% elseif data.food_type == 'water' %}
                            Вода:
                        {% endif %}
                    </h3>

                    <div class="mb5">
                        {% for product in data.products_data %}
                            <span style="display: none;">{{ product.id }}</span> {{product.weight}}г - {{product.name}} <b>+{{product.calories}} ккал</b>;
                        {% endfor %}
                    </div>
                    <b>Итого:</b> <b class="yellow">+{{data.data}} ккал</b>
                </dd>
            </dl>

            {% elseif data.type == 'weight' %}
            <dl class="item_content">
                <dt>
                    <i class="icons icon_bookmark"></i>
                </dt>
                <dd>
                    <h3>Актуализация веса: <b class="yellow">{{ data.data }} кг</b></h3>
                </dd>
            </dl>

            {% elseif data.type == 'photo' %}
            <dl class="item_content">
                <dt>
                    <i class="icons icon_photoupload"></i>
                </dt>
                <dd>
                    <h3>Новое фото</h3>
                </dd>
            </dl>
            <p>
                <img src="{{data.photo_url}}">
            </p>

            <p>
                {{data.photo_descr}}
            </p>
            {% endif %}
        </div>
        {% endfor %}
    </div>

    <div class="right_col">

        {% for data in records.right %}
        <div class="box_border item">
            <i class="arrow"></i>

            <div id="diary-{{ data.id }}" class="edit_block">
                {% if data.type != 'photo' and data.type != 'weight' %}
                <i class="icons icon_edit2">
                    <div class="tooltip edit-link">Редактировать</div>
                </i>
                {% endif %}
                <i class="icons icon_trash">
                    <div class="tooltip delete-link">Удалить</div>
                </i>
            </div>
            <div class="data">{{data.created|date('d.m.Y')}}</div>
            {% if data.type == 'calories' %}
                <dl class="item_content">
                    <dt>
                        <i class="icons icon_note"></i>
                    </dt>
                    <dd>
                        <h3>
                            {% if data.food_type == 'breakfast' %}
                                Завтрак:
                            {% elseif data.food_type == 'breakfast2' %}
                                Поздний завтрак:
                            {% elseif data.food_type == 'lunch' %}
                                Обед:
                            {% elseif data.food_type == 'lunch2' %}
                                Полдник:
                            {% elseif data.food_type == 'dinner' %}
                                Ужин:
                            {% elseif data.food_type == 'dinner2' %}
                                Поздний ужин:
                            {% elseif data.food_type == 'water' %}
                                Вода:
                            {% endif %}
                        </h3>

                        <div class="mb5">
                            {% for product in data.products_data %}
                                <span style="display: none;">{{product.id}}</span> {{product.weight }}г - {{product.name}} <b>+{{product.calories}} ккал</b>;
                            {% endfor %}
                        </div>
                        <b>Итого:</b> <b class="yellow">+{{data.data}} ккал</b>
                    </dd>
                </dl>

            {% elseif data.type == 'weight' %}
                <dl class="item_content">
                    <dt>
                        <i class="icons icon_bookmark"></i>
                    </dt>
                    <dd>
                        <h3>Актуализация веса: <b class="yellow">{{data.data}} кг</b></h3>
                    </dd>
                </dl>

            {% elseif data.type == 'photo' %}
                <dl class="item_content">
                    <dt>
                        <i class="icons icon_photoupload"></i>
                    </dt>
                    <dd>
                        <h3>Новое фото</h3>
                    </dd>
                </dl>
                <p>
                    <img src="{{data.photo_url}}">
                </p>

                <p>
                    {{data.photo_descr}}
                </p>
            {% endif %}
        </div>
        {% endfor %}

    </div>
    {% endfor %}

    {% if show_next_page %}
    <div class="timeline_delimitter">
        <a href="#">Больше записей</a>
    </div>
    {% endif %}

</div>


<div class="box_border" style="margin-top: -55px">
    <div class="mb15 fs20 txt-c black">
        <span class="yellow"><b>{{user.first_name}} {{user.last_name}}</b></span> <b>зарегистрировался</b> с целью "{{user.goal_text}}"
    </div>
    <div class="txt-c">
        <i>{{user.created|date("d.m.Y H:i")}}</i>
    </div>
</div>

</div>


</div>
{% endblock %}

{% block javascript_footer %}
<script type="text/javascript">

function getProducts(dataItem)
{
    var productsData = dataItem.find('div.mb5').text().split(';');
    var products = [];

    for (var x in productsData) {
        var data = $.trim(productsData[x]);

        if (!data) {
            continue;
        }

        //var parts = data.split(' - ');
        var re = /(\d+)\s+(\d+)г\s+\-\s+(.+?)\s+\+(\d+)\s+ккал/;
        var result = re.exec(data);

        var product = {
            'id' : parseInt(result[1]),
            'weight' : parseFloat(result[2]),
            'name' : result[3],
            'calories' : parseFloat(result[4])
        };

        products.push(product);
    }

    return products;
}

$(function() {
    $('div.menu .btn-group').click(function(e) {
        var target = $(e.target);
        if (target.attr('data') == 'social') {
            //return false;
        }

        var old = target.siblings('.selected')
        $('#'+old.attr('data')+'-section').hide();
        old.removeClass('selected');
        target.addClass('selected');
        $('#' + target.attr('data') + '-section').show();
    });

    $('#diary-weight-form .button').click(function(e) {
        e.preventDefault();
        var data = $('#diary-weight-form').serializeObject();
        var re = /^\d+((\.|,)\d+)?$/;

        if (!data['weight'] || !re.test(data['weight'])) {
            alert('Неверное значение');
            return false;
        }

        $.post('/ws/diary/weight/', data, function() {
            window.location.reload(true);
        });
    });

    var uploader = new qq.FileUploader({
        element: document.getElementById('file-uploader-photo'),
        action: 'http://' + document.location.host + '/ws/upload/photo/',
        template: '<span class="qq-upload-drop-area"><span>Drop files here to upload</span></span>' +
                '<span class="qq-upload-button"><a href="#" class="button button-gray">Выберите файл</a></span>' +
                '<ul class="qq-upload-list"></ul>',
        debug: false,
        onComplete: photoUploadComplete
    });

    var uploader2 = new qq.FileUploader({
        element:document.getElementById('uploader-photo-social'),
        action:'http://' + document.location.host + '/ws/upload/photo/',
        template:'<span class="qq-upload-drop-area"><span>Drop files here to upload</span></span>' +
                '<span class="qq-upload-button"><a href="#" class="button button-gray">Выберите файл</a></span>' +
                '<ul class="qq-upload-list"></ul>',
        debug:false,
        onComplete:function(id, fileName, responseJSON) {
            $.post('/ws/diary/photo/', function (response) {
                $('ul.qq-upload-list').empty();
                if (response && response.photo_url) {
                    $('#photo-social').attr('src', response.photo_url);
                }
            }, 'json');
        }
    });

    $('#add-photo-link').click(function(e) {
        e.preventDefault();
        $.post('/ws/diary/photo/', function(response) {
            $('ul.qq-upload-list').empty();
            window.location.reload(true);
        });
    });

    $('div.timeline').click(function(e) {
        var target = $(e.target);

        if (target.hasClass('icon_trash') || target.hasClass('delete-link')) {
            var id = target.parents('div.edit_block').attr('id').split('-');
            $.post('/ws/diary/delete/', {'id' : id[1]}, function(res) {
                target.parents('div.item').filter(':first').remove();
            });
        }

        if (target.hasClass('icon_edit2') || target.hasClass('edit-link')) {
            var id = target.parents('div.edit_block').attr('id').split('-');
            diaryId = id[1];
            var dataItem = target.parents('div.edit_block').siblings('dl');
            var date = dataItem.prev().html().split('.');
            var type = $.trim(dataItem.find('h3').html().replace(':', ''));
            var products = getProducts(dataItem);
            window.date = date[2] + '-' + date[1] + '-' + date[0];
            window.action = '/ws/diary/edit/';

            var header = $('h2.added-products-list');
            var oldVal = header.html();
            $('select[name=food_type]').children().each(function(index, element) {
                var el = $(element);
                el.attr('selected', false);

                if (el.html() == type) {
                    el.attr('selected', true);
                    $('a.select2-choice').children('span').filter(':first').html(type);
                }
            });

            var productsList = $('#products-list');
            var item = productsList.children().not(':first').remove();

            for (var x in products) {
                drawAddedProduct(products[x]);
            }

            $('html, body').animate({scrollTop:$("#calories-section").offset().top}, 200);
        }
    });

    var socialTextHeader = '';
    var socialText = '';

    $('#social-section .btn-group a.btn').click(function(e) {
        e.preventDefault();
        var target = $(this);
        target.addClass('selected').siblings().removeClass('selected');
        var diff = target.attr('data-diff').replace(',', '.');
        var periodName = target.attr('data-period');
        var periodText = target.attr('data-text');

        if (!diff || diff == 0) {
            $('div.diff-not-null').hide();
            $('div.diff-null').show();
            $('i.current-weight-text.not-null').hide();
            $('i.current-weight-text.null').show();
            socialTextHeader = $('div.diff-null').text().replace(/\s+/ig, ' ');
            socialText = $('i.current-weight-text.null').text().replace(/\s+/ig, ' ');
        }
        else {
            $('div.diff-null').hide();

            if (periodName == 'all') {
                $('#period-text').hide();
                $('#all-period-text').show();
                socialTextHeader = $('#all-period-text').html();
            }
            else {
                var header = $('#period-text');
                $('#all-period-text').hide();
                var text = header.html().split(' ');
                text[1] = periodText;
                socialTextHeader = text[0] + ' ' + text[1] + ' ' + text[text.length-1];
                header.html(socialTextHeader);
                header.show();
            }

            if (diff < 0) {
                $('span.weight-gain').hide();
                $('span.weight-loss').show();
                socialTextHeader += ' ' + $.trim($('span.weight-loss').html());
            }
            else {
                $('span.weight-loss').hide();
                $('span.weight-gain').show();
                socialTextHeader += ' ' + $.trim($('span.weight-gain').html());
            }

            socialTextHeader += ' ' + Math.abs(diff) + ' кг';

            $('span.weight').html(Math.abs(diff));
            $('div.diff-not-null').show();
            $('i.current-weight-text.null').hide();
            $('i.current-weight-text.not-null').show();
            socialText = $('i.current-weight-text.not-null').text().replace(/\s+/ig, ' ');
        }
    });

    $('#social-section .btn-group a.btn.selected').click();

    $('ul.social_publick li').click(function(e) {
        e.preventDefault();
        $(this).addClass('selected').siblings('.selected').removeClass('selected');
    });

    $('#social-publish').click(function(e) {
        e.preventDefault();
        var social = $('ul.social_publick li.selected').attr('data');
        var data = {
            'title': socialTextHeader,
            'text':socialText + '\n' + $.trim($('#user-comment').val()),
            'image': $('#photo-social').attr('src')
        };

        var noPhoto = $('input[name=no_photo]').filter(':checked');
        if (noPhoto.size() && noPhoto.val()) {
            data.image = 'http://images.simplefit.ru/images/logo.png';
        }

        switch (social) {
            case 'vk':
                vkontakte.postResults(data);
                break;
            case 'fb':
                facebook.postResults(data);
                break;
            case 'mm':
                mirmailru.postResults(data);
                break;
        }
    });
});

function photoUploadComplete(id, fileName, responseJSON) {
    if (!('error' in responseJSON)) {
        //window.location.reload();
    }
}
</script>
{% endblock %}