{% extends 'common/base.tpl' %}

{% block content %}
<div class="content wrapper">
    {% if template == "remindPassword" %}
    <h1 class="mb15 txt-c">Новый Пароль</h1>
    <form method='post' action='?action=changePassword' class="txt-c" id='pass_form'>
        <input name='password' class="txt-c" style='font-size:40px;'><br>
        <input type='hidden' name='sig' class='mb50' value='{{sig}}' >
        <button class='button button-orange large mt20' type='submit' id='pass_form_submit'>Сохранить</button>
    </form>
    {% else %}
        <h1 class="mb30 txt-c">Ссылка устарела</h1>
    Ссылка в письме &#8211; одноразовая. Вы по ней уже переходили и она устарела.
    Если вы хотите попать на сайт &#8211; воспользуйтесь <a href='#' class='login-link'>формой входа</a>

    {% endif %}

</div>
{% endblock %}