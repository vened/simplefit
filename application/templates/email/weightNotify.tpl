{% extends 'email/base.tpl' %}

{% block content %}
<div style="font-family: 'Trebuchet MS';font-size: 18px;text-align: center;color: #7b7b7b;">
    <i>Самоконтроль - лучший способ скорейшего достижения цели.</i>
</div>
<div style="font-family: 'Trebuchet MS';font-size: 14px;color: #696969;margin: 20px 0 25px 0">
    Регулярно заполняя дневник веса и питания вы получаете возможность отслеживать эффективность вашего меню за выбранный период или конкретный день.
</div>

<div style="font-family: 'Trebuchet MS';font-size: 18px;text-align:center;margin: 20px 0 25px 0">
    <a href="{{url}}" style="text-decoration: none;background-color: #5EB234;background-image: linear-gradient(to bottom, #92C544, #11951C);
                                    border: 1px solid #56A64F;border-radius: 3px;box-shadow: 0 1px 0 0 #D4E8B5 inset;
                                    display:inline-block;height: 40px;line-height: 40px;padding: 0 40px;">
        <span style="font-size: 18px;font-weight: bold;color: #fff;text-shadow: 0 1px 0 #218317">Указать вес</span>
    </a>
</div>
{% endblock %}