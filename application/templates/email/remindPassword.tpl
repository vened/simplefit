{% extends 'email/base.tpl' %}

{% block content %}
<div style="font-family: 'Trebuchet MS';font-size: 18px;text-align: center;color: #7b7b7b;">
    Восстановление доступа
</div>
<div style="font-family: 'Trebuchet MS';font-size: 14px;color: #696969;margin: 20px 0 25px 0">
    Для восстановления пароля перейдите по
    <a style="color: #04a9ff; text-decoration:underline;" href="{{url}}">этой сылке</a>
    или скопируйте ссылку и вставьте в адресную строку браузера, после чего следуйте инструкциям на сайте:
</div>

<div style="font-family: 'Trebuchet MS';font-size: 14px;color: #696969;margin: 20px 0 25px 0">
    <a style="color: #04a9ff; text-decoration:underline;" href="{{url}}">{{url}}</a>
</div>

{% endblock %}