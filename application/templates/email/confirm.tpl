{% extends 'email/base.tpl' %}

{% block content %}
<div style="font-family: 'Trebuchet MS';font-size: 18px;text-align: center;color: #7b7b7b;">
    Добро пожаловать на Симплфит!
</div>
    <div style="font-family: 'Trebuchet MS';font-size: 14px;color: #696969;margin: 20px 0 25px 0">
        Для начала использования сервиса необходимо подтвердить адрес электронной почты.
        Для подтверждения перейдите по
        <a style="color: #04a9ff; text-decoration:underline;" href="{{url}}">этой сылке</a>
        или скопируйте ссылку и вставьте в адресную строку браузера:
    </div>

    <div style="font-family: 'Trebuchet MS';font-size: 14px;color: #696969;margin: 20px 0 25px 0">
        <div style="color: #04a9ff; text-decoration:underline;">{{url}}</div>
    </div>

{% endblock %}