
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
</head>
<body>


<table width="700" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff"
       style="border-collapse:collapse;background:#ffffff;box-shadow: 0 5px 5px rgba(0,0,0,.45)">
    <tr>
        <td width="75">&nbsp;</td>
        <td>
            <h1 style="font-family: 'Trebuchet MS';font-size: 30px;font-weight: normal;text-align: center;color: #2b2b28;margin: 40px 0 25px">
                <i>Здравствуйте{% if name %}, {{name}}{% endif %}!</i>
            </h1>
            {% block content %}
            {% endblock %}
            <div style="font-family: 'Trebuchet MS';font-size: 14px;color: #2b2b28;margin: 20px 0 25px 0">
                <a style="display:inline-block;vertical-align: middle" >
                    <img src="{{logo_img}}" alt="">
                </a>
                <span style="display:inline-block;vertical-align: middle;margin: 0 0 0 20px">
                    С уважением<br>
                    <div style="color: #04a9ff;text-decoration: none" >Simplefit.ru</div>
                </span>
            </div>
            <div style="font-family: 'Trebuchet MS';font-size: 12px;color: #696969;margin: 12px 0 25px 0">
                Для того, чтобы отписаться от рассылки, перейдите на страницу
                <a style="color: #04a9ff;text-decoration: none" href="{{url}}&unsubscribe=on">настройки оповещений</a>, или обратитесь в техническую поддержку: feedback@simplefit.ru
            </div>
            <img src="{{ view_url }}" >
        </td>
        </td>
        <td width="75">&nbsp;</td>
    </tr>
</table>


</body>
</html>

