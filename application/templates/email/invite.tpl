<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
</head>
<body style="background:#eee;">


<table width="700" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff"
       style="border-collapse:collapse;background:#ffffff;box-shadow: 0 5px 5px rgba(0,0,0,.45); margin: 0 auto;">
    <tr>
        <td width="75">&nbsp;</td>
        <td>
            <h1 style="font-family: 'Trebuchet MS';font-size: 25px;font-weight: normal;color: #2b2b28;margin: 40px 0 25px">
                <i>Здравствуйте!</i>
            </h1>

            <div style="font-family: 'Trebuchet MS';font-size: 16px;color: #7b7b7b;">
                <i>{{ name }} указала что вы её друг и пригласила вас на Simplefit.</i>
            </div>

            <div style="font-family: 'Trebuchet MS';font-size: 14px;color: #696969;margin: 20px 0 25px 0">
                <p>Simplefit - это сервис персональных рекомендаций по правильному питанию и слежению за динамикой веса тела.</p>
            </div>

            <div style="font-family: 'Trebuchet MS';font-size: 18px;margin: 20px 0 25px 0">
                <a href="{{ invite }}" style="text-decoration: none;background-color: #5EB234;background-image: linear-gradient(to bottom, #92C544, #11951C);
                                    border: 1px solid #56A64F;border-radius: 3px;box-shadow: 0 1px 0 0 #D4E8B5 inset;
                                    display:inline-block;height: 40px;line-height: 40px;padding: 0 40px;">
                    <span style="font-size: 18px;font-weight: bold;color: #fff;text-shadow: 0 1px 0 #218317">Управлять своим телом</span>
                </a>
            </div>


            <div style="font-family: 'Trebuchet MS';font-size: 14px;color: #696969;margin: 12px 0 15px 0">
                Хотите похудеть, набрать вес или начать правильно питаться? Воспользуйтесь сервисами Simplefit: </br></br>
                - Рекомендации по питанию на одну или две недели </br></br>
                - Дневник питания</br></br>
                - План питания
            </div>
            <div style="border-top: 1px solid #eee; width: 100%"></div>
            <div style="font-family: 'Trebuchet MS';font-size: 14px;color: #2b2b28;margin: 10px 0 10px 0">
                <div style="display:inline-block;vertical-align: middle">
                    <img src="{{ logo_img }}" alt="">
                </div>
                <span style="display:inline-block;vertical-align: middle;margin: 0 0 0 10px">
                    Будьте здоровы. Управляйте своим весом.<br>
                    Simplefit.ru
                </span>
            </div>
            <div style="border-top: 1px solid #eee; width: 100%"></div>
            <div style="font-family: 'Trebuchet MS';font-size: 12px;color: #696969;margin: 12px 0 25px 0">
                Если письмо получено вами по ошибке, не переходите по ссылкам в этом письме.</br>
                Для того, чтобы отписаться от рассылки, нажмите на ссылку
                <a style="color: #04a9ff;text-decoration: none" href="{{ unsubscribe }}">отписаться</a> или обратитесь в техническую поддержку: feedback@simplefit.ru
        </td>
        <td width="75">&nbsp;</td>
    </tr>
</table>
<img src="{{ view_url }}" >

</body>
</html>