{% extends 'admin/common_head.tpl' %}
{% block css %}
.mybtn-search {position:relative; top: -5px;}
.mybtn-radio { margin:0px;}
{% endblock %}
{% block body %}
<form id="search_form">
<input class="input-xxlarge" type="text" placeholder="Название" name="q" value='{{q}}'> <button class="btn btn-primary mybtn-search" type="submit">Найти</button><br>
    <div class="btn-group" data-toggle="buttons-radio">
        <button type="button" class="btn mybtn-radio mbs {% if type == 'new' %}active{% endif %}" value='new' id="new">Новые</button>
        <button type="button" class="btn mybtn-radio mbs {% if type == 'approved' %}active{% endif %}" value='approved'>Одобренные</button>
        <button type="button" class="btn mybtn-radio mbs {% if type == 'rejected' %}active{% endif %}" value='rejected'>Отключенные</button>
        <button type="button" class="btn mybtn-radio mbs {% if type == 'all' %}active{% endif %}" value='all'>Все</button>
    </div>
    <input type="hidden" name="type" value="{{type}}" id="form_hidden">
    <input type="hidden" name="page" value="{{current_page}}" >
</form>
<hr>
total: {{total}}<br><br>
<form method='post' action='/goods/edit' id="save-form">
    <input type="hidden" name="return" value="page={{current_page}}&q={{q}}&type={{type}}" >
    <table class="table table-striped" style='width:400px;'>
        <tr>
            <th>Название</th>
            <th>Белки</th>
            <th>Жиры</th>
            <th>Углеводы</th>
            <th>Энергия</th>
            <th>user_id</th>
            <th></th>
        </tr>
        {%for item in data %}
        <tr>
            <td><textarea class="input-xlarge" type="text" name="data[{{item.id}}][name]" >{{item.name}}</textarea></td>
            <td><input class="input-mini" type="text" name="data[{{item.id}}][proteins]"      value="{{item.proteins}}"></td>
            <td><input class="input-mini" type="text" name="data[{{item.id}}][fats]"          value="{{item.fats}}"></td>
            <td><input class="input-mini" type="text" name="data[{{item.id}}][carbohydrates]" value="{{item.carbohydrates}}"></td>
            <td><input class="input-mini" type="text" name="data[{{item.id}}][calories]"      value="{{item.calories}}"></td>
            <td>{{item.user_id}}</td>
            <td>
                <div class="btn-group" data-toggle="buttons-radio">
                    <button type="button" class="btn mybtn-radio rd-ok {% if item.approved == true %} active btn-success {% endif %}"
                            id="{{item.id}}-ok" next='{{item.id}}-fail' oid='{{item.id}}'>V</button>
                    <button type="button" class="btn mybtn-radio rd-fail {% if item.rejected == true %} active btn-danger {% endif %}"
                            id="{{item.id}}-fail" next='{{item.id}}-ok' oid='{{item.id}}'>X</button>
                    <input type="hidden" name="data[{{item.id}}][moderation]"  value='{{item.moderation}}' id="{{item.id}}-moder">
                </div>
            </td>

        </tr>
        {% endfor %}

    </table>
</form>
<button type="submit" class="btn" id="save-table">Сохранить</button>
<hr>
<div class="pagination">
    <ul>
        <li {% if current_page - 1 < 0 %}class="disabled"{% endif %}><a href="?page={{current_page - 1}}&q={{q}}&type={{type}}"> << </a></li>
    {% for p in pager %}
        <li {% if p == current_page %} class="disabled" {% endif %}><a href="?page={{p}}&q={{q}}&type={{type}}">{{p+1}}</a></li>
    {% endfor %}
        <li {% if current_page == total_pages - 1 %}class="disabled"{% endif %}><a href="?page={{current_page + 1}}&q={{q}}&type={{type}}"> >> </a></li>
    </ul>
</div>
<script>
    $(function(){
        $(".rd-ok").click(function(e){
            var btn = $("#"+e.currentTarget.id);
            btn.addClass("btn-success");
            $("#" + btn.attr('next')).removeClass("btn-danger");
            $("#" + btn.attr("oid") + "-moder").attr("value",'approved');
        });

        $(".rd-fail").click(function(e){
            var btn = $("#"+e.currentTarget.id);
            btn.addClass("btn-danger");
            $("#" + btn.attr('next')).removeClass("btn-success");
            $("#" + btn.attr("oid") + "-moder").attr("value",'rejected');
        });

        $(".mbs").click(function(e){
            $("#form_hidden").attr("value", e.currentTarget.value);
            $("#search_form").submit();
        });

        $("#save-table").click(function(e){
            $("#save-form").submit();
        })

    });
</script>

{% endblock %}