{% extends 'admin/plan/common/tabs.tpl' %}
{% block body_content %}

<div class="tab-pane active" id='new'>
    <form method='POST' action='?action=add'>
        {% include 'admin/plan/common/form.tpl' %}
        <input type='submit' value='Add' class='btn btn-success'>
    </form>
</div>


{% endblock %}