{% extends 'admin/plan/common/tabs.tpl' %}
{% block body_content %}<style>
    .food input {margin:0;}
</style>
    <div class="container-fluid" style='padding:0px;'>
    <div class="row-fluid">
    <div class="span6">
        <h2>Параметры</h2>
        <form method='POST' action='?id={{item.id}}'>
            {% include 'admin/plan/common/form.tpl' %}
            <input type='submit' value='Edit' class='btn btn-success'>
        </form>
    </div>
    <div class="span6">

        {% include 'admin/plan/common/productlist.tpl' %}
        <hr>
        Добавить: <input type='text' id='autocomplit' name='autoc'>

        <table class='table table-striped food food_edit' style='width:800px;'>

        </table>
        <style>
            .food_edit form,button {margin:0}
        </style>
    </div>
    </div>
    </div>




<script>
$(function(){
    $("#autocomplit").keyup(function(e){
        if(e.target.value.length < 3)return

        $.ajax({
            url: "/plan/search?q="+ e.target.value
        }).done(function(data) {

            add_e(data)
        });
    })
    function add_e(data){

        $(".food_edit").html("");
        for(i in data){
            var e = data[i];
            var html =
                "<tr id='w_"+ e["id"] +"'><td>"+e["name"]+"</td>"+
                "<td>"+
                "<form action='/plan/addFood' method='Post'>"+
                "<input type='text' name='weight' data-id='"+e['id']+"' data-call='"+e["calories"]+"' class='input-call input-small' value='100'>г "+
                "<button type='submit' class='btn btn-success'><i class='icon-plus icon-white'></i></button>"+
                "<span>"+e["calories"]+"</span>  ккал"+
                "<input name='id' type='hidden' value='"+e["id"]+"'>" +
                "<input name='rec_id' type='hidden'  value='{{ item.id }}'>" +
                "</form>" +
                "</td></tr>";
            $(".food_edit").html($(".food_edit").html() + html);
            $(".food_edit .input-call").keyup(function(e){
                var val = parseFloat(e.target.value)*parseFloat($(e.target).data("call")) / 100 ;
                var id = $(e.target).data("id")
                $("#w_" + id + " span").text(val);


            })
        }
    }

})
</script>
{% endblock %}