<style>
    td.table-pure{
        border:none;
    }
</style>


<div class="tabbable tabs-below">
    <ul class="nav nav-tabs">
        {% for i in statuses %}
            <li {% if i.active %}class="active"{% endif %}><a href="?status={{ i.link }}" >{{ i.name }} ({{ i.total }})</a></li>
        {% endfor %}
    </ul>
    <div class="tab-content">


        <div class="container-fluid">
            <div class="row-fluid">

                <div class='span10'>
                    <div class="pagination">
                        <ul>
                            <li {% if page == 0 %}class="active"{% endif %}><a href="?{{ pre_link }}">Prev</a></li>
                            {%  for i in paginator %}
                                {% if  i.page == page %}
                                    <li class="active" ><a href="#" >{{ i.page +1 }}</a></li>
                                {%  else %}
                                    <li><a href="?{{ params }}&page={{ i.page }}">{{ i.page +1 }}</a></li>
                                {% endif %}

                            {% endfor %}

                            <li {% if page == max_page %}class="active"{% endif %}><a href="?{{ next_link }}">Next</a></li>
                        </ul>
                    </div>

                    <table class='table table-striped'>
                        <tr>
                            <th>id</th>
                            <th>пол</th>
<!--                            <th>рост</th>
                            <th>Нач. Вес</th>
                            <th>Цел. Вес</th>
                            <th>Тек. Вес</th> -->
                            <th>Время</th>
                            <th>Активность</th>
                            <th>Вегетарианство</th>
                            <th>Продукты</th>
                            <th>&nbsp;</th>
                        </tr>
                        {% for item in list %}
                            <tr class='list_row' data-id='{{item.id}}'>
                                <td><a href='/plan/edit?id={{item.id}}'>{{item.id}}</a></td>

                                <td>{{item.gender}}</td>
<!--                                <td>{{item.height}}</td>
                                <td>{{item.start_weight}}</td>
                                <td>{{item.goal_weight}}</td>
                                <td>{{item.current_weight}}</td>
                                -->
                                <td>{{item.food}}</td>
                                <td>{{item.act}}</td>
                                <td>{{item.veg}}</td>
                                <td>
                                    <table>
                                        {% for i in item.data %}
                                            <tr>
                                                <td class='table-pure' style='background-color: transparent;' >{{ i.2 }}г / {{ i.3 }}ккал</td>
                                                <td class='table-pure' style='background-color: transparent;' >{{ i.1 }}</td>
                                            </tr>
                                        {% endfor %}
                                    </table>

                                </td>
                                <td style='width:150px;'>
                                    <div>
                                        <a href="/plan/approve?id={{ item.id }}" class='btn btn-success {% if item.status == 'Approved' %}disabled{% endif %}'><i class='icon-ok icon-white'></i></a>
                                        <a href="/plan/reject?id={{ item.id }}" class='btn btn-danger {% if item.status == 'Rejected' %}disabled{% endif %}'><i class='icon-remove icon-white'></i></a>
                                        <a href="/plan/pay?id={{ item.id }}" class='btn btn-warning {% if item.user_id == 0 %}disabled{% endif %}'><i class='icon-star icon-white'></i></a>
                                    </div>
                                </td>

                            </tr>
                        {% endfor %}
                    </table>
                </div>

                <div class='span2'>
                    <ul class="nav nav-list">
                        {%  for i in selects %}
                            <li class="nav-header">{{ i.name }}</li>
                            {% for j in i.list %}
                                <li {% if j.active %}class="active"{% endif %}><a href="?{{ j.link }}" >{{ j.name }} ({{ j.total }})</a></li>
                            {% endfor %}
                        {% endfor %}
                    </ul>
                </div>

            </div>
        </div>
        <div class="pagination">
            <ul>
                <li {% if page == 0 %}class="active"{% endif %}><a href="?{{ pre_link }}">Prev</a></li>
                {%  for i in paginator %}
                    {% if  i.page == page %}
                        <li class="active" ><a href="#" >{{ i.page +1 }}</a></li>
                    {%  else %}
                        <li><a href="?{{ params }}&page={{ i.page }}">{{ i.page +1 }}</a></li>
                    {% endif %}

                {% endfor %}

                <li {% if page == max_page %}class="active"{% endif %}><a href="?{{ next_link }}">Next</a></li>
            </ul>
        </div>


    </div>
</div>

