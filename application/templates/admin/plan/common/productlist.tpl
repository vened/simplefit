<h2>Продукты</h2>
<table class='table table-striped food' '>
<tr>
    <td colspan=4>
        всего каллорий: {{ item.calories }}
    </td>
</tr>
<tr>
    <td>Название</td>
    <td>Вес</td>
    <td>Калории</td>
    <td>&nbsp;</td>
</tr>
{% for i in food %}
    <tr>
        <td>{{ i.1 }}</td>
        <td><input type='text' value='{{ i.2 }}' g100='{{i.3/i.2}}'></td>
        <td>{{ i.3 }}</td>
        <td>
            <a href='/plan/del?id=527&product_id={{ i.0 }}' class='btn btn-danger' ><i class='icon-remove icon-white'></i></a>

        </td>
    </tr>
{% endfor %}
</table>
