<table class='table table-striped' style='width: 800px;'>
    <tr>
        <td><label>Пол: </label></td><td>
            <div class="btn-group" data-toggle="buttons-radio">
                <button type="button" class="btn mybtn-radio mbs " value="M">M</button>
                <button type="button" class="btn mybtn-radio mbs " value="F">F</button>
            </div>
        </td>
    </tr>
    <tr>
        <td><label>Время: </label></td><td>
            {% for a in food_type %}
                <label class="radio">
                    <input type="radio" name="food_type" value="{{a.v}}" {% if a.v == item.food_type %} checked {% endif %}  >
                    {{a.text}}
                </label>
            {% endfor %}
        </td>
    </tr>
    <tr><td>Рост</td><td><input name='height' class='input-mini' value='{{ item.height }}'> </td></tr>
    <tr><td>Начальный Вес</td><td><input name='start_weight' value='{{item.start_weight}}' class='input-mini'> </td></tr>
    <tr><td>Целевой Вес</td><td><input name='goal_weight' class='input-mini' value='{{item.goal_weight}}'> </td></tr>
    <tr><td>Текущий Вес</td><td><input name='current_weight' class='input-mini' value='{{item.current_weight}}' > </td></tr>
    <tr><td>Активность {{item.activity}} </td><td>
            {% for a in activity %}
                <label class="radio">
                    <input type="radio" name="activity" value="{{a.v}}" {% if a.v == item.activity %} checked {% endif %}  >
                    {{a.text}}
                </label>
            {% endfor %}

        </td></tr>
    <tr><td>Вегетарианство</td><td>
            <div class="btn-group" data-toggle="buttons-radio">
                {% for a in veg %}
                    <label class="radio">
                        <input type="radio" name="vegetarianism" value="{{a.v}}" {% if a.v == item.vegetarianism %} checked {% endif %}>
                        {{a.text}}
                    </label>
                {% endfor %}
            </div>
        </td></tr>
</table>