{% extends 'admin/common_head.tpl' %}
{% block css %}
    .mybtn-search {position:relative; top: -5px;}
    .mybtn-radio { margin:0px;}
{% endblock %}
{% block body %}

<div class="tabbable tabs-left">
    <ul class="nav nav-tabs" >
        <li {% if action=='system' %}class="active" {% endif %} ><a href="/plan/system" >System</a></li>
        <li {% if action=='users' %}class="active" {% endif %}><a href="/plan/users" >User</a></li>
        <li {% if action=='new' %}class="active" {% endif %}><a href="/plan/new" >Новый</a></li>
    </ul>
    <div class="tab-content">
        {% block body_content %}
        {% endblock %}
    </div>
</div>

{% endblock %}