{% extends 'admin/common_head.tpl' %}

{% block body %}
<link href="{{img_host}}/bootstrap/css/datepicker.css" rel="stylesheet">
<script src="{{img_host}}/bootstrap/js/bootstrap-datepicker.js"></script>

<form id='search_form'>
    <input type="text" class="span2" value="{{date_from}}" id="dp1" name='date_from' style='margin: 2px 0 0 5px'>
    <input type="text" class="span2" value="{{date_to}}" id="dp2" name='date_to' style='margin: 2px 0 0 5px'>
    <input type='hidden' name='orderby' value='{{orderby}}'>
    <button id="submit" class="btn" type="submit" style='margin-top: 1px;'>Find</button>
</form>


<table  class="table table-striped" style='width:300px;'>
    <thead>
    <tr>
        <th>Id</th>
        <th><a href='?date_from={{date_from}}&date_to={{date_to}}&orderby=hits'>Переходы</a></th>
        <th><a href='?date_from={{date_from}}&date_to={{date_to}}&orderby=u_cnt'>Регистрации</a></th>
        <th><a href='?date_from={{date_from}}&date_to={{date_to}}&orderby=email'>подтв.email</a></th>
        <th><a href='?date_from={{date_from}}&date_to={{date_to}}&orderby=d_cnt'>Дневник</a></th>

    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Всего</td>
        <td>{{total.hits}}</td>
        <td>{{total.u_cnt}}</td>
        <td>{{total.email}}</td>
        <td>{{total.d_cnt}}</td>
    </tr>
    {% for link in links %}
    <tr>
        <td>{% if link.link_id == 0 %}прямой&nbsp;переход{% else %}<a href='/links/edit?id={{link.link_id}}'>{{link.link_id}}</a> {% endif %}</td>
        <td>{{link.hits}}</td>
        <td>{{link.u_cnt}}</td>
        <td>{{link.email}}</td>
        <td>{{link.d_cnt}}</td>

    </tr>
    {% endfor %}
    </tbody>
</table>

<script>
    $(function(){
        $('#dp1').datepicker({format: 'yyyy-mm-dd'});
        $('#dp2').datepicker({format: 'yyyy-mm-dd'});
    });

</script>
{% endblock %}

