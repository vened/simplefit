{% extends 'admin/common_head.tpl' %}

{% block body %}

    <form id="login_form" class="form-horizontal" method="POST" action='{{script}}' style='width:350px;'>
        <h2 class="form-signin-heading">Edit Link</h2>
        <input class="input-block-level" type="text" name="url" value='{{url}}'>
        <input class="input-block-level" type="text" name="description" value='{{description}}'>
        <button id="submit" class="btn btn-large btn-primary" type="submit">Edit</button>
        <input type='hidden' name='back' value='{{back}}'>

    </form>
<button id="back" class="btn btn-large btn-primary" type="submit">Назад</button>

<script>
    $(function(){
       $("#back").click(function(e){
           history.back();
       })

    });

</script>
{% endblock %}