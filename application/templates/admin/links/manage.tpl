{% extends 'admin/common_head.tpl' %}

{% block body %}


<form id="login_form" class="form-horizontal" method="POST" action='{{script}}' style='width:350px;'>
    <h2 class="form-signin-heading">New Link</h2>
    <input class="input-block-level" type="text" name="url" placeholder="URL">
    <input class="input-block-level" type="text" name="description" placeholder="Описание">
    <button id="submit" class="btn btn-large btn-primary" type="submit">Add</button>
</form>

<table  class="table table-striped">
    <thead>
    <tr>
        <th>Id</th>
        <th>link</th>
        <th>created</th>
        <th>desc</th>

    </tr>
    </thead>
    <tbody>
    {% for link in links %}
    <tr>
    <td><a href='/links/edit?id={{link.id}}'>{{link.id}}</a></td>
    <td>{{link.url}}</td>
    <td>{{link.created}}</td>
    <td>{{link.description}}</td>
    </tr>
    {% endfor %}
    </tbody>
</table>

{% endblock %}