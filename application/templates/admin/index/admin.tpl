{% extends 'admin/common_head.tpl' %}

{% block body %}

<div style='margin-top:25px;margin-left: 20px; width:200px;'>
    <form id="login_form" class="form-horizontal" method="POST" action='{{script}}'>
        <h2 class="form-signin-heading">New user</h2>
        <input class="input-block-level" type="text" name="login" placeholder="Login">
        <input class="input-block-level" type="text" name="name" placeholder="Name">
        <input class="input-block-level" type="password" name="password" placeholder="Password">
        <button id="submit" class="btn btn-large btn-primary" type="submit">Add</button>
    </form>
</div>


<table class="table table-striped" style='width:300px;'>
    <thead>
    <tr><th>Имя</th><th>login</th></tr>
    </thead>
    <tbody>
{% for admin in admins %}
<tr><td>{{admin.name}}</td><td>{{admin.login}}</td></tr>
{% endfor %}
    </tbody>
</table>

{% endblock %}
