{% extends 'admin/common_head.tpl' %}
{% block css %}
.mybtn-search {position:relative; top: -5px;}
.mybtn-radio { margin:0px;}
{% endblock %}
{% block body %}
<form id="search_form">
    <div class="btn-group" data-toggle="buttons-radio">
        <button type="button" class="btn mybtn-radio mbs {% if type == 'new' %}active{% endif %}" value='new' id="new">Новые</button>
        <button type="button" class="btn mybtn-radio mbs {% if type == 'approved' %}active{% endif %}" value='approved'>Одобренные</button>
        <button type="button" class="btn mybtn-radio mbs {% if type == 'rejected' %}active{% endif %}" value='rejected'>Отклоненные</button>
    </div>
    <input type="hidden" name="type" value="{{type}}" id="form_hidden">
    <input type="hidden" name="page" value="{{current_page}}" >
</form>
<hr>
total: {{total}}<br><br>

{% set dataType = {'diary' : 'Дневник', 'plan' : 'План'} %}

{% for user_id, userData in data %}
<div style="padding-left: 10px;">
    <div><b>User ID: {{user_id}}</b></div>
    {% for date, diaryData in userData %}
        <div style="padding-left: 10px;">
            <div><b>Дата:</b> {{date}}</div>
            {% for row in diaryData %}
            <div id="row-{{row.id}}" class="diary-row" style="padding: 10px; border-bottom: #000000 1px solid;">
                <div>
                    <b>{{row.food_type}}</b>, {{row.diary_date}}
                    <input type="button" class="button" data="approve" value="Одобрить"/>
                    &nbsp;
                    <input type="button" class="button" data="reject" value="Отклонить"/>
                </div>
                <div>
                    Пол: {{row.gender}}, Возраст: {{row.age}}, Рост: {{row.height}}, Начальный вес: {{row.start_weight}},
                    Целевой вес: {{row.goal_weight}}, Текущий вес: {{row.current_weight}}, Активность: {{row.activity}},
                    Вегетарианство: {{row.vegetarianism}}
                </div>
                <div>
                    <b>{{dataType[row.type]}}:</b><br />
                    {% for product in row.products_data %}
                        {{product.weight}}г - {{product.name}} +{{product.calories}} ккал;
                    {% endfor %}
                    <b>Итого:</b> {{row.calories}}
                </div>
            </div>
            {% endfor %}
            <br />
        </div>
    {% endfor %}
</div>
<hr />
{% endfor %}

<div class="pagination">
    <ul>
        <li {% if current_page - 1 < 0 %}class="disabled"{% endif %}><a href="?page={{current_page - 1}}&q={{q}}&type={{type}}"> << </a></li>
    {% for p in pager %}
        <li {% if p == current_page %} class="disabled" {% endif %}><a href="?page={{p}}&q={{q}}&type={{type}}">{{p+1}}</a></li>
    {% endfor %}
        <li {% if current_page == total_pages - 1 %}class="disabled"{% endif %}><a href="?page={{current_page + 1}}&q={{q}}&type={{type}}"> >> </a></li>
    </ul>
</div>
<script>
    $(function() {

        $(".mbs").click(function (e) {
            $("#form_hidden").attr("value", e.currentTarget.value);
            $("#search_form").submit();
        });

        $('.button').click(function(e) {
            var target = $(this);
            var data = {
                'action' : target.attr('data'),
                'id' : target.parents('.diary-row').attr('id').split('-').pop()
            };

            $.post('/diaries/moderate/', data, function(res) {
                target.hide().siblings('input').hide();
            });
        });
    });
</script>

{% endblock %}