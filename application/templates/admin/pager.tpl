<ul class="pager">
{% if params_back %}
    <li class="previous">
        <a href="?{{params_back}}">&larr; Назад</a>
    </li>
{% endif %}
{% if params_next %}
    <li class="next">
        <a href="?{{params_next}}">Вперед &rarr;</a>
    </li>
{% endif %}
</ul>