{% extends 'admin/common_head.tpl' %}

{% block body %}
дней: {{total}}
<ul class="pager" style='width:400px;'>


{% if pageprev > 0 %}
    <li class="previous">
        <a href="?page={{pageprev}}">&larr; Назад</a>
    </li>
{% endif %}
{% if pagenext %}
    <li class="next">
        <a href="?page={{pagenext}}">Вперед &rarr;</a>
    </li>
{% endif %}
</ul>
<table class="table table-striped" style='width:400px;'>
    <thead>
    <th>дата</th>
    <th>вес</th>
    <th>калории</th>
    <th>фото</th>
    <th>план</th>
    <th>продукты</th>
    </thead>
    <tbody>
{% for row in stats %}
        <tr>
            <td>{{row.date}}</td>
            <td>{{row.weight}}</td>
            <td>{{row.calories}}</td>
            <td>{{row.photo}}</td>
            <td>{{row.plan}}</td>
            <td>{{row.product}}</td>
        </tr>
{% endfor %}
    </tbody>
</table>

<ul class="pager" style='width:400px;'>
{% if pageprev %}
    <li class="previous">
        <a href="?page={{pageprev}}">&larr; Назад</a>
    </li>
{% endif %}

{% if pagenext %}
    <li class="next">
        <a href="?page={{pagenext}}">Вперед &rarr;</a>
    </li>
{% endif %}
</ul>


{% endblock %}