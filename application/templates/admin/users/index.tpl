{% extends 'admin/common_head.tpl' %}

{% block body %}
<div style='margin:5px;'>
<div style='margin-top:25px;margin-left:10px;'>
    {{data}}
    total: {{total}}<br>
</div>
<ul class="pager">
{% if pageprev %}
    <li class="previous">
        <a href="?orderby={{orderby}}&page={{pageprev}}">&larr; Назад</a>
    </li>
{% endif %}
{% if pagenext %}
    <li class="next">
        <a href="?orderby={{orderby}}&page={{pagenext}}">Вперед &rarr;</a>
    </li>
{% endif %}
</ul>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Имя</th>
        <th>email</th>
        <th>возраст</th>
        <th>VK</th>
        <th>FB</th>
        <th><a href="?orderby=u.created&page={{page}}">регистрация</a></th>
        <th>last_access</th>
        <th><a href="?orderby=dw_cnt&page={{page}}">вес</a></th>
        <th><a href="?orderby=dc_cnt&page={{page}}">каллории</a></th>
        <th><a href="?orderby=p_cnt&page={{page}}">фото</a></th>
        <th><a href="?orderby=d_cnt&page={{page}}">активность</a></th>
    </tr>
    </thead>
    <tbody>
    {% for user in users %}
<tr>
    <td>{{user.first_name }} {{user.last_name}}</td>
    <td {% if user.email_confirmed > 0 %}style='color:green;'{% else %}style='color:red;'{% endif %}>{{user.email}} </td>
    <td>{{user.age}}</td>
    <td>{% if user.vk_id >0 %}<a href='http://vk.com/id{{user.vk_id}}'>VK</a>{% endif %}</td>
    <td>{% if user.fb_id >0 %}<a href="http://www.facebook.com/{{user.fb_id}}">FB</a>{% endif %}</td>
    <td>{{user.signin}}</td>
    <td>{{user.last_login}}</td>
    <td class='cn'>{{user.dw_cnt}}</td>
    <td class='cn'>{{user.dc_cnt}}</td>
    <td class='cn'>{{user.p_cnt}}</td>
    <td class='cn'>{{user.d_cnt}}</td>


</tr>
{% endfor %}
    </tbody>
</table>
    <ul class="pager">
    {% if pageprev %}
        <li class="previous">
            <a href="?orderby={{orderby}}&page={{pageprev}}">&larr; Назад</a>
        </li>
    {% endif %}
    {% if pagenext %}
        <li class="next">
            <a href="?orderby={{orderby}}&page={{pagenext}}">Вперед &rarr;</a>
        </li>
    {% endif %}
    </ul>

{% endblock %}