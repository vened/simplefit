<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="{{img_host}}/bootstrap/css/bootstrap.css" rel="stylesheet">

    <script src="{{img_host}}/js/jquery-1.8.3.min.js?{{img_version}}" type="text/javascript"></script>
    <script src="{{img_host}}/bootstrap/js/bootstrap-tab.js?{{img_version}}" type="text/javascript"></script>

    <style type="text/css">
        form * {
            margin:5px;
        }
        {% block css %}
        {% endblock %}
    </style>
<body>
{% block content %}
{% endblock %}

</body>
</html>