<div id="copy-plan-layer" class="modal" style="top:30px; width: 440px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Повторить этот план</h2>
        </div>
        <form id="copy-plan-form">
            <select name="date_from" style="display: none;">
                <option value="{{selected_date}}"></option>
            </select>
            <select name="day_to" class="select">
                <option value="01" {% if next_copy_date.day == '01' %}selected="selected"{% endif %}>01</option>
                <option value="02" {% if next_copy_date.day == '02' %}selected="selected"{% endif %}>02</option>
                <option value="03" {% if next_copy_date.day == '03' %}selected="selected"{% endif %}>03</option>
                <option value="04" {% if next_copy_date.day == '04' %}selected="selected"{% endif %}>04</option>
                <option value="05" {% if next_copy_date.day == '05' %}selected="selected"{% endif %}>05</option>
                <option value="06" {% if next_copy_date.day == '06' %}selected="selected"{% endif %}>06</option>
                <option value="07" {% if next_copy_date.day == '07' %}selected="selected"{% endif %}>07</option>
                <option value="08" {% if next_copy_date.day == '08' %}selected="selected"{% endif %}>08</option>
                <option value="09" {% if next_copy_date.day == '09' %}selected="selected"{% endif %}>09</option>
                <option value="10" {% if next_copy_date.day == '00' %}selected="selected"{% endif %}>10</option>
                <option value="11" {% if next_copy_date.day == '11' %}selected="selected"{% endif %}>11</option>
                <option value="12" {% if next_copy_date.day == '12' %}selected="selected"{% endif %}>12</option>
                <option value="13" {% if next_copy_date.day == '13' %}selected="selected"{% endif %}>13</option>
                <option value="14" {% if next_copy_date.day == '14' %}selected="selected"{% endif %}>14</option>
                <option value="15" {% if next_copy_date.day == '15' %}selected="selected"{% endif %}>15</option>
                <option value="16" {% if next_copy_date.day == '16' %}selected="selected"{% endif %}>16</option>
                <option value="17" {% if next_copy_date.day == '17' %}selected="selected"{% endif %}>17</option>
                <option value="18" {% if next_copy_date.day == '18' %}selected="selected"{% endif %}>18</option>
                <option value="19" {% if next_copy_date.day == '19' %}selected="selected"{% endif %}>19</option>
                <option value="20" {% if next_copy_date.day == '20' %}selected="selected"{% endif %}>20</option>
                <option value="21" {% if next_copy_date.day == '21' %}selected="selected"{% endif %}>21</option>
                <option value="22" {% if next_copy_date.day == '22' %}selected="selected"{% endif %}>22</option>
                <option value="23" {% if next_copy_date.day == '23' %}selected="selected"{% endif %}>23</option>
                <option value="24" {% if next_copy_date.day == '24' %}selected="selected"{% endif %}>24</option>
                <option value="25" {% if next_copy_date.day == '25' %}selected="selected"{% endif %}>25</option>
                <option value="26" {% if next_copy_date.day == '26' %}selected="selected"{% endif %}>26</option>
                <option value="27" {% if next_copy_date.day == '27' %}selected="selected"{% endif %}>27</option>
                <option value="28" {% if next_copy_date.day == '28' %}selected="selected"{% endif %}>28</option>
                <option value="29" {% if next_copy_date.day == '29' %}selected="selected"{% endif %}>29</option>
                <option value="30" {% if next_copy_date.day == '30' %}selected="selected"{% endif %}>30</option>
                <option value="31" {% if next_copy_date.day == '31' %}selected="selected"{% endif %}>31</option>
            </select>
            <select name="month_to" class="select">
                <option value="01" {% if next_copy_date.month == '01' %}selected="selected"{% endif %}>январь</option>
                <option value="02" {% if next_copy_date.month == '02' %}selected="selected"{% endif %}>февраль</option>
                <option value="03" {% if next_copy_date.month == '03' %}selected="selected"{% endif %}>март</option>
                <option value="04" {% if next_copy_date.month == '04' %}selected="selected"{% endif %}>апрель</option>
                <option value="05" {% if next_copy_date.month == '05' %}selected="selected"{% endif %}>май</option>
                <option value="06" {% if next_copy_date.month == '06' %}selected="selected"{% endif %}>июнь</option>
                <option value="07" {% if next_copy_date.month == '07' %}selected="selected"{% endif %}>июль</option>
                <option value="08" {% if next_copy_date.month == '08' %}selected="selected"{% endif %}>август</option>
                <option value="09" {% if next_copy_date.month == '09' %}selected="selected"{% endif %}>сентябрь</option>
                <option value="10" {% if next_copy_date.month == '10' %}selected="selected"{% endif %}>октябрь</option>
                <option value="11" {% if next_copy_date.month == '11' %}selected="selected"{% endif %}>ноябрь</option>
                <option value="12" {% if next_copy_date.month == '12' %}selected="selected"{% endif %}>декабрь</option>
            </select>

            <button class="button button-orange va-m">Сохранить</button>
        </form>
    </div>
</div>

<div id="copy-plan-error-layer" class="modal" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Внимание</h2>
            <div>Невозможно добавить план в уже прошедшие дни</div>
        </div>

        <button class="button button-orange va-m">Закрыть</button>
    </div>
</div>

<div id="copy-plan-confirm-layer" class="modal" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Внимание</h2>

            <div>Для выбранного дня уже создан план, вы хотите заменить его?</div>
        </div>

        <form id="copy-plan-confirm-form">
            <input type="hidden" name="date_from" />
            <input type="hidden" name="day_to"/>
            <input type="hidden" name="month_to"/>
        </form>

        <div>
            <button class="button button-orange va-m replace">Заменить</button>
            <button class="button button-orange va-m close">Отменить</button>
        </div>
    </div>
</div>