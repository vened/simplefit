<form id="diary-calories-form" class="clearfix mb20" action="{{form_action}}">
    <input type="hidden" name="date" value="{{current_date}}" />
    <input type="hidden" name="type" value="calories"/>
    <select name="food_type" class="select" style="width: 200px" {% if disable_form %}disabled="disabled"{% endif %}>
        <option value="breakfast">Завтрак</option>
        <option value="breakfast2">Поздний завтрак</option>
        <option value="lunch">Обед</option>
        <option value="lunch2">Полдник</option>
        <option value="dinner">Ужин</option>
        <option value="dinner2">Поздний ужин</option>
    </select>
    <input type="text" id="search-product-name" placeholder="Введите название продукта" class="text"
           style="width: 698px;margin-left: 5px" {% if disable_form %}disabled="disabled"{% endif %} />
    <!--<button class="button button-orange va-m fl-r">Найти</button> -->
</form>

<ul id="found-products" class="items-product mb20" style="display: none;">
    <li style="display: none;">
        <div class="item-description">
				<span class="vam-c">
					<strong>#name#</strong>

					<div class="gray">{#<!--###Вкусы: ###<br/>-->#}

                        <span class="yellow">#calories# ккал</span>
                        в 100 г
                    </div>
				</span>
            <b class="vam"></b>
        </div>

        <div class="item-actions">
            <b class="vam"></b>

				<span class="vam-c">
					<span>Порция: &nbsp;</span>
					<input type="text" class="text small width1" name="weight" value="100" style="width: 65px;"/> гр

					<span class="fs21">
						<span>=</span>
						<span class="yellow cval">#calories# ккал</span>
					</span>
					<a href="#" class="button button-gray">Добавить</a>
				</span>
        </div>
    </li>
</ul>

<div id="add-new-product" class="mb20" style='display:none'>
    <a id="add-product-link" class="fs16" href="#">Добавить свой продукт</a>
</div>

<h2 class="added-products-list" {% if not selected_product %}style="display: none;"{% endif %}>Завтрак {{today}}</h2>

<ul id="products-list" class="product-list" {% if not selected_product %}style="display: none;"{% endif %}>
    <li style="display: none;">
        <b class="fl-l">#name# #weight# г = <span class="yellow">#calories# ккал</span></b>

        <div class="action">
            <i class="icons icon_edit"></i>
            <!--a href="#">Редактировать</a-->
            <a href="#" class="remove">Удалить</a>
        </div>
    </li>
    {% if selected_product %}
        <li id="product-{{selected_product.id}}">
            <b class="fl-l">{{selected_product.name}} {{selected_product.weight}} г = <span class="yellow">{{selected_product.weight_calories}} ккал</span></b>

            <div class="action">
                <i class="icons icon_edit"></i>
                <!--a href="#">Редактировать</a-->
                <a href="#" class="remove">Удалить</a>
            </div>
        </li>
    {% endif %}
</ul>

<div class="clearfix mt25 mb5 added-products-list" {% if not selected_product %}style="display: none;"{% endif %}>
    <div class="fl-l fs20" style="padding-top: 9px"><b>Итого:</b>
        <b class="yellow">+<span id="calories-total">{% if selected_product %}{{selected_product.weight_calories}}{% else %}0{% endif %}</span>
        ккал</b></div>
    <div class="fl-r fs16">
        <button id="save-calories" class="button button-green small">Сохранить запись</button>
    </div>
</div>

<script type="text/javascript">

var foundProducts = {};
var addedProducts = {};

{% if selected_product %}
addedProducts[{{selected_product.id}}] = {
    'id' : '{{selected_product.id}}',
    'name' : '{{selected_product.name}}',
    'weight' : parseFloat('{{selected_product.weight}}'),
    'calories' : parseFloat('{{selected_product.weight_calories}}')
};
{% endif %}

function searchProducts(val) {
    if (val.length >= 3) {
        $.post('/ws/products/search/', {'q':val}, function (response) {
            drawSearch(response);
        }, 'json');
        $('#add-new-product').show();
    }
    else {
        $('#found-products').children().not(':first').remove();
        $('#add-new-product').hide();
    }
}

var action = null;
var date = null;
var diaryId = 0;

$(function () {

    var isEmpty = true;

    for (var x in addedProducts) {
        isEmpty = false;
        break;
    }

    if (!isEmpty) {
        $('html, body').animate({scrollTop:$("h2.added-products-list").offset().top}, 200);
    }

    $('#search-product-name').keyup(function (e) {
        var val = $(this).val();
        searchProducts(val);
        if (e.keyCode == 13 && val.length > 2) {
            $('html, body').animate({scrollTop:$("#calories-section").offset().top}, 200);

        }
    });

    $('#diary-calories-form button').click(function (e) {
        e.preventDefault();
        searchProducts($('#search-product-name').val());
    });

    $('select[name=food_type]').change(function (e) {
        var header = $('h2.added-products-list');
        var title = header.html().split(' ');
        header.html($(this).find('option:selected').text() + ' ' + title[title.length - 1]);
    });

    $('#found-products').click(function (e) {
        e.preventDefault();
        var target = $(e.target);

        if (target.hasClass('button')) {
            var $parent = target.parents('li');
            var height = $parent.height() + 12;
            var offset = 1;

            var elem = $parent;
            while (elem.size() && offset < 10) {
                offset++;
                elem = elem.next();
            }

            window.scrollBy(0, height*offset);
            var id = target.parents('li').attr('id').split('-');
            var calories = target.siblings('input').val() * foundProducts[id[1]].calories / 100;

            var product = {
                'id':id[1],
                'name':foundProducts[id[1]].name,
                'weight':parseFloat(target.siblings('input').val()),
                'calories':parseFloat(calories)
            };

            if (product['id'] in addedProducts) {
                addedProducts[product['id']]['weight'] += product['weight'];
                addedProducts[product['id']]['calories'] += product['calories'];
            }
            else {
                addedProducts[product['id']] = product;
            }

            drawAddedProduct(addedProducts[product['id']]);
        }
    });

    $('#found-products').keyup(function (e) {
        var target = $(e.target);
        if (target.attr('name') == 'weight') {
            var val = target.val();
            var val2 = val.replace(/[^0-9]/g, '');
            if (val2 != val) {
                val = val2;
                target.val(val);
            }
            var calories = $('#found-products .cval').html().split(' ');
            var id = target.parents('li').attr('id');
            var productId = id.split('-')[1];
            $('#' + id + ' .cval').html(foundProducts[productId]['calories'] * val / 100 + ' ' + calories[1]);
        }
    });

    $('#products-list').click(function (e) {
        e.preventDefault();
        var target = $(e.target);

        if (target.hasClass('remove')) {
            var id = target.parents('li').filter(':first').remove().attr('id').split('-');
            var caloriesTotal = $('#calories-total');
            caloriesTotal.html(parseFloat(caloriesTotal.html()) - addedProducts[id[1]]['calories']);
            delete addedProducts[id[1]];

            if ($(this).children().size() == 1) {
                $(this).hide();
                $('.added-products-list').hide();
            }
        }
    });

    $('#save-calories').click(function (e) {
        var data = $('#diary-calories-form').serializeObject();
        data['products'] = addedProducts;
        var action = $('#diary-calories-form').attr('action');

        if (window.action) {
            action = window.action;
            window.action = null;
        }

        if (window.diaryId) {
            data['id'] = window.diaryId;
            window.diaryId = 0;
        }

        $.post(action, data, function () {
            if (data.date) {
                window.location.assign('?date='+data.date);
            }
            else {
                var search = window.location.search;
                if (search) {
                    window.location.assign(window.location.href.replace(search, ''));
                }
                else {
                    window.location.reload(true);
                }
            }
        });
    });

    $('#add-product-link').click(function (e) {
        e.preventDefault();
        showLayer('add-product');
    });

    $('#add-product-form button').click(function (e) {
        e.preventDefault();
        $('#add-product-form div.error').hide();
        var data = $('#add-product-form').serializeObject();

        if (!checkProductData(data, ['proteins', 'fats', 'carbohydrates'])) {
            return false;
        }

        $.post('/ws/products/add/', data, function (response) {

            $('#add-product-form input[type=text]').val('');
            hideLayer();
            drawSearch(response['product']);
        });
    });

    $('#add-product-thanks-layer div button').click(function (e) {
        hideLayer();
    });
});

function checkProductData(data, exclude) {
    var result = true;
    if (!exclude) {
        exclude = [];
    }

    for (x in data) {
        if (exclude.indexOf(x) != -1) {
            continue;
        }

        if (!data[x] || data[x] == 0) {
            result = false;
            $('input[name=' + x + ']').siblings('div.error').show();
            continue;
        }

        var re = /^\d+((\.|,)\d+)?$/;

        if (x != 'name' && !re.test(data[x])) {
            result = false;
            $('input[name=' + x + ']').siblings('div.error').show();
        }
    }

    return result;
}

function drawSearch(response) {
    foundProducts = {};
    var foundProductsList = $('#found-products');
    foundProductsList.children().not(':first').remove();

    for (x in response) {

        foundProducts[response[x].id] = response[x];
        var item = foundProductsList.children(':first').clone().show();
        var html = item.html();
        html = html.replace('#name#', response[x].name);
        html = html.replace(/#calories#/g, response[x].calories);
        item.html(html);
        item.attr('id', 'fp-' + response[x].id);
        foundProductsList.append(item);
    }

    foundProductsList.show();
}

var oldDate = null;

function drawAddedProduct(product)
{
    addedProducts[product['id']] = product;

    var productsList = $('#products-list');
    var item = productsList.children(':first').clone().show();
    var html = item.html();
    html = html.replace('#name#', product['name']);
    html = html.replace(/#weight#/g, product['weight']);
    html = html.replace(/#calories#/g, product['calories']);

    if ($('#product-' + product['id']).size() == 0) {
        item.html(html);
        item.attr('id', 'product-' + product['id']);
        productsList.append(item).show();
    }
    else {
        $('#product-' + product['id']).html(html);
    }

    var header = $('h2.added-products-list');
    var title = header.html().split(' ');
    oldDate = title[title.length - 1];
    var selectedDate = null;

    if (date) {
        var d = date.split('-');
        selectedDate = d[2] + '.' + d[1] + '.' + d[0];
    }
    header.html($('select[name=food_type] option:selected').text() + ' ' + (selectedDate ? selectedDate : oldDate));

    $('.added-products-list').show();
    var caloriesTotal = $('#calories-total');
    caloriesTotal.html(parseFloat(caloriesTotal.html()) + parseFloat(product['calories']));
}
</script>