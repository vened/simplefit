{% if user.id == 0 %}
<div class="modal" id="signin-layer" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Вход на сайт</h2>
        </div>
        <form class="width3" id="signin-form">
            <div class="mb10">
                <input type="text" name="email" placeholder="Email" class="text normal" />
                <input type="password" name="password" placeholder="Пароль" class="text normal ml10" />
            </div>
            <div class="clearfix">
                <div class="fl-l">
                    <label class="va-m"><input type="checkbox" class="checkbox" name="remember" value="1" /><span
                            class="gray ml5">Запомнить меня</span></label>
                    <a href="#" id="password-remind-link" class="ml10 va-m">Забыли пароль?</a>
                </div>
                <div class="fl-r">
                    <a href="#" class="button button-gray signin"><i class="icons key"></i> Войти</a>
                </div>
            </div>
        </form>

        <hr class="width3"/>

        <div class="title">
            <h2>Войти через аккаунт в соц. сетях</h2>
        </div>

        <div class="social-icons">
            <ul>
                <li class="social-ico vk"><a href="#" class="vk-signup"></a></li>
                <li class="social-ico fb"><a href="#" class="fb-signup"></a></li>
                {#<li class="social-ico tw"><a href="#" class="tw-signup"></a></li>#}
                <li class="social-ico mm"><a href="#" class="mm-signup"></a></li>
                {#<li class="social-ico ya"><a href="#" class="ya-signup"></a></li>
                <li class="social-ico gp"><a href="#" class="gp-signup"></a></li>#}
                <!--li class="social-ico od"><a href="#" class="od-signup"></a></li-->
            </ul>
        </div>

        <hr class="width3"/>

        <div class="title width3 m0">
            <button class="button button-orange small fl-r signup-link">Зарегистрироваться</button>
            <h2>У Вас нет логина?</h2>
        </div>
    </div>
</div>


<div class="modal" id="signup-layer" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Регистрация</h2>
        </div>
        <form id="signup-form">
            <div class="mb10">
                <input type="text" name="first_name" placeholder="Имя пользователя" class="text normal width3" />
                <input type="hidden" name="last_name" value="" />
                <input type="hidden" name="gender" value="" />
                <input type="hidden" id="signup-type" value="" />
            </div>
            <div class="mb10">
                <input type="text" name="email" placeholder="Email" class="text normal width3" />
            </div>
            <div class="mb10">
                <input type="password" name="password" placeholder="Пароль" class="text normal width3" />
            </div>
            <div class="mb15">
                <input type="password" name="password2" placeholder="Подтверждение пароля" class="text normal width3" />
            </div>
            <div class="txt-r width3">
                <button class="button button-orange small signup">Продолжить</button>
            </div>
        </form>

        <hr class="width3"/>

        <div class="title">
            <h2>Регистрация через соц. сети</h2>
        </div>

        <div class="social-icons">
            <ul>
                <li class="social-ico vk"><a href="#" class="vk-signup"></a></li>
                <li class="social-ico fb"><a href="#" class="fb-signup"></a></li>
                {#<li class="social-ico tw"><a href="#" class="tw-signup"></a></li>#}
                <li class="social-ico mm"><a href="#" class="mm-signup"></a></li>
                {#<li class="social-ico ya"><a href="#" class="ya-signup"></a></li>
                <li class="social-ico gp"><a href="#" class="gp-signup"></a></li>
                <li class="social-ico od"><a href="#" class="od-signup"></a></li>#}
            </ul>
        </div>

    </div>
</div>

{% endif %}
<div class="modal" id="confirm-email-layer" style="top:470px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Подтверждение email адреса</h2>
        </div>
        <p>
            Подтверждение требуется для исключения несанкционированного использования вашего e-mail адреса при входе
            на
            сайт. На ваш почтовый ящик <a class="confirm-user-email" href="mailto:{% if user %}{{user.email}}{% endif %}">{% if user %}{{user.email}}{% endif %}</a> отправлено письмо с ссылкой
            для
            подтверждения e-mail и данными для входа на сайт.
        </p>

        <p class="gray mb20">
            Для подтверждения вашего e-mail адреса достаточно перейти по ссылке из этого письма. Если письмо с кодом
            не
            пришло на вашу почту в течение часа или он не подошел, запросите код повторно.
        </p>
        <span id="resend-confirm-email-btn" class="button button-gray">Отправить повторно</span>
    </div>
</div>


<div id="add-product-layer" class="modal" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Добавить свой продукт</h2>
        </div>
        <form id="add-product-form">
            <div class="mb10">
                <input type="text" name="name" placeholder="Название" class="text normal width3">
                <sup class="required">*</sup>

                <div class="error" style="display: none;">Некорректное значение</div>
            </div>
            <div class="mb20">
                <input type="text" name="calories" placeholder="ккал" class="text normal width1">
                <sup class="required">*</sup>

                <span class="va-m gray">на 100 г</span>
                <div class="error" style="display: none;">Некорректное значение</div>
            </div>
            <div class="mb20">
                <input type="text" name="proteins" placeholder="белки" class="text normal width1">

                <span class="va-m gray">на 100 г</span>
                <div class="error" style="display: none;">Некорректное значение</div>
            </div>
            <div class="mb20">
                <input type="text" name="fats" placeholder="жиры" class="text normal width1">

                <span class="va-m gray">на 100 г</span>
                <div class="error" style="display: none;">Некорректное значение</div>
            </div>
            <div class="mb20">
                <input type="text" name="carbohydrates" placeholder="углеводы" class="text normal width1">

                <span class="va-m gray">на 100 г</span>
                <div class="error" style="display: none;">Некорректное значение</div>
            </div>
            <!--div class="mb5">
                <input type="file">
            </div>
            <div class="mb15 gray">
                <i>jpg, jpeg, png. Максимальный размер файла: 800 Кб</i>
            </div-->
            <button class="button button-orange small">Добавить продукт</button>
        </form>
    </div>
</div>

<div id="add-product-thanks-layer" class="modal" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Продукт добавлен</h2>
        </div>
        <p>
            Мы запомнили добавленный Вами продукт. Через некоторое время он станет доступен вам в форме поиска.
        </p>
        <button class="button button-orange small">Закрыть</button>
    </div>
</div>

<div id="password-remind-layer" class="modal" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Восстановление доступа</h2>
        </div>
        <p>
            Введите адрес электронной почты, который Вы указывали при регистрации и нажмите кнопку «Далее». На указанный
            Вами адрес будет отправлено письмо с ссылкой для восстановления пароля.
        </p>
        <p>
            <input id="password-remind-email" name="email" placeholder="Email" class="text normal" />
            <sup class="required">*</sup>
            <div class="error error-email" style="display: none;">
                Неверное значение
            </div>

            <div class="error no-user" style="display: none;">
                Такой email не зарегистрирован
            </div>
        </p>
        <button class="button button-orange small">Далее</button>
    </div>
</div>

<div id="password-remind-sent-layer" class="modal" style="top:30px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Время проверить почту!</h2>
        </div>
        <p>
            На <span class="sent-email">email</span> отправлено письмо с ссылкой для восстановления пароля.
        </p>

        <button class="button button-orange small">Закрыть</button>
    </div>
</div>


<div class="modal" style="top:580px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <i class="icons icon_alert"></i>

            <h2>Удалить фото?</h2>
        </div>
        <p>
            <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mauris augue, consequat vitae varius
                vitae.</b>
        </p>

        <p>
            Vivamus tincidunt tellus eu velit congue vulputate. Duis et est mi, at consectetur orci. Maecenas
            suscipit gravida lacus, nec fringilla nulla rhoncus in. Vestibulum velit elit.
        </p>
        <span class="button button-gray">close</span>
    </div>
</div>

<div class="modal" id="feedback-layer" style="top:1360px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Обратная связь</h2>
        </div>
        <form id="feedback-form">
            <div class="mb10">
                <input type="text" placeholder="Эл. адрес" name="email" class="text normal width3">
                <sup class="required">*</sup>

                <div class="error" style="display: none;">
                    Неверное значение
                </div>
            </div>
            <div class="mb20">
                <textarea name="message" class="textarea width3" rows="6" placeholder="Ваш вопрос"></textarea>
            </div>
            <button class="button button-orange small">Отправить вопрос</button>
        </form>
    </div>
</div>

<div class="modal" id="feedback-sent-layer" style="top:1690px; display: none;">
    <div class="inset">
        <i class="icons icon_close"></i>

        <div class="title">
            <h2>Вопрос был отправлен</h2>
        </div>
        <p>
            <b>
                Спасибо за ваш вопрос, наши специалисты постараются ответить вам в ближайшие дни. Не сдавайтесь и идите
                за своей целью!
            </b>
        </p>
        <span class="button button-gray">Закрыть</span>
    </div>
</div>

<script type="text/javascript">
jQuery.fn.serializeObject = function () {
    var arrayData, objectData;
    arrayData = this.serializeArray();
    objectData = {};

    $.each(arrayData, function () {
        var value;

        if (this.value != null) {
            value = $.trim(this.value);
        } else {
            value = '';
        }

        if (objectData[this.name] != null) {
            if (!objectData[this.name].push) {
                objectData[this.name] = [objectData[this.name]];
            }

            objectData[this.name].push(value);
        } else {
            objectData[this.name] = value;
        }
    });

    return objectData;
};

</script>