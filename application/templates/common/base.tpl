<!DOCTYPE html>
<html>
<head>
    {% block title %}
    <title>Симплфит - Дневник питания</title>
    {% endblock %}
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    {% block meta %}
    {% endblock %}
    <script src="{{img_host}}/js/jquery-1.8.3.min.js?{{img_version}}" type="text/javascript"></script>
    <script src="{{img_host}}/js/select2/select2.min.js?{{img_version}}" type="text/javascript"></script>
    <script type="text/javascript" src="http://cdn.connect.mail.ru/js/loader.js"></script>
    <script src="http://www.odnoklassniki.ru/oauth/resources.do?type=js" type="text/javascript" charset="utf-8"></script>
    <script src="{{img_host}}/js/main.js?{{img_version}}" type="text/javascript"></script>
    <script src="{{img_host}}/js/social.js?{{img_version}}" type="text/javascript"></script>


    <!--link href="http://www.odnoklassniki.ru/oauth/resources.do?type=css" rel="stylesheet" /-->
    <link rel="stylesheet" type="text/css" href="{{img_host}}/css/style.css?{{img_version}}" />
    <link href="/favicon.ico" rel="shortcut icon" />

	<!--<script src="{{img_host}}/js/less-1.3.0.min.js?{{img_version}}" type="text/javascript"></script>-->
	<!--<link rel="stylesheet/less" type="text/css" href="{{img_host}}/less/style.less?{{img_version}}">-->

    {% block css %}{% endblock %}
    {% block javascript %}{% endblock %}

</head>
<body>

<div class="body">

<div class="header">
    <div class="wrapper">
        <div class="logo">
            <a href="/"><img src="{{img_host}}/images/logo.png" alt="Simplefit"></a>
        </div>
        <div class="fl-r" style="width: 730px">

            <div class="mb50 clearfix">
                <div class="userbar">
                    {% if user %}
                    <div class="username">
                        <i class="icons icon_user"></i>
                        <a href="{{nav('profile')}}">{{user.first_name}} {{user.last_name}}</a>
                    </div>
                    <div class="settings">
                        <i class="icons icon_settings"></i>
                        <a class="gray" href="{{nav('settings/profile')}}">Настройки</a>
                    </div>
                    <a class="logout button button-gray" href="{{nav('signout')}}">Выйти</a>
                    {% else %}
                    <a class="button button-gray ml15 login-link" href="#"><i class="icons key"></i> Войти</a>
                    <button class="button button-orange small ml15 signup-link">Зарегистрироваться</button>
                    {% endif %}
                </div>
                <div class="social_buttons">
                    <span id="vk_like"></span>
                    <span class="fb-like" data-send="false" data-layout="button_count" data-width="90"
                         data-show-faces="false" data-href="http://simplefit.ru"></span>
                    <!--img src="{{img_host}}/images/tmp/social_button.png" /-->
                </div>
            </div>

            <div class="clearfix">
                <ul class="main_menu">
                    <!--li><a href="{{nav('about')}}">О проекте</a></li-->
                    {#
                    <li><a href="{{nav('diaries')}}">Дневники</a></li>
                    <li><a href="{{nav('community')}}">Сообщество</a></li>
                    <li><a href="{{nav('recommendations')}}">Рекомендации</a></li>
                    #}
                    {% if user %}
                    <li><a href="{{nav('plan')}}">План питания</a></li>
                    <li><a href="{{nav('profile')}}">Дневник</a></li>
                    <li><a href="{{nav('analytics')}}">Аналитика</a></li>

                    {% else %}
                    <li><a href="#" class="login-link">План питания</a></li>
                    <li><a href="#" class="login-link">Дневник</a></li>
                    <li><a href="#" class="login-link">Аналитика</a></li>
                    {% endif %}
                    <li><a href="{{nav('recommendations')}}">Рекомендации {% if user %}({{user.recommendation_count}}){% endif %}</a></li>
                    <li><a href="{{nav('search')}}">Калорийность</a></li>
                    <li><a href="http://blogs.simplefit.ru">Блоги</a></li>
                </ul>
            </div>

        </div>
    </div>
    <b class="header-after"></b>
</div>

{% block content %}{% endblock %}

<div class="footer">
    <div class="wrapper">

        <div class="fl-l">
            <div class="mb15">
                <b>© {{current_year}} SimpleFit.ru</b>
            </div>
            <div class="mb15">
                Пользуясь сервисом SimpleFit, Вы безусловно<br>
                соглашаетесь с <a href="{{nav('license')}}">условиями обслуживания</a>
            </div>
            <div class="mb15">
                Использование и размещение материалов с сайта возможно только<br>
                с разрешения администрации и с указанием активной<br>
                гиперссылки на данный ресурс.
            </div>
        </div>

        <div class="feedback fl-r">
            <i class="icons icon_mail"></i>
            <a href="#" onclick="showLayer('feedback'); return false;">Свяжитесь с нами</a>
            <br><br>
            <a href="http://passport.webmoney.ru/asp/certview.asp?wmid=154437512633" style="text-decoration: none;">
                <img src="http://www.webmoney.ru/img/icons/88x31_wm_v_white_on_transparent_ru.png" alt="Simplefit" />
            </a>
        </div>

    </div>
</div>

<div class="overlay" id="overlay" style="display: none;"></div>

{% include 'common/layers.tpl' %}

{% block layers %}
{% endblock %}

</div>

<div id="vk_api_transport"></div>

<div id="fb-root"></div>

{% block javascript_footer %}{% endblock %}

<script type="text/javascript">

var openedLayaer = null;

function showLayer(id)
{
    if (openedLayaer) {
        hideLayer();
    }

    var w = $(window);
//    var top = Math.max(0, w.scrollTop() + (document.documentElement.clientHeight) / 2) + 'px';
    var top = Math.max(0, w.scrollTop() + 30) + 'px';

    $('#overlay').show();
    openedLayaer = id + '-layer';
    $('#'+openedLayaer).css('top', top).show();

    $('body').keyup(function(e) {
        if (e.keyCode == 27) {
            hideLayer();
            $(this).unbind('keyup');
        }
    });
}

function hideLayer()
{
    $('#'+openedLayaer).hide().find('div:hidden').show();
    $('#' + openedLayaer).find('form').each(function() {
        this.reset();
        $(this).find('input[type=hidden]').val('');
    });
    $('#overlay').hide();

    openedLayaer = null;
}

$(function () {
    $('i.icon_close').click(function (e) {
        e.preventDefault();
        hideLayer();
    });

    $('a.login-link').click(function (e) {
        e.preventDefault();
        showLayer('signin');
    });

    $('button.signup-link').click(function (e) {
        e.preventDefault();
        showLayer('signup');
    });

    $('#resend-confirm-email-btn').click(function (e) {
        e.preventDefault();
        resend();

    });

    {% if user and (not user.email_confirmed) %}
    showLayer('confirm-email');
    {% endif %}
});
</script>

<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-37653216-1']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter19380835 = new Ya.Metrika({id:19380835,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="//mc.yandex.ru/watch/19380835" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>