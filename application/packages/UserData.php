<?php

class UserData extends DomainObject
{
    protected $id = 0;

    protected $properties = array(
        'user_id' => 0,
        'country_id' => 0,
        'region_id' => 0,
        'city_id' => 0,
        'vk_posted' => 0,
        'fb_posted' => 0,
        'mm_posted' => 0,
    );

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUserId()
    {
        return $this->getProperty('user_id');
    }

    public function setUserId($userId)
    {
        $this->setProperty('user_id', $userId);
    }

    public function setVkPosted()
    {
        $this->setProperty('vk_posted', 1);
    }

    public function setFbPosted()
    {
        $this->setProperty('fb_posted', 1);
    }

    public function setMmPosted()
    {
        $this->setProperty('mm_posted', 1);
    }
}