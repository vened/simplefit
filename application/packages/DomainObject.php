<?php

class DomainObject implements ArrayAccess
{
    protected $properties = array();
    private $modified = array();

    protected $virtualProperties = array();


    public function __construct($properties = array())
    {
        if ($properties) {
            $this->setProperties($properties);
        }
    }

    public function setProperties($properties, $init = false)
    {
        foreach ($properties as $propertyName => $propertyValue)
        {
            if(!$this->offsetExists($propertyName)){
                Error::stderrLog("not set: $propertyName" );
            }else{
                $this->setProperty($propertyName, $propertyValue, $init);
            }
        }

        return $this;
    }

    public function setProperty($propertyName, $propertyValue, $init = false)
    {
        if (!$init)
        {
            $this->markAsModified($propertyName, $propertyValue);
        }

        $this->properties[$propertyName] = $propertyValue;
    }

    public function getProperty($name)
    {
        return isset($this->properties[$name])
            ? $this->properties[$name]
            : null;
    }

    public function getProperties()
    {
        return $this->properties;
    }

    public function getId()
    {
        if(isset($this->properties['id'])){
            return $this->properties['id'];
        }
        else{
            return false;
        }
    }

    public function isEmpty()
    {
        return $this->getId() ? false : true;
    }

    public function countModified()
    {
        return count($this->modified);
    }

    public function getModified()
    {
        return $this->modified;
    }

    private function markAsModified($propertyName, $propertyValue)
    {
        if ($this->getProperty($propertyName) == $propertyValue)
        {
            return;
        }

        if (in_array($propertyName, $this->modified))
        {
            return;
        }

        $this->modified[] = $propertyName;
    }

    public function toArray()
    {
        return $this->properties;
    }

    public function __get($name){
        return $this->getProperty($name);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset) {
        return array_key_exists($offset,$this->properties) || in_array($offset,$this->virtualProperties);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset) {
        return $this->getProperty($offset);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value) {
        $this->setProperty($offset,$value);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset) {

    }

    /**
     * @return DomainObject
     */
    public function _clone(){
        $class = get_class($this);
        $clone = new $class;
        foreach($this->getProperties() as $key => $value){
            if($key == "id")continue;
            $clone->setProperty($key,$value);
        }
        return $clone;
    }
}