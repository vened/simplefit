<?php

class PlanMapper extends Mapper
{
    protected $tableName = '#db_prefix#_Cluster.#table_prefix#_Plan';
    protected $domainObject = 'Plan';
    protected $pk = 'id';
    protected $useCache = false;

    public function __construct($prefixId) {
        parent::__construct($prefixId);
    }

    public function getByUserId($userId, $start, $end, $order = true)
    {
        $end = substr($end, 0, 10) . ' 23:59:59';
        $sql = "SELECT *
                FROM %s
                WHERE
                    user_id = ? AND created >= ? AND created <= ?";

        if ($order) {
            $sql .= " ORDER BY created DESC";
        }

        $sql = sprintf($sql, $this->quotedTableName());

        $params = array($userId, $start, $end);

        return $this->query($sql, $params);
    }

    /**
     * @param $userId
     * @param $foodType
     * @param $date
     * @return Plan
     */
    public function getMenuByUserId($userId, $foodType, $date)
    {
        $date = substr($date, 0, 10);

        $sql = "SELECT *
                FROM %s
                WHERE
                    user_id = ? AND type = ?
                    AND created >= ? AND created <= ?
                LIMIT 1";

        $sql = sprintf($sql, $this->quotedTableName());
        $params = array($userId, $foodType, $date, $date . ' 23:59:59');

        $result = $this->query($sql, $params);

        return isset($result[0]) ? $result[0] : new Plan();
    }

    public function getPlanFromDate($userId, $date)
    {
        $sql = "SELECT *
                FROM %s
                WHERE
                    user_id = ?
                    AND created >= ?
                ORDER BY
                    created";

        $sql = sprintf($sql, $this->quotedTableName());
        $params = array($userId, $date);
        return $this->query($sql, $params);
    }

    public function insert(DomainObject $do){
        $m = new UserStatsMapper();
        $m->hitStat("plan");
        return parent::insert($do);
    }

    /**
     * @param $user_id int
     * @param Recommendation_Object[] $items
     * @param $date
     */
    public function saveRecommendations($user_id,$items, $date){

        $this->query(
            sprintf(
                "delete from %s where user_id = ? and date(created) = '$date'",
                $this->quotedTableName()
            ),
            array($user_id)
        );

        foreach($items as $item){

            $item->getCalories();
            $o = new Plan();
            $o->setProperties(array(
                'user_id' => $user_id,
                'type' => $item->getType(),
                'data' => json_encode($item->getProducts()),
                'calories' => $item->getCalories(),
                'created' => $date ." 10:00:00",
            ));
            $this->save($o);
        }
    }
}