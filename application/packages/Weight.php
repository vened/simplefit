<?php

class Weight extends DomainObject
{
    protected $properties = array(
        'id' => null,
        'gender' => 0,
        'height' => 0,
        'min_weight' => 0,
        'max_weight' => 0,
        'created' => '0000-00-00',
    );

    public function getId()
    {
        return $this->getProperty('id');
    }

    public function getMinWeight()
    {
        return $this->getProperty('min_weight');
    }

    public function getMaxWeight()
    {
        return $this->getProperty('max_weight');
    }

    public function getCreated()
    {
        return $this->getProperty('created');
    }
}