<?php

class Helper_Diary
{
    protected static $foodTime = array(
        'breakfast' => '03:00',
        'breakfast2' => '10:00',
        'lunch' => '12:00',
        'lunch2' => '14:30',
        'dinner' => '17:00',
        'dinner2' => '20:00',
    );

    protected static $foodTypeText = array(
        'breakfast' => 'Завтрак',
        'breakfast2' => 'Поздний завтрак',
        'lunch' => 'Обед',
        'lunch2' => 'Полдник',
        'dinner' => 'Ужин',
    );

    /**
     * @array $product
     * @return json string
     */
    public static function serializeProduct($product)
    {
        return sprintf('[%s,"%s",%s,%s]', $product['id'], addslashes($product['name']),
                        $product['weight'], $product['calories']);
    }

    /**
     * @array $products
     * @return json string
     */
    public static function serializeProductData($products)
    {
        $data = array();

        foreach ($products as $product) {
            $data[] = self::serializeProduct($product);
        }

        return '[' . implode(',', $data) . ']';
    }

    public static function unserializeProductsData($data)
    {
        $data = json_decode($data, true);
        $result = array();

        foreach ($data as $product) {
            $result[] = array_combine(array('id', 'name', 'weight', 'calories'), $product);
        }

        return $result;
    }

    public static function getFoodTime($foodType, $date = null)
    {
        $time = '';
        if (isset(self::$foodTime[$foodType])) {
            $time = self::$foodTime[$foodType];
        }
        else {
            $time = date('H:i');
        }

        if (!$date) {
            $date = date('Y-m-d');
        }

        return $date . ' ' . $time;
    }

    public static function getFoodTypeText($v)
    {
        return isset(self::$foodTypeText[$v]) ? self::$foodTypeText[$v] : '';
    }
}