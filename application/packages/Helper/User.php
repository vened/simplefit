<?php

class Helper_User
{
    protected static $goal = array(
        '' => 'Нет ответа',
        'weight_loss' => 'Снижение веса',
        'weight_gain' => 'Набор веса',
        'weight_maintenance' => 'Поддержание веса',
    );

    protected static $vegetarianism = array(
        '' => 'Без ответа',
        'no' => 'Не готов отказаться от продуктов животного происхождения',
        'vegetarianism' => 'Вегетарианство',
        'veganism' => 'Веганство',
        'raw_food_diet' => 'Сыроедение',
        'fruit_diet' => 'Фрукторианство',
    );

    protected static $activity = array(
        0 => 0,
        1 => 1.2,
        2 => 1.375,
        3 => 1.55,
        4 => 1.725,
        5 => 1.9,
    );

    protected static $activityText = array(
        0 => 'Нет ответа',
        1 => 'Сидячий образ жизни',
        2 => 'Средняя активность (легкие упражнения 1-3 в неделю)',
        3 => 'Высокая активность (интенсивные занятия 3-5 раз в неделю)',
        4 => 'Очень высокая активность (тяжелые физические нагрузки 6-7 раз в неделю)',
        5 => 'Экстремальная активность (очень тяжелые физические нагрузки)'
    );

    protected static $foodType = array(
        'breakfast' => "Завтрак",
        'breakfast2' => "Поздний завтрак",
        'lunch' => "Обед",
        'lunch2' => "Поздний обед",
        'dinner' => "Ужин",
        'dinner2' => "Поздний ужин",
    );

    public static function goalList(){
        $out = array();
        foreach(self::$goal as $key => $val){
            $out[] = array("v" => $key, "text" => $val);
        }
        return $out;
    }

    public static function getGoal($goal) {
        return self::$goal[$goal];
    }


    public static function getVegetarianism($v)
    {
        return isset(self::$vegetarianism[$v]) ? self::$vegetarianism[$v] : '';
    }

    public static function vegetarianismList(){
        $out = array();
        foreach(self::$vegetarianism as $key => $v){
            $out[] = array("v" => $key, "text" => $v);
        }
        return $out;
    }

    public static function getActivityText($v)
    {
        //if()
        return self::$activityText[$v];
    }

    public static function activityList(){
        $out = array();
        foreach(self::$activity as $id => $val){
            $out[$id] = array("v" => $id, "text" => self::$activityText[$id]);
        }
        return $out;
    }



    public static function getFoodList(){
        $out = array();
        foreach(self::$foodType as $id => $val){
            $out[$id] = array("v" => $id, "text" => $val);
        }
        return $out;
    }


    public static function diffDate($start, $end = false)
    {
        if (!$end) {
            $end = date('Y-m-d H:i:s');
        }

        $interval = date_diff(date_create($start), date_create($end));
        $out = $interval->format("years:%Y,months:%M,days:%d,hours:%H,minutes:%i,seconds:%s");

        $result = array();

        foreach (explode(',', $out) as $val) {
            $v = explode(':', $val);
            $result[$v[0]] = (int) $v[1];
        }

//        array_walk(explode(',', $out),
//            function($val, $key) use(&$result)
//            {
//                $v = explode(':', $val);
//                $result[$v[0]] = $v[1];
//            });

        return $result;
    }

    public static function getActivity($code){
        return self::$activity[$code];
    }

    public static function getFood($code){
        return self::$foodType[$code];
    }
}