<?php

class Helper_Photo
{
    const PATH = '/simplefit/photos/';
    const DOMAIN = 'photos.';
    const TMP_DIR = '/tmp/';

    const SIZE_ORIGINAL = 0;
    const SIZE_400 = '400x400';
    const SIZE_329 = '329x329';
    const SIZE_140 = '140x140';
    const SIZE_213x156 = '213x156';

    protected static $sizeNames = array(
        self::SIZE_ORIGINAL => 0,
        self::SIZE_400 => 1,
        self::SIZE_329 => 2,
        self::SIZE_140 => 3,
        self::SIZE_213x156 => 4,
    );

    protected static function getPath(Photo $photo, $size, $type)
    {
        if (!in_array($size, array_keys(self::$sizeNames))) {
            return false;
        }

        if (!in_array($type, array('main', 'diary', 'original')) || !$photo->getId()) {
            return false;
        }

        $part1 = $part2 = $part3 = 0;
        $photoId = $photo->getId();
        $strlen = strlen($photoId);

        if ($strlen > 2) {
            $part1 = substr($photoId, -3, 1);
        }

        if ($strlen > 1) {
            $part2 = substr($photoId, -2, 1);
        }

        $part3 = substr($photoId, -1, 1);

        $path = $part1 . '/' . $part2 . '/' . $part3 . '/' . $photo->getUserId() . '/';

        if (!is_dir(self::PATH . $path)) {
            mkdir(self::PATH . $path, 0777, true);
            chmod(self::PATH . $path, 0777);
        }

        $suffix = '.jpg';

        if ($type != 'original') {
            if ($type == 'main') {
                $suffix = '.png';
            }
            $suffix = substr($type, 0, 1) . $suffix;
        }

        $path .= $photo->getId() . '_' . self::$sizeNames[$size] . $suffix;

        return $path;
    }

    public static function uploadPhoto(Photo $photo, $source, $isMainPhoto = false)
    {
        $source = self::TMP_DIR . $source;

        self::copyOriginal($photo, $source);

        if ($isMainPhoto) {
            self::resizeMainPhoto($photo, $source, self::SIZE_329);
            self::resizeMainPhoto($photo, $source, self::SIZE_213x156, false);
            self::resizeMainPhoto($photo, $source, self::SIZE_140);
        }
        else {
            self::resizeDiaryPhoto($photo, $source);
        }

        unlink($source);

        return true;
    }

    public static function getSystemPath(Photo $photo, $size, $type) {
        return self::PATH . self::getPath($photo, $size, $type);
    }

    protected static function getUrl(Photo $photo, $size, $type)
    {
        return 'http://' . self::DOMAIN . BASE_HOST . '/' . self::getPath($photo, $size, $type);
    }

    public static function getMainPhotoUrl(Photo $photo, $size)
    {
        return self::getUrl($photo, $size, 'main');
    }

    public static function getDiaryPhotoUrl(Photo $photo)
    {
        return self::getUrl($photo, self::SIZE_400, 'diary');
    }

    public static function resizeMainPhoto(Photo $photo, $source, $size, $round = true)
    {
        $path = self::getSystemPath($photo, $size, 'main');
        $newSize = explode('x', $size);

        if (!$path) {
            return false;
        }

        list($width, $height) = getimagesize($source);
        $image = new Imagick($source);
        //$image->setImageFormat('jpg');
        $image->setImageFormat('png');

        if ($width > $height) {
            // Horizontal image
            $newHeight = $newSize[1];
            $backup = $image->clone();
            $image->thumbnailimage(0, $newHeight);
            $newWidth = $image->getimagewidth();

            if ($newWidth >= $newSize[0]) {
                $x = (round($newWidth / 2) - round($newSize[0] / 2));
                $newWidth = $newSize[0];

                $image->cropImage($newWidth, $newHeight, $x, 0);
            }
            else {
                $image->destroy();
                $image = $backup->clone();
                $backup->destroy();
                unset($backup);

                $newWidth = $newSize[0];
                $image->thumbnailImage($newWidth, 0);
                $newHeight = $newSize[1];
                $image->cropImage($newWidth, $newHeight, 0, 0);
            }
        }
        else {
            // Vertical image
            $newWidth = $newSize[0];
            $image->thumbnailImage($newWidth, 0);
            $newHeight = $newSize[1];
            $image->cropImage($newWidth, $newHeight, 0, 0);
        }

        if ($round) {
            $r = round($size / 2);
            $image->roundCorners($r, $r);
        }

        $image->writeImage($path);
        $image->destroy();

        return true;
    }

    public function resizeDiaryPhoto(Photo $photo, $source)
    {
        $path = self::getSystemPath($photo, self::SIZE_400, 'diary');
        $newSize = explode('x', self::SIZE_400);

        $newWidth = $newSize[0];

        $image = new Imagick($source);
        $image->setImageFormat('jpg');

        // Resize proportionally
        $image->thumbnailImage($newWidth, 0);

        $image->writeImage($path);
        $image->destroy();

        return true;
    }

    public static function copyOriginal(Photo $photo, $source)
    {
        $path = self::getSystemPath($photo, self::SIZE_ORIGINAL, 'original');

        $image = new Imagick($source);
        $image->setImageFormat('jpg');

        $image->writeImage($path);
        $image->destroy();

        return true;
    }


    public function resizeDiaryPhotoOLD(Photo $photo, $source)
    {
        $path = self::getSystemPath($photo, self::SIZE_400, 'diary');

        list($width, $height) = getimagesize($source);
        $newWidth = self::SIZE_400;

        $ratio = $width / $newWidth;
        $newHeight = round($height / $ratio);

        $img = imagecreatefromstring(file_get_contents($source));
        $newImg = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresized($newImg, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        imagedestroy($img);

        return imagejpeg($newImg, $path, 100);
    }

    public static function copyOriginalOLD(Photo $photo, $source)
    {
        list($width, $height) = getimagesize($source);
        $path = self::getSystemPath($photo, self::SIZE_ORIGINAL, 'original');

        $img = imagecreatefromstring(file_get_contents($source));
        $newImg = imagecreatetruecolor($width, $height);
        imagecopy($newImg, $img, 0, 0, 0, 0, $width, $height);
        imagedestroy($img);

        return imagejpeg($newImg, $path, 100);
    }

    public static function resizeMainPhotoOLD(Photo $photo, $source, $size)
    {
        $path = self::getSystemPath($photo, $size, 'main');

        if (!$path) {
            return false;
        }

        list($width, $height) = getimagesize($source);

        $img = imagecreatefromstring(file_get_contents($source));
        $newImg = null;

        if ($width > $height) {
            // Horizontal image
            $newHeight = $size;
            $ratio = $height / $newHeight;
            $newWidth = round($width / $ratio);
            $srcX = (round($newWidth / 2) - round($size / 2)) * $ratio;
            $newWidth = $size;
            $width = $newWidth * $ratio;

            $newImg = imagecreatetruecolor($newWidth, $newHeight);
            imagecopyresized($newImg, $img, 0, 0, $srcX, 0, $newWidth, $newHeight, $width, $height);
        } else {
            // Vertical image
            $newWidth = $size;
            $ratio = $width / $newWidth;
            $newHeight = $size;
            $height = $newHeight * $ratio;
            $newImg = imagecreatetruecolor($newWidth, $newHeight);
            imagecopyresized($newImg, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        }

        imagedestroy($img);

        return imagejpeg($newImg, $path, 100);
    }
}