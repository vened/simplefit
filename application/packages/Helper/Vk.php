<?php

class Helper_Vk
{
    protected $appId;
    protected $appSecret;

    public function __construct($appId, $appSecret)
    {
        $this->appId = $appId;
        $this->appSecret = $appSecret;
    }

    public static function parseCookieData($cookieData)
    {
        $cookie = array();

        if ($cookieData) {
            parse_str($cookieData, $cookie);
        }

        return $cookie;
    }

    public static function checkSignature($cookie, $appSecret)
    {
        ksort($cookie);
        $sign = '';

        foreach ($cookie as $key => $val) {
            if ($key != 'sig') {
                $sign .= $key . '=' . $val;
            }
        }

        $sign .= $appSecret;

        return (md5($sign) == $cookie['sig'] && $cookie['expire'] > time());
    }

    public static function getSex($sex)
    {
        $result = null;

        switch ($sex) {
            case 1:
                $result = 'F';
                break;

            case 2:
                $result = 'M';
                break;
        }

        return $result;
    }
}