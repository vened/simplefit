<?php
/**
 * @author: mix
 * @date: 11.07.13
 */

class Helper_Email {

    /**
     * @var UserMapper
     */
    private $mapper;
    const LIMIT = 500;

    private $sended = 0;


    public function sendWeightNotification(){

        $this->mapper = new UserMapper();

        $this->sendFirstTime();
        $this->sendAgain();
    }

    private function getLimit(){
        return self::LIMIT - $this->sended;
    }

    public function sendFirstTime(){
        // not notified

        $sql = "select u.user_id as id from Simplefit.User u
            left outer join Simplefit.MailStats as ms on u.user_id=ms.user_id and template='weightNotify'
            where
            1
            and u.email_confirmed=1
            and last_login < subdate(now(), interval 2 day)
            and FIND_IN_SET('reminder', email_notify) > 0
            and ms.id is null
            limit " . $this->getLimit();

        $res = Db_Database::getInstance()->query($sql);
        echo "find new: " . $res->rowCount() . "\n";
        foreach($res as $row){
            $u = $this->mapper->find($row["id"]);
            if($u){

                if($this->sendNotify($u)){
                    echo "send new to :" . $row["id"] . " success\n";
                    $this->sended++;
                }else{
                    echo "send new to :" . $row["id"] . " fail\n";
                }

            }
        }
    }


    public function sendAgain(){
        $sql = "select u.user_id as id,
            first_name, last_name, u.email, max(ms.created)
            , (TIMESTAMPDIFF(day, last_login, max(ms.created)) <= 4 and TIMESTAMPDIFF(day,now(),max(ms.created)) >= 2) as day2
            , (TIMESTAMPDIFF(day, last_login, max(ms.created)) <= 18 and TIMESTAMPDIFF(day,now(),max(ms.created)) >= 7) as day7
            , (TIMESTAMPDIFF(day, last_login, max(ms.created)) <= 46 and TIMESTAMPDIFF(day,now(),max(ms.created)) >= 14) as day14

            from Simplefit.User u
            left join Simplefit.MailStats as ms on u.user_id=ms.user_id and template='weightNotify'
            where
            1
            and last_login < subdate(now(), interval 2 day)
            and FIND_IN_SET('reminder', email_notify) > 0
            and ms.id is not null
            group by(u.user_id) limit " . $this->getLimit();

        $res = Db_Database::getInstance()->query($sql);
        echo "find again: " . $res->rowCount() . "\n";
        foreach($res as $row){
            if($row["day2"] || $row["day7"] || $row["day14"]){
                $u = $this->mapper->find($row["id"]);
                if($u){

                    if($this->sendNotify($u)){
                        echo "send again to :" . $row["id"] . " success\n";
                        $this->sended++;
                    }else{
                        echo "send again to :" . $row["id"] . " fail\n";
                    }
                }
            }
        }
    }

    protected function sendNotify(\User $u){
        $mail = new Email_WeightNotify($u);
        return $mail->send();
    }




}