<?php
/**
 * @author: mix
 * @date: 14.08.13
 */
class Helper_Register {

    public static function Register(User $user, $properties){

        $prefixMapper = new PrefixMapper();
        $userMapper = new UserMapper();

        $prefix = $prefixMapper->getFirstActive();
        $properties['prefix_id'] = $prefix['prefix_id'];

        if (!empty($properties['password'])) {
            $user->setPassword($properties['password']);
            unset($properties['password']);
        }
        else {
            $user->setPassword(uniqid());
        }

        if(isset($_COOKIE["link_id"])){
            $properties["link_id"] = (int)$_COOKIE["link_id"];
        }

        $user->setProperties($properties);
        $user->setSecret();
        $user->setLastLogin();
        $user->setProperty("email_notify", "reminder,news");

        $result = $userMapper->save($user);

        if ($result) {
            $blogsUserMapper = new \Blogs\UserMapper();
            $blogsUser = $blogsUserMapper->findByEmail($user->getEmail());
            if (!$blogsUser->getId()) {
                $blogsUser->setLogin($user->getId());
                $blogsUser->setPassword($user->getPassword());
                $blogsUser->setEmail($user->getEmail());
                $blogsUser->setDateRegister();
                $blogsUser->setUserIpRegister($_SERVER['REMOTE_ADDR']);
                $userName = $user->getFirstName();
                if ($user->getLastName()) {
                    $userName .= ' ' . $user->getLastName();
                }
                $blogsUser->setUserName($userName);
            }

            if ($sex = $user->getGender()) {
                $blogsUser->setUserSex($sex);
            }
            $blogsUser->setUserProfileDate();
            $blogsUser->setUserActivate();
            $blogsUserMapper->save($blogsUser);

            $blogsSessionMapper = new \Blogs\SessionMapper();
            $blogsSession = $blogsSessionMapper->findByUserId($user->getId());
            $blogsSession->setSessionKey();
            $blogsSession->setUserId($blogsUser->getId());
            $blogsSession->setIpCreate($_SERVER['REMOTE_ADDR']);
            $blogsSession->setIpLast($_SERVER['REMOTE_ADDR']);
            $blogsSession->setDateCreate();
            $blogsSession->setDateLast();
            $blogsSessionMapper->save($blogsSession);

            $blogsBlogMapper = new \Blogs\BlogMapper();
            $blog = $blogsBlogMapper->findByUserId($blogsUser->getId());

            if ($blog->isEmpty()) {
                $blog->setUserId($blogsUser->getId());
                $blog->setUserName($blogsUser->getUserName());
                $blogsBlogMapper->save($blog);
            }

            Auth::authByUser($user);
        }

        return $result;
    }

}