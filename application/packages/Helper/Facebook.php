<?php

class Helper_Facebook
{
    protected $appId;
    protected $appSecret;

    public function __construct($appId, $appSecret)
    {
        $this->appId = $appId;
        $this->appSecret = $appSecret;
    }

    public static function parseSignedRequest($signedRequest, $appSecret)
    {
        list($encodedSig, $payload) = explode('.', $signedRequest, 2);

        // decode the data
        $sig = self::base64_url_decode($encodedSig);
        $data = json_decode(self::base64_url_decode($payload), true);

        // Unknown algorithm. Expected HMAC-SHA256
        if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
            return null;
        }

        // Adding the verification of the signed_request below
        $expectedSig = hash_hmac('sha256', $payload, $appSecret, $raw = true);
        if ($sig !== $expectedSig) {
            return null;
        }

        return $data;
    }

    protected static function base64_url_decode($input)
    {
      return base64_decode(strtr($input, '-_', '+/'));
    }

    public static function getGender($gender)
    {
        $result = null;

        switch ($gender) {
            case 'female':
                $result = 'F';
                break;

            case 'male':
                $result = 'M';
                break;
        }

        return $result;
    }
}