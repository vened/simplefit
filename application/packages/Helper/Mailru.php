<?php

class Helper_Mailru
{
    const API_URL = 'http://www.appsmail.ru/platform/api';

    protected static $apiParams = array(
        'app_id' => '',
        'method' => '',
        'secure' => 1,
        'session_key' => '',
        'sig' => '',
    );

    protected static $appId;
    protected static $appSecret;

    public static function setParams($appId, $appSecret)
    {
        self::$appId = $appId;
        self::$appSecret = $appSecret;
    }

    public static function getCookieData()
    {
        parse_str(urldecode(Request::getCookie('mrc')), $result);
        return $result;
    }

    protected static function getSignature($cookie, $appSecret)
    {
        ksort($cookie);
        $sign = '';

        foreach ($cookie as $key => $val) {
            if ($key != 'sig') {
                $sign .= $key . '=' . $val;
            }
        }

        return md5($sign . $appSecret);
    }

    public static function checkSignature($cookie, $appSecret)
    {
        $sign = self::getSignature($cookie, $appSecret);
        return ($sign == $cookie['sig'] && $cookie['exp'] > time());
    }

    public static function getGender($gender)
    {
        $result = null;

        switch ($gender) {
            case 0:
                $result = 'M';
                break;

            case 1:
                $result = 'F';
                break;
        }

        return $result;
    }

    protected static function doRequest($method, $sessionData)
    {
        self::$apiParams['app_id'] = self::$appId;
        self::$apiParams['method'] = $method;
        self::$apiParams['session_key'] = $sessionData['session_key'];
        $sig = self::getSignature(self::$apiParams, self::$appSecret);
        self::$apiParams['sig'] = $sig;

        $params = array();

        foreach (self::$apiParams as $key => $val) {
            $params[] = $key . '=' . $val;
        }

        $url = self::API_URL . '?' . implode('&', $params);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return json_decode($result, true);
    }

    public static function getUserInfo($sessionData)
    {
        $result =  self::doRequest('users.getInfo', $sessionData);
        return isset($result[0]) ? $result[0] : $result;
    }
}