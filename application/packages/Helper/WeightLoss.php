<?php
/**
 * @author: mix
 * @date: 25.02.13
 */

class Helper_WeightLoss {

    private $table = "Simplefit.UserWeightLoss";

    protected function getMemCacheKey(){
        return "total_weight_loss";
    }

    public function updateTotal($amount){
        $db = Db_Database::getInstance();
        $total = str_replace(",", ".",$this->getTotal());
        $amount = str_replace(",", ".",$db->quote($amount));

        $sql = "insert into $this->table set date=now(), weight = ".$total." + ".$amount.
            " on duplicate key update weight = weight + " .$amount;
        $db->query($sql);

        MCache::getInstance()->delete($this->getMemCacheKey());
    }

    public function getTotal(){
        if($res = MCache::getInstance()->get($this->getMemCacheKey())){
            return $res;
        }
        $db = Db_Database::getInstance();
        $res = $db->query("Select weight from $this->table order by date desc limit 1");
        $data = $res->fetchAll();

        $res = $data[0]['weight'];
        MCache::getInstance()->set($this->getMemCacheKey(),$res);
        return $res;
    }
}