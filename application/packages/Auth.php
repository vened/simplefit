<?php
class Auth
{
    const MAX_SECRET_TRY = 3;
    const KEY_SECRET_TRY = 'stry_%u';
    //const AUTH_VALID = 31536000; // 60 * 60 * 24 * 365
    const AUTH_VALID = 15552000; // 60 * 60 * 24 * 180
    private static $user = null;

    private function __construct() { }

    public function __clone() {}

    public static function init()
    {
        if (!Session::started()) {
            Session::start();
        }

        $sid = Session::id();

        if (!empty($sid) && self::getUserBySession()) {
            return;
        }

        self::authorizeBySecret();
    }

    public static function loggedIn()
    {
        return self::$user ? true : false;
    }

    public static function authorize($email, $password, $rememberMe = false)
    {
        $email = trim($email);
        $password = trim($password);

        if (!$email || !$password) {
            return false;
        }

        $userMapper = new UserMapper();
        $user = $userMapper->findByEmail($email);
        $fakeUser = new User();
        $fakeUser->setPassword($password);

        if ($user->getPassword() == $fakeUser->getPassword())
        {
            $user->setSecret();
            $user->setLastLogin();
            $userMapper->save($user);

            Request::setCookie('u', $user->getId());

            if ($rememberMe) {
                Request::setCookie('sk', $user->getSecret(), self::AUTH_VALID);
            }

            self::setUser($user);
            self::onSuccess();

            return true;
        }

        return false;
    }

    public static function hackAuthorize($userId)
    {
        self::logout();
        $userMapper = new UserMapper();
        $user = $userMapper->find($userId);

        if (!$user->isEmpty()) {
            self::setUser($user);
        }

        return true;
    }

    public static function authByUser($user)
    {
        self::setUser($user);
        self::onSuccess();
    }

    private function getUserBySession()
    {
        $userId = Session::get('user_id');

        if (!$userId) {
            return false;
        }

        $userMapper = new UserMapper();
        $user = $userMapper->find($userId);

        if (self::userValidate($user)) {
            self::setUser($user);
            return true;
        }

        return false;
    }

    public static function authorizeBySecret()
    {
        $secretKey = Request::getCookie('sk');

        if ($secretKey)
        {
            $ts = Request::getCookie('ts');
            if ($ts + self::AUTH_VALID > time()) {
                return false;
            }
        }
        else {
            return false;
        }

        $userId = Request::getCookie('u');
        $mcache = MCache::getInstance();
        $secretCheck = self::checkSecretTries($userId);

        if ($secretCheck)
        {
            $mcacheKey = sprintf('user_sk_%s', $secretKey);
            $user = $mcache->get($mcacheKey);
            if (!$user) {
                $userMapper = new UserMapper();
                $user = $userMapper->findBySecretKey($secretKey);
            }

            if (self::userValidate($user))
            {
                if (is_numeric($secretCheck)) {
                    $mcache->delete(sprintf(self::KEY_SECRET_TRY, $userId));
                }

                $mcache->set($mcacheKey, $user, MCache::EXPIRE_DAY);
                self::setUser($user);
                self::onSuccess();
                return true;
            }
            else {
                if ($secretCheck === true) {
                    $mcache->set(sprintf(self::KEY_SECRET_TRY, $user->getId()), 1, MCache::EXPIRY_SHORT);
                }
                else {
                    $mcache->increment(sprintf(self::KEY_SECRET_TRY, $user->getId()));
                }

                $mcache->delete($mcacheKey);
            }
        }

        return false;
    }

    private function checkSecretTries($uid)
    {
        $mcache = MCache::getInstance();
        $secretTried = $mcache->get(sprintf(self::KEY_SECRET_TRY, $uid));

        if (!$secretTried) {
            return true;
        }
        if ($secretTried > self::MAX_SECRET_TRY) {
            return false;
        }

        return $secretTried;
    }

    private function userValidate($user)
    {
        $userId = Request::getCookie('u');
        $secretKey = Request::getCookie('sk');

        if (!$user || !$userId || $user->isEmpty() || $user->getId() != $userId) {
            return false;
        }
        elseif ($secretKey && $secretKey != $user->getSecret()) {
            return false;
        }

        return true;
    }

    private static function setUser($user)
    {
        self::$user = $user;
        Session::set('user_id', $user->getProperty('user_id'));
        Request::setCookie('u', $user->getId(), self::AUTH_VALID);
    }

    /**
     * @param $uid
     * @param $token
     * @return Admin_MailStat
     */
    public static function loginByToken($uid,$token){
        $m = new Admin_MailStatMapper();
        $o = $m->findByToken($uid,$token);

        if($o){
            $um = new UserMapper();
            $user = $um->find($uid);
            $o->setProperty("used",1);
            $o->setProperty("updated", date("Y-m-d H:i:s"));
            $m->save($o);
            if($user){
                self::setUser($user);
                return $o;
            }
        }
        return null;
    }

    /**
     * @return User
     */
    public static function getUser()
    {
        return self::$user;
    }

    public function getUserId()
    {
        if (self::loggedIn() && self::$user->getId()) {
            return self::$user->getId();
        }

        return null;
    }

    public static function logout()
    {
        self::$user = null;
        Request::delCookie('sk');
        Request::delCookie('u');
        Request::delCookie('ts');
        Session::set('user_id', null);
        Session::generate();
    }

    private static function onSuccess()
    {
        return;
    }
}
?>