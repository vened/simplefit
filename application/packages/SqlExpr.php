<?php

class SqlExpr
{
    protected $expression;

    public function __construct($expr)
    {
        $this->expression = $expr;
    }

    public function getExpr()
    {
        return $this->expression;
    }
}