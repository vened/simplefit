<?php
/**
 * @author: mix
 * @date: 12.01.13
 */
class Error_Log{

    private $callPointer = 0;

    private $path;

    private $trace;

    private $nl = "\n";

    private $useDebugInfo = true;

    private $fullTrace = true;

    /**
     * Максимальная длина значения аргумента, которую нужно вывести в трейсе.
     */
    const MAX_ARG_SIZE = 32;

    /**
     * @param string $path путь до файла
     * @param array|null $trace debug_backtrace
     */
    public function __construct($path, &$trace = null){
        $this->path = $path;
        if($trace){
            $this->trace = &$trace;
        }else{
            $this->trace = debug_backtrace();
            $this->callPointer++;
        }
    }

    /**
     * разделитель строк
     *
     * @param $nl
     * @return Error_Log
     */
    public function setDelimiter($nl){
        $this->nl = $nl;
        return $this;
    }

    /**
     * перемещает указатель вызванного места в трейсе
     *
     * нужен для внутренних запросов
     *
     * @param $i
     * @return Error_Log
     */

    public function addCallPointer($i){
        $this->callPointer += $i;
        return $this;
    }

    /**
     * выводить расширенную инфу
     *
     * @param $use
     * @return Error_Log
     */
    public function useDebugInfo($use){
        $this->useDebugInfo = $use;
        return $this;
    }

    /**
     * выводить полный трейс
     *
     * @param $trace
     * @return Error_Log
     */
    public function setFullTrace($trace){
        $this->fullTrace = $trace;
        return $this;
    }

    /**
     * Возвращает трейс + debugInfo.
     *
     * @return string
     */
    public function getText() {
        $msg = '';
        if($this->fullTrace){
            $msg .= $this->getTrace();
        }else{
            $msg .= $this->getCodePointer();
        }

        if($this->useDebugInfo){
            $msg .= $this->getDebugInfo();
        }

        return $msg;
    }

    /**
     * выводит в лог сообщение
     *
     * @param $msg
     * @return void
     */
    public function message($msg){

        $msg = "[".date("Y-m-d H:i:s") . "] " . $msg;

        if($this->fullTrace){
            $msg .= $this->getTrace();
        }else{
            $msg .= $this->getCodePointer();
        }

        if(!$this->useDebugInfo){
            $msg .= $this->getDebugInfo();
        }

        $msg .= "\n";

        $path = $this->path;

        if($this->isStdLog()){

            $path = ini_get("error_log");

            if(!$path){
                $path = (php_sapi_name() == "cli"? "/monamour/php/logs/php-cli-error.log" : "/monamour/php/logs/php-error.log");
            }
        }
        file_put_contents($path, $msg, FILE_APPEND);

    }

    private function isStdLog(){
        return $this->path == "php://stderr";
    }

    protected function getCodePointer(){
        return " in " . $this->trace[$this->callPointer]["file"] . " at " . $this->trace[$this->callPointer]["line"];
    }

    protected function getTrace(){
        $result   = array();
        $result[] = "\nStack trace:";
        $skip = 0;
        $debugBackTrace = $this->trace;

        if( count($debugBackTrace) > 0)
        {
            $numLine  = " ";
            $fileLine = "FILE";
            $funcLine = "FUNCTION";

            $numMaxLen  = strlen($numLine);
            $fileMaxLen = strlen($fileLine);
            $funcMaxLen = strlen($funcLine);

            foreach ( $debugBackTrace as $index => &$trace ) {
                $line = "#".($index-$skip);
                $trace['column1'] = $line;
                if ( strlen($line) > $numMaxLen ) {
                    $numMaxLen = strlen($line);
                }

                if ( !empty($trace['file']) ) {
                    $line = @"$trace[file]:$trace[line]";
                } else {
                    $line = "internal_function";
                }
                $trace['column2'] = $line;
                if ( strlen($line) > $fileMaxLen ) {
                    $fileMaxLen = strlen($line);
                }

                $line = @"$trace[class]$trace[type]$trace[function]";
                $trace['column3'] = $line;
                if ( strlen($line) > $funcMaxLen ) {
                    $funcMaxLen = strlen($line);
                }

                $objectClass = '';
                if (isset($trace['object']) && is_object($trace['object'])) {
                    $objectClass = get_class($trace['object']);
                }
                $className = '';
                if (isset($trace['class'])) {
                    $className = $trace['class'];
                }
                if ($objectClass && $className != $objectClass) {
                    $classLine = "<$objectClass> $className";
                } else {
                    $classLine = $className;
                }
                $line = @"$classLine$trace[type]$trace[function]";
                $trace['column4'] = $line;
            }
            unset($trace);

            $result[] = $numLine.str_repeat(" ", $numMaxLen - strlen($numLine) + 1)
                .$fileLine.str_repeat(" ", $fileMaxLen - strlen($fileLine) + 1)
                .$funcLine.str_repeat(" ", $funcMaxLen - strlen($funcLine) + 1)
                ."LINE";

            foreach ( $debugBackTrace as $index => $trace ) {
                if ( $index < $skip ) {
                    continue;
                }
                $nextTrace = isset($debugBackTrace[$index+1])? $debugBackTrace[$index+1]: array('function'=>'{main}', 'column3'=>'{main}');
                $numLine = "#".($index-$skip);
                $fileLine = $trace['column2'];
                $funcLine = $nextTrace['column3'];
                $resultString = $numLine.str_repeat(" ", $numMaxLen - strlen($numLine) + 1)
                    .$fileLine.str_repeat(" ", $fileMaxLen - strlen($fileLine) + 1)
                    .$funcLine.str_repeat(" ", $funcMaxLen - strlen($funcLine) + 1)
                    .$trace['column4'];
                $args = array();
                if ( isset($trace['args']) ) {
                    foreach ( $trace['args'] as $arg ) {
                        if ( is_scalar($arg) || is_null($arg) ) {
                            $arg = str_replace("\n", "\\n",var_export($arg, 1));
                            if ( strlen($arg) > self::MAX_ARG_SIZE ) {
                                $arg = substr($arg, 0, self::MAX_ARG_SIZE)."...";
                            }
                        } elseif ( is_object($arg) ) {
                            $arg = get_class($arg);
                        } elseif ( is_array($arg) ) {
                            $arg = "array[".count($arg)."]";
                        } elseif ( is_resource($arg) ) {
                            $arg = "<resource>";
                        } else {
                            $arg = "<unknown>";
                        }
                        $args[] = $arg;
                    }
                }
                $result[] = $resultString."(".implode(", ", $args).")";
            }
        }

        return implode($this->nl, $result);
    }

    protected function getDebugInfo(){

        if (php_sapi_name() != "cli") {
            $info = "\nRequest:";
            foreach (array('REQUEST_METHOD', 'SERVER_PROTOCOL', 'SERVER_NAME', 'REQUEST_URI') as $varName) {
                if (!empty($_SERVER[$varName])) {
                    $info .= ' '.$_SERVER[$varName];
                }
            }
            if (!empty($_SERVER['HTTP_REFERER'])) {
                $info .= "\nReferer: ".$_SERVER['HTTP_REFERER'];
            }
            /*
            if (Api_User::getInstance()->isLogined()) {
                $info .= "\nAnketa: " . Anketa_Helper::getInstance()->getCurrentObject()->getId();
                $info .= "\nlang_id: " . Language_Helper::getInstance()->getCurrent()->getId();
            }

            $info .= "\npartner_id:". Partner_Helper::getInstance()->getCurrent()->getId();
            */

        }else $info = "";

        return $info;
    }

}