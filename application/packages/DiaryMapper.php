<?php

class DiaryMapper extends Mapper
{
    protected $tableName = '#db_prefix#_Cluster.#table_prefix#_Diary';
    protected $domainObject = 'Diary';
    protected $pk = 'id';
    protected $useCache = false;

    public function __construct($prefixId) {
        parent::__construct($prefixId);
    }

    public function findByUserId($userId, $limit = null, $offset = 0) {
        $options = 'ORDER BY created DESC';

        if ($limit) {
            $options .= ' LIMIT ' . $limit;
            if ($offset) {
                $options .= ' OFFSET ' . $offset;
            }
        }

        $select = array(new SqlExpr('SQL_CALC_FOUND_ROWS *'));
        return $this->findByField(array('user_id' => $userId), $select, $options);
    }

    public function findByDate($userId, $date)
    {
        $date = substr($date, 0, 10);
        $dateFrom = $this->quote($date);
        $dateTo = $this->quote($date . ' 23:59:59');
        $options = sprintf('AND created >= %s AND created <= %s', $dateFrom, $dateTo);

        return $this->findByField(array('user_id' => $userId), array(), $options);
    }

    /**
     * @return Diary
     */
    public function getCurrentWeight($userId, $date = null)
    {
        $options = '';
        if ($date) {
            $date = substr($date, 0, 10);
            $options .= 'AND created >= ' . $this->quote($date)
                        . ' AND created <= ' . $this->quote($date . ' 23:59:59');
        }

        $options .= ' ORDER BY created DESC LIMIT 1';
        return $this->findOneByField(array('user_id' => $userId, 'type' => 'weight'), array(), $options);
    }

    public function getWeightData($userId, $start, $end)
    {
        $end = substr($end, 0, 10) . ' 23:59:59';
        $sql = "SELECT *
                FROM %s
                WHERE
                    user_id = ? AND type='weight' AND created >= ? AND created <= ?
                ORDER BY created DESC";

        $sql = sprintf($sql, $this->quotedTableName());

        $params = array($userId, $start, $end);

        return $this->query($sql, $params);
    }

    public function getAllWeightData($userId)
    {
        $options = 'ORDER BY created DESC';
        return $this->findByField(array('user_id' => $userId, 'type' => 'weight'), array(), $options);
    }

    public function getCaloriesData($userId, $start, $end)
    {
        $end = substr($end, 0, 10) . ' 23:59:59';
        $sql = "SELECT *
                FROM %s
                WHERE
                    user_id = ? AND type='calories' AND created >= ? AND created <= ?
                ORDER BY created DESC";

        $sql = sprintf($sql, $this->quotedTableName());

        $params = array($userId, $start, $end);

        return $this->query($sql, $params);
    }

    public function getMenuByUserId($userId, $foodType, $date)
    {
        $date = substr($date, 0, 10);

        $sql = "SELECT *
                FROM %s
                WHERE
                    user_id = ? AND type='calories' AND food_type = ?
                    AND created >= ? AND created <= ?
                LIMIT 1";

        $sql = sprintf($sql, $this->quotedTableName());
        $params = array($userId, $foodType, $date, $date . ' 23:59:59');

        $result = $this->query($sql, $params);

        return isset($result[0]) ? $result[0] : new Diary();
    }


    public function insert(DomainObject $do){
        switch($do->getType()){
            case "weight" :
            case "calories" :
                $m = new UserStatsMapper();
                $m->hitStat($do->getType());
        }
        return parent::insert($do);
    }
}