<?php

class Mapper
{
    protected $tableName = '';
    protected $domainObject = '';
    protected $pk = 'id';
    protected $useCache = false;
    protected $prefix;

    /**
     * @var Db_Database
     */
    protected $db;
    protected $memcache;

    public function __construct($prefixId = null)
    {
        $this->db = Db_Database::getInstance();

        $this->memcache = MCache::getInstance();

        if ($prefixId) {
            $prefixMapper = new PrefixMapper();
            $prefix = $prefixMapper->find($prefixId);
            $this->prefix = $prefix;

            $this->tableName = str_replace(
                                        array('#db_prefix#', '#table_prefix#'),
                                        array($prefix['db_prefix'], $prefix['table_prefix']),
                                        $this->tableName);
        }
    }

    public function newObject()
    {
        return new $this->domainObject;
    }

    /**
     * @param $id
     * @return DomainObject
     */
    public function find($id)
    {
        return $this->findOneByField(array($this->pk => $id));
    }

    protected function findOneByField($cond = array(), $select = array(), $options = '')
    {
        $result = $this->findByField($cond, $select, $options);
        return isset($result[0]) ? $result[0] : new $this->domainObject;
    }

    protected function findByField($cond = array(), $select = array(), $options = '')
    {
        $sql = $this->buildSelectSQL(array_keys($cond), $select, $options);

        return $this->query($sql, $cond);
    }

    public function query($sql, $bind = array())
    {

        //Error::stderrLog($sql);
        $return = array();

        if (!$this->db) {
            return null;
        }
        $result = $this->queryResult($sql, $bind);
        try {

            while ($row = $result->fetch()) {
                /**
                 * @var DomainObject $do
                 */
                if ($this->domainObject) {
                    $do = $this->newObject();
                    $do->setProperties($row, true);
                    $return[] = $do;
                } else {
                    $return[] = $row;
                }
            }
        } catch (PDOException $e) {
            //echo $e->getMessage();
        }
        return $return;
    }

    public function queryResult($sql, $bind = array()){
        $stmt = $this->db->query($sql,array_values($bind));
        return new Db_Result($stmt,$this->db);
    }

    public function calcFoundRows()
    {
        $s = $this->db->query("SELECT FOUND_ROWS()");
        return (int)$s->fetchColumn();
    }


    protected function quote($string) {
        return $this->db->quote($string);
    }

    public function save(DomainObject $do)
    {
        if (!$do->countModified())
        {
            return false;
        }

        $this->beforeSave($do);

        $id = null;

        if ($do->getId())
        {
            $this->update($do);
        }
        else
        {
            $id = $this->insert($do);

            if ($this->pk && $id) {
                $do->setProperty($this->pk, $id);
            }
        }

        $this->afterSave($do);

        return $id;
    }

    protected function insert(DomainObject $do)
    {
        $modified = $do->getModified();
        unset($modified[$this->pk]);
        $this->db->query(
            $this->buildInsertSQL($modified),
            $this->buildArgs($do, $modified)
        );
        return $this->db->lastInsertId();
    }

    protected function update(DomainObject $do)
    {
        $modified = $do->getModified();
        unset($modified[$this->pk]);

        $sql = $this->buildUpdateSQL($modified);
        array_push($modified, $this->pk);

        $this->db->query(
            $sql,
            $this->buildArgs($do, $modified)
        );
    }

    public function drop(DomainObject $do)
    {
        $sql = $this->buildDeleteSQL();
        $stmt = $this->db->query($sql,array($do->getId()));
    }

    protected function buildInsertSQL($modified)
    {
        $sql = 'INSERT INTO %s (%s) VALUES (%s)';

        $fields = array();
        $markers = array();
        foreach ($modified as $field)
        {
            $fields[] = $field;
            $markers[] = '?';
        }

        return sprintf($sql, $this->quotedTableName(), implode(', ', $fields), implode(', ', $markers));
    }

    protected function buildUpdateSQL($modified)
    {
        $sql = 'UPDATE %s SET %s WHERE %s=?';

        $forUpdate = array();
        foreach ($modified as $field)
        {
            $forUpdate[] = sprintf('`%s` = ?', $field);
        }

        return sprintf($sql, $this->quotedTableName(), implode(', ', $forUpdate), $this->pk);
    }

    protected function buildDeleteSQL()
    {
        $sql = 'DELETE FROM %s WHERE %s = ?';
        return sprintf($sql, $this->quotedTableName(), $this->pk);
    }

    protected function buildSelectSQL($cond = array(), $select = array(), $options = '')
    {
        $sql = 'SELECT %s FROM %s %s %s';

        $selectFields = array();
        if (count($select))
        {
            foreach ($select as $key => $value)
            {
                if (!is_numeric($key))
                {
                    $selectFields[] = sprintf('`%s` AS `%s`', $value, $key);
                }
                else
                {
                    if ($value instanceof SqlExpr) {
                        $selectFields[] = sprintf('%s', $value->getExpr());
                    }
                    else {
                        $selectFields[] = sprintf('`%s`', $value);
                    }
                }
            }
        }

        $condFields = array();
        if (count($cond))
        {
            foreach ($cond as $field)
            {
                $condFields[] = sprintf('`%s` = ?', $field, $field);
            }
        }

        return sprintf($sql,
            count($selectFields) ? implode(', ', $selectFields) : '*',
            $this->quotedTableName(),
            count($condFields) ? 'WHERE ' . implode(' AND ', $condFields) : '',
            $options
        );
    }

    protected function quoteFieldName($fieldName)
    {
        if (strpos($fieldName, '.')) {
            return '`' . str_replace('.', '`.`', $fieldName) . '`';
        }

        return '`' . $fieldName . '`';
    }

    protected function quotedTableName()
    {
        return $this->quoteFieldName($this->tableName);
    }

    protected function buildArgs(DomainObject $pl, $fieldNames)
    {
        $args = array();
        foreach ($fieldNames as $f)
        {
            $args[] = $pl->getProperty($f);
        }
        return $args;
    }

    protected function beforeSave($do) { }

    protected function afterSave($do) { }
}