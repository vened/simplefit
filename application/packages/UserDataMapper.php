<?php

class UserDataMapper extends Mapper
{
    protected $tableName = 'Simplefit.UserData';
    protected $domainObject = 'UserData';
    protected $pk = 'user_id';
    protected $useCache = false;

}