<?php

class ProductsMapper extends Mapper
{
    protected $tableName = 'Simplefit.Products';
    protected $domainObject = 'Product';
    protected $pk = 'id';
    protected $useCache = false;

    public function findByFirstLetter($query, $limit = null)
    {
        $query = $this->quote($query . '%');
        $sql = sprintf("SELECT * FROM %s WHERE name LIKE %s", $this->quotedTableName(), $query);

        if (!is_null($limit)) {
            $sql .= ' LIMIT ' . intval($limit);
        }

        return $this->query($sql);
    }

    public function findByIds($ids, $sort = false)
    {
        $ids = implode(', ', array_map('intval', $ids));
        $sql = sprintf('SELECT * FROM %s WHERE id IN (%s)', $this->quotedTableName(), $ids);

        if ($sort) {
            $sql .= sprintf(' ORDER BY FIELD(id, %s)', $ids);
        }

        return $this->query($sql);
    }

    public function getLastProducts($userId = null, $limit = 10, $offset = 0)
    {
        $sql = sprintf("SELECT * FROM %s WHERE " . $this->avalibleToView() , $this->quotedTableName());

        if ($userId) {
            $sql .= " OR user_id = '" . $userId . "'";
        }

        $sql .= ' ORDER BY id DESC LIMIT ' . $limit;

        if ($offset) {
            $sql .= ' OFFSET ' . $offset;
        }

        return $this->query($sql);
    }


    public function findUserProducts($where = array(), $offset=0, $limit=10){
        $where_sql = "";
        if($where)$where_sql = "where " . implode(" AND ", $where);
        $sql = "select SQL_CALC_FOUND_ROWS * from $this->tableName $where_sql order by name limit $limit offset $offset";
        return $this->query($sql);
    }

    public function findUserProductsTotal(){
        $data = $this->queryResult("select FOUND_ROWS() as total")->fetchAll();
        return $data[0]["total"];
    }

    private function getTotalKey(){
        return "products_total";
    }

    public function getTotal(){

        if($res = MCache::getInstance()->get($this->getTotalKey())){
            return $res;
        }
        $sql = "select count(*) as cnt from $this->tableName where " . $this->avalibleToView();
        $res = $this->db->query($sql)->fetch();
        MCache::getInstance()->set($this->getTotalKey(), $res["cnt"]);
        return $res["cnt"];
    }

    protected function update(DomainObject $do){
        if(in_array("is_available",$do->getModified())){
            MCache::getInstance()->delete($this->getTotalKey());
        }
        parent::update($do);
    }


    public function productList($page, $limit){
         $sql = "Select * from $this->tableName where ".$this->avalibleToView() . "order by name asc " . "limit $limit " . "offset " . $page*$limit;
        return $this->db->query($sql)->fetchAll();
    }

    public function insert(DomainObject $do){
        if($do->getProperty("user_id") > 0){
            $m = new UserStatsMapper();
            $m->hitStat("product");
        }
        return parent::insert($do);
    }

    private function avalibleToView(){
        return " is_available = '1' and moderated='1' ";
    }
}