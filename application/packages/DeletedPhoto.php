<?php

class DeletedPhoto extends DomainObject
{
    protected $properties = array(
        'id' => null,
        'photo_id' => 0,
        'user_id' => 0,
    );

    public function getId()
    {
        return $this->getProperty('id');
    }

    public function getPhotoId()
    {
        return $this->getProperty('photo_id');
    }

    public function getUserId()
    {
        return $this->getProperty('photo_id');
    }

    public function setPhotoId($photoId)
    {
        $this->setProperty('photo_id', $photoId);
    }

    public function setUserId($userId)
    {
        $this->setProperty('user_id', $userId);
    }
}