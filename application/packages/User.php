<?php

class User extends DomainObject
{
    protected $properties = array(
        'user_id' => null,
        'prefix_id' => 0,
        'first_name' => '',
        'last_name' => '',
        'email' => '',
        'password' => '',
        'secret' => '',
        'gender' => '',
        'birthdate' => '0000-00-00',
        'height' => 0,
        'goal' => '',
        'weight' => 0,
        'weight_goal' => 0,
        'activity' => 0,
        'vegetarianism' => 'no',
        'main_photo_id' => 0,
        'email_confirmed' => 0,
        'status' => 'Filling',
        'last_login' => '0000-00-00',
        'created' => '',
        'vk_id' => 0,
        'fb_id' => '0',
        'mm_id' => '0',
        'link_id' => 0,
        'email_notify' => '',
        'recommendation_count' => 0
    );

    protected $virtualProperties = array(
        'user_id',
        'country_id',
        'region_id',
        'city_id',
        'vk_posted',
        'fb_posted',
        'mm_posted',
    );

    public function getId()
    {
        return $this->getProperty('user_id');
    }

    public function getFirstName()
    {
        return $this->getProperty('first_name');
    }

    public function getLastName()
    {
        return $this->getProperty('last_name');
    }

    public function getEmail()
    {
        return $this->getProperty('email');
    }

    public function getPrefix()
    {
        return $this->getProperty('prefix_id');
    }

    public function getPassword()
    {
        return $this->getProperty('password');
    }

    public function getSecret()
    {
        return $this->getProperty('secret');
    }

    public function getGender()
    {
        return $this->getProperty('gender');
    }

    public function getHeight()
    {
        return $this->getProperty('height');
    }

    public function getWeight()
    {
        return $this->getProperty('weight');
    }

    public function getStatus()
    {
        return $this->getProperty('status');
    }

    public function getMainPhotoId()
    {
        return $this->getProperty('main_photo_id');
    }

    public function getVegetarianism()
    {
        return $this->getProperty('vegetarianism');
    }

    public function isActive()
    {
        return $this->getStatus() == 'Active';
    }

    public function getBirthdate()
    {
        return $this->getProperty('birthdate');
    }

    public function getCreated()
    {
        return $this->getProperty('created');
    }

    public function getCreatedDate()
    {
        return substr($this->getProperty('created'), 0, 10);
    }

    public function setFirstName($firstName)
    {
        $this->setProperty('first_name', $firstName);
    }

    public function emailConfirmed()
    {
        return $this->getProperty('email_confirmed') ? true : false;
    }

    public function getGoal()
    {
        return $this->getProperty('goal');
    }

    public function getWeightGoal()
    {
        return $this->getProperty('weight_goal');
    }

    public function getActivity()
    {
        return $this->getProperty('activity');
    }

    public function getRecommendationCount()
    {
        return $this->getProperty('recommendation_count');
    }

    public function setRecommendationCount($count)
    {
        $this->setProperty('recommendation_count', $count);
    }

    public function setLastName($lastName)
    {
        $this->setProperty('last_name', $lastName);
    }

    public function setEmail($email)
    {
        $this->setProperty('email', $email);
    }

    public function setPrefix($prefixId)
    {
        $this->setProperty('prefix_id', $prefixId);
    }

    public function setPassword($password)
    {
        $salt = 'pwd_';
        $password = md5($salt . $password);

        $this->setProperty('password', $password);
    }

    public function setSecret($secret = false)
    {
        if (!$secret) {
            $secret = md5(uniqid(microtime()));
        }

        $this->setProperty('secret', $secret);
    }

    public function setGender($gender)
    {
        if (in_array($gender, $this->enumGender())) {
            $this->setProperty('gender', $gender);
        }
    }

    public function setActivity($activity)
    {
        $this->setProperty('activity', $activity);
    }

    public function setStatus($status)
    {
        if (in_array($status, $this->enumStatus())) {
            $this->setProperty('status', $status);
        }
    }

    public function setEmailConfirmed()
    {
        $this->setProperty('email_confirmed', 1);
    }

    public function setLastLogin($lastLogin = null)
    {
        if (!$lastLogin) {
            $lastLogin = date('Y-m-d H:i:s');
        }

        $this->setProperty('last_login', $lastLogin);
    }

    public function setCreated()
    {
        $this->setProperty('created', date('Y-m-d H:i:s'));
    }

    public function enumGender()
    {
        return array('M', 'F');
    }

    public function enumGoal()
    {
        return array('weight_loss', 'weight_gain', 'weight_maintenance');
    }

    private function enumStatus()
    {
        return array('Active', 'Blocked', 'Deleted', 'Filling');
    }

    public function enumVegetarianism()
    {
        return array('no', 'vegetarianism', 'veganism', 'raw_food_diet', 'fruit_diet');
    }

    public function setVkId($vkId)
    {
        $this->setProperty('vk_id', $vkId);
    }

    public function setFbId($fbId)
    {
        $this->setProperty('fb_id', $fbId);
    }

    public function setMailruId($fbId)
    {
        $this->setProperty('mm_id', $fbId);
    }

    public function getAge()
    {
        $birthdate = getdate(strtotime($this->getBirthdate()));
        $now = getdate();

        if ($now[0] < $birthdate[0]) {
            return 0;
        }

        $years = $now['year'] - $birthdate['year'];
        if ($now['yday'] < $birthdate['yday']) {
            $years--;
        }

        return $years;
    }

    public function toArray()
    {
        $properties = $this->properties;
        $properties['age'] = $this->getAge();
        $properties['goal_text'] = Helper_User::getGoal($this->getGoal());
        $properties['vegetarianism_text'] = Helper_User::getVegetarianism($this->getProperty('vegetarianism'));
        foreach(array("reminder","news") as $key){
            $properties["email_$key"] = (int)(strpos($this->getProperty("email_notify"),$key) !== false);
        }
        return $properties;
    }

    public function vkPosted()
    {
        return $this->getProperty('vk_posted');
    }

    public function fbPosted()
    {
        return $this->getProperty('fb_posted');
    }

    public function mmPosted()
    {
        return $this->getProperty('mm_posted');
    }

    public function getCalories(){
        $calories = null;

        $activity = \Helper_User::getActivity($this->getActivity());

        if ($this->getGender() == 'M') {
            $calories = (
                66
                + 13.7 * $this->getWeight()
                + 5 * $this->getHeight()
                - 6.8 * $this->getAge()
            ) * $activity;

        }
        else {
            $calories = (
                65.5
                + 9.6 * $this->getWeight()
                + 1.8 * $this->getHeight()
                - 4.7 * $this->getAge()
            ) * $activity;
        }

        switch ($this->getGoal()) {
            case 'weight_loss':
                $calories -= $calories * 0.15;
                break;

            case 'weight_gain':
                $calories += $calories * 0.15;
                break;
        }

        if ($calories < 1200) {
            $calories = 1200;
        }

        $calories = round($calories);

        return $calories;
    }
}