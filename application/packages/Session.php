<?php

class Session
{
    const NAME = 'sid';
    const CTRY = 5;
    const LENGHT = 32;
    const SESSION_CHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890';

    private static $expireTime = null;

    private static $started, $changed = false;
    private static $system = array();

    const CACHE_PREFIX = 'sid_';
    private static $status = true;


    public static function start($name = self::NAME, $id = null)
    {
        if (self::$started) {
            return;
        }

        ini_set('session.cookie_domain', '.' . $_SERVER['HTTP_HOST']);
        self::setExpire(3600 * 7);

        /**
         * Set session handlers only
         * for test and production.
         * Because there is no Memcached module at local.
         */
        if (extension_loaded('memcached')) {
            self::init();
        }

        self::name($name);
        $sid = self::detectID();
        self::$started = true;
        session_start();
        if (self::check(self::id(), true) === false) {
            self::generate();
        }
    }

    public static function generate()
    {
        session_regenerate_id();
    }

	protected static function check($sid, $check_params = false) {
		$len = strlen($sid);
		if ($len !== self::LENGHT) {
			return false;
		}
		if ($len !== strspn($sid, self::SESSION_CHARS)) {
			return false;
		}
		if ($check_params === true) {
			return self::check_params();
		}
		return true;
	}

	private static function check_params() {
		$params = array(
			'REMOTE_ADDR' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '',
//			'HTTP_USER_AGENT' => !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
		);
		foreach ($params as $k => $v) {
			$var = self::get($k);
			if (empty($var)) {
				self::set($k, $v);
			}
			else if ($var !== $v) {
				return false;
			}
		}
		return true;
	}

	public static function started()
	{
		return self::$started ? true : false;
	}

	public static function changed()
	{
		return self::$changed ? true : false;
	}

	public static function pause()
	{
		self::$changed = false;
		session_write_close();
		self::$started = false;
	}

	public static function destroy()
	{
		self::$changed = true;
		session_unset();
		$_SESSION = array();
		if (isset($_COOKIE[self::name()])) {
			setcookie(self::name(), false,null,"/");
			unset($_COOKIE[self::name()]);
		}
		if  (isset($_GET[self::name()])) {
			unset($_GET[self::name()]);
		}
		session_destroy();
	}

	public static function clear()
	{
		self::$changed = true;
		session_unset();
		$_SESSION = array();
	}

	public static function detectID()
	{
	    $name = self::name();
	    if (self::useCookies() && isset($_COOKIE[$name])) {
        	return $_COOKIE[$name];
        };
        if (isset($_GET[$name])) {
        	return $_GET[$name];
        }
        if (isset($_POST[$name])) {
        	return $_POST[$name];
        }
        return null;
	}

	public static function name($name = null)
	{
	   	if (!is_null($name)) {
    		return session_name($name);
    	}
    	return session_name();
	}

	public static function id($id = null)
	{
    	if ($id !== null) {
    		return session_id($id);
    	}
     	return session_id();
	}

	public static function setExpire($seconds = 3600)
	{
        if (isset($seconds) && is_int($seconds) && $seconds >= 1) {
            ini_set('session.gc_maxlifetime', $seconds);
            self::$expireTime = $seconds;
            return true;
        }
        return false;
	}

	public static function getExpire()
	{
	    if (!self::$expireTime)
	    {
	        self::$expireTime = ini_get('session.gc_maxlifetime');
	    }
	    return self::$expireTime;
	}

	public static function useCookies($useCookies = null)
	{
	    $return = false;
	    if (ini_get('session.use_cookies') == '1') {
            $return = true;
        }

        if ($useCookies != null) {
            if ($useCookies) {
                ini_set('session.use_cookies', 1);
            } else {
                ini_set('session.use_cookies', 0);
            }
        }
        return $return;
	}

    public static function & get($name, $default = null)
    {
    	$rel = & $_SESSION;
    	$null = null;

    	if (is_array($name)) {
    		$key = array_pop($name);
    		foreach ($name as $k) {
    			if(!isset($rel[$k]) || !is_array($rel[$k])) {
    				if (is_null($default)) return $null;
    				else $rel[$k] = array();
    			}
    			$rel = & $rel[$k];
    		}
    	}
		else {
    		$key = $name;
    	}

    	if (!isset($rel[$key]) && !is_null($default)) {
    		$rel[$key] = $default;
    	}
		else if (!isset($rel[$key])) {
    		return $null;
    	}

    	return $rel[$key];
    }

    public static function set($name, $value = null)
    {
		self::$changed = true;
    	$rel = & $_SESSION;
    	if (is_array($name)) {
    		$key = array_pop($name);
    		foreach ($name as $k) {
    			if(!isset($rel[$k]) || !is_array($rel[$k])) $rel[$k] = array();
    			$rel = & $rel[$k];
    		}
    	}
		else {
    		$key = $name;
    	}

        $return = (isset($rel[$key])) ? $rel[$key] : null;

        if (null === $value) {
            unset($rel[$key]);
        }
		else {
            $rel[$key] = $value;
        }

        return $return;
    }

    public static function remove($name)
    {
    	if (self::defined($name)) {
			self::$changed = true;
    		return self::set($name);
		}
    	else {
    		return null;
		}
    }

    public static function defined($name)
    {
    	if (is_array($name)) {
    		$rel = & $_SESSION;
    		foreach ($name as $key) {
    			if (isset($rel[$key])) {
    				$rel = & $rel[$key];
   				}
				else {
    				return false;
    			}
    		}
    		return true;
    	}
		else {
    		return (isset($_SESSION) && isset($_SESSION[$name]));
    	}
    }

	/**
	 * Some default cofing settings
	 *
	 */
	public static function init($HandlerClass = 'SessionMCache')
	{

		ini_set('session.hash_function', 1);
		ini_set('session.hash_bits_per_character', 5);

		$result = session_set_save_handler(
  			array(__CLASS__, "sess_open"),
  			array(__CLASS__, "sess_close"),
  			array(__CLASS__, "sess_read"),
  			array(__CLASS__, "sess_write"),
  			array(__CLASS__, "sess_destroy"),
  			array(__CLASS__, "sess_gc")
		) or die("Failed to register session handler");
	}

	private function __construct(){}

    /**
     * Establish connection to a memcache
     */
    public static function sess_open($save_path, $session_name)
    {
        return true;
    }

    /**
     * Closes connection and free resources
     */
    public static function sess_close()
    {
        return true;
    }

    public static function sess_read($id)
    {
        return MCache::getInstance()->get(self::CACHE_PREFIX . $id);
    }

    /**
     * Writes data to Memcache from $_SESSION[]
     */
    public static function sess_write($id, $sess_data)
    {
        MCache::getInstance()->set(self::CACHE_PREFIX . $id, $sess_data, self::getExpire());
        return true;
    }


    /**
     * Perfoms mechanism for session_destroy() function
     */
    public static function sess_destroy($id)
    {
        MCache::getInstance()->delete(self::CACHE_PREFIX . $id);
        return true;
    }

    /**
     * Perfoms mechanism for session.garbage_collection
     */
    public static function sess_gc($maxlifetime)
    {
        return true;
    }
}
