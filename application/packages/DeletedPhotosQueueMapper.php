<?php

class DeletedPhotosQueueMapper extends Mapper
{
    protected $tableName = 'Queue.DeletedPhotos';
    protected $domainObject = 'DeletedPhoto';
    protected $pk = 'id';
    protected $useCache = false;

    public function getAll($maxId)
    {
        $sql = sprintf('SELECT * FROM %s WHERE id <= ?', $this->quotedTableName());

        return $this->query($sql, array($maxId));
    }

    public function getMaxId()
    {
        $do = $this->domainObject;
        $this->domainObject = null;

        $sql = sprintf('SELECT MAX(id) AS max_id FROM %s', $this->quotedTableName());
        $result = $this->query($sql);
        $this->domainObject = $do;

        return $result[0]['max_id'];
    }
}