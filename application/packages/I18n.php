<?php

class I18n
{
    /**
     * !!! Do not change !!!
     */
    const LOCALE_ENCODING = 'utf8';
    const LOCALES_PATH = 'locales';

    /**
     * @var array
     */
    static protected $locales = array(
        'ru' => 'ru_RU',
        'en' => 'en_US',
    );

    /**
     * @var string
     */
    static protected $language = null;

    /**
     * @var string
     */
    static protected $activeDomain = 'simplefit';

    static protected $domains = array('simplefit');


    /**
     * Set language
     *
     */
    static public function setLanguage($language, $geoLng)
    {
        if (!array_key_exists($language, self::$locales))
        {
            if ($geoLng == 'rus') {
                $language = 'ru';
            }
            else {
                $language = 'en';
            }
        }

        self::$language = $language;

        setlocale(LC_ALL, self::$locales[$language]);

        bindtextdomain(self::$activeDomain, self::LOCALES_PATH);
        textdomain(self::$activeDomain);
    }

    /**
     * Get current language
     */
    static public function getLanguage()
    {
        return self::$language;
    }

    public static function _()
    {
        if (func_num_args() == 0) { return; }

        $args = func_get_args();
        $text = array_shift($args);
        return vsprintf(_($text), $args);
    }

    public static function _n()
    {
        if (func_num_args() == 0) { return; }

        $args = func_get_args();
        $msgid = array_shift($args);
        $msgid_pural = array_shift($args);
        $n = array_shift($args);

        return vsprintf(ngettext($msgid, $msgid_pural, $n), $args);
    }

    /*
     * Функция для подстановки нужного склонения в зависимости от цифры
     * принимает 4 аргумента
     * 1 - текст для числа "1"
     * 2 - текст для чисел "2,3,4"
     * 3 - текст для чисел "5,6,7,8,9,0"
     * 4 - само число
     * функция учитывает порядок чисел, т.е. правило будет работать и для чисел больше 10 и для чисел больше 100 итд,
     * дополнительных аргументов не требуется
     */
    public function _n_rus(){
        if (func_num_args() != 4) { return; }

        $args = func_get_args();
        $count_1 = array_shift($args);
        $count_2to4 = array_shift($args);
        $count_5to0 = array_shift($args);
        $n = array_shift($args);
        $m = $n % 100;
        if ($m < 20) {
            if ($m == 1){
                $returnString = self::_n($count_1, $count_1, $n, $n);
            }
            elseif ($m > 1 && $m < 5) {
                $returnString = self::_n($count_2to4, $count_2to4, $n, $n);
            }
            elseif ($m > 4 || $m == 0) {
                $returnString = self::_n($count_5to0, $count_5to0, $n, $n);
            }
        }
        else {
            $divisionTail = $m % 10;
            if($divisionTail == 1) {
                $returnString = self::_n($count_1, $count_1, $n, $n);
            }
            elseif ($divisionTail > 1 && $divisionTail < 5) {
                $returnString = self::_n($count_2to4, $count_2to4, $n, $n);
            }
            elseif ($divisionTail > 4 || $divisionTail == 0) {
                $returnString = self::_n($count_5to0, $count_5to0, $n, $n);
            }
        }
        return $returnString;
    }
}