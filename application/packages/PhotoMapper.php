<?php

class PhotoMapper extends Mapper
{
    protected $tableName = '#db_prefix#_Cluster.#table_prefix#_Photos';
    protected $domainObject = 'Photo';
    protected $pk = 'photo_id';
    protected $useCache = false;

    public function __construct($prefixId) {
        parent::__construct($prefixId);
    }

    protected function beforeSave(Photo $do)
    {
        if ($do->isEmpty()) {
            $do->setCreated();
        }
    }

    public function drop(Photo $photo)
    {
        parent::drop($photo);

        $deletedPhotoMapper = new DeletedPhotosQueueMapper();
        $deletedPhoto = new DeletedPhoto();
        $deletedPhoto->setPhotoId($photo->getId());
        $deletedPhoto->setUserId($photo->getUserId());
        $deletedPhotoMapper->save($deletedPhoto);
    }

    public function insert(DomainObject $do){
        $m = new UserStatsMapper();
        $m->hitStat("photo");
        return parent::insert($do);
    }
}