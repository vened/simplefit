<?php

class MCache extends Memcached
{
    const EXPIRY_SHORT    = 300;
    const EXPIRY_NORMAL   = 1800;
    const EXPIRY_MEDIUM   = 3600;
    const EXPIRY_LONG     = 21600;
    const EXPIRE_DAY      = 86400;

    protected static $instance = null;

    protected $servers = array(
        array('127.0.0.1', 11211)
    );
    public function __construct()
    {
        parent::__construct();
        $this->addServer($this->servers[0][0], $this->servers[0][1]);
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}