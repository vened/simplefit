<?php
/**
 * @author: mix
 * @date: 06.03.13
 */
class UserStats extends DomainObject{
    protected $properties = array(
        'date' => null,
        'weight' => 0,
        'calories' => 0,
        'photo' => 0,
        'plan' => 0,
        'product' => 0,
    );
}
