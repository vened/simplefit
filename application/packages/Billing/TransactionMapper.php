<?php
namespace Billing;

/**
 * @method \Billing\Transaction newObject()
 * @method \Billing\Transaction find()
 */
class TransactionMapper extends \Mapper
{
    protected $tableName = 'Billing.Transaction';
    protected $domainObject = 'Billing\Transaction';
    protected $pk = 'id';
    protected $useCache = false;

    public function __construct() {
        parent::__construct(null);
    }
}