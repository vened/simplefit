<?php

/**
 * http://www.robokassa.ru/ru/Doc/Ru/Interface.aspx#222
 */

namespace Billing;

class Robokassa
{
    const MERCHANT_LOGIN = 'simplefit';
    const MERCHANT_PASS1 = 'simplefitpass1';
    const MERCHANT_PASS2 = 'simplefitpass2';

    const URL = 'https://auth.robokassa.ru/Merchant/Index.aspx';
    //const URL = 'http://test.robokassa.ru/Index.aspx';
    const SECURITY_KEY_MASK = 'sMerchantLogin:nOutSum:nInvId:sMerchantPass1';

    protected $params = array(
        'MrchLogin' => self::MERCHANT_LOGIN,
        'OutSum' => 0,
        'InvId' => 0,
        'Desc' => '',
        'SignatureValue' => '',
        'IncCurrLabel' => '',
        'Email' => '',
        'Culture' => 'ru'
    );

    public function getParams()
    {
        $this->params['SignatureValue'] = $this->getSecurityKey();
        return $this->params;
    }

    public function setParam($param, $value)
    {
        if (!isset($this->params[$param])) {
            return false;
        }

        $this->params[$param] = $value;
        return true;
    }

    protected function getSecurityKey()
    {
        $replacements = array(self::MERCHANT_LOGIN, $this->params['OutSum'], $this->params['InvId'], self::MERCHANT_PASS1);
        return md5(str_replace(explode(':', self::SECURITY_KEY_MASK), $replacements, self::SECURITY_KEY_MASK));
    }

    public function checkCallbackParams(array $params)
    {
        $signatureMask = 'nOutSum:nInvId:sMerchantPass2';

        $replacements = array($params['OutSum'], $params['InvId'], self::MERCHANT_PASS2);
        return md5(str_replace(explode(':', $signatureMask), $replacements, $signatureMask)) == strtolower($params['sig']);
    }

    public function getUrl()
    {
        $url = self::URL . '?';
        $pos = 0;
        foreach ($this->getParams() as $param => $value) {
            $url .= ($pos ? '&' : '') . $param . '=' . $value;
            $pos++;
        }

        return $url;
    }
}