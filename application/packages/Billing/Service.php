<?php

namespace Billing;

class Service
{
    protected static $services = array(
        1 => 'Рекомендации'
    );

    public static function getService($serviceId)
    {
        if (!isset(self::$services[$serviceId])) {
            return false;
        }

        return self::$services[$serviceId];
    }
}