<?php

namespace Billing;

class Payment
{
    const COST_1 = 100;
    const COST_2 = 150;
    const COST_3 = 240;

    protected $transactionMapper;
    protected $transaction;
    protected $paymentMethod;

    public function __construct()
    {
        $this->transactionMapper = new TransactionMapper();
    }

    public function init(\User $user, $amount, $serviceId = 1)
    {
        $service = Service::getService($serviceId);
        if (!$this->checkAmount($amount) || !$service) {
            return false;
        }

        $transaction = $this->getTransaction();
        $transaction->setUserId($user->getId());
        $transaction->setServiceId($serviceId);
        $transaction->setAmount($amount);
        $this->transactionMapper->save($transaction);

        if (!$transaction->getId()) {
            return false;
        }

        $this->transaction = $transaction;

        $this->paymentMethod = $this->getPaymentMethod();
        $this->paymentMethod->setParam('OutSum', $amount);
        $this->paymentMethod->setParam('InvId', $this->transaction->getId());
        $this->paymentMethod->setParam('Desc', $service);
        $this->paymentMethod->setParam('Email', $user->getEmail());

        return true;
    }

    public function closeTransaction()
    {
        $transaction = $this->getTransaction();

        if (!$transaction || $transaction->isEmpty()) {
            return false;
        }

        if (!$this->runService()) {
            return false;
        }

        $transaction->setStatus('Paid');
        $transaction->setUpdated();
        $this->transactionMapper->save($transaction);

        return true;
    }

    protected function checkAmount($amount)
    {
        if (!in_array($amount, array(self::COST_1, self::COST_2, self::COST_3))) {
            return false;
        }

        return true;
    }

    protected function runService()
    {
        $transaction = $this->getTransaction();
        switch ($transaction->getServiceId()) {
            case 1:
                $userMapper = new \UserMapper();
                $user = $userMapper->find($transaction->getUserId());
                $count = 0;

                switch ($transaction->getAmount()) {
                    case self::COST_1:
                        $count = 2;
                        break;
                    case self::COST_2:
                        $count = 7;
                        break;
                    case self::COST_3:
                        $count = 14;
                        break;
                }

                $user->setRecommendationCount($user->getRecommendationCount() + $count);
                $userMapper->save($user);
                return true;
        }

        return false;
    }

    protected function checkService($serviceId)
    {
        return Service::getService($serviceId) ? true : false;
    }

    public function getTransaction($transactionId = 0)
    {
        if (!$this->transaction) {
            if ($transactionId) {
                $this->transaction = $this->transactionMapper->find($transactionId);
            }
            else {
                $this->transaction = new Transaction();
            }
        }

        return $this->transaction;
    }

    public function getPaymentMethod()
    {
        if (!$this->paymentMethod) {
            $this->paymentMethod = new Robokassa();
        }
        return $this->paymentMethod;
    }

    public function getRedirectUrl()
    {
        return $this->paymentMethod->getUrl();
    }
}