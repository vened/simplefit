<?php
namespace Billing;

class Transaction extends \DomainObject
{
    protected $properties = array(
        'id' => null,
        'user_id' => 0,
        'service_id' => 0,
        'amount' => 0,
        'status' => 'Init',
        'updated' => null,
        'created' => null,
    );

    public function getId()
    {
        return $this->getProperty('id');
    }

    public function getUserId()
    {
        return $this->getProperty('user_id');
    }

    public function getServiceId()
    {
        return $this->getProperty('service_id');
    }

    public function getAmount()
    {
        return $this->getProperty('amount');
    }

    public function getStatus()
    {
        return $this->getProperty('status');
    }

    public function getUpdated()
    {
        return $this->getProperty('updated');
    }

    public function getCreated()
    {
        return $this->getProperty('created');
    }

    public function getCreatedDate()
    {
        return substr($this->getProperty('created'), 0, 10);
    }

    public function isPaid()
    {
        return $this->getStatus() == 'Paid';
    }

    public function setId($id) {
        $this->setProperty('id', $id);
    }

    public function setUserId($userId)
    {
        $this->setProperty('user_id', $userId);
    }

    public function setServiceId($serviceId)
    {
        $this->setProperty('service_id', $serviceId);
    }

    public function setAmount($amount)
    {
        $this->setProperty('amount', $amount);
    }

    public function setStatus($status)
    {
        $this->setProperty('status', $status);
    }

    public function setUpdated($updated = null)
    {
        if (!$updated) {
            $updated = date('Y-m-d H:i:s');
        }

        $this->setProperty('updated', $updated);
    }

    public function setCreated($created = null)
    {
        if (!$created) {
            $created = date('Y-m-d H:i:s');
        }
        $this->setProperty('created', $created);
    }
}