<?php
/**
 * @author: mix
 * @date: 25.01.13
 */
class Admin_AdminMapper extends Mapper
{
    protected $tableName = 'Simplefit.Admin';
    protected $domainObject = 'Admin_Admin';
    protected $pk = 'id';
    protected $useCache = false;

    /**
     * @param $login
     * @param $password
     * @return Admin_Admin
     */
    public function login($login,$password){
        $sql = "select * from $this->tableName where login=? and password=md5(concat(?,created))";
        $ret = $this->query($sql, array($login,$password));
        if($ret){
            return $ret[0];
        }
        else return false;
    }

    public function getList(){
        return $this->queryResult("select * from $this->tableName")->fetchAll();
    }

    public function saveAdmin(Admin_Admin $admin, $password){
        $sql = "insert into $this->tableName set name=?, login=?, password=md5(concat(?,now())), created=now()";
        $this->query($sql, array($admin->getProperty("name"),$admin->getProperty("login"),$password));
    }
}