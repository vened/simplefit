<?php
/**
 * @author: mix
 * @date: 22.01.13
 */
class Admin_AppContext extends AppContext {

    protected function createController(){

        $this->module = "admin";
        try{
            return parent::createController();
        }catch (Exception $e){
            $this->actionName = strtolower($this->controllerName);
            $this->controllerName = "index";
            return parent::createController();
        }
    }

    public function getNotFoundTemplate(){
        return 'admin/not_found.tpl';
    }

    public function getTemplate(){
        return "$this->module/$this->controllerName/$this->actionName.tpl";
    }
}
