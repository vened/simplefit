<?php
/**
 * @author: mix
 * @date: 19.03.13
 */
class Admin_MailStatMapper extends Mapper{
    protected $tableName = 'Simplefit.MailStats';
    protected $domainObject = 'Admin_MailStat';
    protected $pk = 'id';
    protected $useCache = false;

    /**
     * @param $uid
     * @param $token
     * @return Admin_MailStat
     */
    public function findByToken($uid,$token){
        $sql = "select * from $this->tableName where user_id=? and token=? and used=0";
        $ret = $this->query($sql, array($uid,$token));
        if($ret)return $ret[0];
        else return false;
    }
}