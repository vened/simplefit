<?php
/**
 * @author: mix
 * @date: 27.01.13
 */
class Admin_LinkStatMapper extends Mapper{
    protected $tableName = 'Simplefit.Linkstats';
    protected $domainObject = 'Admin_LinkStats';
    protected $pk = 'id';
    protected $useCache = false;


    public function setHit($link_id,$session_id){
        $q = "insert into $this->tableName (link_id,session_id,created) VALUES(?,?,now()) on duplicate key update created=now()";

        $this->query($q,array($link_id,$session_id,));
    }

}