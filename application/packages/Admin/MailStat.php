<?php
/**
 * @author: mix
 * @date: 19.03.13
 */
class Admin_MailStat extends DomainObject{
    protected $properties = array(
        'id' => null,
        'template' => '',
        'user_id' => '',
        'email' => '',
        'token' => '',
        'used' => 0,
        'created' => '0000-00-00',
        'viewed'  => "0000-00-00",
        'updated' => '0000-00-00',
    );
}