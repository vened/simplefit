<?php
/**
 * @author: mix
 * @date: 27.01.13
 */
class Admin_LinkMapper extends Mapper{
    protected $tableName = 'Simplefit.Links';
    protected $domainObject = 'Admin_Link';
    protected $pk = 'id';
    protected $useCache = false;

    public function findAll($limit = null, $offset=0){
        $limit_str = "";
        if($limit){
            $limit_str = "limit $limit offset=$offset";
        }

        return $this->queryResult("select SQL_CALC_FOUND_ROWS * from $this->tableName order by created desc $limit_str");
    }


    public function getStat($date_from,$date_to, $orderby='hits'){

        $between = "between '$date_from 00:00:00' and '$date_to 23:59:59'";

        $sql = "
            select link_id, sum(hits) as hits, sum(u_cnt)as u_cnt, sum(d_cnt) as d_cnt, sum(email) as email from (
                select link_id, count(*) as hits, null as u_cnt, null as d_cnt, null as email from Simplefit.Linkstats where created $between group by link_id
                union
                select link_id, null as hits, count(*) as u_cnt, null as d_cnt, null as email from Simplefit.User where created $between group by link_id
                union
                select link_id, null as hits, null as u_cnt, count(*) as d_cnt, null as email from Simplefit.User u left join 0_Cluster.0_Diary as d on u.user_id=d.user_id where d.created $between group by u.link_id
                union
                select link_id, null as hits, null as u_cnt, null as d_cnt, count(*) as email from Simplefit.User where email_confirmed=1 and created $between group by link_id
            ) as cc  group by link_id order by $orderby desc
        ";
        return $this->queryResult($sql);
    }

    public function getTotal($date_from,$date_to){
        $between = "between '$date_from 00:00:00' and '$date_to 23:59:59'";

        $sql = "
            select  sum(hits) as hits, sum(u_cnt)as u_cnt, sum(d_cnt) as d_cnt, sum(email) as email from (
                select  count(*) as hits, null as u_cnt, null as d_cnt, null as email from Simplefit.Linkstats where created $between
                union
                select  null as hits, count(*) as u_cnt, null as d_cnt, null as email from Simplefit.User where created $between
                union
                select  null as hits, null as u_cnt, count(*) as d_cnt, null as email from Simplefit.User u left join 0_Cluster.0_Diary as d on u.user_id=d.user_id where d.created $between
                union
                select  null as hits, null as u_cnt, null as d_cnt, count(*) as email from Simplefit.User where email_confirmed=1 and created $between
            ) as cc ";
        $res = $this->queryResult($sql)->fetchAll();
        return $res[0];
    }
}