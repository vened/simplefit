<?php
/**
 * @author: mix
 * @date: 25.01.13
 */
class Admin_Admin extends DomainObject{

    protected $properties = array(
        'id' => null,
        'login' => '',
        'name' => '',
        'password' => '',
        'status' => 'Active',
        'created' => null,
    );

    public function checkRight($name){
        if(
            in_array(
                $this->getProperty("login"),
                array("moderate1", "moderate2", "moderate3", "moderate", )
            )
        ){
            if($name == "products"){
                return true;
            }else{
                return false;
            }

        }else{
            return true;
        }
    }

}