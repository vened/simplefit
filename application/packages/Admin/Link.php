<?php
/**
 * @author: mix
 * @date: 27.01.13
 */
class Admin_Link extends DomainObject{
    protected $properties = array(
        'id' => null,
        'url' => '',
        'description' => '',
        'created' => '',
    );

}