<?php
/**
 * @author: mix
 * @date: 14.08.13
 */

class Invite extends DomainObject
{
    protected $properties = array(
        'id' => null,
        'email' => 0,
        'name' => "",
        'gender' => "",
        "sent" => "0000-00-00",
        "view" => "0000-00-00",
        "hit" => "0000-00-00",
        "unsubscribe" => "0000-00-00",
    );
}