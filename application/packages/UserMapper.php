<?php

class UserMapper extends Mapper
{
    protected $tableName = 'Simplefit.User';
    protected $domainObject = 'User';
    protected $pk = 'user_id';

    protected $joinTable = '`Simplefit`.`UserData`';

    /**
     * @param int $id
     * @return User
     */
    public function find($id)
    {
        return $this->findOneByField(array('u.user_id' => $id));
    }

    /**
     * @param $email
     * @return User
     */
    public function findByEmail($email)
    {
        return $this->findOneByField(array('email' => $email));
    }

    public function findBySecretKey($sk)
    {
        return $this->findOneByField(array('secret' => $sk));
    }

    public function findByVkId($vkId)
    {
        return $this->findOneByField(array('vk_id' => $vkId));
    }

    public function findByFbId($fbId)
    {
        return $this->findOneByField(array('fb_id' => $fbId));
    }

    public function findByMailruId($mailruId)
    {
        return $this->findOneByField(array('mm_id' => $mailruId));
    }

    protected function beforeSave($do)
    {
        if ($do->isEmpty()) {
            $do->setCreated();
            $prefixMapper = new PrefixMapper();
            $prefixMapper->incRecordsCount($do->getPrefix());
        }
    }





    protected function buildSelectSQL($cond = array(), $select = array(), $options = '')
    {
        $sql = 'SELECT %s FROM %s u LEFT JOIN %s ud ON (ud.user_id = u.user_id) %s %s';

        $selectFields = array();
        if (count($select)) {
            foreach ($select as $key => $value) {
                if (!is_numeric($key)) {
                    $selectFields[] = sprintf('`%s` AS `%s`', $value, $key);
                } else {
                    $selectFields[] = sprintf('`%s`', $value);
                }
            }
        }

        $condFields = array();
        if (count($cond)) {
            foreach ($cond as $field) {
                $condFields[] = $this->quoteFieldName($field) . ' = ?';
            }
        }

        $userDataFields = 'ud.country_id, ud.region_id, ud.city_id, ud.vk_posted, fb_posted, mm_posted';
        return sprintf($sql,
            count($selectFields) ? implode(', ', $selectFields) : 'u.*, ' . $userDataFields,
            $this->quotedTableName(),
            $this->joinTable,
            count($condFields) ? 'WHERE ' . implode(' AND ', $condFields) : '',
            $options
        );

    }

    public function getStatList($offset,$limit,$orderBy="dc_cnt"){

        $sql =  "select SQL_CALC_FOUND_ROWS u.user_id,first_name, last_name, email, floor(datediff(now(), birthdate)/365)as age,
            vk_id,fb_id, date(u.created) as signin, date(u.last_login) as last_login, email_confirmed,
            count(un.dw) as dw_cnt, count(un.dc) as dc_cnt, count(un.p) as p_cnt, count(un.days) as d_cnt
            from Simplefit.User as u
            left join
                (select user_id, id as dw, null as dc,null as p, null as days from 0_Cluster.0_Diary where type='weight'
                union
                select user_id, null as dw, id as dc, null as p, null as days from 0_Cluster.0_Diary where type='calories'
                union
                select user_id, null as dw, null as dc, photo_id as p, null as days from 0_Cluster.0_Photos
                union
                select user_id, null as dw, null as dc,null as p, id as days from 0_Cluster.0_Diary group by user_id, date(created)
                ) as un on u.user_id=un.user_id

            group by u.user_id

            order by $orderBy desc
            limit $limit offset $offset

            ";

        $res =  $this->queryResult($sql);
        $out =  array("users" => $res->fetchAll());
        $out["total"] = $res->calcFoundRows();
        return $out;
    }
}