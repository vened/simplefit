<?php
/**
 * объект запроса
 */
class HttpRequest
{
    protected $vars = array();

    /**
     * @param $var
     * @param null $default
     * @return mixed
     */
    public function get($var, $default = null)
    {
        return (isset($_GET[$var]) ? $_GET[$var] :
            (isset($_POST[$var]) ? $_POST[$var] : $default));
    }

    public function getHost()
    {
        return $_SERVER['HTTP_HOST'];
    }

    public function getRemoteAddr()
    {
        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
    }

    /**
     * Метод запроса
     *
     * @see RFC 2616 http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
     * @return string
     */
    public function getMethod()
    {
        return isset($_SERVER['REQUEST_METHOD'])
            ? $_SERVER['REQUEST_METHOD']
            : '';
    }

    /** @return string */
    public function getRequestUri()
    {
        return (string)$this->getHttpParam('SERVER', 'REQUEST_URI');
    }

    /**
     * Проверка на пост запрос
     *
     * @return bool
     */
    public function isPost()
    {
        return $this->getMethod() === 'POST';
    }

    /**
     * Проверка на гет запрос
     *
     * @return bool
     */
    public function isMethodGet()
    {
        return $this->getMethod() === 'GET';
    }

    public function getReferer($default = '/')
    {
        return !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $default;
    }

    /**
     * Получить произвольные параметры из HTTP запроса (GET, POST, COOKIE, FILES, SERVER, ENV, REQUEST).
     *
     * Если $name === null,
     * то возвращается массив целиком.
     *
     * @param string $method        - Название типа переменных (GET, POST, COOKIE, FILES, SERVER, ENV, REQUEST).
     * @param string|null $name        - Имя параметра или null.
     * @return mixed
     */
    public function getHttpParam($method, $name = null)
    {
        $methodName = '_' . $method;

        switch ($method) {
            case 'GET':
            case 'POST':
            case 'COOKIE':
            case 'FILES':
            case 'SERVER':
                $variables = $GLOBALS[$methodName];
                break;

            case 'ENV':
                $variables = $_ENV;
                break;

            case 'REQUEST':
                $variables = $_REQUEST;
                break;

            default:
                $variables = null;
        }

        if (is_null($name)) {
            $return = $variables;
        } elseif (!isset($variables[$name])) {
            $return = null;
        } else {
            $return = $variables[$name];
        }

        return $return;
    }

    /**
     * Получить данные из php массива $_REQUEST.
     *
     * Если $name === null,
     * то возвращается массив целиком.
     *
     * @param string|null $name        - Имя параметра или null.
     * @return mixed
     */
    public function getHttpRequest($name = null)
    {
        return $this->getHttpParam("REQUEST", $name);
    }

    /**
     * Получить данные из php массива $_COOKIE.
     *
     *
     * @param  string $name        Имя параметра.
     * @return mixed
     */
    public function getCookie($name)
    {
        return $this->getHttpParam("COOKIE", $name);
    }

    /**
     * Проверяет есть ли в реквесте параметр с заданным кулючём
     *
     * @param  string $name Имя параметра
     * @return bool
     */
    public function hasHttpRequest($name)
    {
        return !is_null($this->getHttpRequest($name));
    }

    /**
     * Защищено или нет соединение
     * @return boolean
     */
    public function isSecure()
    {
        return false;
    }

    /**
     * Возвращает значения из $_POST массива
     * @param string $name
     * @return mixed
     */
    public function getPost($name)
    {
        return $this->getHttpParam("POST", $name);
    }

    /**
     * getPostData
     *
     * @return array
     */
    public function getPostData()
    {
        return $this->getHttpParam('POST');
    }

    public function isAjax()
    {
        $requestedWith = empty($_SERVER['HTTP_X_REQUESTED_WITH']) ? '' : strtoupper($_SERVER['HTTP_X_REQUESTED_WITH']);
        return ($requestedWith == 'XMLHTTPREQUEST') || (!empty($_REQUEST['_json_']) && $_REQUEST['_json_'] = 'Y');
    }

    /**
     * Возвращает сырые данные из тела POST запроса.
     *
     * @return string
     */
    public function getRawPostData()
    {
        return file_get_contents("php://input");
    }

    public function exportPost()
    {
        return $_POST;
    }

    public function has($name){
        return array_key_exists($name,$_REQUEST);
    }
}
