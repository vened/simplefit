<?php
/**
 * @author: mix
 * @date: 12.01.13
 */
class Error
{
    const MYSQL_ERROR_LOG = "mysql.log";
    const FATAL_ERROR_LOG = "fatal.log";

    const LOG_NONE = 0;
    const LOG_NORMAL = 1;
    const LOG_DEBUG = 2;


    /**
     * Максимальная длина значения аргумента, которую нужно вывести в трейсе.
     */
    const MAX_ARG_SIZE = 32;

    /**
     * Функции которые будут вызыватся для логирования каждого сообщения
     *
     * @var callable[]
     */
    private static $errorMsgLoggers = array(
        'self::error_log',
        'self::logToFileTrigger',
    );

    /**
     * Дополнительные файлы с логами
     *
     * @var string[]
     */
    private static $additionLogFiles = array();

    private static function getPhpErrorDir(){
        $dir = ini_get("error_log");
        return $dir === '/dev/null'
            ? false
            : dirname($dir) . "/";
    }

    private static function btpCountAll($service, $op) {
        self::btpCountAny($service, false, $op);
    }

    public static function btpCountAny($service, $server, $op) {
        $hostname = gethostname();
        $grp = preg_replace('~\d+~', '', $hostname);

        //$btp = new Stat_Btp_Counter(Stat_Btp_Request::getInstance(),array('service'=>"{$service}_{$grp}", 'srv'=>$hostname,'op'=>$op));
        //unset($btp);

        $service    = "{$service}_{$grp}";
        $server     = $server?: $hostname;
        $operation  = $op;
        $time       = 1;

    }

    private static function btpCountExceptions($e) {
        self::btpCountAll('Php_Exceptions', get_class($e));
    }

    public static function btpCountErrors($str) {
        self::btpCountAll('Php_Errors', $str);
    }

    private static function btpCountLogs($logFileName) {
        self::btpCountAll('Php_Logs', $logFileName);
    }

    /**
     * Обработчик ошибок.
     *
     * @param   int         $errno      - тип ошибки
     * @param   string      $errmsg     - текст ошибки
     * @param   string      $filename   - имя файла
     * @param   int         $linenum    - номер строки
     * @param   array       $vars       - контекст ошибки
     * @return  boolean
     */
    public static function handler($errno, $errmsg, $filename, $linenum, array $vars) {
        // на девелоперских машинах используем xdebug

        //если генерируемая ошибка не соответвует текущему уровню error_reporting, то ее не надо обрабатывать
        if ((ini_get('error_reporting') & $errno) != $errno) {
            return false;
        }

        if($errno == E_STRICT) {
            return null;
        }

        // define an assoc array of error string
        // in reality the only entries we should
        // consider are E_WARNING, E_NOTICE, E_USER_ERROR,
        // E_USER_WARNING and E_USER_NOTICE
        $errortype = array (
            E_ERROR              => 'Error',
            E_WARNING            => 'Warning',
            E_PARSE              => 'Parsing Error',
            E_NOTICE             => 'Notice',
            E_CORE_ERROR         => 'Core Error',
            E_CORE_WARNING       => 'Core Warning',
            E_COMPILE_ERROR      => 'Compile Error',
            E_COMPILE_WARNING    => 'Compile Warning',
            E_USER_ERROR         => 'User Error',
            E_USER_WARNING       => 'User Warning',
            E_USER_NOTICE        => 'User Notice',
            E_STRICT             => 'Runtime Notice',
            E_RECOVERABLE_ERROR  => 'PHP Catchable fatal error',
            E_DEPRECATED         => 'Deprecated',
            E_USER_DEPRECATED    => 'User Deprecated',
        );

        if ($errno == E_WARNING) {
            if (substr($errmsg,0,7)=='blitz::') {
                self::btpCountErrors("Blitz warning");
            }
        }

        if (!isset($errortype[$errno])) {
            $errortype[$errno] = "Unknown error ($errno)";
        }

        self::btpCountErrors($errortype[$errno]);

        $message = "$errortype[$errno]: $errmsg in $filename on $linenum";
        $msg = $message . self::getDebugInfo();
        self::write($msg);

        /**
         * Если в этом месте вернуть false то сообщение продублируется стандартным хандлером
         * Ошибки, которые должны приводить к фаталам
         */
        if ( in_array($errno, array(E_USER_ERROR, E_RECOVERABLE_ERROR)) ) {
            exit;
        }

        return true;
    }

    /**
     * Сырая запись данных во все связанные файлы
     * @param string $msg
     */
    private static function write($msg) {
        self::logToFileTrigger($msg);
    }

    /**
     * @param string $file
     *
     * @return void
     */
    public static function registerAdditionLogFile($file) {
        self::$additionLogFiles[] = $file;
    }

    public static function error_internal($data = null){
        $msg = "at ". $_SERVER['PHP_SELF'];
        if($data) {
            $msg .= "\n".var_export($data,1);
        }
        self::localLog($msg, 'Debug/error_internal', true);
        return true;
    }

    public static function message($text, $data = null) {
        $msg = $text.self::getDebugInfo();
        if($data) {
            $msg .= "\n".var_export($data, 1);
        }
        self::write($msg);
    }

    public static function getDebugInfo($trace = null){
        static $called = false;
        if ($called) {
            return '';
        }
        $called = true;

        $logObject = new Error_Log('', $trace);
        $info = $logObject->setFullTrace(true)->useDebugInfo(true)->getText();

        $called = false;
        return $info . "\n";
    }

    /**
     * @deprecated Используйте Errors::localLog или Errors::stderrLog.
     *
     * Для вывода в лог своей информации нужно использовать этот метод.
     *
     * Параметры $logTo* могут принимать значения
     * 0   - не логировать
     * 1   - логировать без трейса
     * 2   - логировать с трейсом до места вызова логирования
     *
     *
     * @param string $text             - текст для вывода в лог без \n
     * @param string $filename         - имя файла без расширения
     * @param int $logToPhpLog      - логировать ли в стандартный лог пыха
     * @param int $logToLocalFile   - логировать в отдельный файл на каждом www сервере в папку /monamour/local/logs/
     *
     * @return void
     */
    public static function log($text, $filename, $logToPhpLog = self::LOG_NORMAL, $logToLocalFile = self::LOG_NONE) {

        $date = date("Y-m-d H:i:s");
        $server = php_uname('n');
        $remote = isset($_SERVER['REMOTE_ADDR'])? $_SERVER['REMOTE_ADDR']: 'no_remote_addr';
        $pid = getmypid();

        // Трейс для лога
        if ($logToPhpLog >= self::LOG_DEBUG || $logToLocalFile >= self::LOG_DEBUG ) {
            $trace = self::getDebugInfo();
        }

        // Индивидуальный лог для каждой машины
        if ( $logToLocalFile ) {
            $line = $text;
            if ( $logToLocalFile >= self::LOG_DEBUG ) {
                $line .= "\n$trace";
            }
            self::btpCountLogs("custom:$filename");
            error_log("[$date] [$pid] [$server $remote] $line\n", 3, "/monamour/local/logs/{$filename}.log");
        }

        // Стандартный лог пыха
        if ( $logToPhpLog ) {
            $line = $text;
            if ( $logToPhpLog >= self::LOG_DEBUG ) {
                $line .= "\n$trace";
            } else {
                $debugBackTrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
                $fileName   = $debugBackTrace[0]['file'];
                $lineNumber = $debugBackTrace[0]['line'];
                $line .= " ($fileName:$lineNumber)";
            }
            self::write("[$pid] [$server $remote] [$filename] $line");
        }
    }

    /**
     * сообщение в локальный лог ( в файлик на локальной ввшке )
     *
     * @param $message
     * @param $filename
     * @param bool $fullTrace
     *
     * @return void
     */
    public static function localLog($message, $filename, $fullTrace = false){
        self::btpCountLogs("custom:$filename");
        self::getErrorLog("/monamour/local/logs/{$filename}.log")->addCallPointer(1)->setFullTrace($fullTrace)->message($message);
    }

    /**
     * сообщение в обычный еррор лог.
     *
     * @param $message
     * @param bool $fullTrace
     *
     * @return void
     */
    public static function stderrLog($message, $fullTrace = true){
        self::btpCountLogs("stderr");
        self::getErrorLog("php://stderr")->addCallPointer(1)->setFullTrace($fullTrace)->message($message);
    }

    /**
     * Стандартная функция логирования пойманного исключения.
     * непойманные будут пойманы автоматически.
     * @see Errors::exceptionHandler()
     *
     * @param Exception $exception
     *
     * @return void
     */
    public static function exception(Exception $exception, $short = false)
    {
        self::btpCountExceptions($exception);

        self::exceptionLogger(
            "Caught exception '%s' with message: %s on %s at %s%s" ,
            $exception,
            $short
        );
    }

    /**
     * Обработкик Непойманных исключений
     * Для пойманных используйте Errors::exception()
     *
     * @param Exception $exception
     *
     * @return void
     */
    public static function exceptionHandler(Exception $exception)
    {
        self::btpCountExceptions($exception);

        self::exceptionLogger(
            "PHP Fatal error: Uncaught exception '%s' with message: %s on %s at %s%s",
            $exception
        );
    }

    /**
     * Абстракная функция логирования исключений
     *
     * @param  string       $template   Шаблон сообщения
     * @param  Exception    $exception  Исключение
     * @return void
     */
    private static function exceptionLogger($template, Exception $exception, $short = false)
    {
        $msg = sprintf(
            $template,
            get_class($exception),
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine(),
            (!$short)?"\n".self::getDebugInfo( $exception->getTrace() ):""
        );

        self::write($msg);
    }

    private static function logToFileTrigger($msg) {
        $msg = date("[Y-m-d H:i:s] ") . $msg . "\n";
        $files = array_merge( array(ini_get("error_log")), self::$additionLogFiles );
        foreach( $files as $file ) {
            if (SERVER_ROLE == 'dev') {
                echo nl2br($msg) . '<br />' . PHP_EOL;
            }
            file_put_contents($file, $msg, FILE_APPEND);
        }
    }

    public static function logMysqlError($host, $query, $errorMessage){
        self::logToFile("{$errorMessage}\nHost: $host\nQuery: $query", self::MYSQL_ERROR_LOG);
    }

    public static function logFatal($msg){
        self::logToFile($msg, self::FATAL_ERROR_LOG);
    }

    private static function logToFile($msg, $filename){
        $errorLogDir = self::getPhpErrorDir();
        if(!file_exists($errorLogDir)){
            self::message("'$errorLogDir' error log dir not found");
        }
        else {
            $logFileName = $errorLogDir.$filename;
            $debugInfo = self::getDebugInfo();
            $dateTime = date("[Y-m-d H:i:s]");

        }
    }

    /**
     * хелпер для чейнинга
     *
     * @param $path
     * @return Error_Log
     */
    private static function getErrorLog($path){
        return new Error_Log($path);
    }

    public static function logRegistration($type, array $dataArray) {
        $data = "";
        foreach($dataArray as $k=>$v) {
            $data .= " $k=$v";
        }
        $typeAllowed   = array('Registered', 'TryRegister', 'AnketaConfirmedAsSocial', 'ActivateRealByPhone');
        if (!in_array($type, $typeAllowed)) {
            self::log("NOTICE: Unknown reg log type = '$type'", 'REG_SERVICE');
            return;
        }
        self::logAuthToFile($type, trim($data));
    }

    private function logAuthToFile($type, $data) {
        $logParts = array();
        $logParts[] = date("Y-m-d H:i:s");
        $logParts[] = isset($_SERVER['REMOTE_ADDR'])? $_SERVER['REMOTE_ADDR']: 'NO_REMOTE_ADDR';
        $logParts[] = isset($_SERVER['X_FORWARDED_FOR'])? $_SERVER['X_FORWARDED_FOR']: 'NO_X_FORWARDED_FOR';
        $logParts[] = $type;
        $logParts[] = $data;
        $logParts[] = isset($_SERVER['HTTP_USER_AGENT'])? $_SERVER['HTTP_USER_AGENT']: 'NO_HTTP_USER_AGENT';
        $logParts[] = isset($_SERVER['HTTP_REFERER'])? $_SERVER['HTTP_REFERER']: 'NO_HTTP_REFERER';

        $cookie = array();
        foreach(array('mrcu','AUTH_LOGIN','mmbsid') as $name) {
            if (isset($_COOKIE[$name])) {
                $cookie[$name] = $_COOKIE[$name];
            }
        }
        $logParts[] = $cookie? json_encode($cookie): ($_COOKIE? 'NO_USEFULL_COOKIES': 'NO_COOKIES');

        self::log(" | ". implode(" | ", $logParts), 'reg_analytics', self::LOG_NONE, self::LOG_NORMAL);
    }
}
