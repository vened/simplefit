<?php
/**
 * @author: mix
 * @date: 26.05.13
 */

class Recommendation_Factory {

    /**
     * @var User
     */
    private $user;

    public function __construct(\User $user){
        $this->user = $user;

    }

    private $menus = array(
        "triple" => array(
            "breakfast" => 30,
            "lunch"     => 50,
            "dinner"    => 20
        )
    );


    /**
     * @param $type
     * @param $system
     * @return Recommendation_Object[]
     */
    public function getRecommendations($type, $system){

        $out = array();

        return $out;
    }

    /**
     * @param $type
     * @param $food_type
     * @param $system
     * @param $count
     * @return Recommendation_Object[]
     */
    private function getRecommendation($type, $food_type, $system, $count){

        $c = $this->user->getCalories();
        $percent = $this->menus[$type][$food_type];
        $calls = round($c*$percent/100,2);
        $m = new \Staff\UserDiaryMapper();

        $out = array();
        foreach($m->findBest($this->user,$food_type,$calls,$system,$count) as $res){
            $out[] = new Recommendation_Object($res,$calls);
        }
        return $out;
    }


    private function saveSysPack($plans){
        $key = "plans_" . $this->user->getId() . "_sys";
        MCache::getInstance()->set($key,$plans,MCache::EXPIRE_DAY);
    }

    private function getSysPack($type){
        $key = "plans_" . $this->user->getId() . "_sys";
        $plans = MCache::getInstance()->get($key);

        if(!$plans){
            $plans = array();

            foreach(array_keys($this->menus[$type]) as $food_type ){
                $plans["product"][$food_type] = $this->getRecommendation($type,$food_type,false,3);
                $plans["current"][$food_type] = 0;
            }
            $this->saveSysPack($plans);
        }
        return $plans;

    }

    private function getSysRecommendation($plans, $food_type){
        return $plans["product"][$food_type][ $plans["current"][$food_type] ];
    }

    /**
     * @param $type
     * @param $food_type
     * @return Recommendation_Object
     */
    public function changeSystem($type,$food_type){
        $plans = $this->getSysPack($type);

        $count = count($plans["product"][$food_type]);
        $current = $plans["current"][$food_type] ;

        $current = ($current + 1 ) % $count;

        $plans["current"][$food_type] = $current;
        $this->saveSysPack($plans);

        return $this->getSysRecommendation($plans,$food_type);


    }

    /**
     * @param $type
     * @return Recommendation_Object[]
     */
    public function getSystemRecommendation($type){

        $plans = $this->getSysPack($type);
        $out = array();

        foreach(array_keys($this->menus[$type]) as $food_type){
            $out[$food_type] = $this->getSysRecommendation($plans,$food_type);
        }

        return $out;

    }


    /**
     * @param $type
     * @return Recommendation_Object[]
     */
    public function getUserRecommendation($type){
        $key = "plans_" . $this->user->getId() . "_usr";

        $plan = MCache::getInstance()->get($key);

        if(!$plan){
            $plan = array();
            foreach(array_keys($this->menus[$type]) as $food_type ){
                $p = $this->getRecommendation($type,$food_type,false,1);
                $plan[$food_type] = $p[0];
            }

            MCache::getInstance()->set($key,$plan,MCache::EXPIRE_DAY);
        }
        return $plan;
    }




    public function saveSystem(){

    }

    public function saveUser(){

    }

}