<?php
/**
 * @author: mix
 * @date: 26.05.13
 */

class Recommendation_Object {


    const CALL_KEY = 3;
    const WEIGHT_KEY = 2;

    private $data;

    private $type;


    public function __construct(\Staff\UserDiary $data, $scaleTo){
        $this->data = json_decode($data->getProperty("products_data"));
        $this->type = $data->getProperty("food_type");

        $total = $this->getCalories();

        $ratio = $scaleTo / $total;
        foreach($this->data as &$product){
            if($product[self::WEIGHT_KEY] == 0){
                error_log(print_r($product,1));
                continue;
            }
            $calPerGram = $product[self::CALL_KEY] / $product[self::WEIGHT_KEY];


            $grams = round($product[self::WEIGHT_KEY] * $ratio / 10) * 10;
            $calls = round($calPerGram * $grams);
            $product[self::CALL_KEY] = $calls;
            $product[self::WEIGHT_KEY] = $grams;
        }
    }


    public function getProducts(){
        return $this->data;
    }

    public function getType(){
        return $this->type;
    }

    public function getCalories(){

        $out = 0;
        foreach($this->data as $product){
            $out += $product[self::CALL_KEY];
        }
        return $out;
    }

    public function toArray(){
        $i = array();
        $i["food_type"] = $this->getType();
        $i["calories"] = $this->getCalories();
        $i["products_data"] = $this->getProducts();
        return $i;
    }

}