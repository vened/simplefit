<?php
namespace Blogs;

class SessionMapper extends \Mapper
{
    protected $tableName = 'Blogs.session';
    protected $domainObject = 'Blogs\Session';
    protected $pk = 'session_key';

    public function newObject()
    {
        return new \Blogs\Session();
    }

    /**
     * @param int $userId
     * @return Session
     */
    public function findByUserId($userId)
    {
        return $this->findOneByField(array('user_id' => $userId));
    }
}