<?php
namespace Blogs;

class UserMapper extends \Mapper
{
    protected $tableName = 'Blogs.user';
    protected $domainObject = 'Blogs\User';
    protected $pk = 'user_id';

    public function newObject()
    {
        return new \Blogs\User();
    }

    public function find($id)
    {
        return $this->findOneByField(array('user_id' => $id));
    }

    /**
     * @param $email
     * @return User
     */
    public function findByEmail($email)
    {
        return $this->findOneByField(array('user_mail' => $email));
    }

//    protected function beforeSave(\Blogs\User $user)
//    {
//        if (!$user->getId()) {
//            $user->setDateRegister();
//        }
//
//        $user->setUserProfileDate();
//    }
}