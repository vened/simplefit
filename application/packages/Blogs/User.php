<?php
namespace Blogs;

class User extends \DomainObject
{
    protected $properties = array(
        'user_id' => null,
        'user_login' => '',
        'user_password' => '',
        'user_mail' => '',
        'user_skill' => '0.000',
        'user_date_register' => '',
        'user_date_activate' => '',
        'user_date_comment_last' => null,
        'user_ip_register' => '',
        'user_rating' => '0.000',
        'user_count_vote' => '0',
        'user_activate' => 0,
        'user_activate_key' => null,
        'user_profile_name' => null,
        'user_profile_sex' => 'other',
        'user_profile_country' => null,
        'user_profile_region' => null,
        'user_profile_city' => null,
        'user_profile_birthday' => null,
        'user_profile_about' => null,
        'user_profile_date' => null,
        'user_profile_avatar' => null,
        'user_profile_foto' => null,
        'user_settings_notice_new_topic' => 1,
        'user_settings_notice_new_comment' => 1,
        'user_settings_notice_new_talk' => 1,
        'user_settings_notice_reply_comment' => 1,
        'user_settings_notice_new_friend' => 1,
        'user_settings_timezone' => null,
    );

    public function getId()
    {
        return $this->getProperty('user_id');
    }

    public function setLogin($login)
    {
        $this->setProperty('user_login', $login);
    }

    public function setPassword($password)
    {
        $this->setProperty('user_password', $password);
    }

    public function setEmail($email)
    {
        $this->setProperty('user_mail', $email);
    }

    public function setDateRegister($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d H:i:s');
        }

        $this->setProperty('user_date_register', $date);
    }

    public function setUserIpRegister($ip)
    {
        $this->setProperty('user_ip_register', $ip);
    }

    public function setUserActivate()
    {
        $this->setProperty('user_activate', 1);
    }

    public function setUserName($userName)
    {
        $this->setProperty('user_profile_name', $userName);
    }

    public function setUserSex($sex)
    {
        switch ($sex) {
            case 'M':
            case 'm':
                $sex = 'man';
                break;

            case 'F':
            case 'f':
                $sex = 'woman';
                break;

            default:
                $sex = 'other';
        }
        $this->setProperty('user_profile_sex', $sex);
    }

    public function setUserProfileDate($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d H:i:s');
        }

        $this->setProperty('user_profile_date', $date);
    }

    public function getUserName()
    {
        return $this->getProperty('user_profile_name');
    }
}