<?php
namespace Blogs;

class Session extends \DomainObject
{
    protected $properties = array(
        'session_key' => null,
        'user_id' => 0,
        'session_ip_create' => '',
        'session_ip_last' => '',
        'session_date_create' => '0000-00-00',
        'session_date_last' => '0000-00-00',
    );

    public function getId()
    {
        return null;
        //return $this->getProperty('session_key');
    }

    public function setSessionKey($sessionKey = null)
    {
        if (!$sessionKey) {
            $sessionKey = md5(uniqid(mt_rand(), true) . microtime());
        }
        $this->setProperty('session_key', $sessionKey);
    }

    public function setUserId($userId)
    {
        $this->setProperty('user_id', $userId);
    }

    public function setIpCreate($ip)
    {
        $this->setProperty('session_ip_create', $ip);
    }

    public function setIpLast($ip)
    {
        $this->setProperty('session_ip_last', $ip);
    }

    public function setDateCreate($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d H:i:s');
        }

        $this->setProperty('session_date_create', $date);
    }

    public function setDateLast($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d H:i:s');
        }

        $this->setProperty('session_date_last', $date);
    }
}