<?php
namespace Blogs;

class Blog extends \DomainObject
{
    protected $properties = array(
        'blog_id' => null,
        'user_owner_id' => 0,
        'blog_title' => 'Блог им. %s',
        'blog_description' => '',
        'blog_type' => 'personal',
        'blog_date_add' => '0000-00-00',
        'blog_date_edit' => null,
        'blog_rating' => '0.000',
        'blog_count_vote' => 0,
        'blog_count_user' => 0,
        'blog_count_topic' => 0,
        'blog_limit_rating_topic' => 0,
        'blog_url' => null,
        'blog_avatar' => null,
    );

    public function __construct()
    {
        parent::__construct();

        $this->setDescription('Это ваш персональный блог.');
        $this->setDateAdd();
        $this->setLimitRatingTopic('-1000');
    }
    public function getId()
    {
        return $this->getProperty('blog_id');
    }

    public function setUserId($userId)
    {
        $this->setProperty('user_owner_id', $userId);
    }

    public function setUserName($userName)
    {
        $this->setProperty('blog_title', sprintf($this->getProperty('blog_title'), $userName));
    }

    public function setDescription($description)
    {
        $this->setProperty('blog_description', $description);
    }

    public function setDateAdd($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d H:i:s');
        }

        $this->setProperty('blog_date_add', $date);
    }

    public function setLimitRatingTopic($rating)
    {
        $this->setProperty('blog_limit_rating_topic', $rating);
    }
}