<?php
namespace Blogs;

class BlogMapper extends \Mapper
{
    protected $tableName = 'Blogs.blog';
    protected $domainObject = 'Blogs\Blog';
    protected $pk = 'blog_id';

    public function newObject()
    {
        return new \Blogs\Blog();
    }

    /**
     * @param int $userId
     * @return Blog
     */
    public function findByUserId($userId)
    {
        return $this->findOneByField(array('user_owner_id' => $userId));
    }
}