<?php

class PrefixMapper extends Mapper
{
    protected $tableName = 'Prefix.Prefix';
    protected $domainObject = '';
    protected $pk = 'prefix_id';

    public function __construct()
    {
        parent::__construct();
    }

    protected function buildSelectSQL($cond = array(), $select = array(), $options = '')
    {
        $sql = 'SELECT
                        p.prefix_id, p.server_id, p.db_prefix, p.table_prefix, p.records, p.status as prefix_status,
                        s.name, s.host, s.user, s.pass, s.port, s.registration, s.prefixes, s.prefixes_max,
                        s.prefix_records_max, s.status as server_status
                FROM
                        %s p, `Prefix`.`Server` s
                WHERE
                        s.server_id = p.server_id %s %s';

        $condFields = array();
        if (count($cond)) {
            foreach ($cond as $field) {
                $condFields[] = sprintf('`%s` = ?', $field, $field);
            }
        }

        return sprintf($sql,
            $this->quotedTableName(),
            count($condFields) ? 'AND ' . implode(' AND ', $condFields) : '',
            $options
        );
    }

    public function getActive()
    {
        $sql = "SELECT
                        p.prefix_id, p.server_id, p.db_prefix, p.table_prefix, p.records, p.status as prefix_status,
                        s.name, s.host, s.user, s.pass, s.port, s.registration, s.prefixes, s.prefixes_max,
                        s.prefix_records_max, s.status as server_status
                FROM
                        `Prefix`.`Prefix` p, `Prefix`.`Server` s
                WHERE
                        p.status = 'Active' AND s.status = 'Active' AND
                        s.server_id = p.server_id AND s.registration = 'Opened' AND
                        s.prefixes < s.prefixes_max AND p.records < s.prefix_records_max
                ORDER BY
                        p.records";

        return $this->query($sql);
    }

    public function getFirstInactive()
    {
        $sql = "SELECT
                        p.prefix_id, p.server_id, p.db_prefix, p.table_prefix, p.records, p.status as prefix_status,
                        s.name, s.host, s.user, s.pass, s.port, s.registration, s.prefixes, s.prefixes_max,
                        s.prefix_records_max, s.status as server_status
                FROM
                        `Prefix`.`Prefix` p, `Prefix`.`Server` s
                WHERE
                        p.status = 'Blocked' AND s.status = 'Active' AND
                        s.server_id = p.server_id AND s.registration = 'Opened' AND
                        s.prefixes < s.prefixes_max AND p.records < s.prefix_records_max
                ORDER BY
                        p.records, p.prefix_id
                LIMIT 1";

        $result = $this->query($sql);

        return isset($result[0]) ? $result[0] : false;
    }

    public function getFirstActive()
    {
        $result = $this->getActive();

        if (empty($result[0])) {
            $inactive = $this->getFirstInactive();
            $this->setActive($inactive['prefix_id']);

            return $inactive;
        }

        return $result[0];
    }

    public function incRecordsCount($prefixId)
    {
        $sql = sprintf('UPDATE %s SET records = records + 1 WHERE prefix_id = ?', $this->quotedTableName());
        $this->query($sql, array($prefixId));
    }

    public function setActive($prefixId) {
        $sql = sprintf("UPDATE %s SET status = 'Active' WHERE prefix_id = ?", $this->quotedTableName());
        $this->query($sql, array($prefixId));
    }
}