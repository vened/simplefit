<?php

class Controller_Base
{
    private $request;

    /**
     * @var User
     */
    protected $user;
    protected $needAuth = true;
    protected $needProfileData = true;

    protected $currentAction = "index";
    /**
     * @var Application
     */
    private $app;


    protected function getUser(){
        return Auth::getUser();
    }



    public function __construct()
    {
        if (Auth::loggedIn()) {
            $this->user = Auth::getUser();
        }
    }

    public function getTemplateVars()
    {
        $user = array();
        if($this->user){
            $user = $this->user->toArray();
            $user["recommendation_count"] = (int)$user["recommendation_count"];
        }

        $vars = array(
            'current_year' => date('Y'),
            'user' => $user,
        );

        return $vars;
    }

    public function indexAction()
    {
        return array();
    }

    public function request()
    {
        if (!$this->request) {
            $this->request = new HttpRequest();
        }
        return $this->request;
    }

    /**
     * рендер внутри экшена
     * @param $action
     * @return bool
     */
    public function selfRender($action){
        return false;
    }

    public function setApplication(Application $app){
        $this->app = $app;
    }

    /**
     * @return Application
     */
    protected function app(){
        return $this->app;
    }

    protected function needAuth(){
        return $this->needAuth;
    }


    public function preAction(){
        //Auth::init();
        if (isset($_GET["link_id"])) {
            $m = new Admin_LinkStatMapper();
            $m->setHit($_GET["link_id"],Session::id());
            setcookie("link_id", $_GET["link_id"]);
        }

        if ($this->needAuth()){

            if(!Auth::getUser()) {
                throw new RedirectException("/");
            }else{
                if($this->user->getStatus() == "Filling"){
                    if(
                        strpos($this->request()->getRequestUri(), Navigation::get('settings/profile')) !== 0
                        && strpos($this->request()->getRequestUri(),"/ws/settings/profile") !== 0
                    ){
                        Navigation::navigate("settings/profile");
                    }
                }
            }

        }

    }

    public function setAction($action){
        $this->currentAction = $action;
    }
}