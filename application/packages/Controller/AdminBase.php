<?php
/**
 * @author: mix
 * @date: 25.01.13
 */
class Controller_AdminBase extends Controller_Base
{
    /**
     * @var Admin_Admin|null
     */
    private $admin = null;

    private $rights = array(
        "users",
        "links",
        "products",
        "admins",
        "mail",
        "diaries",
    );

    public function getAdmin(){
        return $this->admin;
    }

    public function __construct(){



    }

    public function getTemplateVars(){

        $common = array("script" => $this->request()->getRequestUri());
        if($this->admin){
            $common["login"] = $this->admin->getProperty("login");

            foreach($this->rights as $right){
                if($this->admin->checkRight($right)){
                    $common["rights"][$right] = 1;
                }
            }
        }

        return array_merge(parent::getTemplateVars(), $common);
    }

    public function preAction(){
        $m = new Admin_AdminMapper();
        $admin = $m->find(Session::get("admin_id"));

        if(!$admin->getId() && strpos($this->request()->getRequestUri(), "/login") === false){
            throw new RedirectException("/login");
        }else{
            $this->admin = $admin;
        }
    }

}