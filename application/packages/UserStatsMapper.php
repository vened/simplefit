<?php

class UserStatsMapper extends Mapper{

    const LIMIT = 50;

    protected $tableName = 'Simplefit.UserStats';
    protected $domainObject = 'UserStats';
    protected $pk = 'date';
    protected $useCache = false;

    public function hitStat($type){
        $sql = "insert into $this->tableName set date = date(now()), `$type`=1 on duplicate key update `$type` = $type +1";
        $this->query($sql);

    }

    public function fetchStat($page){

        $total = $this->queryResult("select
            'Всего' as date,
            sum(weight) as weight,
            sum(calories) as calories,
            sum(photo) as photo,
            sum(plan) as plan,
            sum(product) as product
            from $this->tableName")->fetchAll();
        $r = $this->queryResult("select SQL_CALC_FOUND_ROWS * from $this->tableName order by date desc limit " . self::LIMIT . " offset " . (self::LIMIT * $page));

        return array("stats" => array_merge($total, $r->fetchAll()), "total" => $r->calcFoundRows());
    }

}