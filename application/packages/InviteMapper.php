<?php
/**
 * @author: mix
 * @date: 14.08.13
 */
class InviteMapper extends Mapper
{
    protected $tableName = 'Simplefit.invite';
    protected $domainObject = 'Invite';
    protected $pk = 'id';
    protected $useCache = false;


    public function findMAilruGirls(){
        $sql = "select i.*
            from invite as i
            left join User u on i.email=u.email
            where u.email is null and sent is null and i.gender='F' and i.email like '%@mail.ru'
            limit 1000";
        return $this->query($sql);
    }
}