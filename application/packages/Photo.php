<?php

class Photo extends DomainObject
{
    protected $properties = array(
        'photo_id' => null,
        'user_id' => 0,
        'description' => '',
        'created' => null,
        'status' => 'Active',
    );

    public function getId()
    {
        return $this->getProperty('photo_id');
    }

    public function getUserId()
    {
        return $this->getProperty('user_id');
    }

    public function getDescription()
    {
        return $this->getProperty('description');
    }

    public function getCreated()
    {
        return $this->getProperty('created');
    }

    public function getStatus()
    {
        return $this->getProperty('status');
    }

    public function isActive()
    {
        return $this->getStatus() == 'Active';
    }

    public function setUserId($userId)
    {
        $this->setProperty('user_id', $userId);
    }

    public function setDescription($description)
    {
        $this->setProperty('description', $description);
    }

    public function setCreated($created = null)
    {
        if (!$created) {
            $created = date('Y-m-d H:i:s');
        }
        $this->setProperty('created', $created);
    }

    public function setStatus($status)
    {
        if (in_array($status, $this->enumStatus())) {
            $this->setProperty('status', $status);
        }
    }

    public function enumGender()
    {
        return array('M', 'F');
    }
}