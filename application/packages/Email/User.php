<?php
/**
 * @author: mix
 * @date: 03.11.13
 */
abstract class Email_User extends Email_Abstract
{
    private $user;
    protected  $test = false;

    public function __construct(User $user, $test = false) {
        $this->test = $test;
        $this->user = $user;
        $this->createMailer($this->user->getEmail(), $this->user->getFirstName());
    }

    public function setTemplateParams($params)
    {
        $this->params = array_merge($this->params,$params);
    }

    protected $params = array();

    public function prepareRender(){
        $token = $this->getToken();
        $this->params["token"] = $token;
        $this->params["url"] = 'http://' . $_SERVER['HTTP_HOST'] . "/access?code=$token&uid=".$this->user->getId();
        $this->params['logo_img'] = $this->createImageAttachment('email/logomail.gif', "image/gif");
        $this->params["name"] = $this->user->getFirstName();
        $this->params["view_url"] =  'http://' . $_SERVER['HTTP_HOST'] . "/access/a.jpg?code=$token&uid=".$this->user->getId();

    }


    public function send(){
        $token = $this->getToken();
        $o = new Admin_MailStat();
        $o->setProperties(array(
                "user_id" => $this->user->getId(),
                "email" => $this->user->getEmail(),
                "token" => $token,
                "template" => str_replace("email/", "", str_replace(".tpl","",$this->getTemplate())),
                'created' => date("Y-m-d H:i:s")
            ));
        $m = new Admin_MailStatMapper();
        $m->save($o);
        parent::send($o->getId());
    }
}