<?php
/**
 * @author: mix
 * @date: 03.11.13
 */

class Email_Transport_Factory
{
    /**
     * @return Email_Transport_Factory
     */
    public static function getInstance() {
        static $i;
        if(!$i)$i=new self;
        return $i;
    }

    private $zendSmtpOptions = array(

        "info" => array(
            'name' => 'simplefit.info',
            'host' => 'simplefit.info',
            'port' => 25,
        ),
        "yandex" => array(
            'name' => 'simplefit.ru',
            'host' => 'smtp.yandex.ru',
            'port' => 465,
            'connection_class' => 'login',
            'connection_config' => array(
                'username' => 'noreply@simplefit.ru',
                'password' => 'f34ksd81',
                'ssl' => 'ssl',
            ),
        )
    );

    /**
     * @param $type
     * @return Email_Transport_Interface
     */
    public static function getMailer($type) {
        return self::getInstance()->_getMailer($type);
    }

    private function _getMailer($type) {

        if(SERVER_ROLE == "dev"){
            return new Email_Transport_Zend($this->zendSmtpOptions["yandex"]);
        }

        //return new Email_Transport_Zend($this->zendSmtpOptions["info"]);
        return new Email_Transport_Zend($this->zendSmtpOptions["yandex"]);
    }
}