<?php
/**
 * @author: mix
 * @date: 03.11.13
 */
interface  Email_Transport_Interface {

    public function __construct($params);



    public function setSubject($subj);
    public function setHtmlBody($body);
    public function send($id = null);
    public function addTo($email,$name);
    public function setHeader($header,$value);



}