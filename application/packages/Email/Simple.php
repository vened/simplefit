<?php

class Email_Simple
{
    protected $to = array();
    protected $subject;

    protected $attachments = array();
    protected $textBody;
    protected $htmlBody;

    private $message;
    private $mail;



    private $from = array(
        "yandex" => "simplefit.ru",
        "info" => "simplefit.info",
        'eu' =>"simplefit.eu",
    );

    public function __construct($email, $name){

        if(true){
            $type = "eu";
        }else{
            $type = "yandex";
        }

        echo $type . "\n";

        $smtpOptions = new \Zend\Mail\Transport\SmtpOptions($this->smtpOptions[$type]);

        $this->mail = new \Zend\Mail\Transport\Smtp($smtpOptions);
        $this->message = new \Zend\Mail\Message();
        $this->message->addTo($email, $name);

        $this->message->getHeaders()->addHeader(new \Zend\Mail\Header\GenericHeader("Precedence","bulk"));
        $this->message->setFrom('noreply@' . $this->from[$type], 'Simplefit');

    }

    public function setSubject($subj){
        $this->message->setSubject($subj);
    }

    public function setHtmlBody($html)
    {
        $html = new \Zend\Mime\Part($html);
        $html->charset = "utf-8";
        $html->type = "text/html";
        $this->htmlBody = $html;
    }

    public function send($id)
    {
        $body = new \Zend\Mime\Message();
        $body->setParts(array_merge(array($this->htmlBody), $this->attachments));
        $this->message->getHeaders()->addHeader(new \Zend\Mail\Header\GenericHeader("Message-Id","<$id@simplefit.info>"));
        $this->message->setBody($body);

        if ($this->attachments) {
            /**
             * Replace content type "multipart/mixed"
             * with "multipart/related"
             */
            $headers = $this->message->getHeaders();
            $contentType = $headers->get('content-type');
            $contentType->setType(\Zend\Mime\Mime::MULTIPART_RELATED);
        }
        try{
            $this->mail->send($this->message);
            return true;
        }catch(Exception $e){
            Error::exception($e);
            return false;
        }

    }


    public function createImageAttachment($subj,$type = null)
    {
        $baseName = basename($subj);
        $image = new \Zend\Mime\Part(fopen($subj, 'r'));

        if($type === null){
            $image->type = 'image/png';
        }else{
            $image->type = $type;
        }
        $image->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
        $image->disposition = \Zend\Mime\Mime::DISPOSITION_INLINE;
        $image->filename = $baseName;
        $image->id = md5(uniqid(rand(), true));
        $this->attachments[] = $image;
        return $image->id;
    }

    public function addTo($email, $name = null)
    {

    }

    public function setHeader($header,$value){
        $this->message->getHeaders()->addHeader(new \Zend\Mail\Header\GenericHeader($header,$value));
    }
}