<?php
/**
 * @author: mix
 * @date: 25.03.13
 */

abstract class Email_Abstract {
    /**
     * @var Email_Transport_Interface
     */
    protected $mail;
    private   $token;
    protected $params;


    /**
     * get template name
     *
     * @return mixed
     */
    protected function getTemplate(){
        return "email/" . $this->getType() . ".tpl";
    }

    /**
     * get mail subject
     *
     * @return mixed
     */
    abstract protected function getSubject();


    abstract function prepareRender();
    /**
     * creates attachments etc
     */
    protected function init(){}



    public function getType(){
        return lcfirst(str_replace("Email_","",get_class($this)));
    }

    public function getToken(){
        return $this->token;
    }

    protected function createMailer($email,$name){
        $chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0987654321";
        $token = "";
        for($i = 0;$i<36;$i++){
            $token .= $chars[mt_rand(0,strlen($chars)-1)];
        }

        $this->token = $token;
        $this->mail = Email_Transport_Factory::getMailer($this->getType());
        $this->mail->addTo($email,$name);

    }

    protected function render($params){
        $this->init();

        $loader = new Twig_Loader_Filesystem('templates');
        $templateCfg = Config::get('template');

        $template = new Twig_Environment($loader, array(
            'cache' => $templateCfg['cache_path'][SERVER_ROLE],
            'auto_reload' => true,
            'autoescape' => false,
        ));

        $template->addFunction('_', new Twig_Function_Function('I18n::_'));
        $template->addFunction('_n', new Twig_Function_Function('I18n::_n'));
        $template->addFunction('_n_rus', new Twig_Function_Function('I18n::_n_rus'));
        $template->addFunction('nav', new Twig_Function_Function('Navigation::get'));

        return $template->render($this->getTemplate(), $params);
    }


    protected function sendMail($id){

        $this->mail->setHeader("X-Mailru-Msgtype",$this->getType());
        $mailer = $this->mail;
        $body = $this->render($this->params);
        $mailer->setHtmlBody($body);
        $mailer->setSubject($this->getSubject());
        return $mailer->send($id);
    }

    public function send($id = null){
        $this->sendMail($id);
        return true;
    }

    protected function createImageAttachment($subj, $type=null){
        return Application::getImgHost() ."/".$subj;
    }
}