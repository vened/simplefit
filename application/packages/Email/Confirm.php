<?php
/**
 * @author: mix
 * @date: 25.03.13
 */

class Email_Confirm  extends Email_User{

    /**
     * get mail subject
     *
     * @return mixed
     */
    protected function getSubject() {
        return "Симплфит: Добро пожаловать!";
    }
}