<?php
/**
 * @author: mix
 * @date: 25.03.13
 */

class Email_WeightNotify  extends Email_User{

    protected $user;
    protected $test;

    protected $template;

    /**
     * get mail subject
     *
     * @return mixed
     */
    protected function getSubject() {
        return "Симплфит: Отслеживание веса";
    }
}