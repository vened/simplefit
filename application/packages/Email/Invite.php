<?php
/**
 * @author: mix
 * @date: 14.08.13
 */

class Email_Invite  extends Email_Abstract{

    protected $invite;

    private $names = array(
        "Анна",
        "Ольга",
        "Марина",
        "Маша",
        "Татьяна",
        "Наталья",
        "Юлия",
        "Екатерина",
    );


    public function __construct(Invite $invite){
        $this->invite = $invite;


        $this->mail = Email_Transport_Factory::getMailer("invite");
        $this->mail->addTo(
            $invite->getProperty("email"),
            $invite->getProperty("name")
        );
    }

    /**
     * get mail subject
     *
     * @return mixed
     */
    protected function getSubject() {
        return "Вас пригласили на Симплфит";
    }

    public final function prepareRender(){

        $params = array();
        $params['logo_img'] = $this->createImageAttachment('email/logomail.gif', "image/gif");

        $uri_params = "?id=".$this->invite->getId() . "&email=".$this->invite->getProperty("email");

        $params["invite"] = 'http://' . $_SERVER['HTTP_HOST'] . "/invite/hit". $uri_params;

        $params["name"] = $this->names[array_rand($this->names)];
        $params["view_url"] =  'http://' . $_SERVER['HTTP_HOST'] . "/invite/a.jpg".$uri_params;
        $params["unsubscribe"] =  'http://' . $_SERVER['HTTP_HOST'] . "/invite/unsubscribe".$uri_params;

    }
    public function send(){
        $i = new InviteMapper();
        $this->invite->setProperty("sent", date("Y-m-d H:i:s"));
        $i->save($this->invite);
        return $this->sendMail($this->invite->getId());
    }
}