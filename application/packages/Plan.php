<?php

class Plan extends DomainObject
{
    protected $properties = array(
        'id' => null,
        'user_id' => 0,
        'type' => '',
        'data' => '',
        'calories' => 0,
        'created' => null,
    );

    public function getId()
    {
        return $this->getProperty('id');
    }

    public function getUserId()
    {
        return $this->getProperty('user_id');
    }

    public function getType()
    {
        return $this->getProperty('type');
    }

    public function getData()
    {
        return $this->getProperty('data');
    }

    public function getCalories()
    {
        return $this->getProperty('calories');
    }

    public function getCreated()
    {
        return $this->getProperty('created');
    }

    public function getCreatedDate()
    {
        return substr($this->getProperty('created'), 0, 10);
    }

    public function setId($id)
    {
        $this->setProperty('id', $id);
    }

    public function setUserId($userId)
    {
        $this->setProperty('user_id', $userId);
    }

    public function setType($type)
    {
        $this->setProperty('type', $type);
    }

    public function setData($data)
    {
        $this->setProperty('data', $data);
    }

    public function setCalories($calories)
    {
        $this->setProperty('calories', $calories);
    }

    public function setCreated($created = null)
    {
        if (!$created) {
            $created = date('Y-m-d H:i:s');
        }
        $this->setProperty('created', $created);
    }
}