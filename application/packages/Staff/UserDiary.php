<?php
namespace Staff;

class UserDiary extends \DomainObject
{
    protected $properties = array(
        'id' => null,
        'user_id' => 0,
        'diary_id' => 0,
        'gender' => null,
        'age' => null,
        'height' => 0,
        'start_weight' => 0,
        'goal_weight' => 0,
        'current_weight' => 0,
        'activity' => 0,
        'vegetarianism' => null,
        'type' => 'diary',
        'food_type' => null,
        'calories' => 0,
        'products_data' => null,
        'diary_date' => null,
        'status' => 'New',
        'created' => null,
    );

    public function getId()
    {
        return $this->getProperty('id');
    }

    public function getUserId()
    {
        return $this->getProperty('user_id');
    }

    public function getFoodType()
    {
        return $this->getProperty('food_type');
    }

    public function getProductsData()
    {
        return $this->getProperty('products_data');
    }

    public function getCreated()
    {
        return $this->getProperty('created');
    }

    public function getCreatedDate()
    {
        return substr($this->getProperty('created'), 0, 10);
    }

    public function getStatus()
    {
        return $this->getProperty('status');
    }

    public function isApproved()
    {
        return $this->getStatus() == 'Approved';
    }

    public function isNew()
    {
        return $this->getStatus() == 'New';
    }

    public function setId($id) {
        $this->setProperty('id', $id);
    }

    public function setUserId($userId)
    {
        $this->setProperty('user_id', $userId);
    }

    public function setDiaryId($diaryId)
    {
        $this->setProperty('diary_id', $diaryId);
    }

    public function setGender($gender)
    {
        $this->setProperty('gender', $gender);
    }

    public function setAge($age)
    {
        $this->setProperty('age', $age);
    }

    public function setHeight($height)
    {
        $this->setProperty('height', $height);
    }

    public function setStartWeight($weight)
    {
        $this->setProperty('start_weight', $weight);
    }

    public function setGoalWeight($weight)
    {
        $this->setProperty('goal_weight', $weight);
    }

    public function setCurrentWeight($weight)
    {
        $this->setProperty('current_weight', $weight);
    }

    public function setActivity($activity)
    {
        $this->setProperty('activity', $activity);
    }

    public function setVegetarianism($vegetarianism)
    {
        $this->setProperty('vegetarianism', $vegetarianism);
    }

    public function setType($type)
    {
        $this->setProperty('type', $type);
    }

    public function setFoodType($type)
    {
        $this->setProperty('food_type', $type);
    }

    public function setCalories($calories)
    {
        $this->setProperty('calories', $calories);
    }

    public function setProductsData($productsData)
    {
        $this->setProperty('products_data', $productsData);
    }

    public function setDiaryDate($date)
    {
        $this->setProperty('diary_date', $date);
    }

    public function setCreated($created = null)
    {
        if (!$created) {
            $created = date('Y-m-d H:i:s');
        }
        $this->setProperty('created', $created);
    }

    public function setStatus($status)
    {
        $this->setProperty('status', $status);
    }


    public function getProducts(){
        return json_decode($this->getProductsData(),true);
    }

    /**
     * @param \Product $product
     * @param int $weight
     */
    public function addProduct(\Product $product,$weight){
        $data = $this->getProducts();

        $data[] = array(
            $product->getId(),
            $product->getName(),
            $weight,
            round($product->getProperty("calories") * $weight /100 , 2)
        );

        $this->setProductsData(json_encode($data));
        $cc = 0;
        foreach($data as $item){
            $cc += $item[3];
        }
        $this->setCalories($cc);

    }


    public function delProduct($id){
        $data = $this->getProducts();
        foreach($data as $i => $d){
            if($d[0] == $id){
                unset($data[$i]);
                break;
            }
        }
        $this->setProductsData(json_encode($data));
    }

}