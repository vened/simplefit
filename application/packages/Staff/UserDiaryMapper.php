<?php
namespace Staff;

/**
 * @method \Staff\UserDiary newObject()
 * @method \Staff\UserDiary find()
 */
class UserDiaryMapper extends \Mapper
{

    private $triple = array(
        "breakfast" => 30,
        "lunch"     => 50,
        "dinner"    => 20
    );

    protected $tableName = 'Staff.UserDiary';
    protected $domainObject = 'Staff\UserDiary';
    protected $pk = 'id';
    protected $useCache = false;

    public function __construct() {
        parent::__construct(null);
    }

    public function findByUserId($userId, $status = 'New', $limit = null, $offset = 0) {
        $today = date('Y-m-d');
        $last = date('Y-m-d', time() - 86400*10);
        $options = "AND created < '$today' AND created >= '$last'";

        if (!in_array(strtolower($status), array('new', 'approved', 'rejected'))) {
            $status = 'New';
        }

        if ($limit) {
            $options .= ' LIMIT ' . $limit;
            if ($offset) {
                $options .= ' OFFSET ' . $offset;
            }
        }

        $select = array(new \SqlExpr('SQL_CALC_FOUND_ROWS *'));
        return $this->findByField(array('user_id' => $userId, 'status' => $status), $select, $options);
    }

    /**
     * @param $diaryId
     * @return \Staff\UserDiary
     */
    public function findByDiaryId($diaryId) {
        return $this->findOneByField(array('diary_id' => $diaryId));
    }

    public function getUserIds($status = 'New', $limit = null, $offset = 0)
    {
        if (!in_array(strtolower($status), array('new', 'approved', 'rejected'))) {
            $status = 'New';
        }

        $today = date('Y-m-d');
        $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT(user_id) FROM {$this->tableName}
                WHERE
                    status='$status' AND created < '$today'
                ORDER BY id DESC";

        if ($limit) {
            $sql .= ' LIMIT ' . $limit;
            if ($offset) {
                $sql .= ' OFFSET ' . $offset;
            }
        }

        return $this->queryResult($sql);
    }

    private function getWhereByParams($params){
        $prms = array();
        unset($params["page"]);

        if(isset($params["stype"])){
            switch($params["stype"]){
                case "system" : $prms[]  = "user_id=0";  break;
                case "users"   : $prms[]  = "user_id!=0"; break;
            }
            unset($params["stype"]);
        }

        foreach($params as $name => $value){
            if($value == "nope")continue;
            $prms[] = "$name = '$value'";
        }
        return implode(" AND ", $prms);
    }

    public function getTotalByParams($params){

        $where = $this->getWhereByParams($params);

        $sql = "select count(*) as cnt from $this->tableName" .($where? " where $where":"");
        $p = $this->queryResult($sql)->fetch();
        return $p["cnt"];

    }


    public function findByParams($params, $count){
        $where =  $this->getWhereByParams($params);

        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM $this->tableName " .($where?" where ".$where:""). " limit $count" . " offset " . ($params["page"] * $count) ;
        $out = array();
        foreach($this->query($sql) as $item){
            /** @var \Staff\UserDiary $item */
            $arr = $item->toArray();
            $arr["data"] = $item->getProducts();
            $arr["food"] = \Helper_User::getFood($item->getProperty("food_type"));
            $arr["act"] = \Helper_User::getActivity($item->getProperty("activity"));
            $arr["veg"] = \Helper_User::getVegetarianism($item->getProperty("vegetarianism"));

            $out[] = $arr;
        }
        $count = $this->queryResult("select FOUND_ROWS()")->fetchColumn();
        return array("list" => $out, "total" => $count[0]);


    }


    public function findBest(\User $user, $type,$calories, $system=false, $count=1){

        $sql = "select * from $this->tableName ";
        $sql .= "where food_type='$type' and type='plan'";
        if($system){
            $sql .= " and status!='Rejected' and user_id=0";
        }else{
            $sql .= " and status='Approved' and user_id > 0";
        }
        $sql .= " and vegetarianism='".$user->getVegetarianism()."'";
        $sql .= "group by products_data ";
        $sql .= " order by abs(calories - '$calories') limit 5";
        $res = $this->query($sql);


        if(!$res) return null;

        $out = array();

        for($i=0; $i < $count; $i++){
            $rand = mt_rand(0,count($res)-1);
            $out[] = $res[$rand];
            unset($res[$rand]);
            $res = array_values($res);
        }

        return $out;
    }





}