<?php

class WeightMapper extends Mapper
{
    protected $tableName = 'Simplefit.OptimalWeight';
    protected $domainObject = 'Weight';
    protected $pk = 'id';
    protected $useCache = false;

    /**
     * @param $height
     * @param $gender
     * @return Weight
     */
    public function findByHeight($height, $gender)
    {
        return $this->findOneByField(array('gender' => $gender, 'height' => $height));
    }
}