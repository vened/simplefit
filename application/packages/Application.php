<?php

class Application
{
    /**
     * @var Twig_Environment
     */
    private $template = null;
    private $templateData = array();
    private $geoLng = 'rus';
    private $modules = array();
    protected $module = '';
    protected $controllerName = 'index';
    protected $actionName = 'index';

    /**
     * @var Controller_Base
     */
    protected $controller = null;

    public function __construct($module = '')
    {
        $this->module = $module;
        $lang = Request::getCookie('lang');

        I18n::setLanguage($lang, $this->geoLng);

        $loader = new Twig_Loader_Filesystem('templates');
        $templateCfg = Config::get('template');

        $this->template = new Twig_Environment($loader, array(
            'cache' => $templateCfg['cache_path'][SERVER_ROLE],
            'auto_reload' => true,
            'autoescape' => false,
        ));

        $this->template->addFunction('_', new Twig_Function_Function('I18n::_'));
        $this->template->addFunction('_n', new Twig_Function_Function('I18n::_n'));
        $this->template->addFunction('_n_rus', new Twig_Function_Function('I18n::_n_rus'));
        $this->template->addFunction('nav', new Twig_Function_Function('Navigation::get'));

        $this->templateData = array(
            'http_host' => $_SERVER['HTTP_HOST'],
            'http_base_host' => BASE_HOST,
            'img_host' => self::getImgHost(),
            'img_version' => 2,
            'lang' => $lang,
        );
        if(in_array(trim(SERVER_ROLE),array("dev","test"))){
            $this->templateData["is_test"] = 1;
        }
    }

    public static function getImgHost(){
        return sprintf(Config::get('images_host', SERVER_ROLE), BASE_HOST);
    }

    public function run()
    {
        try {
            Auth::init();
            $this->findController();

            try{

                $controller = $this->createController();
                $controller->preAction();
                $result = $this->callAction($controller);
            }catch(RedirectException $e){
                $e->redirect();
                exit();
            }


            if ($this->module == 'ws') {
                header('Content-type: text/json');
                echo json_encode($result);
            } elseif($controller->selfRender($this->actionName)){
                echo $result;
            }else{

                $template = $this->template->loadTemplate($this->getTemplate());

                if (!$result) {
                    $result = array();
                }

                $templateVars = $controller->getTemplateVars();
                $template->display(array_merge($this->templateData, $templateVars, $result));
            }

        } catch (Exception $e) {
            header("HTTP/1.0 404 Not Found");
            Error::exception($e);
            $template = $this->template->loadTemplate($this->getNotFoundTemplate());
            if (Request::get('DEBUG')) {
                $this->templateData['DEBUG'] = 1;
                $this->templateData['error_message'] = $e->getMessage();
            }
            $template->display($this->templateData);
        }
    }

    public function addModule($module)
    {
        $this->modules[] = $module;
    }

    public function hasModule($module)
    {
        return in_array($module, $this->modules);
    }

    protected function findController()
    {
        $parts = explode('?', $_SERVER['REQUEST_URI']);
        $url = preg_replace('#(^/|/$)#', '', $parts[0]);

        if ($url) {
            $parts = explode('/', $url);
            $module = array_shift($parts);

            if ($this->hasModule($module)) {
                $this->module = $module;
                if ($parts) {
                    $this->controllerName = array_shift($parts);
                }
            } else {
                $this->controllerName = $module;
            }

            if ($parts) {
                $this->actionName = array_shift($parts);
            }

            if ($parts) {
                $this->actionName .= implode('', array_map('ucfirst', $parts));
            }
        }

        if (strpos($this->controllerName, '-')) {
            $parts = explode('-', $this->controllerName);
            $this->controllerName = implode('', array_map('ucfirst', $parts));
        }
    }

    public function getControllerName()
    {
        return ($this->module ? $this->module . '_' : '') . ucfirst($this->controllerName);
    }

    /**
     * @return Controller_Base
     * @throws Exception
     */
    protected function createController()
    {
        $ret = null;
        $controllerName = $this->getControllerName() . 'Controller';
        if (class_exists($controllerName)) {
            $ret = new $controllerName();
        } else {
            $controller = new IndexController();

            if($this->checkAction($controller,$this->getControllerName())){
                $this->actionName = strtolower($this->getControllerName());
                $this->controllerName = "index";
                $ret = new IndexController();
            }else{
                throw new Exception("Error: Controller '$controllerName' does not exist");
            }
        }
        $ret->setApplication($this);
        return $ret;
    }

    private function checkAction($controller, $action) {
        return method_exists($controller, $action);
    }

    protected function callAction(Controller_Base $controller)
    {
        $action = str_replace(".","_",$this->actionName) . 'Action';
        if (!$this->checkAction($controller,$action)) {
            throw new Exception(sprintf('Error: Action %s does not exist for the "%s" controller', $action, $this->controllerName));
        }
        $controller->setAction($this->actionName);
        return $controller->$action();
    }

    public function getTemplate()
    {
        return
            ($this->module ? $this->module : 'default') . DIRECTORY_SEPARATOR
            . strtolower($this->controllerName)
            . (strtolower($this->actionName) == 'index' ? '' : '_' . strtolower($this->actionName)) . '.tpl';
    }

    public function getNotFoundTemplate() {
        return 'default/not_found.tpl';
    }

    /**
     * @return Twig_Environment
     */
    public function getTemplateEngine(){
        return $this->template;
    }

    /**
     * @return array
     */
    public function getTemplateGlobals(){
        return $this->templateData;
    }
}