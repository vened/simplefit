<?php

class Request
{
    public static function get($name)
    {
        return isset($_REQUEST[$name]) ? $_REQUEST[$name] : null;
    }

    public static function getPost($name)
    {
        return isset($_POST[$name]) ? $_POST[$name] : null;
    }

    public static function getCookie($name)
    {
        return isset($_COOKIE[$name]) ? $_COOKIE[$name] : null;
    }

    public static function setCookie($name, $value, $lifetime = 86400)
    {
        setcookie($name, $value, time() + $lifetime, '/', '.' . BASE_HOST);
    }

    public static function delCookie($name)
    {
        setcookie($name, "", mktime(0,0,0,1,1,1971), '/', '.' . BASE_HOST);
    }

    public static function getSession($name)
    {
        return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
    }

    public static function setSession($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    public static function set($name, $value)
    {
        $_REQUEST[$name] = $value;
    }
    public static function isPost(){
        return $_SERVER["REQUEST_METHOD"] == "POST";
    }
}