<?php
class Navigation
{
    private static $alias = array(
        'index' => '/',
        'profile' => '/profile/',
        'settings' => '/profile/settings/',
        'about' => '/about/',
        'settings/profile' => '/profile/settings/?sec=profile',
        'plan' => "/plan",
    );

    public static function get($pageName)
    {
        if (!isset(self::$alias[$pageName])) {
            return '/' . $pageName . '/';
        }

        $return = self::$alias[$pageName];
        if (func_num_args() > 1) {
            for ($i = 1; $i < func_num_args(); ++$i) {
                $p = func_get_arg($i);
                $return = str_replace('$' . $i, $p, $return);
            }
        }
        return $return;
    }

    public static function navigate($pageName){
        throw new RedirectException(self::get($pageName));
    }
}