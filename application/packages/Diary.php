<?php

class Diary extends DomainObject
{
    protected $properties = array(
        'id' => null,
        'user_id' => 0,
        'type' => null,
        'food_type' => null,
        'data' => '0',
        'products_data' => null,
        'status' => 'Active',
        'created' => null,
    );

    public function getId()
    {
        return $this->getProperty('id');
    }

    public function getUserId()
    {
        return $this->getProperty('user_id');
    }

    public function getType()
    {
        return $this->getProperty('type');
    }

    public function getFoodType()
    {
        return $this->getProperty('food_type');
    }

    public function getData()
    {
        return $this->getProperty('data');
    }

    public function getProductsData()
    {
        return $this->getProperty('products_data');
    }

    public function getCreated()
    {
        return $this->getProperty('created');
    }

    public function getCreatedDate()
    {
        return substr($this->getProperty('created'), 0, 10);
    }

    public function getStatus()
    {
        return $this->getProperty('status');
    }

    public function isActive()
    {
        return $this->getStatus() == 'Active';
    }

    public function setId($id) {
        $this->setProperty('id', $id);
    }

    public function setUserId($userId)
    {
        $this->setProperty('user_id', $userId);
    }

    public function setType($type)
    {
        if (in_array($type, $this->enumType())) {
            $this->setProperty('type', $type);
        }
    }

    public function setFoodType($type)
    {
        if (in_array($type, $this->enumFoodType())) {
            $this->setProperty('food_type', $type);
        }
    }

    public function setData($data)
    {
        $this->setProperty('data', $data);
    }

    public function setProductsData($productsData)
    {
        $this->setProperty('products_data', $productsData);
    }

    public function setCreated($created = null)
    {
        if (!$created) {
            $created = date('Y-m-d H:i:s');
        }
        $this->setProperty('created', $created);
    }

    public function setStatus($status)
    {
        if (in_array($status, $this->enumStatus())) {
            $this->setProperty('status', $status);
        }
    }

    public function enumType()
    {
        return array('calories', 'weight', 'photo');
    }

    public function enumFoodType()
    {
        return array('breakfast', 'breakfast2', 'lunch', 'lunch2', 'dinner', 'dinner2');
    }

    public function enumStatus()
    {
        return array('Active', 'Blocked', 'Deleted');
    }
}