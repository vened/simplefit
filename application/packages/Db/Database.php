<?php

class Db_Database
{
    private static  $_instance;

    private $pdo;


    private function __construct(){
        $config = Config::get('database', SERVER_ROLE);

        // Using PDO
        try {
            $dsn = "mysql:host={$config['host']};dbname={$config['dbname']};charset=utf8";
            if (!empty($config['port'])) {
                $dsn .= ';port=' . $config['port'];
            }

            $this->pdo = new PDO($dsn, $config['user'], $config['password']);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->query("set names UTF8");
        }
        catch (PDOException $e) {
            Error::exception($e);
        }

    }

    /**
     * @return Db_Database
     */
    public static function getInstance()
    {
        if (!self::$_instance){
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    public function begin(){
        $this->query("SET autocommit = 0");
        return $this->query("BEGIN");
    }

    public function commit(){
        return $this->query("COMMIT");
    }

    public function rollback(){
        return $this->query("ROLLBACK");
    }

    public function quote($str){
        return $this->pdo->quote($str);
    }

    /**
     * @param $sql
     * @param array $matches
     * @throws Db_Exception
     * @return PDOStatement
     */
    public function query($sql,$matches = array()){
        //Error::stderrLog($sql. print_r($matches,1),false);
        try{
            $stmp = $this->pdo->prepare($sql);
            $stmp->execute($matches);
        }catch(PDOException $e){
            throw new Db_Exception($e->getMessage() . "\nat $sql \nwith args:".print_r($matches,1));
        }
        return $stmp;

    }

    public function lastInsertId(){
        return $this->pdo->lastInsertId();
    }
}