<?php
/**
 * @author: mix
 * @date: 24.01.13
 */
class Db_Result {

    /**
     * @var PDOStatement
     */
    private $stmt;

    /**
     * @var PDO
     */
    private $db;

    public function __construct($stmt,$db){
        $this->stmt = $stmt;
        $this->db = $db;
    }

    public function fetch(){
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function calcFoundRows(){
        $s = $this->db->query("SELECT FOUND_ROWS()");
        return (int) $s->fetchColumn();
    }

    public function fetchAll(){
        $out = array();
        while($row = $this->fetch()){
            $out[] = $row;
        }
        return $out;
    }

    public function fetchColumn()
    {
        $out = array();
        while ($row = $this->fetch()) {
            $out[] = array_shift($row);
        }
        return $out;
    }
}