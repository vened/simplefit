<?php
/**
 * @author: mix
 * @date: 21.06.13
 */
class RedirectException extends Exception{

    private $url;

    public function __construct($url){
        $this->url = $url;
        parent::__construct("redirect to $url");
    }

    public function redirect(){
        header("Location: $this->url");
    }

}