<?php

class Product extends DomainObject
{
    protected $properties = array(
        'id' => null,
        'name' => '',
        'calories' => 0,
        'proteins' => 0,
        'fats' => 0,
        'carbohydrates' => 0,
        'user_id'       => 0,
        'is_available'   => 0,
        "moderated"     => 0,
    );

    protected $virtualProperties = array("approved", "rejected","moderation");

    public function getId()
    {
        return $this->getProperty('id');
    }

    public function getName()
    {
        return $this->getProperty('name');
    }

    public function getProperty($name){
        switch($name){
            case "approved"   : return $this->getProperty("is_available") && $this->getProperty("moderated");
            case "rejected"   : return !$this->getProperty("is_available") && $this->getProperty("moderated");
            case "moderation" : return (
                !$this->getProperty("moderated")?"none":(
                    $this->getProperty("is_available")?"approved":"rejected"
                )
            );
            default : return parent::getProperty($name);
        }
    }
}