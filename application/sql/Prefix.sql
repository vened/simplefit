# --------------------------------------------------------
# Host:                         192.168.6.4
# Database:                     Prefix
# Server version:               5.0.51a-community
# Server OS:                    pc-linux-gnu
# HeidiSQL version:             5.0.0.3272
# Date/time:                    2012-12-29 16:18:15
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table Prefix.Prefix
CREATE TABLE IF NOT EXISTS `Prefix` (
  `prefix_id` int(10) unsigned NOT NULL auto_increment,
  `server_id` int(10) unsigned NOT NULL,
  `db_prefix` varchar(10) collate utf8_unicode_ci NOT NULL,
  `table_prefix` varchar(10) collate utf8_unicode_ci NOT NULL,
  `records` int(10) unsigned NOT NULL default '0',
  `updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL default '0000-00-00 00:00:00',
  `status` enum('Active','Blocked','Deleted') collate utf8_unicode_ci NOT NULL default 'Active',
  PRIMARY KEY  (`prefix_id`),
  KEY `records` (`records`),
  KEY `FK_Prefix` (`server_id`),
  CONSTRAINT `FK_Prefix` FOREIGN KEY (`server_id`) REFERENCES `Server` (`server_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Dumping data for table Prefix.Prefix: 1 rows
/*!40000 ALTER TABLE `Prefix` DISABLE KEYS */;
INSERT INTO `Prefix` (`prefix_id`, `server_id`, `db_prefix`, `table_prefix`, `records`, `updated`, `created`, `status`) VALUES (1, 1, '0', '0', 0, '2012-12-29 14:19:42', '2012-12-29 12:59:46', 'Active');
/*!40000 ALTER TABLE `Prefix` ENABLE KEYS */;


# Dumping structure for table Prefix.Server
CREATE TABLE IF NOT EXISTS `Server` (
  `server_id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(15) collate utf8_unicode_ci NOT NULL default '',
  `host` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  `user` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  `pass` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  `port` int(10) unsigned NOT NULL default '0',
  `registration` enum('Opened','Closed') character set utf8 NOT NULL default 'Closed',
  `prefixes` int(10) unsigned NOT NULL default '0',
  `prefixes_max` int(10) unsigned NOT NULL default '0',
  `prefix_records_max` int(10) unsigned NOT NULL default '0',
  `updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL default '0000-00-00 00:00:00',
  `status` enum('Active','Blocked','Deleted') character set utf8 NOT NULL default 'Active',
  PRIMARY KEY  (`server_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Dumping data for table Prefix.Server: 1 rows
/*!40000 ALTER TABLE `Server` DISABLE KEYS */;
INSERT INTO `Server` (`server_id`, `name`, `host`, `user`, `pass`, `port`, `registration`, `prefixes`, `prefixes_max`, `prefix_records_max`, `updated`, `created`, `status`) VALUES (1, 'db1', '192.168.6.4', 'monamour2', 'monamourchik', 3306, 'Opened', 1, 1000, 1000, '2012-12-29 14:19:00', '2012-12-29 12:56:56', 'Active');
/*!40000 ALTER TABLE `Server` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
