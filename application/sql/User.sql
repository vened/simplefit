# --------------------------------------------------------
# Host:                         192.168.6.4
# Database:                     Simplefit
# Server version:               5.0.51a-community
# Server OS:                    pc-linux-gnu
# HeidiSQL version:             5.0.0.3272
# Date/time:                    2012-12-29 16:19:12
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table Simplefit.User
CREATE TABLE IF NOT EXISTS `User` (
  `user_id` int(10) NOT NULL auto_increment,
  `prefix_id` int(10) NOT NULL default '0',
  `first_name` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  `last_name` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  `email` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  `password` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  `secret` varchar(32) collate utf8_unicode_ci NOT NULL default '',
  `gender` enum('M','F','') collate utf8_unicode_ci default '',
  `birthdate` date NOT NULL default '0000-00-00',
  `height` tinyint(3) unsigned NOT NULL default '0',
  `goal` enum('weight_loss','weight_gain','weight_maintenance') collate utf8_unicode_ci default NULL,
  `weight` float unsigned NOT NULL default '0',
  `weight_goal` float unsigned NOT NULL default '0',
  `vegetarianism` enum('no','vegetarianism','veganism','raw_food_diet','fruit_diet') collate utf8_unicode_ci NOT NULL default 'no',
  `main_photo_id` int(10) unsigned NOT NULL default '0',
  `email_confirmed` tinyint(1) unsigned NOT NULL default '0',
  `status` enum('Active','Blocked','Deleted') collate utf8_unicode_ci NOT NULL default 'Active',
  `country_id` int(10) unsigned NOT NULL default '0',
  `region_id` int(10) unsigned NOT NULL default '0',
  `city_id` int(10) unsigned NOT NULL default '0',
  `last_login` datetime default NULL,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `vk_id` int(10) unsigned NOT NULL default '0',
  `fb_id` varchar(64) collate utf8_unicode_ci NOT NULL default '0',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Dumping data for table Simplefit.User: 3 rows
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` (`user_id`, `prefix_id`, `first_name`, `last_name`, `email`, `password`, `secret`, `gender`, `birthdate`, `height`, `goal`, `weight`, `weight_goal`, `vegetarianism`, `main_photo_id`, `email_confirmed`, `status`, `country_id`, `region_id`, `city_id`, `last_login`, `created`, `vk_id`, `fb_id`) VALUES (3, 1, 'aaa', '', '', '', '', 'M', '0000-00-00', 0, NULL, 0, 0, 'no', 0, 0, 'Active', 0, 0, 0, NULL, '2012-12-13 16:12:02', 1, '0'), (11, 1, 'Vadim', 'Valitov', 'rockwell@list.ru', 'a148bdd7182d7ab522e089903c143ba0', 'fea2dcd94447ece29ff22af364df8ec8', 'M', '1986-03-05', 173, 'weight_maintenance', 65.5, 68, 'no', 0, 1, 'Active', 0, 0, 0, '2012-12-29 12:11:40', '2012-12-23 18:04:31', 4495758, '100001729905899'), (12, 1, 'Vitaliy', 'Borisow', 'vitaliy@borisow.info', '59f423dc1be53afa997537f3be2567db', '0164e15235a192337bbf31c2240acc75', 'M', '0000-00-00', 0, NULL, 0, 0, 'no', 0, 1, 'Active', 0, 0, 0, '2012-12-28 14:55:26', '2012-12-28 11:12:04', 0, '100000533681913');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
