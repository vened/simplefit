# --------------------------------------------------------
# Host:                         192.168.6.4
# Database:                     0_Cluster
# Server version:               5.0.51a-community
# Server OS:                    pc-linux-gnu
# HeidiSQL version:             5.0.0.3272
# Date/time:                    2012-12-29 16:14:43
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table 0_Cluster.0_Photos
CREATE TABLE IF NOT EXISTS `0_Photos` (
  `photo_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL,
  `description` varchar(255) collate utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `status` enum('Active','Blocked','Deleted') collate utf8_unicode_ci NOT NULL default 'Active',
  PRIMARY KEY  (`photo_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Dumping data for table 0_Cluster.0_Photos: 0 rows
/*!40000 ALTER TABLE `0_Photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_Photos` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
