<?php
date_default_timezone_set('Etc/GMT-4');


if(php_sapi_name() == "cli"){
    switch(gethostname()){
        case "indigo.flysoft-team.ru" :
            $path = explode("/",__DIR__);
            if($path[2] == "www.test"){
                $_SERVER['SERVER_ROLE'] = "test";
                $_SERVER['HTTP_HOST'] = 'testsimple.simplefit.ru';
            }else{
                $_SERVER['SERVER_ROLE'] = "production";
                $_SERVER['HTTP_HOST'] = 'simplefit.ru';
            }
            break;
        default :
            $_SERVER['SERVER_ROLE'] = "dev";
            $_SERVER['HTTP_HOST'] = 'simpletest.ru';
    }

}else{
    if (!isset($_SERVER['HTTP_HOST'])) {
        $_SERVER['HTTP_HOST'] = 'cela.ru';
    }

}

// comes from nginx
define('SERVER_ROLE', isset($_SERVER['SERVER_ROLE']) ? $_SERVER['SERVER_ROLE'] : 'dev');

$root = __DIR__;
chdir($root);

define("ROOT", $root . "/");

if (SERVER_ROLE == 'dev') {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}


$include_paths = array($root.'/packages', $root, $root.'/config', $root.'/controllers');
$paths = implode(PATH_SEPARATOR, $include_paths);
set_include_path($paths);

spl_autoload_register('autoload');

set_error_handler    ( array('Error', 'handler') );
set_exception_handler( array('Error', 'exceptionHandler') );

$img_conf = Config::get("images_root", SERVER_ROLE);

define("IMAGES_ROOT", realpath($img_conf[0]=="/"? $img_conf : ROOT .$img_conf) . "/");
define('BASE_HOST', preg_replace('/^[\d\w]+\.([\d\w]+\.[\d\w]+)$/', '$1', $_SERVER['HTTP_HOST']));



require_once $root . '/library/Twig/Autoloader.php';
Twig_Autoloader::register();

function autoload($class)
{
    global $root;
    if (0 === strpos($class, 'Twig')) {
        return;
    }
    elseif (0 === strpos($class, 'Zend')) {
        require $root . '/library/' . str_replace(array('_', '\\'), '/', $class) . '.php';
    }

    $file = str_replace(array('_', '\\'), '/', $class) . '.php';
    global $include_paths;
    foreach($include_paths as $path){
        if(file_exists("$path/$file")){
            require $file;
            break;
        }
    }
}