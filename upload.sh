git archive master | gzip > /tmp/simplefit.tgz
echo 'Uploading ' `git rev-parse HEAD` " ..."
scp /tmp/simplefit.tgz indigo@46.4.121.153:/tmp/
rm /tmp/simplefit.tgz
ssh indigo@46.4.121.153 'rm -rf /simplefit/www.test/*; mv /tmp/simplefit.tgz /simplefit/www.test; cd /simplefit/www.test; tar -xzf simplefit.tgz; rm simplefit.tgz; cd /simplefit/images.test; rm -rf *; mv /simplefit/www.test/application/images/* ./'
